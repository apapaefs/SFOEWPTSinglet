## PhaseTracer-1.0.0 [March 4, 2020]
 * Initial release
## PhaseTracer-1.0.1 [April 10, 2020]
 * Setup automatic build & tests etc
 * BugFix: calculate_sm_masses setting in FlexibleSUSY should be set to 1 or 0.
 * Fix cmake building of FS example to always use tagged version v2.4.1 of the code.
## PhaseTracer-1.0.2 [April 11, 2020]
 * Specify axis limits for phase_plotter
 * Update FS version used to 2.4.2, avoids compilation complaining about an unused Mathematica interafce problem on Mac OS with Mathamatica 12.
## PhaseTracer-1.0.3 [April ?, 2020]
 * Many thanks to Jingwei Lian for pointing out a bug in THDMIISNMSSMBCsimple.hpp. get_vector_debye_sq() returns two W boson masses, instead of one W boson mass and one Z boson mass.
