// ====================================================================
// This file is part of PhaseTracer

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// ====================================================================

#ifndef POTENTIAL_TEST_MODEL_HPP_INCLUDED
#define POTENTIAL_TEST_MODEL_HPP_INCLUDED

/**
   Make dummy model for testing.

  This is `model1` from the examples in `CosmoTransitions`.
*/

#include <vector>
#include <iostream>
#include <fstream>
#include "one_loop_potential.hpp"
#include "pow.hpp"
#include <sstream>
#include <string>
#include<iostream>
#include<string>
#include<vector>


std::vector<std::vector<double> > readfile(std::string filename) {
  //print 
  //std::cout << "Reading in " << filename.c_str() << std::endl;

  //declare the file stream for input
  std::ifstream fileinput;

  //open the filename
  fileinput.open(filename);

  //check if the file has been opned
  if(!fileinput.is_open()) { std::cout << "file " << filename.c_str() << " does not exist!" << std::endl; std::exit(0); }

  //the following vector will contain all the parameters in the form of a vectors of doubles:
  std::vector<std::vector<double> > allpoints;
  
  //the following will contain each individual parameter
  std::string inparam;
  std::string line;
  //the following separates the parameters:
  char delim = ',';
  //this is the new line character:
  char newline = '\n';
  
  std::vector<double> parameters; //a vector of doubles to push back to the big vector<vector<double>>
  
  //start loop through the lines
  while(std::getline(fileinput, line, newline)) {
    
    //define a string stream for the corresponding line:
    std::stringstream ss(line);
    
    //now split the line with the comma as delimitter 
    while(std::getline(ss, inparam, delim)) {
      parameters.push_back(std::stod(inparam));
    }
    
    //we have split the line and pushed back doubles into the vector of parameters, push back into the big vector:
    allpoints.push_back(parameters);
    //clear the parameters vector for the next line
    parameters.clear();
  }	  

  // return the allpoints vector<vector<double>>
  return allpoints;
}

namespace EffectivePotential {

class RSModel : public OneLoopPotential {
 public:
    const double sqrtr(const double D) const { if (D>0) return std::sqrt(D); else return 0; }

  double get_RGEscale() {
    return RGEscale;
  }
  
  //sets the RS parameters given the array params
  double set_RS_parameters(std::vector<double> params) {

    mu2 = params[0];
    MS = params[1];
    K1 = params[2];
    K2 = params[3];
    kap = params[4];
    Ls = params[5];
    lh = params[6];
    yt=params[7];
    g1=params[8];
    g2=params[9];
    xiW=params[10];
    xiZ=params[10];
    RGEscale=params[11];

  return 0.0;
};
  //prints the RS parameters:
  double print_RS_parameters() {
    std::cout << "Parameters:" << std::endl;
    std::cout << "mu2= " << mu2 << std::endl;
    std::cout << "MS= " << MS << std::endl;
    std::cout << "K1= " << K1 << std::endl;
    std::cout << "K2= " << K2 << std::endl;
    std::cout << "kap= " << kap << std::endl;
    std::cout << "Ls= " << Ls << std::endl;
    std::cout << "lh= " << lh << std::endl;
    std::cout << "xiW= " << xiW << std::endl;
    std::cout << "xiZ= " << xiZ << std::endl;
    std::cout << "g2= " << g2 << std::endl;
    std::cout << "g1= " << g1 << std::endl; 
    std::cout << "yt= " << yt << std::endl;
    return 0.0;
  }

  
  double V0(Eigen::VectorXd phi) const override {
    return 0.5 * mu2 * square(phi[0])+0.5*MS*square(phi[1])+0.5*K1*square(phi[0])*phi[1]+0.25*K2*square(phi[0])*square(phi[1])+0.3333*kap*phi[1]*phi[1]*phi[1]+0.5*Ls*square(square(phi[1]))+0.125*lh*square(square(phi[0]));
  }

  std::vector<double> get_scalar_masses_sq(Eigen::VectorXd phi, double xi) const override {
    const double mgm = mu2+0.5*square(phi[0])*lh+0.5*K2*square(phi[1])+K1*phi[1];
    const double mode1 = 0.5*(2*mu2+phi[1]*(2*K1+phi[1]*K2)+lh*square(phi[0]))*square(g2)*xiW*square(phi[0]);
    const double mode2 = 0.5*(2*mu2+phi[1]*(2*K1+phi[1]*K2)+lh*square(phi[0]))*(square(g2)*xiW+square(g1)*xiZ)*square(phi[0]);
    const double m1p = sqrtr(0.5*(mgm+sqrtr(square(mgm)-mode1)));
    const double m1m = sqrtr(0.5*(mgm-sqrtr(square(mgm)-mode1)));
    const double m2p = sqrtr(0.5*(mgm+sqrtr(square(mgm)-mode2)));
    const double m2m = sqrtr(0.5*(mgm-sqrtr(square(mgm)-mode2)));
    const double mhb = -MS-0.5*phi[1]*(2*K1+phi[1]*K2+12*Ls*phi[1]+4*kap)-mu2-0.5*square(phi[0])*(K2+3*lh);
    const double mhc = 0.25*(2*(MS+2*phi[1]*(3*Ls*phi[1]+kap))*(2*K1*phi[1]+K2*phi[1]*phi[1]+2*mu2)+(-4*K1*K1-6*K1*K2*phi[1]-3*K2*K2*phi[1]*phi[1]+6*(MS+2*phi[1]*(3*Ls*phi[1]+kap))*lh+2*K2*mu2)*phi[0]*phi[0]+3*K2*lh*phi[0]*phi[0]*phi[0]*phi[0]);
    const double mhp=sqrtr(0.5*(-mhb-sqrtr(mhb*mhb-4*mhc)));
    const double msp=sqrtr(0.5*(-mhb+sqrtr(mhb*mhb-4*mhc)));
   // std::cout << "get_scalar_masses_sq: mhb, mhc = " << mhb << " " << mhc << endl;
   // std::cout << "get_scalar_masses_sq: m1p, m1m, m2p, m2m, mhp, msp= " << m1p << " " << m1m << " " << m2p << " " << m2m << " " << mhp << " " << msp << std::endl;
    return {m1p, m1m,m2p,m2m,mhp,msp};
  }
    
std::vector<double> get_scalar_debye_sq(Eigen::VectorXd phi, double xi, double T) const override {
    const double mgm = mu2+0.5*square(phi[0])*lh+0.5*K2*square(phi[1])+K1*phi[1];
    const double mode1 = 0.5*(2*mu2+phi[1]*(2*K1+phi[1]*K2)+lh*square(phi[0]))*square(g2)*xiW*square(phi[0]);
    const double mode2 = 0.5*(2*mu2+phi[1]*(2*K1+phi[1]*K2)+lh*square(phi[0]))*(square(g2)*xiW+square(g1)*xiZ)*square(phi[0]);
    const double Pi1 = (0.25*lh+0.0625*g1*g1+3*0.0625*g2*g2+0.25*yt*yt)*T*T;
    const double m1p = sqrtr(0.5*(mgm+sqrtr(square(mgm)-mode1)))+Pi1;
    const double m1m = sqrt(0.5*(mgm-sqrtr(square(mgm)-mode1)))+Pi1;
    const double m2p = sqrt(0.5*(mgm+sqrtr(square(mgm)-mode2)))+Pi1;
    const double m2m = sqrt(0.5*(mgm-sqrtr(square(mgm)-mode2)))+Pi1;
    const double Pi2 = 0.5*Ls*T*T;
    const double m11 = mu2+K1*phi[1]+0.5*K2*phi[1]*phi[1]+1.5*lh*phi[0]*phi[0];
    const double m22 = MS+0.5*K2*phi[0]*phi[0]+2*kap*phi[1]+6*Ls*phi[1]*phi[1];
    // The next line was changed from K1*phi[1] to K1*phi[0]
    const double m12 = K1*phi[0]+K2*phi[1]*phi[0];
    const double mhp=sqrt(0.5*(m11+m22+Pi1+Pi2-sqrtr((m11+m22+Pi1+Pi2)*(m11+m22+Pi1+Pi2)-4*((m11+Pi1)*(m22+Pi2)-m12*m12))));
    const double msp=sqrt(0.5*(m11+m22+Pi1+Pi2+sqrtr((m11+m22+Pi1+Pi2)*(m11+m22+Pi1+Pi2)-4*((m11+Pi1)*(m22+Pi2)-m12*m12))));
 //   std::cout << "get_scalar_debye_sq: m1p, m1m, m2p, m2m, mhp, msp= " << m1p << " " << m1m << " " << m2p << " " << m2m << " " << mhp << " " << msp << std::endl;
                          return {m1p,m1m,m2p,m2m,mhp,msp};
}
    
  std::vector<double> get_scalar_dofs() const override { return {2., 2., 1., 1., 1., 1.}; }
  size_t get_n_scalars() const override { return 2; }

    // W, Z and photon
    std::vector<double> get_vector_dofs() const override {
        return {6., 3.};
    }
std::vector<double> get_fermion_dofs() const override {
        return {12.}; }
    std::vector<Eigen::VectorXd> apply_symmetry(Eigen::VectorXd phi) const override {
        phi[0] = - phi[0];
        return {phi};
    }

std::vector<double> get_vector_debye_sq(Eigen::VectorXd phi, double T) const override {

        const double W_debye = square(g2) * (0.25 * square(phi[0]) + 1.83333 * square(T));
        const double Z_debye = ((square(square(g2)) + square(square(g1)))/(square(g2) + square(g1))) * 1.83333*square(T) + 0.125 *square(g2)*square(phi[0]);
        return {W_debye, Z_debye};
}

std::vector<double> get_vector_masses_sq(Eigen::VectorXd phi) const override {
        return get_vector_debye_sq(phi, 0.);
    }

std::vector<double> get_fermion_masses_sq(Eigen::VectorXd phi) const override {
    const double top=yt*phi[0]*0.7071;
        return {square(top)};
};


  

 private:
  /*const double mu2=-10509.4739857;
    const double MS=-191547.95;
    const double K1=-82.935;
    const double K2=3.6407;
    const double kap=524.305;
    const double Ls = 0.486;
    const double lh =0.1690627;
    const double xiW=0.00001;
    const double xiZ=0.00001;
    const double g2=0.652954;
    const double g1=0.3497;
    const double yt=0.990445;*/

  double mu2=-10509.4739857;
  double MS=-191547.95;
  double K1=-82.935;
  double K2=3.6407;
  double kap=524.305;
  double Ls = 0.486;
  double lh =0.1690627;
  double xiW=0.00001;
  double xiZ=0.00001;
  double g2=0.652954;
  double g1=0.3497;
  double yt=0.990445;
  double RGEscale=91.;
  
  
};

}  // namespace EffectivePotential

#endif
