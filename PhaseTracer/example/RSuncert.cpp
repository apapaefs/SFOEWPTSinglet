/**
  1D example program for PhaseTracer.
*/

#include <iostream>

#include <boost/thread.hpp>

#include "models/RS.hpp"
#include "phase_finder.hpp"
#include "transition_finder.hpp"
#include "logger.hpp"
#include "phase_plotter.hpp"

int main(int argc, char* argv[]) {

  const bool debug_mode = argc > 4 and strcmp(argv[4],"-d")==0;


  std::string inputfilename = "thefile.txt";
  std::string outputfilename = "textoutput.txt";
  std::string pointoutputfilename = "pointoutput.txt";


  if(argc > 1) inputfilename = argv[1];
  if(argc > 2) outputfilename = argv[2];
  if(argc > 3) pointoutputfilename = argv[3];

  bool print_to_screen = false;
  
  // Set level of screen  output
  if (debug_mode) {
    LOGGER(debug);
  } else {
    LOGGER(fatal);
  }

  //print the input and output files:
  std::cout << "Reading in parameters from " << inputfilename << " and outputing results in " << outputfilename << std::endl;
														   
  std::vector<std::vector<double> > input_points;

  //read the input file into input_points:
  input_points = readfile(inputfilename);
  std::cout << "Finished reading in " << inputfilename << ", found " << input_points.size() << " parameter points " << std::endl;
  

  //open output stream to write results:
  std::ofstream outputstream;
  outputstream.open(outputfilename);
  
  //open output stream to write results:
  std::ofstream pointoutputstream;
  pointoutputstream.open(pointoutputfilename);

  //open output stream to write the "end" of run file
  std::ofstream endstream;
  std::string endfilename = "full" + pointoutputfilename;
  endstream.open(endfilename);

  //open output stream to write Higgs vevs (FOR TESTING)
  std::ofstream higgsvev;
  std::string higgsvevfilename = "higgsvev.txt";
  higgsvev.open(higgsvevfilename);
   

  // Construct model
  EffectivePotential::RSModel model;
  
  //loop through the input_points entries and calculate phases/transitions, output to file
  int max_points = input_points.size();

  //count the number of points that satisfy SFOEWPT:
  int number_SFO(0);

  //TESTING: reset the upper limit of points to consider 
  max_points = 100;

  //count number of points with SM-like vev:
  int passed_vev = 0;
  //count number of points with SM-like vev and SM-like field value deepest
  int passed_deepest = 0;
  
  for(size_t i = 0; i < max_points; i++) {

    //print progress:
    if(!print_to_screen) { if(i%1 == 0) { std::cout << "Point #/max points: " << i << "/" << max_points << ", SFO found/points scanned: " << number_SFO << "/" << i << "\r" << std::flush; } }

    // set the parameters
    model.set_RS_parameters(input_points[i]);
    
    //print the parameters
    if(print_to_screen) model.print_RS_parameters();

    //write the parameters to the outputfile
    outputstream << "Point parameters: " << std::endl; 
    for(size_t j = 0; j < input_points[i].size(); j++) outputstream << input_points[i][j] << "\t";
    outputstream << std::endl;
    
    
    //model.set_renormalization_scale(91.0);
    if(print_to_screen) std::cout << "RGE scale= " << model.get_RGEscale() << std::endl;
    model.set_renormalization_scale(model.get_RGEscale());

    // Make PhaseFinder object and find the phases
    PhaseTracer::PhaseFinder pf(model);
     
    pf.set_check_vacuum_at_high(false);

    try {
      pf.find_phases();
    }
    catch (const std::runtime_error& error) {
      std::cout << error.what() << std::endl;
      continue;
    }

    if(print_to_screen) std::cout << pf;

    /* auto phases = pf.phases;
    for (auto &p : phases) {
      std::cout << p << std::endl;
      }*/

    //get all the phases
    //The member function get_phases() has been added by AP (29/7/2020)
    auto all_phases = pf.get_phases();

    //the chosen minimum temperature (phase transitions to be considered have to satisfy this):
    double min_TC = 30.;

    //loop over the phases and make sure one of them contains a "Field at tmin" within field_tolerance_min, field_tolerance_max
    //define maximum/minimum tolerances for the Higgs vev:
    double field_tolerance_max = 246.+min_TC;
    double field_tolerance_min = 246.-min_TC;

    //the maximum acceptable vev for the singlet field:
    double singlet_max_vev = 1000.;

    //flag whether at least one SM-like phase exists:
    //with the correct Higgs vev value in [field_tolerance_min, field_tolerance_max], the correct min temperature
    bool found_SM_like = false;
    //if a SM-like vev exists, this is the value of the deepest minimum
    double minimum_SM_like_vev = 1E99;
    //the deepest NON-sm-like vev:
    double minimum_NONSM_vev = 1E99;

    //to write out in the output:
    int found_SM_like_bool(0);
    int SM_is_deepest_bool(0);

    
    double deepest_minimum_vev(0);
    double deepest_minimum_value(1E99);
    
    for (auto &p : all_phases) {
      bool SM_like = false;
      bool singlet_vev_ok = false;
      //write out the Higgs vev before testing:
      higgsvev << fabs(p.X.front()[0]) << std::endl;
      //if the Higgs vev is within [field_tolerance_min, field_tolerance_max] then it is SM-like
      if( fabs(p.X.front()[0]) > field_tolerance_min && fabs(p.X.front()[0]) < field_tolerance_max ) {
	SM_like = true; //this point is SM-like
	found_SM_like = true; //we have found an SM-like point
	//if this is deeper than the previous SM-like minimum, update the minimum_SM_like_vev
	if(p.V.front() < minimum_SM_like_vev) { minimum_SM_like_vev = p.V.front(); }
      } else { //if it not SM like, consider the phase only if the singlet vev is less than singlet_max_vev
	if( fabs(p.X.front()[1]) < singlet_max_vev ) {
	  singlet_vev_ok = true;
	  //if this is deeper than the previous NON-SM, update minimum_NONSM_vev:
	  if(p.V.front() < minimum_NONSM_vev) { minimum_NONSM_vev = p.V.front(); }
	}
      }
      if(p.V.front() < deepest_minimum_value) {
	deepest_minimum_value = p.V.front();
	deepest_minimum_vev = p.X.front()[0];
      }
    }
    
    //now check the phases:    
    if(found_SM_like) {
      found_SM_like_bool = 1;
      passed_vev++;
      //std::cout << "Point HAS an SM-like minimum with vev in " << field_tolerance_min << ", " << field_tolerance_max << std::endl;
    }

    //if SM-like vevs were found, check the deepest value of the minimum, against the deepest minimum of the non-SM that also pass the conditions:
    //if(minimum_NONSM_vev < minimum_SM_like_vev) std::cout << "\tNon-SM vev was found that is deeper than the deepest SM-like vev: " << minimum_SM_like_vev << "\t" << minimum_NONSM_vev << std::endl;
    if(minimum_NONSM_vev > minimum_SM_like_vev) {
      SM_is_deepest_bool = 1;
      passed_deepest++;
    }
    
    //write phases to file:
    outputstream << pf;

    //if the field does not have a phase transition with field at tmin within field_tolerance GeV then do not proceed
    //if(!passed_tolerance) continue;
    
    // Make TransitionFinder object and find the transitions
    PhaseTracer::TransitionFinder *tf = new PhaseTracer::TransitionFinder(pf);
    //PhaseTracer::TransitionFinder tf(pf);
    
    //use boost threads to check how long the find_transitions function is running:
    boost::thread t(boost::bind(&PhaseTracer::TransitionFinder::find_transitions, tf));
    if(!t.timed_join(boost::posix_time::minutes(5))){
      // thread still running, use interrupt or detach
      std::cout << "Point is taking too long! skipping" << std::endl;
      model.print_RS_parameters();
      continue;
    }
    
    if(print_to_screen) std::cout << tf;

    //check first order and print if found:
    auto transitions = tf->get_transitions();
    //write transitions to file:
    outputstream << tf;

    //if there are no transitions go to the next point
    if(transitions.size() == 0) continue;

    double TC(0.), false_vacuum_higgs(0.), true_vacuum_higgs(0.), rho(0.);
    if(print_to_screen) std::cout << "number of transitions (all rho values) = " << transitions.size() << std::endl;

    double rho_cut(0.1); //the minimum value of rho
    bool SFO(false); //boolean for whether we have strong first-order EWPT
    
    std::vector<std::pair<double, double>> TC_rho_vector; //contains all the TC and corresponding rho for points with rho > rho_cut
    std::vector<std::pair<double, double>> TC_rho_SFO_vector; //contains the TC and corresponding rho for points with rho > 1.0
    std::vector<std::pair<double, double>> TC_rho_noSFO_vector; //contains the TC and corresponding rho for points with rho_cut < rho < 1.0

    if(print_to_screen) std::cout << "printing only transitions with rho > " << rho_cut << std::endl;

    double rho_max(0.); //the maximum value of rho


    for(size_t t = 0; t < transitions.size(); t++) {
      //get the true and false vacua, TC and rho = | |true| - |false| | / TC:
      true_vacuum_higgs = transitions[t].true_vacuum[0];
      false_vacuum_higgs = transitions[t].false_vacuum[0];
      TC = transitions[t].TC;
      //only proceed with the phase transition if TC > min_TC
      if(TC < min_TC) std::cout << "TC = " << TC << " < min_TC" << std::endl;
      if(TC < min_TC) continue;
      //calculate phi_c/TC
      rho = fabs(fabs(true_vacuum_higgs) - fabs(false_vacuum_higgs))/TC;
      //only print and manipulate if rho > rho_cut:
      std::pair<double, double> TC_rho;
      if(rho > rho_cut) { 
	if(print_to_screen) std::cout << "true, false, TC, rho=\t\t\t\t" << true_vacuum_higgs << "\t\t\t\t" << false_vacuum_higgs <<  "\t\t\t\t" << TC << "\t\t\t\t" << rho << std::endl;
	//assign the TC and rho to the pair of doubles
	TC_rho.first = TC;
	TC_rho.second = rho;
	TC_rho_vector.push_back(TC_rho);
	//add to the appropriate vector:
	if(rho>1.0) {
	  TC_rho_SFO_vector.push_back(TC_rho);
	}
	else {
	  TC_rho_noSFO_vector.push_back(TC_rho);
	}
	if(rho>rho_max) rho_max = rho;
      }
    }

    //if there are no transition go to the next point:
    if(TC_rho_SFO_vector.size() == 0 && TC_rho_noSFO_vector.size()) continue;

    //if we have reached this point, there are phase transitions if TC_rho_SFO_vector.size() > 0 (i.e. there are transitions with rho > 1)
    //set SFO to true and check other conditions:
    if(TC_rho_SFO_vector.size() > 0) {
      //SFO found, set to true and check condition below
      SFO = true;
      
      //variable to contain the highest TC for the SFOEWPT
      double TC_SFO_highest = 0.;
      
      //sort in descending TC order:
      std::sort(TC_rho_SFO_vector.begin(), TC_rho_SFO_vector.end());
      
      //use this to get the highest-TC for the rho > 1 transitions: 
      if(print_to_screen) std::cout << "Highest-TC phase transition with rho > 1 at TC, rho = " << TC_rho_SFO_vector[TC_rho_SFO_vector.size()-1].first << ", " << TC_rho_SFO_vector[TC_rho_SFO_vector.size()-1].second << std::endl;
      TC_SFO_highest = TC_rho_SFO_vector[TC_rho_SFO_vector.size()-1].first;
      
      //check if the following conditions for SFOEWPT are satisfied:
      //if there exists any point with rho_cut < rho < 1.0 with TC greater than the TC of the highest TC point, then it's NOT an SFOEWPT
      for(size_t tt = 0; tt < TC_rho_SFO_vector.size(); tt++) {
	if(TC_rho_SFO_vector[tt].first > TC_SFO_highest) {
	  SFO = false;
	  if(print_to_screen) std::cout << "Point rejected because: " << TC_rho_SFO_vector[tt].first << "\t" << TC_rho_SFO_vector[tt].second << " vs. " <<  TC_rho_SFO_vector[TC_rho_SFO_vector.size()-1].first << "\t" << TC_rho_SFO_vector[TC_rho_SFO_vector.size()-1].second << std::endl; }
      }
    }

    //write out to the file of points:
    for(size_t j = 0; j < input_points[i].size()-1; j++) pointoutputstream << input_points[i][j] << ", ";
    //add maximum value of rho
    //std::cout << "rho_max= " << rho_max << std::endl;
    pointoutputstream << rho_max << ", ";
    pointoutputstream << int(SFO) << ", ";
    pointoutputstream << found_SM_like_bool << ", "; 
    pointoutputstream << SM_is_deepest_bool << ", ";
    pointoutputstream << deepest_minimum_vev << ", ";
    pointoutputstream << input_points[i][input_points[i].size()-1] << std::endl;
    if(SFO) number_SFO++;
    
 
    if (debug_mode) {
      PhaseTracer::phase_plotter(*tf, "RS");
    }
    delete tf;
		    
  }//end of loop over parameter space points
  std::cout << "# of points that have SM vev: " << passed_vev << " and # that also possess deepest minimimum at SM vev: " << passed_deepest << std::endl;
  std::cout << "Finished processing " << max_points << " points. Found " << number_SFO << " that satisfy SFOEWPT " << std::endl;

  //write an "end" filename to check whether the run has been completed:
  endstream << "DONE" << std::endl;
  
  outputstream.close();
  pointoutputstream.close();
  endstream.close();
    
  return 0;
}
