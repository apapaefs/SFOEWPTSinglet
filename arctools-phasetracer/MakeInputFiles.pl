#!/usr/bin/perl -aw

#Which input file are we using?
#$inputfile=$ARGV[0];
$xrslfile=$ARGV[0];
#How many runs to perform
$runsmin=$ARGV[1];
$runsmax=$ARGV[2];
$outputdir=$ARGV[3];
$jobname=$ARGV[4];

if (@ARGV!=5) {
    printf "########################################################################################\n";
    printf "# Welcome to the input and xrsl file generator                                         #\n";
    printf "# Usage:                                                                               #\n";
    printf "# MakeInputFiles.pl input_file xrsl_file sherpafile number_of_runs outputdir jobname   #\n";
    printf "########################################################################################\n";
    exit 0;
}
system("mkdir InputFiles");
for ($temp=$runsmin;$temp<=$runsmax;$temp++) {
  #system("cp $inputfile InputFiles/input$temp");
  #system("cp $sherpafile InputFiles/SherpaRun$temp.dat");

  #system("echo \"rootfile: output$temp.root\" >> InputFiles/input$temp");
  #system("echo \"raninitfile: /cvmfs/pheno.egi.eu/HEJ/HEJ/ranlux_files/ranlux64.$temp\" >> InputFiles/input$temp");

  # make the xrsl file
  system("cp $xrslfile InputFiles/run$temp.xrsl");
  system("echo \"(jobName = \\\"$jobname\\\")\" >> InputFiles/run$temp.xrsl");
  system("echo \"(inputFiles = (\\\"InputFiles/thefile$temp.txt\\\" \\\"\\\")  )\" >> InputFiles/run$temp.xrsl");
  system("echo \"(arguments = \\\"$temp\\\" \\\"$outputdir\\\")\" >> InputFiles/run$temp.xrsl");
}
