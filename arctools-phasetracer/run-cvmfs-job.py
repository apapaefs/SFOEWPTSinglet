#!/usr/bin/env python

__author__ = "Tuomas Hapola, Jeppe R. Andersen"
__date__ = "10.06.2016"

"""
Driver script to be executed at the worker node.
"""

import sys
import os
import time


class PTJob:
    

    def __init__(self,jobarg1,jobarg2):
        self.jobarg1 = str(jobarg1)
        self.jobarg2 = str(jobarg2)


    def SetEnv(self):
        os.environ["PATH"]=os.environ.get("PATH",'')+":"+'/usr/bin:/bin'+'/cvmfs/pheno.egi.eu/HEJ/gcc_9/bin'
        os.environ["LD_LIBRARY_PATH"]="/cvmfs/pheno.egi.eu/HEJ/gcc_9/lib64:/cvmfs/pheno.egi.eu/HEJ/gcc_9/lib:./lib/"

        os.environ["CC"]="/cvmfs/pheno.egi.eu/HEJ/gcc_9/bin/gcc"
        os.environ["PKG_CONFIG_PATH"]="/cvmfs/pheno.egi.eu/HEJ/QCDloop/lib/pkgconfig"

    def RunJob(self):
        cmd = 'uname -a'
        print cmd
        os.system(cmd)
        cmd = 'gfal-copy gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/%s/thefile%s.txt .' % (self.jobarg2, self.jobarg1)
        print cmd
        os.system(cmd)
        cmd = '/mt/home/apapaefstathiou/SFOEWPT_Singlet/PhaseTracer/bin/RS thefile%s.txt output%s.txt pointoutput%s.txt' % (self.jobarg1, self.jobarg1, self.jobarg1)
        print cmd
        os.system(cmd)
        pwd=os.getcwd();
        cmd = 'ls -ltr output*.txt | grep -o "[^ ]*$" | xargs -I{} gfal-copy "{}" gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/%s' % (self.jobarg2)
        print cmd
        os.system(cmd)
	cmd = 'ls -ltr *pointoutput*.txt | grep -o "[^ ]*$" | xargs -I{} gfal-copy "{}" gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/%s' % (self.jobarg2)
        print cmd
        os.system(cmd)



    def PrintInfo(self):
        print "Disk used"
        os.system("du -ksh")
        print "File list"
        os.system("ls -lR")
        print "cpuinfo"
        os.system("cat /proc/cpuinfo")

# Start...
t0 = time.time()

ptjob = PTJob(sys.argv[1],sys.argv[2])

ptjob.SetEnv()

# End of setup (and library download)
t1 = time.time()

ptjob.PrintInfo()
ptjob.RunJob()

# End of processing....
t2 = time.time()

print "Transfer time (s)",t1-t0
print "Execution time (s)",t2-t1
print "Total time (s)",t2-t0

