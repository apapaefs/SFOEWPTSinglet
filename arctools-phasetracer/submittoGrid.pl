#!/usr/bin/perl -w
use Cwd;
# This PERL program will run a program on several computers at once
# The path used for running
#$path=getcwd();
#How many runs to perform
$xrslinput=$ARGV[0];
$outputpath=$ARGV[1];
$nrunsmin=$ARGV[2];
$nrunsmax=$ARGV[3];
$jobname=$ARGV[4];
$ce=$ARGV[5];

if (@ARGV!=6) {
    printf "Usage: ./submittoGrid.pl xrslinputfile outputpath nrunsmin nrunsmax jobname ce\n";
    exit;
}

printf "$inputpath $outputpath $nruns $ce\n";

sub submit_batch_job()
{
    # Now, we submit the batch script to the queue
    system("arcsub -c $ce $inputpath/run$runnumber".".xrsl");
}


# Print out a welcome greeting
printf "--------------------------------------------------------------------------------\n";
printf "Welcome to the batch submitting perl script\n";
printf "--------------------------------------------------------------------------------\n";
printf "Creating grid disk directory for output \n";
$cmd="gfal-mkdir gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/$outputpath";
printf "$cmd\n";
system("$cmd");
printf "Making input files\n";
$cmd="./MakeInputFiles.pl $xrslinput $sherpainput $nrunsmin $nrunsmax $outputpath $jobname";
printf "$cmd\n";
system("$cmd");
$runnumberr=$nrunsmin;
printf "running $nrunsmin -- $nrunsmax\n";
while ($runnumberr<=$nrunsmax) {
  $cmd="gfal-copy -f InputFiles/thefile$runnumberr.txt gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/$outputpath";
  printf "$cmd\n";
  system("$cmd");
  $runnumberr++;
}

$runnumber=$nrunsmin;

$numbers=$nruns;
printf "Starting submitting $numbers jobs to $ce. Please be patient\n";

$retry=0;
while (($runnumber<=$nrunsmax)&&($retry<10)) {
#    if ($ce =~ /dur/) {
    $thisrun=0;
    while (($runnumber<=$nrunsmax-1)&&($thisrun<5)) {
	system("arcsub -d 1 -j jobs10.xml -S org.nordugrid.gridftpjob --direct -c $ce InputFiles/run$runnumber".".xrsl &");
	$runnumber++;
	$thisrun++; 
    }
    system("arcsub -d 1 -j jobs10.xml -S org.nordugrid.gridftpjob --direct -c $ce InputFiles/run$runnumber".".xrsl");
    $runnumber++;
#    } else {
#	$val=system("arcsub -c $ce $inputpath/run$runnumber".".xrsl");
#    }
    # if ($val1!=0) {
    # 	$retry++;
    # 	printf "Submission failed. Resubmitting job $runnumber\n";
    # } else {
    # 	$runnumber+=5;
    # }
}

printf "\nLast program submitted\n";

printf "This perl program has finished!\n";
exit;

