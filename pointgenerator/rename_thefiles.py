#! /usr/bin/env python
import sys
import time
import datetime
import os.path
from optparse import OptionParser
import subprocess

# rename thefile*.txt to correctly increment the numbers:
# e.g. rename_thefiles.py points_for_cluster_andreas 1 60 200
# will rename thefile1.txt to thefile201.txt, thefile2.txt to thefile202.txt etc.

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 4:
    print 'usage: rename_thefiles.py [subfolder] [min #] [max #] [new min #]'
    exit()

print 'renaming thefileX.txt in', args[0], 'starting from', args[1], 'to', args[2],'and adding the number', args[3]

filetag = 'thefile'
fileext = '.txt'

path = args[0]
minNum = args[1]
maxNum = args[2]
newminNum = args[3]

for r in range(int(minNum), int(maxNum)+1):
    # check if file exists:
    thefile = path + '/' + filetag + str(r) + fileext 
    if os.path.exists(thefile) is True:
        newthefile = path + '/' + filetag + str(int(newminNum)+r) + fileext
        print 'renaming', thefile, 'to', newthefile
        mvcommand = 'mv ' + thefile + ' ' + newthefile
        print mvcommand
        p = subprocess.Popen(mvcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd= '.')
    else:
        print 'Warning, file', thefile, 'does not exist'
        continue
    
