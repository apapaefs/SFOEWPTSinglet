import sys
import time
import datetime
import os.path
import math
from optparse import OptionParser
import subprocess
import numpy as np

# splits the output files into conservative (all 8 variations pass) or centrist (at least 1 variation passes)
# writes out pointoutput_conservative.txt and pointoutput_centrist.txt that contain the "central" parameters for each:
# i.e. the one corresponding to RGE scale = 91 GeV and gauge param xi = 0.0001

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 3:
    print 'usage: postprocess_thefiles.py [input subfolder] [min #] [max #]'
    exit()

print 'processing pointoutputX.txt in', args[0], 'starting from', args[1]
print 'writing out', args[0] + '/pointoutput_conservative.txt, and ' + args[0] + '/pointoutput_liberal.txt'
thefiletag = 'thefile'
filetag = 'pointoutput'
fileext = '.txt'

path = args[0]
minNum = args[1]
maxNum = args[2]

fascist_file = path + '/pointoutput_fascist.txt'
conservative_file = path + '/pointoutput_conservative.txt'
centrist_file = path + '/pointoutput_centrist.txt'
liberal_file = path + '/pointoutput_liberal.txt'

fascist_stream = open(fascist_file,'w')
conservative_stream = open(conservative_file,'w')
centrist_stream = open(centrist_file, 'w')
liberal_stream = open(liberal_file, 'w')


point_multiplicity = {} # this dictionary will contain the point multiplicity, i.e. how many times a tag appears in a file.

thefile_central_point = {} # this dictionary contains the "central" point, i.e. RG = 91, xi = 0.0001
RG_element = 11
xi_element = 10
RG_central = 91
xi_central = 0.0001

rho_max = {} # contains the rho_max for each point


rho_max_sum = {} # contains the sum of rho_max over the group of points
rho_max_sumsq = {} # contains the sum SQUARED of rho_max over the group of points

rho_max_average = {} # contains the average of rho_max over the group of points
rho_max_std = {} # contains the standard deviation of rho_max over the group of points

rho_max_isnonzero = {} # counts the number of points with rho_max > 0.0

vev_conditions = {} # 1 if there exists a deepest minimum with vev < 246. and another with vev > 246 then this is true, false otherwise
vev_values = {}
SFO_condition = {} # 1 if the SFO condition is satisfied by at least one point (rho_max > 1.0 and largest TC) 

vev_conditions_ALL = {} # 1 if the vev conditions are satisfied by ALL poijnts in group
SFO_condition_ALL = {} # 1 if the SFO condition is satisfied by ALL points in group (rho_max > 1.0 and largest TC) 


# change this according to the length of the lines:
pointfile_line_length = 21
thefile_line_length = 16

delim = ','
delimoutput = ' '

# position of elements in a line
rho_max_loc = -6
SFO_condition_loc = -5
found_SM_like_bool_loc = -4
SM_is_deepest_bool_loc = -3
deepest_minimum_vev_loc = -2

# SM vev
SM_vev = 246.
    
for r in range(int(minNum), int(maxNum)+1):
    # check if file exists:
    point_file = path + '/' + filetag + str(r) + fileext
    thefile = path + '/' + thefiletag + str(r) + fileext
    if os.path.exists(point_file) is True and os.path.exists(thefile) is True:
        # read in thefile with the original points, grab the RG = 91, xi = 0.0001
        thefile_stream = open(thefile, 'r')
        for line in thefile_stream:
            if len(line.split(delim)) == thefile_line_length:
                if float(line.split(delim)[RG_element]) == RG_central and float(line.split(delim)[xi_element]) == xi_central:
                    thefile_central_point[int(line.split(delim)[-1])] = line
        # read in the point results (these satisfy SFOEWPT)
        point_stream = open(point_file,'r')
        count = 1
        for line in point_stream:
            if len(line.rstrip().split()) == pointfile_line_length: # if the line has the correct length
                # first get the rho_max into the dictionary
                if line.rstrip().split()[-1] not in rho_max.keys():
                    rho_max[line.rstrip().split()[-1]] = line.rstrip().split(delim)[rho_max_loc]
                else:
                    if float(line.rstrip().split(delim)[rho_max_loc]) > float(rho_max[line.rstrip().split()[-1]]):
                        rho_max[line.rstrip().split()[-1]] = line.rstrip().split(delim)[rho_max_loc]
                # sum up the rho_max values:
                if line.rstrip().split()[-1] not in rho_max_sum.keys():
                    if float(line.rstrip().split(delim)[rho_max_loc]) != 0.0:
                        rho_max_sum[line.rstrip().split()[-1]] = float(line.rstrip().split(delim)[rho_max_loc])
                        rho_max_isnonzero[line.rstrip().split()[-1]] = 1
                else:
                    if float(line.rstrip().split(delim)[rho_max_loc]) != 0.0:
                        rho_max_sum[line.rstrip().split()[-1]] = float(rho_max_sum[line.rstrip().split()[-1]]) + float(line.rstrip().split(delim)[rho_max_loc])
                        rho_max_isnonzero[line.rstrip().split()[-1]] = rho_max_isnonzero[line.rstrip().split()[-1]] + 1
                # sum up the rho_max values squared:
                if line.rstrip().split()[-1] not in rho_max_sumsq.keys():
                    if float(line.rstrip().split(delim)[rho_max_loc]) != 0.0:
                        rho_max_sumsq[line.rstrip().split()[-1]] = float(line.rstrip().split(delim)[rho_max_loc])**2
                else:
                    if float(line.rstrip().split(delim)[rho_max_loc]) != 0.0:
                        rho_max_sumsq[line.rstrip().split()[-1]] = float(rho_max_sumsq[line.rstrip().split()[-1]]) + float(line.rstrip().split(delim)[rho_max_loc])**2
                # count the number of times the point appears in the point output file:
                if line.rstrip().split()[-1] not in point_multiplicity.keys():
                    point_multiplicity[line.rstrip().split()[-1]] = 1
                else:
                    point_multiplicity[line.rstrip().split()[-1]] = point_multiplicity[line.rstrip().split()[-1]] + 1
                # determine whether ANY of the group of points satisfies the SFO condition:
                if line.rstrip().split()[-1] not in SFO_condition.keys():
                    SFO_condition[line.rstrip().split()[-1]] = int(line.rstrip().split(delim)[SFO_condition_loc].rstrip())
                else: 
                    if int(line.rstrip().split(delim)[SFO_condition_loc].rstrip()) == 1: # only change the entry if it's 1
                        SFO_condition[line.rstrip().split()[-1]] = 1
                # check if ALL points that appear satisfy the SFO condition:
                if line.rstrip().split()[-1] not in SFO_condition_ALL.keys():
                    SFO_condition_ALL[line.rstrip().split()[-1]] = int(line.rstrip().split(delim)[SFO_condition_loc].rstrip())
                else: 
                    if int(line.rstrip().split(delim)[SFO_condition_loc].rstrip()) == 0: # only change the entry if it's 0
                        SFO_condition_ALL[line.rstrip().split()[-1]] = 0

                # record the vevs for further processing
                if line.rstrip().split()[-1] not in vev_values.keys():
                    vev_values[line.rstrip().split()[-1]] = [abs(float(line.rstrip().split(delim)[deepest_minimum_vev_loc].rstrip()))]
                else:
                    vev_values[line.rstrip().split()[-1]].append(abs(float(line.rstrip().split(delim)[deepest_minimum_vev_loc].rstrip())))
        point_stream.close()
    else:
        if os.path.exists(point_file) is False:
            print 'Warning, file', point_file, 'does not exist'
        if os.path.exists(thefile) is False:
            print 'Warning, file', thefile, 'does not exist'
        continue

# process the vev conditions:
for key in vev_values.keys():
    
    # count number of values above SM_vev:
    above_SM_vev = (np.asarray( vev_values[key] ) > SM_vev).sum()
    # count numbre of values below SM_vev:
    below_SM_vev =  (np.asarray( vev_values[key] ) < SM_vev).sum()

    # print for debugging:
    print 'key, vev_values[key]=', key, vev_values[key], above_SM_vev, below_SM_vev
    
    # if there are point above the SM value and below the SM value, then there has been a transition at the SM vev
    if above_SM_vev > 0 and below_SM_vev > 0:
        vev_conditions[key] = 1
    else:
        vev_conditions[key] = 0
    

#Classification of parameter space points:
#Conservative POINTS: 
# - ALL 8 of them have to satisfy SFO and group has to satisfy VEV conditions
#LIBERAL POINTS:
# - AT LEAST ONE point satisfying SFO and group has to satisfy VEV conditions

#where: 
#SFO conditions: rho > 1.0 and no other transition with rho in [0.1, 1.0] with higher TC than the rho > 1.0 transition
#VEV conditions: one deepest minimum < 246. and one > 246.

count_conservative = 0
count_total = 0
count_liberal = 0
print 'key, point_multiplicity[key], SFO_condition[key], vev_conditions[key], SFO_condition_ALL[key]'
for key in sorted(point_multiplicity.keys()):
    if int(key) not in thefile_central_point.keys():
        print 'Warning: key', key, 'not found in thefile_central_point'
        continue
    #print key, point_multiplicity[key], SFO_condition[key], vev_conditions[key], SFO_condition_ALL[key]
    if key in rho_max_isnonzero.keys():
        #print key, rho_max_sum[key], rho_max_sumsq[key], rho_max_isnonzero[key]
        rho_max_average[key] = rho_max_sum[key]/float(rho_max_isnonzero[key])
        rho_max_avsquared = rho_max_sumsq[key]/float(rho_max_isnonzero[key])
        rho_max_std[key] = math.sqrt(rho_max_avsquared - rho_max_average[key]**2)
    else:
        rho_max_std[key] = -1.0
    #print key, point_multiplicity[key], rho_max_std[key]
    if point_multiplicity[key] == 8:
        # CONSERVATIVE POINTS: ALL 8 of them have to satisfy SFO and group satisfies VEV conditions
        if SFO_condition_ALL[key] == 1 and vev_conditions[key] == 1:
            count_conservative = count_conservative + 1
            cons_line = ','.join(thefile_central_point[int(key)].rstrip().split(delim)[:-1]) + ',' + rho_max[key] + ',' + str(rho_max_average[key]) + ',' + str(rho_max_std[key]) + ',' + thefile_central_point[int(key)].rstrip().split(delim)[-1] 
            conservative_stream.write(cons_line + '\n')
    if point_multiplicity[key] != 8:
        # LIBERAL POINTS: AT LEAST ONE satisfies SFO and group satisfies VEV conditions
        if SFO_condition[key] == 1 and vev_conditions[key] == 1:
            count_liberal = count_liberal + 1
            lib_line = ','.join(thefile_central_point[int(key)].rstrip().split(delim)[:-1]) + ',' + rho_max[key] + ',' + str(rho_max_average[key]) + ',' + str(rho_max_std[key]) + ',' + thefile_central_point[int(key)].rstrip().split(delim)[-1] 
            liberal_stream.write(lib_line + '\n')
    count_total = count_total + 1

    
print 'total number:', count_total
print 'conservative points:', count_conservative
print 'liberal points:', count_liberal

print 'Done!'
