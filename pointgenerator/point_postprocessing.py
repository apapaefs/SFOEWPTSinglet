import sys
import time
import datetime
import os.path
from optparse import OptionParser
import subprocess

# adds a tag to my files at the end of each line:
# one every 8 points

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 2:
    print 'usage: postprocess_thefiles.py [input subfolder] [min #] [max #]'
    exit()

print 'processing pointoutputX.txt in', args[0], 'starting from', args[1]
print 'writing out', args[0] + '/pointoutpout_conservative.txt, and ' + args[0] + '/pointoutpout_liberal.txt'

filetag = 'pointoutput'
fileext = '.txt'

path = args[0]
minNum = args[1]
maxNum = args[2]

conservative_file = path + '/pointoutpout_conservative.txt'
liberal_file = path + '/pointoutpout_liberal.txt'

conservative_stream = open(conservative_file,'w')
liberal_stream = open(liberal_file, 'w')

for r in range(int(minNum), int(maxNum)+1):
    # check if file exists:
    pointfile = path + '/' + filetag + str(r) + fileext
    if os.path.exists(pointfile) is True:
        point_stream = open(pointfile,'r')
        count = 1
        for line in thefile_stream:
            if len(line.split()) == 14:
                print line
        point_stream.close()
    else:
        print 'Warning, file', pointfile, 'does not exist'
        continue
    
