import sys
import time
import datetime
import os.path
from optparse import OptionParser
import subprocess

# splits the output files into conservative (all 8 variations pass) or centrist (at least 1 variation passes)
# writes out pointoutput_conservative.txt and pointoutput_centrist.txt that contain the "central" parameters for each:
# i.e. the one corresponding to RGE scale = 91 GeV and gauge param xi = 0.0001

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 3:
    print 'usage: postprocess_thefiles.py [input subfolder] [min #] [max #]'
    exit()

print 'processing pointoutputX.txt in', args[0], 'starting from', args[1]
print 'writing out', args[0] + '/pointoutput_conservative.txt, and ' + args[0] + '/pointoutput_centrist.txt'
print args[0] + '/pointoutput_fascist.txt, and ' + args[0] + '/pointoutput_liberal.txt'
thefiletag = 'thefile'
filetag = 'pointoutput'
fileext = '.txt'

path = args[0]
minNum = args[1]
maxNum = args[2]

fascist_file = path + '/pointoutput_fascist.txt'
conservative_file = path + '/pointoutput_conservative.txt'
centrist_file = path + '/pointoutput_centrist.txt'
liberal_file = path + '/pointoutput_liberal.txt'

fascist_stream = open(fascist_file,'w')
conservative_stream = open(conservative_file,'w')
centrist_stream = open(centrist_file, 'w')
liberal_stream = open(liberal_file, 'w')


point_multiplicity = {} # this dictionary will contain the point multiplicity, i.e. how many times a tag appears in a file.

thefile_central_point = {} # this dictionary contains the "central" point, i.e. RG = 91, xi = 0.0001
RG_element = 11
xi_element = 10
RG_central = 91
xi_central = 0.0001

rho_max = {} # contains the rho_max for each point

vev_conditions = {} # 1 if the Higgs vev condition 246+-30 GeV/deepest minimum is satisfied by AT LEAST ONE POINT, 0 otherwise
SFO_condition = {} # 1 if the SFO condition is satisfied by at least one point (rho_max > 1.0 and largest TC) 
SFOvev_conditions = {} # 1 if at least one point satisfies simultaneously SFO and vev conditions

vev_conditions_ALL = {} # 1 if the vev conditions are satisfied by ALL poijnts in group
SFO_condition_ALL = {} # 1 if the SFO condition is satisfied by ALL points in group (rho_max > 1.0 and largest TC) 


# change this according to the length of the lines:
pointfile_line_length = 20
thefile_line_length = 16

delim = ','
delimoutput = ' '
    
for r in range(int(minNum), int(maxNum)+1):
    # check if file exists:
    point_file = path + '/' + filetag + str(r) + fileext
    thefile = path + '/' + thefiletag + str(r) + fileext
    if os.path.exists(point_file) is True and os.path.exists(thefile) is True:
        # read in thefile with the original points, grab the RG = 91, xi = 0.0001
        thefile_stream = open(thefile, 'r')
        for line in thefile_stream:
            if len(line.split(delim)) == thefile_line_length:
                if float(line.split(delim)[RG_element]) == RG_central and float(line.split(delim)[xi_element]) == xi_central:
                    thefile_central_point[int(line.split(delim)[-1])] = line
        # read in the point results (these satisfy SFOEWPT)
        point_stream = open(point_file,'r')
        count = 1
        for line in point_stream:
            if len(line.rstrip().split()) == pointfile_line_length: # if the line has the correct length
                # first get the rho_max into the dictionary
                if line.rstrip().split()[-1] not in rho_max.keys():
                    rho_max[line.rstrip().split()[-1]] = line.rstrip().split(delim)[-5]
                else:
                    if float(line.rstrip().split(delim)[-2]) > float(rho_max[line.rstrip().split()[-1]]):
                        rho_max[line.rstrip().split()[-1]] = line.rstrip().split(delim)[-5]
                # count the number of times the point appears in the point output file:
                if line.rstrip().split()[-1] not in point_multiplicity.keys():
                    point_multiplicity[line.rstrip().split()[-1]] = 1
                else:
                    point_multiplicity[line.rstrip().split()[-1]] = point_multiplicity[line.rstrip().split()[-1]] + 1
                # determine whether ANY of the group of points satisfies the SFO condition:
                if line.rstrip().split()[-1] not in SFO_condition.keys():
                    SFO_condition[line.rstrip().split()[-1]] = int(line.rstrip().split(delim)[-4].rstrip())
                else: 
                    if int(line.rstrip().split(delim)[-4].rstrip()) == 1: # only change the entry if it's 1
                        SFO_condition[line.rstrip().split()[-1]] = 1
                # check if ALL points that appear satisfy the SFO condition:
                if line.rstrip().split()[-1] not in SFO_condition_ALL.keys():
                    SFO_condition_ALL[line.rstrip().split()[-1]] = int(line.rstrip().split(delim)[-4].rstrip())
                else: 
                    if int(line.rstrip().split(delim)[-4].rstrip()) == 0: # only change the entry if it's 0
                        SFO_condition_ALL[line.rstrip().split()[-1]] = 0
                # determine whether ANY of the group of points satisfies the vev conditions:
                if line.rstrip().split()[-1] not in vev_conditions.keys():
                    vev_conditions[line.rstrip().split()[-1]] = min(int(line.rstrip().split(delim)[-3].rstrip()), int(line.rstrip().split(delim)[-2].rstrip()))
                else: 
                    if int(line.rstrip().split(delim)[-3].rstrip()) == 1 and int(line.rstrip().split(delim)[-2].rstrip()) == 1: # only change the entry if BOTH are 1
                        vev_conditions[line.rstrip().split()[-1]] = 1
                # check if ALL of the group of points satisfies the vev conditions:
                if line.rstrip().split()[-1] not in vev_conditions_ALL.keys():
                    vev_conditions_ALL[line.rstrip().split()[-1]] = min(int(line.rstrip().split(delim)[-3].rstrip()), int(line.rstrip().split(delim)[-2].rstrip()))
                else: 
                    if int(line.rstrip().split(delim)[-3].rstrip()) == 0 or int(line.rstrip().split(delim)[-2].rstrip()) == 0: # change if either entry is zero
                        vev_conditions_ALL[line.rstrip().split()[-1]] = 0
                # determine whether any of the group of points satisfies the vev and SFO conditions simultaneously
                if line.rstrip().split()[-1] not in SFOvev_conditions.keys():
                    SFOvev_conditions[line.rstrip().split()[-1]] = min(int(line.rstrip().split(delim)[-4].rstrip()), min(int(line.rstrip().split(delim)[-3].rstrip()), int(line.rstrip().split(delim)[-2].rstrip())))
                else: 
                    if int(line.rstrip().split(delim)[-4].rstrip()) == 1 and int(line.rstrip().split(delim)[-3].rstrip()) == 1 and int(line.rstrip().split(delim)[-2].rstrip()) == 1: # only change the entry if ALL THREE are 1
                        SFOvev_conditions[line.rstrip().split()[-1]] = 1
                    
        point_stream.close()
    else:
        if os.path.exists(point_file) is False:
            print 'Warning, file', point_file, 'does not exist'
        if os.path.exists(thefile) is False:
            print 'Warning, file', thefile, 'does not exist'
        continue

#Classification of parameter space points:

#FASCIST POINTS:
# - ALL 8 points have to satisfy BOTH SFO and VEV conditions
#CONSERVATIVE POINTS: 
# - ALL 8 of them have to satisfy SFO and AT LEAST one has to satisfy VEV conditions
#CENTRIST POINTS:
# - AT LEAST ONE point satisfying SFO and VEV conditions simultaneously.
#LIBERAL POINTS:
# - AT LEAST ONE satisfies SFO and ANY OTHER satisfies VEV conditions
#where: 
#SFO conditions: rho > 1.0 and no other transition with rho in [0.1, 1.0] with higher TC than the rho > 1.0 transition
#VEV conditions: Higgs VEV at tmin = 246+-30 GeV and deepest minimum

count_conservative = 0
count_total = 0
count_centrist = 0
count_liberal = 0
count_fascist = 0
print 'key, point_multiplicity[key], SFO_condition[key], vev_conditions[key], SFO_condition_ALL[key], vev_conditions_ALL[key]'
for key in sorted(point_multiplicity.keys()):
    if int(key) not in thefile_central_point.keys():
        print 'Warning: key', key, 'not found in thefile_central_point'
        continue
    print key, point_multiplicity[key], SFO_condition[key], vev_conditions[key], SFO_condition_ALL[key], vev_conditions_ALL[key]
    fascist_pt = False
    centrist_pt = False
    if point_multiplicity[key] == 8:
        # FASCIST POINTS: ALL 8 points have to satisfy BOTH SFO and VEV conditions
        if SFO_condition_ALL[key] == 1 and vev_conditions_ALL[key] == 1:
            count_fascist = count_fascist + 1
            fascist_line = ','.join(thefile_central_point[int(key)].rstrip().split(delim)[:-1]) + ',' + rho_max[key] + ',' + thefile_central_point[int(key)].rstrip().split(delim)[-1]
            fascist_stream.write(fascist_line + '\n')
            fascist_pt = True
        # CONSERVATIVE POINTS: ALL 8 of them have to satisfy SFO and AT LEAST one has to satisfy VEV conditions
        if SFO_condition_ALL[key] == 1 and vev_conditions[key] == 1 and fascist_pt == False:
            count_conservative = count_conservative + 1
            cons_line = ','.join(thefile_central_point[int(key)].rstrip().split(delim)[:-1]) + ',' + rho_max[key] + ',' + thefile_central_point[int(key)].rstrip().split(delim)[-1]
            conservative_stream.write(cons_line + '\n')
    if point_multiplicity[key] != 8:
        # CENTRIST POINTS: AT LEAST ONE point satisfying SFO and VEV conditions simultaneously.
        if SFOvev_conditions[key] == 1:
            count_centrist = count_centrist + 1
            centr_line = ','.join(thefile_central_point[int(key)].rstrip().split(delim)[:-1]) + ',' + rho_max[key] + ',' + thefile_central_point[int(key)].rstrip().split(delim)[-1]
            centrist_stream.write(centr_line + '\n')
            centrist_pt = True
        # LIBERAL POINTS: AT LEAST ONE satisfies SFO and ANY OTHER satisfies VEV conditions
        if SFO_condition[key] == 1 and vev_conditions[key] == 1 and not centrist_pt:
            count_liberal = count_liberal + 1
            lib_line = ','.join(thefile_central_point[int(key)].rstrip().split(delim)[:-1]) + ',' + rho_max[key] + ',' + thefile_central_point[int(key)].rstrip().split(delim)[-1]
            liberal_stream.write(lib_line + '\n')

            
    count_total = count_total + 1

    
print 'total number:', count_total
print 'conservative points:', count_conservative
print 'centrist points:', count_centrist
print 'liberal points:', count_liberal
print 'fascist points:', count_fascist

print 'Done!'
