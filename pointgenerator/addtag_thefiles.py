#! /usr/bin/env python
import sys
import time
import datetime
import os.path
from optparse import OptionParser
import subprocess

# adds a tag to my files at the end of each line:
# one every 8 points

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 5:
    print 'usage: addtag_thefiles.py [input subfolder] [min #] [max #] [output subfolder] [starting tag number]'
    exit()

print 'adding missing tags in thefileX.txt in', args[0], 'starting from', args[1], 'to', args[2],'and starting from the number', args[3]

filetag = 'thefile'
fileext = '.txt'

path = args[0]
minNum = args[1]
maxNum = args[2]
outpath = args[3]
startingNum = int(args[4])


for r in range(int(minNum), int(maxNum)+1):
    # check if file exists:
    thefile = path + '/' + filetag + str(r) + fileext
    thefilenew = outpath + '/' + filetag + str(r) + fileext
    if os.path.exists(thefile) is True:
        thefile_stream = open(thefile,'r')
        thefilenew_stream = open(thefilenew, 'w')
        count = 1
        print 'writing out', thefilenew
        for line in thefile_stream:
            if len(line.split()) == 13:
                newline = line.rstrip() + ', ' + str(startingNum)
                thefilenew_stream.write(newline + '\n')
                count = count + 1
                if count%9 == 0:
                    startingNum = startingNum+1
        thefile_stream.close()
        thefilenew_stream.close()
    else:
        print 'Warning, file', thefile, 'does not exist'
        continue
    

