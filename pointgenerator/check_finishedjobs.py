import sys
import time
import datetime
import os.path
from optparse import OptionParser
import subprocess
import numpy as np

# check which jobs have not finished:
# get the finished jobs e.g. by:
# grep -Hr -F1 'Finished processing' | grep run3 | grep output | grep -o "[^ ]*$" > finished_jobs_run3.txt

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 3:
    print 'usage: check_finishedjobs [file with integers representing # of finished jobs] [min number of jobs] [max number of jobs]'
    exit()

set_no = 9
run_no = 10
#tag = 'run'
tag = 'vev'
xrslfile = 'xrslfile-vev'
xrslfile = 'xrslfile'

inputfile = args[0]
r1 = int(args[1])
r2 = int(args[2])
if len(args) > 3: 
    run_no = int(args[3])

input_stream = open(inputfile, 'r')

completed_array = []

for line in input_stream:
    completed_array.append(int(line))

full_array = np.arange(r1, r2+1, 1)

diff = set(full_array) - set(completed_array)

print 'missing:', sorted(diff)

print 'total number of jobs missing:', len(diff)

print 'printing commands to re-launch, for run number=', run_no


filestream = open("resubmit" + str(run_no) + ".sh", "w")

for diff_i in sorted(diff):
    comm2 = 'gfal-rm gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/apapaefstathiou/singlet/phasetracer_SET' + str(set_no) + '_' + tag + str(run_no) + '/pointoutput' + str(diff_i) + '.txt'
    print comm2
    filestream.write(comm2 + '\n')
    comm3 = 'gfal-rm gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/apapaefstathiou/singlet/phasetracer_SET' + str(set_no) + '_' + tag + str(run_no) + '/output' + str(diff_i) + '.txt'
    print comm3
    filestream.write(comm3 + '\n')
    comm4 = 'gfal-rm gsiftp://se01.dur.scotgrid.ac.uk/dpm/dur.scotgrid.ac.uk/home/pheno/apapaefstathiou/singlet/phasetracer_SET' + str(set_no) + '_' + tag + str(run_no) + '/fullpointoutput' + str(diff_i) + '.txt'
    print comm4
    filestream.write(comm4 + '\n')
    comm = './submittoGrid.pl ' + xrslfile + ' apapaefstathiou/singlet/phasetracer_SET' + str(set_no) + '_' + tag + str(run_no) + ' '  + str(diff_i) + ' ' + str(diff_i) + ' phasetracer_SET' + str(set_no) + '_' + tag + str(run_no) + 'r ce1.dur.scotgrid.ac.uk'
    print comm
    filestream.write(comm + '\n')
