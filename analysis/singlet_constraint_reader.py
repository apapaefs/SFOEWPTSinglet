from singlet_constraint_functions import *

# Choose the category:
category = 1

# and date of data:
dateofdata = '07042022'

# include excluded points or not:
IncludeExcluded = True

# DIRECTORIES
addtag = ''
outputdirectory = 'plots_' + str(todaysdate) + '_cat' + str(category) + addtag + '/'
pickledir = 'data_' + str(dateofdata) + '_cat' + str(category) + addtag + '/'

# check if the plot directory exists and create if not
if not os.path.exists(outputdirectory):
    os.makedirs(outputdirectory)

if category == 0:
    # change the colour scheme
    change_color_scheme2()

if category == 2:
    # change the colour scheme
    change_color_scheme()


# unpickle the data 
SIGN_SINTHETA_scatter = read_datfile(pickledir + 'SIGN_SINTHETA_scatter')
OneLoop_mh1 = read_datfile(pickledir + 'OneLoop_mh1')
OneLoop_mh2 = read_datfile(pickledir + 'OneLoop_mh2')
OneLoop_sintheta = read_datfile(pickledir + 'OneLoop_sintheta')
OneLoop_costheta = read_datfile(pickledir + 'OneLoop_costheta')
Tree_l112 = read_datfile(pickledir + 'Tree_l112')
Tree_l122 = read_datfile(pickledir + 'Tree_l122')
Tree_l222 = read_datfile(pickledir + 'Tree_l222')
Tree_l111 = read_datfile(pickledir + 'Tree_l111')
AbsTree_l112 = read_datfile(pickledir + 'AbsTree_l112')
BR_HH_scatter = read_datfile(pickledir + 'BR_HH_scatter')
BR_ZZ_scatter = read_datfile(pickledir + 'BR_ZZ_scatter')
BR_WW_scatter = read_datfile(pickledir + 'BR_WW_scatter')
M2_scatter = read_datfile(pickledir + 'M2_scatter')
Constraints = read_datfile(pickledir + 'Constraints')
RHO_MAX_scatter = read_datfile(pickledir + 'RHO_MAX_scatter')
RHO_STD_scatter = read_datfile(pickledir + 'RHO_STD_scatter')
RHO_AV_scatter = read_datfile(pickledir + 'RHO_AV_scatter')
RHO_STDOVAV_scatter = read_datfile(pickledir + 'RHO_STDOVAV_scatter')
FINETUNING_scatter = read_datfile(pickledir + 'FINETUNING_scatter')
FINETUNING2_scatter = read_datfile(pickledir + 'FINETUNING2_scatter')
FINETUNING3_scatter = read_datfile(pickledir + 'FINETUNING3_scatter')
FINETUNING_1loop_m1_scatter = read_datfile(pickledir + 'FINETUNING_1loop_m1_scatter')
FINETUNING_1loop_m2_scatter = read_datfile(pickledir + 'FINETUNING_1loop_m2_scatter')
FINETUNINGMAX_scatter = read_datfile(pickledir + 'FINETUNINGMAX_scatter')
XS100_HH_scatter = read_datfile(pickledir + 'XS100_HH_scatter')
XS100_WW_scatter = read_datfile(pickledir + 'XS100_WW_scatter')
XS100_ZZ_scatter = read_datfile(pickledir + 'XS100_ZZ_scatter')
SIGNIF100_HH_scatter = read_datfile(pickledir + 'SIGNIF100_HH_scatter')
SIGNIF100_WW_scatter = read_datfile(pickledir + 'SIGNIF100_WW_scatter')
SIGNIF100_ZZ_scatter = read_datfile(pickledir + 'SIGNIF100_ZZ_scatter')
XS27_HH_scatter = read_datfile(pickledir + 'XS27_HH_scatter')
XS27_WW_scatter = read_datfile(pickledir + 'XS27_WW_scatter')
XS27_ZZ_scatter = read_datfile(pickledir + 'XS27_ZZ_scatter')
XS100_MAX_scatter = read_datfile(pickledir + 'XS100_MAX_scatter')
SIGNIF100_MAX_scatter = read_datfile(pickledir + 'SIGNIF100_MAX_scatter')
SIGNIF27_MAX_scatter = read_datfile(pickledir + 'SIGNIF27_MAX_scatter')
SIGNIF27_HH_scatter = read_datfile(pickledir + 'SIGNIF27_HH_scatter')
SIGNIF27_WW_scatter = read_datfile(pickledir + 'SIGNIF27_WW_scatter')
SIGNIF27_ZZ_scatter = read_datfile(pickledir + 'SIGNIF27_ZZ_scatter')
SIGNIF14_MAX_scatter = read_datfile(pickledir + 'SIGNIF14_MAX_scatter')
SIGNIF14_HH_scatter = read_datfile(pickledir + 'SIGNIF14_HH_scatter')
SIGNIF14_WW_scatter = read_datfile(pickledir + 'SIGNIF14_WW_scatter')
SIGNIF14_ZZ_scatter = read_datfile(pickledir + 'SIGNIF14_ZZ_scatter')
DELTAMH1_scatter = read_datfile(pickledir + 'DELTAMH1_scatter')
DELTAMH2tree_scatter = read_datfile(pickledir + 'DELTAMH2tree_scatter')
SIGNIF100_CurrentFT_HH_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_CurrentFT_HH_lumi_scatter')
SIGNIF100_CurrentFT_WW_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_CurrentFT_WW_lumi_scatter')
SIGNIF100_CurrentFT_ZZ_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_CurrentFT_ZZ_lumi_scatter')
SIGNIF100_CurrentFT_MAX_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_CurrentFT_MAX_lumi_scatter')
SIGNIF100_HH_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_HH_lumi_scatter')
SIGNIF100_WW_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_WW_lumi_scatter')
SIGNIF100_ZZ_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_ZZ_lumi_scatter')
SIGNIF100_MAX_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_MAX_lumi_scatter')
SIGNIF27_CurrentFT_HH_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_CurrentFT_HH_lumi_scatter')
SIGNIF27_CurrentFT_WW_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_CurrentFT_WW_lumi_scatter')
SIGNIF27_CurrentFT_ZZ_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_CurrentFT_ZZ_lumi_scatter')
SIGNIF27_CurrentFT_MAX_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_CurrentFT_MAX_lumi_scatter')
SIGNIF27_HH_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_HH_lumi_scatter')
SIGNIF27_WW_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_WW_lumi_scatter')
SIGNIF27_ZZ_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_ZZ_lumi_scatter')
SIGNIF27_MAX_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_MAX_lumi_scatter')
SIGNIF27_Current_HH_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_Current_HH_lumi_scatter')
SIGNIF27_Current_WW_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_Current_WW_lumi_scatter')
SIGNIF27_Current_ZZ_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_Current_ZZ_lumi_scatter')
SIGNIF27_Current_MAX_lumi_scatter = read_datfile(pickledir + 'SIGNIF27_Current_MAX_lumi_scatter')
SIGNIF100_Current_HH_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_Current_HH_lumi_scatter')
SIGNIF100_Current_WW_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_Current_WW_lumi_scatter')
SIGNIF100_Current_ZZ_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_Current_ZZ_lumi_scatter')
SIGNIF100_Current_MAX_lumi_scatter = read_datfile(pickledir + 'SIGNIF100_Current_MAX_lumi_scatter')
CLIC_VV_scatter = read_datfile(pickledir + 'CLIC_VV_scatter')
CLIC14_HH_scatter = read_datfile(pickledir + 'CLIC14_HH_scatter') 
CLIC3_HH_scatter = read_datfile(pickledir + 'CLIC3_HH_scatter')

K1_scatter = read_datfile(pickledir + 'K1_scatter')
K2_scatter = read_datfile(pickledir + 'K2_scatter')
Kappa_scatter = read_datfile(pickledir + 'Kappa_scatter')
Lambda_scatter = read_datfile(pickledir + 'Lambda_scatter')
LambdaS_scatter = read_datfile(pickledir + 'LambdaS_scatter')
x0_scatter = read_datfile(pickledir + 'x0_scatter')
MS_scatter = read_datfile(pickledir + 'MS_scatter')
mu2_scatter = read_datfile(pickledir + 'mu2_scatter')

MASSFIT_scatter = read_datfile(pickledir + 'MASSFIT_scatter')
DMASSFIT_scatter = read_datfile(pickledir + 'DMASSFIT_scatter')

XS_HETA0_scatter = read_datfile(pickledir + 'XS_HETA0_scatter')

sintheta_fit_prec_scatter = read_datfile(pickledir + 'sintheta_fit_prec_scatter')
l112_fit_prec_scatter = read_datfile(pickledir + 'l112_fit_prec_scatter')

l112_max_fit_errormass_scatter = read_datfile(pickledir + 'l112_max_fit_errormass_scatter')
l112_min_fit_errormass_scatter = read_datfile(pickledir + 'l112_min_fit_errormass_scatter')
sintheta_max_fit_errormass_scatter = read_datfile(pickledir + 'sintheta_max_fit_errormass_scatter')
sintheta_min_fit_errormass_scatter = read_datfile(pickledir + 'sintheta_min_fit_errormass_scatter')    

PointName = read_datfile(pickledir + 'PointName')

# set the luminosities
Lumis = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
Lumis27 = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]


########################
# PLOTTING STARTS HERE #
########################

# DEFINE THE MARKER MAPS FOR VARIOUS COLLIDER SCENARIOS
marker_map = {}
marker_map['HL-LHC'] = ''
marker_map['HL-LHC-CS'] = '^'
marker_map['CURRENT'] = 'o' #'o'
marker_map['EXCLUDED'] = 'X'

if IncludeExcluded is False:
    print('Removing currently-excluded points!')
    for key in list(Constraints.keys()):
        for i in range(len(Constraints[key])):
            if Constraints[key][i] == 'x':
                #print 'Changed key'
                Constraints[key][i] = ''
        #print key, Constraints[key]

corrcolors_key = {}
# default:
corrcolors_key['GWAPCons']='red'
corrcolors_key['GWAPLib']='orange'
corrcolors_key['GWAPUCons']='magenta'
corrcolors_key['GWAPCentr']='green'
corrcolors_key['excl']='yellow'
corrcolors_key['NoTrans']='blue'
corrcolors_key['GWAPTight']='blue'
corrcolors_key['GWAPLoose']='violet'
corrcolors_key['MRMBmax']='brown'
corrcolors_key['MRMBmin']='cyan'
corrcolors_key['excl']='yellow'
exclalpha = 0.1

#########################
# OVERRIDE FOR SNOWMASS:
#########################
print("OVERRIDING FOR SNOWMASS")
corrcolors_key['GWAPCons']='red'
corrcolors_key['GWAPLib']='red'
corrcolors_key['GWAPUCons']='red'
corrcolors_key['GWAPCentr']='red'
corrcolors_key['excl']='red'
exclalpha = 1.0
for key in list(Constraints.keys()):
    for i in range(len(Constraints[key])):
        Constraints[key][i] = 'o'
############################
# END OVERRIDE FOR SNOWMASS
############################

ColorExcluded = corrcolors_key['excl']
corrcolors = {}
corralphas = {}
corrcolors_array = {}

for key in list(Constraints.keys()):
    TypeTag = ''.join([i for i in key if not i.isdigit()])
    corrcolors[TypeTag] = []
    corralphas[TypeTag] = []
    corrcolors_array[TypeTag] = []
    
for key in list(Constraints.keys()):
    for i in range(len(Constraints[key])):
        if Constraints[key][i] == 'x':
            corralphas[key].append([corrcolors_key['excl'], exclalpha])
        else:
            corralphas[key].append([corrcolors_key[key], 1.0])

for key in list(Constraints.keys()):
    #for alphacol in corralphas[key]:
    #    print alphacol[0], alphacol[1], to_rgb(alphacol[0])
    corrcolors_array[key] = [(to_rgb(alphacol[0])[0], to_rgb(alphacol[0])[1], to_rgb(alphacol[0])[2], alphacol[1]) for alphacol in corralphas[key]]
    #print len(corrcolors_array[key]), np.array(corrcolors_array[key]).shape, corrcolors_array[key][0], corrcolors_array[key][0], corrcolors_array[key][1], corrcolors_array[key][1]
    

#execfile('singlet_constraint_correlation.py')

exec(compile(open('singlet_constraint_plots.py', "rb").read(), 'singlet_constraint_plots.py', 'exec'))

