 
from singlet_constraint_functions import *

# Get the single h2 cross section from XS_interpolator_SM_100TeV_N3LON3LL for a given mass:

# point parameters:
#mh1 = 125.1
#mh2 = 466.29
#ctreal = 0.990457406445 # the true value of costheta for this point
#streal = math.sqrt(1-ctreal**2)
#k112real = 100.36 # true value of k112
#k122real = 502.56 # true value of k122
#RunNum = 3
#prec_sigma_zz = 1E-5
#prec_sigma_hh = 1E-4


# point parameters
mh1 = 125.1
mh2 = 738.6
streal = 0.2212410143
ctreal = math.sqrt(1-streal**2)
k112real = 39.081 
RunNum = 5
signif_zz = 9210.442472962828
signif_hh = 5.221805220423036
prec_sigma_zz = 1/signif_zz
prec_sigma_hh = 1/signif_hh


def line(p1, p2):
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0]*p2[1] - p2[0]*p1[1])
    return A, B, -C

def intersection(L1, L2):
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x,y
    else:
        return False

def sigma_pp_h2_to_ZZ_or_hh(mh2, stval, l112val):
    stval = np.array(stval)
    l112val = np.array(l112val)
    # get the cross section at N^3LO+N3LL @ 100 TeV (ihixs):
    
    ggF_XS_mh2 = stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2)

    print(ggF_XS_mh2)
    #print 'gg -> h2 xsec, (mh2,stval) = (', mh2, stval, ')=', ggF_XS_mh2, 'pb'

    # get the BR to h1h1 and to ZZ:
    if mh2 < 1000.: # 
        Gamma_SM = BR_interpolators_SM[-1](mh2)
    else:
        Gamma_SM = BR_interpolators_SM[-1](1000.)
        
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(stval, Gamma_SM, mh1, mh2, l112val)
    BR_hh = BR_h2_to_h1h1(stval, mh1, mh2, l112val, Gamma_SM)
    BR_ZZ = BR_interpolators_SM[10](mh2) * rescale_fac

    #print rescale_fac
    
    #print BR_ZZ
     
    
    ggF_XS_mh2_ZZ = ggF_XS_mh2 * BR_ZZ
    ggF_XS_mh2_hh = ggF_XS_mh2 * BR_hh

    return ggF_XS_mh2_ZZ, ggF_XS_mh2_hh

def BR_h2_to_ZZ_or_hh(mh2, stval, l112val):
    stval = np.array(stval)
    l112val = np.array(l112val)
    # get the cross section at N^3LO+N3LL @ 100 TeV (ihixs):
    
    ggF_XS_mh2 = stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2)

    print(ggF_XS_mh2)
    #print 'gg -> h2 xsec, (mh2,stval) = (', mh2, stval, ')=', ggF_XS_mh2, 'pb'

    # get the BR to h1h1 and to ZZ:
    if mh2 < 1000.: # 
        Gamma_SM = BR_interpolators_SM[-1](mh2)
    else:
        Gamma_SM = BR_interpolators_SM[-1](1000.)
        
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(stval, Gamma_SM, mh1, mh2, l112val)
    BR_hh = BR_h2_to_h1h1(stval, mh1, mh2, l112val, Gamma_SM)
    BR_ZZ = BR_interpolators_SM[10](mh2) * rescale_fac

    return BR_ZZ, BR_hh

def sigma_pp_h2_to_ZZ_or_hh_plot(mh2, st, l112val):
    l112val = np.array(l112val)
    ggF_XS_mh2_ZZ = []
    ggF_XS_mh2_hh = []
    for stval in st:
            ggF_XS_mh2_ZZ.append(stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2) * BR_interpolators_SM[10](mh2) * RES_BR_h2_to_xx(stval, BR_interpolators_SM[-1](mh2), mh1, mh2, l112val))
            ggF_XS_mh2_hh.append(stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2) * BR_h2_to_h1h1(stval, mh1, mh2, l112val, BR_interpolators_SM[-1](mh2)))
    return ggF_XS_mh2_ZZ, ggF_XS_mh2_hh

##### the real cross sections:
sigma_zz_real, sigma_hh_real = sigma_pp_h2_to_ZZ_or_hh(mh2, streal, k112real)
print('real cross sections, ZZ, hh=', sigma_zz_real, sigma_hh_real)
BR_ZZ_real, BR_hh_real = BR_h2_to_ZZ_or_hh(mh2, streal, k112real)
print('real BRs, ZZ, hh=', BR_ZZ_real, BR_hh_real)


################### PLOTS START HERE #################################################

# generate sigmas for multiple values:

xmin = -1000
xmax = 1200.

lambda112_x = np.arange(xmin, xmax, (xmax-xmin)/1000)

sigma_zz, sigma_hh = sigma_pp_h2_to_ZZ_or_hh(mh2, streal, lambda112_x)


outputdirectory = './hSstudy/'

gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# range of figure
xminp = -500
xmaxp = 500.

plt.plot( lambda112_x, sigma_zz, marker='', color='blue', ms=9, label='$ZZ$')
plt.plot( lambda112_x, sigma_hh, marker='', color='red', ms=9, label='$h_1 h_1$')



#ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$\\sigma(gg \\rightarrow h_2 \\rightarrow xx)$ [pb]', fontsize=20)
ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
titletext = '$\\sigma(gg \\rightarrow h_2 \\rightarrow xx)_\\mathrm{UCons30}$ @ 100 TeV, fixed $\\sin \\theta =' + str(round_sig(streal,3)) + '$'
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_xlim(xminp,xmaxp)

ax.xaxis.set_major_locator(MultipleLocator(100))
ax.xaxis.set_minor_locator(MultipleLocator(50))

ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))

# create legend and plot/font size
#ax.legend()
ax.legend(loc="center right", numpoints=1, frameon=False, prop={'size':10})

plot_type = 'xx_sigma_heta0_UCons30'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '_' + str(RunNum) + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

################### CONTOUR PLOT STARTS HERE #################################################

#####################    
# plot zoomed in    #
#####################

outputdirectory = './hSstudy/'

gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)
#ymin = 0.13780
#ymax = 0.13784
#xmin = 100.30
#xmax = 100.45
xmin = k112real * (1 - prec_sigma_hh)
xmax = k112real * (1 + prec_sigma_hh)
ymin = streal * (1 - 10*prec_sigma_zz)
ymax = streal * (1 + 10*prec_sigma_zz)

#yminp = 0.05
#ymaxp = 0.2
#xminp = -250
#xmaxp = 250.

yminp = ymin
ymaxp = ymax
xminp = xmin
xmaxp = xmax

xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
x1, y1 = np.meshgrid(xi,yi)

zi_zz, zi_hh = sigma_pp_h2_to_ZZ_or_hh_plot(mh2, yi, xi)

#print zi_hh

#print zi_zz


#zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
#ZZ
cs = plt.contour(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], extend='both', colors='k')
cs2 = plt.contourf(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

#hh
cs3 = plt.contour(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], extend='both', colors='k')
cs4 = plt.contourf(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

# get the paths for the zz curve
#p = cs.collections[0].get_paths()[0]
#v = p.vertices
#xm_zz = v[:,0]
#ym_zz = v[:,1]
#p = cs.collections[1].get_paths()[0]
#v = p.vertices
#xp_zz = v[:,0]
#yp_zz = v[:,1]

# get the paths for the hh curve
#p = cs3.collections[0].get_paths()[0]
#v = p.vertices
#xm_hh = v[:,0]
#ym_hh = v[:,1]
#p = cs3.collections[1].get_paths()[0]
#v = p.vertices
#xp_hh = v[:,0]
#yp_hh = v[:,1]

#L1_zz = line([xm_zz[0],ym_zz[0]], [xm_zz[-1],ym_zz[-1]])
#L2_zz = line([xp_zz[0],yp_zz[0]], [xp_zz[-1],yp_zz[-1]])
#L1_hh = line([xm_hh[0],ym_hh[0]], [xm_hh[-1],ym_hh[-1]])
#L2_hh = line([xp_hh[0],yp_hh[0]], [xp_hh[-1],yp_hh[-1]])

#R11 = intersection(L1_zz, L1_hh)
#R12 = intersection(L1_zz, L2_hh)
#R22 = intersection(L2_zz, L2_hh)
#R21 = intersection(L2_zz, L1_hh)
#print 'intersections:', R11, R12, R22, R21

#print 'limits:'
#print 'lambda122 in [', min(R11[0], R12[0], R22[0], R21[0]), max(R11[0], R12[0], R22[0], R21[0]), ']', 'precision=', 2*(max(R11[0], R12[0], R22[0], R21[0])-min(R11[0], R12[0], R22[0], R21[0]))/(max(R11[0], R12[0], R22[0], R21[0])+min(R11[0], R12[0], R22[0], R21[0]))

#print 'sintheta in [', min(R11[1], R12[1], R22[1], R21[1]), max(R11[1], R12[1], R22[1], R21[1]), ']', 'precision=', 2*(max(R11[1], R12[1], R22[1], R21[1])-min(R11[1], R12[1], R22[1], R21[1]))/(max(R11[1], R12[1], R22[1], R21[1])+min(R11[1], R12[1], R22[1], R21[1]))


manual_locations = [(500, -2000), (1000, -4000)]

strs = ['$0.5\\times$', '$1.5\\times$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s

# true location:
#plt.plot(k112real, k122real, marker='*', ms=3, color='red')
#plt.axhline(k122real, ls='-', lw=0.5)
#plt.axvline(k112real, ls='-', lw=0.5)

# precision on kappa112
prec112 = 0.10
#ax.axvspan((1-prec112)*k112real, (1+prec112)*k112real, alpha=0.4, color='blue')
#ax.axvspan(-(1-prec112)*k112real, -(1+prec112)*k112real, alpha=0.4, color='blue')


# get the limits assuming a certain precision for kappa112
p = cs.collections[0].get_paths()[0]
v = p.vertices
xm = v[:,0]
ym = v[:,1]
p = cs.collections[1].get_paths()[0]
v = p.vertices
xp = v[:,0]
yp = v[:,1]
# solve:
# positive
#idxp1 = np.argwhere(np.diff(np.sign(xp - (1-prec112)*k112real))).flatten()
#idxp2 = np.argwhere(np.diff(np.sign(xp - (1+prec112)*k112real))).flatten()
#idxm1 = np.argwhere(np.diff(np.sign(xm - (1-prec112)*k112real))).flatten()
#idxm2 = np.argwhere(np.diff(np.sign(xm - (1+prec112)*k112real))).flatten()
#plt.plot( (1+prec112)*k112real, max(ym[idxm2]), marker='o', color='black', ms=4)
#plt.plot( (1-prec112)*k112real, max(yp[idxp1]), marker='o', color='black', ms=4)
#plt.plot( (1+prec112)*k112real, min(yp[idxp2]), marker='o', color='black', ms=4)
#plt.plot( (1-prec112)*k112real, min(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(max(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(ym[idxm1]), ls='--', lw=0.5, color='green')
#print 'lambda122 in:', '[', max(ym[idxm2]), ',', max(yp[idxp1]), ']U[', min(yp[idxp2]), ',', min(ym[idxm1]), ']'
# negative:
#idxp1 = np.argwhere(np.diff(np.sign(xp + (1-prec112)*k112real))).flatten()
#idxp2 = np.argwhere(np.diff(np.sign(xp + (1+prec112)*k112real))).flatten()
#idxm1 = np.argwhere(np.diff(np.sign(xm + (1-prec112)*k112real))).flatten()
#idxm2 = np.argwhere(np.diff(np.sign(xm + (1+prec112)*k112real))).flatten()
#plt.plot( -(1+prec112)*k112real, min(ym[idxm2]), marker='o', color='black', ms=4)
#plt.plot( -(1-prec112)*k112real, min(yp[idxp1]), marker='o', color='black', ms=4)
#plt.plot( -(1+prec112)*k112real, max(yp[idxp2]), marker='o', color='black', ms=4)
#plt.plot( -(1-prec112)*k112real, max(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(min(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(ym[idxm1]), ls='--', lw=0.5, color='green')

#print 'or lambda122 in:', '[', min(ym[idxm2]), ',', min(yp[idxp1]), ']U[', max(yp[idxp2]), ',', max(ym[idxm1]), ']'

plt.plot( k112real, streal, marker='*', color='red', ms=11)

#ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$\\sin \\theta$', fontsize=20)
ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
#titletext = 'pp@100 TeV/30 ab$^{-1}$,' + str(int(prec_sigma_zz*100000.)) + '$\\times 10^{-5}$ error on $ZZ$, ' + str(int(prec_sigma_hh*10000.)) + '$\\times 10^{-4}$ error on $h_1 h_1$ '
titletext = 'pp@100 TeV/30 ab$^{-1}$, $10^{-5}$ error on $ZZ$, $10^{-4}$ error on $h_1 h_1$ '
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_xlim(xminp,xmaxp)
ax.set_ylim(yminp,ymaxp)

#ax.xaxis.set_major_locator(MultipleLocator(200))
#ax.xaxis.set_minor_locator(MultipleLocator(50))

#ax.yaxis.set_major_locator(MultipleLocator(200))
#ax.yaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
#ax.legend()
#ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':6})

plot_type = 'xx_sigma_eta0_UCons30_2D'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '_' + str(RunNum) + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)


#####################    
# plot zoomed out    #
#####################

outputdirectory = './hSstudy/'

gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)
ymin = 0.22
ymax = 0.222
xmin = 30
xmax = 50.

#yminp = 0.05
#ymaxp = 0.2
#xminp = 50
#xmaxp = 250.

yminp = ymin
ymaxp = ymax
xminp = xmin
xmaxp = xmax

xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
x1, y1 = np.meshgrid(xi,yi)


zi_zz, zi_hh = sigma_pp_h2_to_ZZ_or_hh_plot(mh2, yi, xi)

#print zi_zz

#print sigma
#prec_sigma_zz = 1E-5
#prec_sigma_hh = 1E-4

#zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
#ZZ
cs = plt.contour(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], extend='both', colors='k')
cs2 = plt.contourf(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

#hh
cs3 = plt.contour(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], extend='both', colors='k')
cs4 = plt.contourf(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

# get the paths for the zz curve
#p = cs.collections[0].get_paths()[0]
#v = p.vertices
#xm_zz = v[:,0]
#ym_zz = v[:,1]
#p = cs.collections[1].get_paths()[0]
#v = p.vertices
#xp_zz = v[:,0]
#yp_zz = v[:,1]

# get the paths for the hh curve
#p = cs3.collections[0].get_paths()[0]
#v = p.vertices
#xm_hh = v[:,0]
#ym_hh = v[:,1]
#p = cs3.collections[1].get_paths()[0]
#v = p.vertices
#xp_hh = v[:,0]
#yp_hh = v[:,1]

#L1_zz = line([xm_zz[0],ym_zz[0]], [xm_zz[-1],ym_zz[-1]])
#L2_zz = line([xp_zz[0],yp_zz[0]], [xp_zz[-1],yp_zz[-1]])
#L1_hh = line([xm_hh[0],ym_hh[0]], [xm_hh[-1],ym_hh[-1]])
#L2_hh = line([xp_hh[0],yp_hh[0]], [xp_hh[-1],yp_hh[-1]])

#R11 = intersection(L1_zz, L1_hh)
#R12 = intersection(L1_zz, L2_hh)
#R22 = intersection(L2_zz, L2_hh)
#R21 = intersection(L2_zz, L1_hh)
#print 'intersections:', R11, R12, R22, R21

#print 'limits:'
#print 'lambda122 in [', min(R11[0], R12[0], R22[0], R21[0]), max(R11[0], R12[0], R22[0], R21[0]), ']', 'precision=', 2*(max(R11[0], R12[0], R22[0], R21[0])-min(R11[0], R12[0], R22[0], R21[0]))/(max(R11[0], R12[0], R22[0], R21[0])+min(R11[0], R12[0], R22[0], R21[0]))

#print 'sintheta in [', min(R11[1], R12[1], R22[1], R21[1]), max(R11[1], R12[1], R22[1], R21[1]), ']', 'precision=', 2*(max(R11[1], R12[1], R22[1], R21[1])-min(R11[1], R12[1], R22[1], R21[1]))/(max(R11[1], R12[1], R22[1], R21[1])+min(R11[1], R12[1], R22[1], R21[1]))


manual_locations = [(500, -2000), (1000, -4000)]

strs = ['$0.5\\times$', '$1.5\\times$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s

# true location:
#plt.plot(k112real, k122real, marker='*', ms=3, color='red')
#plt.axhline(k122real, ls='-', lw=0.5)
#plt.axvline(k112real, ls='-', lw=0.5)

# precision on kappa112
prec112 = 0.10
#ax.axvspan((1-prec112)*k112real, (1+prec112)*k112real, alpha=0.4, color='blue')
#ax.axvspan(-(1-prec112)*k112real, -(1+prec112)*k112real, alpha=0.4, color='blue')


# get the limits assuming a certain precision for kappa112
#p = cs.collections[0].get_paths()[0]
#v = p.vertices
#xm = v[:,0]
#ym = v[:,1]
#p = cs.collections[1].get_paths()[0]
#v = p.vertices
#xp = v[:,0]
#yp = v[:,1]
# solve:
# positive
#idxp1 = np.argwhere(np.diff(np.sign(xp - (1-prec112)*k112real))).flatten()
#idxp2 = np.argwhere(np.diff(np.sign(xp - (1+prec112)*k112real))).flatten()
#idxm1 = np.argwhere(np.diff(np.sign(xm - (1-prec112)*k112real))).flatten()
#idxm2 = np.argwhere(np.diff(np.sign(xm - (1+prec112)*k112real))).flatten()
#plt.plot( (1+prec112)*k112real, max(ym[idxm2]), marker='o', color='black', ms=4)
#plt.plot( (1-prec112)*k112real, max(yp[idxp1]), marker='o', color='black', ms=4)
#plt.plot( (1+prec112)*k112real, min(yp[idxp2]), marker='o', color='black', ms=4)
#plt.plot( (1-prec112)*k112real, min(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(max(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(ym[idxm1]), ls='--', lw=0.5, color='green')
#print 'lambda122 in:', '[', max(ym[idxm2]), ',', max(yp[idxp1]), ']U[', min(yp[idxp2]), ',', min(ym[idxm1]), ']'
# negative:
#idxp1 = np.argwhere(np.diff(np.sign(xp + (1-prec112)*k112real))).flatten()
#idxp2 = np.argwhere(np.diff(np.sign(xp + (1+prec112)*k112real))).flatten()
#idxm1 = np.argwhere(np.diff(np.sign(xm + (1-prec112)*k112real))).flatten()
#idxm2 = np.argwhere(np.diff(np.sign(xm + (1+prec112)*k112real))).flatten()
#plt.plot( -(1+prec112)*k112real, min(ym[idxm2]), marker='o', color='black', ms=4)
#plt.plot( -(1-prec112)*k112real, min(yp[idxp1]), marker='o', color='black', ms=4)
#plt.plot( -(1+prec112)*k112real, max(yp[idxp2]), marker='o', color='black', ms=4)
#plt.plot( -(1-prec112)*k112real, max(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(min(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(ym[idxm1]), ls='--', lw=0.5, color='green')

#print 'or lambda122 in:', '[', min(ym[idxm2]), ',', min(yp[idxp1]), ']U[', max(yp[idxp2]), ',', max(ym[idxm1]), ']'

plt.plot( k112real, streal, marker='*', color='red', ms=11)

#ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$\\sin \\theta$', fontsize=20)
ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
#titletext = 'pp@100 TeV/30 ab$^{-1}$,' + str(int(prec_sigma_zz*100.)) + '% error on $ZZ$, ' + str(int(prec_sigma_hh*100.)) + '% error on $h_1 h_1$ '
titletext = 'pp@100 TeV/30 ab$^{-1}$, $10^{-5}$ error on $ZZ$, $10^{-4}$ error on $h_1 h_1$ '
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_xlim(xminp,xmaxp)
ax.set_ylim(yminp,ymaxp)

#ax.xaxis.set_major_locator(MultipleLocator(200))
#ax.xaxis.set_minor_locator(MultipleLocator(50))

#ax.yaxis.set_major_locator(MultipleLocator(200))
#ax.yaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
#ax.legend()
#ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':6})

plot_type = 'xx_sigma_eta0_UCons30_2D_out'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '_' + str(RunNum) + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
