from scipy.optimize import curve_fit
import numpy as np
import subprocess
import os.path
import math
from math import log10, floor
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
import matplotlib.font_manager as fm
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as mpatches
import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import scipy.interpolate
from scipy.interpolate import interp1d


###########################################
# FIT THE h+S cross section
###########################################


# Functions

def round_sig(x, sig=2):
    if x == 0.:
        return 0.
    if math.isnan(x) is True:
        print('Warning, NaN!')
        return 0.
    return round(x, sig-int(floor(log10(abs(x))))-1)

ct = math.sqrt(2)/2. # the costheta used in the fitting
st = math.sqrt(1-ct**2) # the costheta used in the fitting
# function at given values of ct and st 
def func(XY, A, B, C, D, E, F):
    x, y = XY
    return A * x**2 * ct**2 + B * y**2 * st**2 + C * x * y * ct * st + D * y * st**2 * ct + E * x * st * ct**2 + F * ct**2 * st**2

def func_t(XY, A, B, C, D, E, F, stval, ctval):
    x, y = XY
    return A * x**2 * ctval**2 + B * y**2 * stval**2 + C * x * y * ctval * stval + D * y * stval**2 * ctval + E * x * stval * ctval**2 + F * ctval**2 * stval**2

def func_t_plot(x, y, A, B, C, D, E, F, stval, ctval):
    return A * x**2 * ctval**2 + B * y**2 * stval**2 + C * x * y * ctval * stval + D * y * stval**2 * ctval + E * x * stval * ctval**2 + F * ctval**2 * stval**2

def read_files_heta0(X, Y, Z, XSEC, kap112, kap122, ProcLocation):
    for k112 in kap112:
        for k122 in kap122:
            lhe = 'run' + str(RunNum) + '_' + str(k112) + '_' + str(k122) + '/unweighted_events.lhe.gz'
            lhefile = MGLocation + ProcLocation + 'Events/' + lhe
            if os.path.exists(lhefile) is False:
                print('Error, lhe file or summary file:', lhefile, 'does not exist!')
                exit()
            else:
                zgrepcommand = 'zgrep "Integrated weight" ' + lhefile
                p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
                for line in iter(p.stdout.readline, b''):
                    xsec = float(line.split()[5])
                print(k112, k122, xsec)
                X.append(float(k112))
                Y.append(float(k122))
                Z.append(float(xsec))
                XSEC[(float(k112), float(k122))] = float(xsec)
    return X, Y, Z, XSEC

def read_files_hh(X, Y, XSEC, kap112, ProcLocation):
    for k112 in kap112:
        lhe = 'run' + str(RunNum) + '_' + str(k112) + '_' + str(k122) + '/unweighted_events.lhe.gz'
        lhefile = MGLocation + ProcLocation + 'Events/' + lhe
        if os.path.exists(lhefile) is False:
            print('Error, lhe file or summary file:', lhefile, 'does not exist!')
            exit()
        else:
            zgrepcommand = 'zgrep "Integrated weight" ' + lhefile
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
            for line in iter(p.stdout.readline, b''):
                    xsec = float(line.split()[5])
            print(k112, xsec)
            X.append(float(k112))
            Y.append(float(xsec))
            XSEC[float(k112)] = float(xsec)
    return X, Y, XSEC

# read an explicit file and return the cross section
def read_file_single_heta0(runtag, ProcLocation):
    lhe = 'run' + RunNum + '_' + runtag + '/unweighted_events.lhe.gz'
    lhefile = MGLocation + ProcLocation + 'Events/' + lhe
    if os.path.exists(lhefile) is False:
        print('Error, lhe file or summary file:', lhefile, 'does not exist!')
        exit()
    else:
        zgrepcommand = 'zgrep "Integrated weight" ' + lhefile
        p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
        for line in iter(p.stdout.readline, b''):
            xsec = float(line.split()[5])
    return xsec

def generate_mg_input_heta0(runnum, mgloc, procloc, ctchoice, ctreal, kap_array, k112real, k122real, m2, w2, nevents, pointname):
    filename = mgloc + 'gg_heta0_lambdavar_run' + str(runnum) + '.script'
    filestream = open(filename,'w')
    filestream.write('launch ' + procloc + ' -i\n\n')
    for k112 in kap_array:
        for k122 in kap_array:
            filestream.write('launch run' + str(runnum) + '_' + str(int(k112)) + '_' + str(int(k122)) + '\n0\n')
            filestream.write('set Meta ' + str(m2) + '\n')
            filestream.write('set Weta ' + str(w2) + '\n')
            filestream.write('set ctheta ' + str(ctchoice) + '\n')
            filestream.write('set stheta ' + str(math.sqrt(1-ctchoice**2)) + '\n')
            filestream.write('set kap112 ' + str(k112) + '\n')
            filestream.write('set kap122 ' + str(k122) + '\n')
            filestream.write('set nevents ' + str(nevents) + '\n')
            filestream.write('\n')
    filestream.write('launch run' + str(runnum) + '_' + pointname + '\n0\n')
    filestream.write('set Meta ' + str(m2) + '\n')
    filestream.write('set Weta ' + str(w2) + '\n')
    filestream.write('set ctheta ' + str(ctreal) + '\n')
    filestream.write('set stheta ' + str(math.sqrt(1-ctreal**2)) + '\n')
    filestream.write('set kap112 ' + str(k112real) + '\n')
    filestream.write('set kap122 ' + str(k122real) + '\n')
    filestream.write('set nevents ' + str(nevents) + '\n')
    filestream.write('\n')

def generate_mg_input_hh(runnum, mgloc, procloc, ctchoice, ctreal, kap_array, k112real, m2, w2, nevents, pointname):
    filename = mgloc + 'gg_eta0_hh_lambdavar_run' + str(runnum) + '.script'
    filestream = open(filename,'w')
    filestream.write('launch ' + procloc + ' -i\n\n')
    for k112 in kap_array:
        filestream.write('launch run' + str(runnum) + '_' + str(int(k112)) + '\n0\n')
        filestream.write('set Meta ' + str(m2) + '\n')
        filestream.write('set Weta ' + str(w2) + '\n')
        filestream.write('set ctheta ' + str(ctchoice) + '\n')
        filestream.write('set stheta ' + str(math.sqrt(1-ctchoice**2)) + '\n')
        filestream.write('set kap112 ' + str(k112) + '\n')
        filestream.write('set nevents ' + str(nevents) + '\n')
        filestream.write('\n')
    filestream.write('launch run' + str(runnum) + '_' + pointname + '\n0\n')
    filestream.write('set Meta ' + str(m2) + '\n')
    filestream.write('set Weta ' + str(w2) + '\n')
    filestream.write('set ctheta ' + str(ctreal) + '\n')
    filestream.write('set stheta ' + str(math.sqrt(1-ctreal**2)) + '\n')
    filestream.write('set kap112 ' + str(k112real) + '\n')
    filestream.write('set nevents ' + str(nevents) + '\n')
    filestream.write('\n')
    
###########################################



print('Fitting the h+S cross section\n')

# MG5/aMC sub-dir:
MGLocation = '/Users/apapaefs/Documents/Projects/MadGraph/MG5_aMC_v2_7_3/'

# Process sub-dir:
ProcLocation_heta0 = 'gg_heta0/'
ProcLocation_hh = 'gg_hh/'


# run number
RunNum = '3'

# generate the madgraph array:
karray_full = [0, 100, 200, 300, 400, 500, 600]
karray = karray_full[:-1] # reduced array used for fitting, last element not used
ctreal = 0.990457406445 # the true value of costheta for this point
streal = math.sqrt(1-ctreal**2)
k112real = 100.36 # true value of k112
k122real = 502.56 # true value of k122
pointname = 'UCons30'
m2 = 466.29 
w2 = 1.6863 
nevents = 10000
generate_mg_input_heta0(RunNum, MGLocation, ProcLocation_heta0, ct, ctreal, karray_full, k112real, k122real, m2, w2, nevents, pointname)
generate_mg_input_hh(RunNum, MGLocation, ProcLocation_hh, ct, ctreal, karray_full, k112real, m2, w2, nevents, pointname)

# print info
print('parameter space point and run attributes:')
print('m2, w2=', m2, w2)
print('nevents=', nevents)
print('fitting costheta choice=', ct)
print('scanning over:', karray_full)
print('fitting over:', karray)

# read the files
print('reading in files')
X = []
Y = []
Z = []
XSEC = {}
X, Y, Z, XSEC = read_files_heta0(X, Y, Z, XSEC, karray, karray, ProcLocation_heta0)
print('\n')

# do the fit and print parameters

# read in variations of the couplings:
kap112 = [str(x) for x in karray]
kap122 = kap112
print('doing the fit')
popt, pcov = curve_fit(func, (X,Y), Z)
print('parameters=', popt)
A, B, C, D, E, F = popt

# do a check:
kap112_check = [100]
kap122_check = [600]
X, Y, Z, XSEC = read_files_heta0(X, Y, Z, XSEC, kap112_check, kap122_check, ProcLocation_heta0)

# print cross-check
print('h+eta0 cross check:', kap112_check[0], kap122_check[0], func((kap112_check[0], kap122_check[0]), *popt), 'vs', XSEC[(kap112_check[0],kap122_check[0])], 'error = ', round_sig(100. * abs(func((kap112_check[0],kap122_check[0]), *popt) - XSEC[(kap112_check[0],kap122_check[0])])/XSEC[(kap112_check[0],kap122_check[0])],2), '%')

################### CROSS CHECK THE FULL SINTHETA/COSTHETA DEPENDENCE #######################

xsec_paramspace = {}
xsec_paramspace['UCons30'] = read_file_single_heta0('UCons30', ProcLocation_heta0)
print('h+eta0 cross check UCons, real vs. fitted', xsec_paramspace['UCons30'], func_t((k112real,k122real), A, B, C, D, E, F, streal, ctreal))

################### CONTOUR PLOT STARTS HERE #################################################

#####################    
# plot zoomed out    #
#####################

outputdirectory = './hSstudy/'

gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)
ymin = -5000.
ymax = 5000.
xmin = -1000
xmax = 1200.

yminp = -4500.
ymaxp = 4500.
xminp = -1000
xmaxp = 1200.

xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
x1, y1 = np.meshgrid(xi,yi)
#zi = func_t_plot(x1, y1, A, B, C, D, E, F, streal, ctreal)
zi = func_t_plot(x1, y1, A, B, C, D, E, F, streal, ctreal)


#print sigma
prec_sigmahs = 0.25
#zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
cs = plt.contour(x1, y1, zi, levels=[xsec_paramspace['UCons30']*(1-prec_sigmahs), xsec_paramspace['UCons30']*(1+prec_sigmahs)], extend='both', colors='k')
cs2 = plt.contourf(x1, y1, zi, levels=[xsec_paramspace['UCons30']*(1-prec_sigmahs), xsec_paramspace['UCons30']*(1+prec_sigmahs)], cmap='inferno', alpha=0.4)



manual_locations = [(500, -2000), (1000, -4000)]

strs = ['$0.5\\times$', '$1.5\\times$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s

# true location:
#plt.plot(k112real, k122real, marker='*', ms=3, color='red')
#plt.axhline(k122real, ls='-', lw=0.5)
#plt.axvline(k112real, ls='-', lw=0.5)

# precision on kappa112
prec112 = 0.10
#ax.axvspan((1-prec112)*k112real, (1+prec112)*k112real, alpha=0.4, color='blue')
#ax.axvspan(-(1-prec112)*k112real, -(1+prec112)*k112real, alpha=0.4, color='blue')


# get the limits assuming a certain precision for kappa112
p = cs.collections[0].get_paths()[0]
v = p.vertices
xm = v[:,0]
ym = v[:,1]
p = cs.collections[1].get_paths()[0]
v = p.vertices
xp = v[:,0]
yp = v[:,1]
# solve:
# positive
#idxp1 = np.argwhere(np.diff(np.sign(xp - (1-prec112)*k112real))).flatten()
#idxp2 = np.argwhere(np.diff(np.sign(xp - (1+prec112)*k112real))).flatten()
#idxm1 = np.argwhere(np.diff(np.sign(xm - (1-prec112)*k112real))).flatten()
#idxm2 = np.argwhere(np.diff(np.sign(xm - (1+prec112)*k112real))).flatten()
#plt.plot( (1+prec112)*k112real, max(ym[idxm2]), marker='o', color='black', ms=4)
#plt.plot( (1-prec112)*k112real, max(yp[idxp1]), marker='o', color='black', ms=4)
#plt.plot( (1+prec112)*k112real, min(yp[idxp2]), marker='o', color='black', ms=4)
#plt.plot( (1-prec112)*k112real, min(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(max(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(ym[idxm1]), ls='--', lw=0.5, color='green')
#print 'lambda122 in:', '[', max(ym[idxm2]), ',', max(yp[idxp1]), ']U[', min(yp[idxp2]), ',', min(ym[idxm1]), ']'
# negative:
#idxp1 = np.argwhere(np.diff(np.sign(xp + (1-prec112)*k112real))).flatten()
#idxp2 = np.argwhere(np.diff(np.sign(xp + (1+prec112)*k112real))).flatten()
#idxm1 = np.argwhere(np.diff(np.sign(xm + (1-prec112)*k112real))).flatten()
#idxm2 = np.argwhere(np.diff(np.sign(xm + (1+prec112)*k112real))).flatten()
#plt.plot( -(1+prec112)*k112real, min(ym[idxm2]), marker='o', color='black', ms=4)
#plt.plot( -(1-prec112)*k112real, min(yp[idxp1]), marker='o', color='black', ms=4)
#plt.plot( -(1+prec112)*k112real, max(yp[idxp2]), marker='o', color='black', ms=4)
#plt.plot( -(1-prec112)*k112real, max(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(min(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(ym[idxm1]), ls='--', lw=0.5, color='green')
#print 'or lambda122 in:', '[', min(ym[idxm2]), ',', min(yp[idxp1]), ']U[', max(yp[idxp2]), ',', max(ym[idxm1]), ']'

plt.plot( k112real, k122real, marker='*', color='blue', ms=9)


#ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$\\lambda_{122}$ [GeV]', fontsize=20)
ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
titletext = '$\\sigma(h \\eta_0 )_\\mathrm{UCons30}$ @ 100 TeV, ' + str(int(prec_sigmahs*100.)) + '% error'
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_xlim(xminp,xmaxp)
ax.set_ylim(yminp,ymaxp)

#ax.xaxis.set_major_locator(MultipleLocator(200))
#ax.xaxis.set_minor_locator(MultipleLocator(50))

#ax.yaxis.set_major_locator(MultipleLocator(200))
#ax.yaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
#ax.legend()
#ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':6})

plot_type = 'heta0_sigma_heta0_UCons30_out'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '_' + str(RunNum) + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)



#####################    
# plot zoomed in    #
#####################

gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)
ymin = -8000.
ymax = 8000.
xmin = -2000
xmax = 2000.

yminp = -4200.
ymaxp = 4000.
xminp = -850
xmaxp = 1100

xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
x1, y1 = np.meshgrid(xi,yi)
#zi = func_t_plot(x1, y1, A, B, C, D, E, F, streal, ctreal)
zi = func_t_plot(x1, y1, A, B, C, D, E, F, streal, ctreal)

#print sigma
prec_sigmahs = 0.33
#zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
cs = plt.contour(x1, y1, zi, levels=[xsec_paramspace['UCons30']*(1-prec_sigmahs), xsec_paramspace['UCons30']*(1+prec_sigmahs)], extend='both', colors='k', antialiased=True)
cs2 = plt.contourf(x1, y1, zi, levels=[xsec_paramspace['UCons30']*(1-prec_sigmahs), xsec_paramspace['UCons30']*(1+prec_sigmahs)], cmap='inferno', alpha=0.4, antialiased=True)

manual_locations = [(500, -2000), (1000, -4000)]

strs = ['$0.5\\times$', '$1.5\\times$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s

# true location:
#plt.plot(k112real, k122real, marker='*', ms=3, color='red')
#plt.axhline(k122real, ls='-', lw=0.5)
#plt.axvline(k112real, ls='-', lw=0.5)

# precision on kappa112
#prec112 = 0.00014731811682811672
prec122 = 0.1
ax.axvspan((1-prec112)*k112real, (1+prec112)*k112real, alpha=0.4, color='blue')
ax.axvspan(-(1-prec112)*k112real, -(1+prec112)*k112real, alpha=0.4, color='blue')


# get the limits assuming a certain precision for kappa112
p = cs.collections[0].get_paths()[0]
v = p.vertices
xm = v[:,0]
ym = v[:,1]
p = cs.collections[1].get_paths()[0]
v = p.vertices
xp = v[:,0]
yp = v[:,1]

print(len(xp))
# solve:
# positive
idxp1 = np.argwhere(np.diff(np.sign(xp - (1-prec112)*k112real))).flatten()
idxp2 = np.argwhere(np.diff(np.sign(xp - (1+prec112)*k112real))).flatten()
idxm1 = np.argwhere(np.diff(np.sign(xm - (1-prec112)*k112real))).flatten()
idxm2 = np.argwhere(np.diff(np.sign(xm - (1+prec112)*k112real))).flatten()
print(idxp1, idxp2, idxm1, idxm2)
plt.plot( (1+prec112)*k112real, max(ym[idxm2]), marker='o', color='black', ms=4)
plt.plot( (1-prec112)*k112real, max(yp[idxp1]), marker='o', color='black', ms=4)
plt.plot( (1+prec112)*k112real, min(yp[idxp2]), marker='o', color='black', ms=4)
plt.plot( (1-prec112)*k112real, min(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(max(ym[idxm2]), ls='--', lw=0.5, color='green')
#plt.axhline(max(yp[idxp1]), ls='--', lw=0.5, color='green')
#plt.axhline(min(yp[idxp2]), ls='--', lw=0.5, color='green')
#plt.axhline(min(ym[idxm1]), ls='--', lw=0.5, color='green')
print(max(ym[idxm2]), max(yp[idxp1]), min(yp[idxp2]), min(ym[idxm1]))
print(ym[idxm2], yp[idxp1], yp[idxp2], ym[idxm1])

print('lambda122 in:', '[', max(ym[idxm2]), ',', max(yp[idxp1]), ']U[', min(yp[idxp2]), ',', min(ym[idxm1]), ']')
# negative:
idxp1 = np.argwhere(np.diff(np.sign(xp + (1-prec112)*k112real))).flatten()
idxp2 = np.argwhere(np.diff(np.sign(xp + (1+prec112)*k112real))).flatten()
idxm1 = np.argwhere(np.diff(np.sign(xm + (1-prec112)*k112real))).flatten()
idxm2 = np.argwhere(np.diff(np.sign(xm + (1+prec112)*k112real))).flatten()
plt.plot( -(1+prec112)*k112real, min(ym[idxm2]), marker='o', color='black', ms=4)
plt.plot( -(1-prec112)*k112real, min(yp[idxp1]), marker='o', color='black', ms=4)
plt.plot( -(1+prec112)*k112real, max(yp[idxp2]), marker='o', color='black', ms=4)
plt.plot( -(1-prec112)*k112real, max(ym[idxm1]), marker='o', color='black', ms=4)
#plt.axhline(min(ym[idxm2]), ls='--', lw=0.5, color='blue')
#plt.axhline(min(yp[idxp1]), ls='--', lw=0.5, color='blue')
#plt.axhline(max(yp[idxp2]), ls='--', lw=0.5, color='blue')
#plt.axhline(max(ym[idxm1]), ls='--', lw=0.5, color='blue')
print('or lambda122 in:', '[',min(yp[idxp1]) , ',', min(ym[idxm2]), ']U[', max(ym[idxm1]), ',', max(yp[idxp2]), ']')

plt.plot( k112real, k122real, marker='*', color='red', ms=7)


#ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$\\lambda_{122}$ [GeV]', fontsize=20)
ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
#titletext = '$\\sigma(h \\eta_0 )_\\mathrm{UCons30}$ @ 100 TeV, ' + str(int(prec_sigmahs*100.)) + '% error (+' + str(int(prec112*10000.))+ '$\\times 10^{-4}$ error on $\\lambda_{112}$)'
titletext = '$\\sigma( pp \\rightarrow h_1 h_2  )_\\mathrm{UCons30}$ @ 100 TeV, ' + str(int(prec_sigmahs*100.)) + '% error (+' + str(int(prec112*100.))+ '% error on $\\lambda_{112}$)'
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_xlim(xminp,xmaxp)
ax.set_ylim(yminp,ymaxp)

#ax.xaxis.set_major_locator(MultipleLocator(200))
#ax.xaxis.set_minor_locator(MultipleLocator(50))

#ax.yaxis.set_major_locator(MultipleLocator(200))
#ax.yaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
#ax.legend()
#ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':6})

plot_type = 'heta0_sigma_UCons30'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '_' + str(RunNum) + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
