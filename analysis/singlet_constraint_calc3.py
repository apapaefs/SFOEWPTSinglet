--- singlet_constraint_calc.py	(original)
+++ singlet_constraint_calc.py	(refactored)
@@ -85,7 +85,7 @@
     MRMBenchmarkPoints["MRMB9min"] = [ 0.990, 0.138, 729.0, 4.22,  27.3, 0.21, -909., 6.15, 603., 0.93 ]
     MRMBenchmarkPoints["MRMB10min"] = [ 0.995, 0.104, 792.0, 3.36, 22.2, 0.18, -936., 9.47, -848., 0.66 ]
     MRMBenchmarkPoints["MRMB11min"] = [ 0.994, 0.105, 841.0, 3.95, 21.2, 0.19, -955., 8.69, 684., 0.53 ]
-    for key in MRMBenchmarkPoints.keys():
+    for key in list(MRMBenchmarkPoints.keys()):
         OurBenchmarkPoints[key] = ConvertMRMBenchmarkToGWAPBenchmark(MRMBenchmarkPoints[key])
 
 
@@ -326,12 +326,12 @@
     SIGNIF27_Current_ZZ_lumi_scatter[Lumi] = {}
     SIGNIF27_Current_MAX_lumi_scatter[Lumi] = {}
 
-for key in OurBenchmarkPoints.keys():
+for key in list(OurBenchmarkPoints.keys()):
     BenchmarkPoints[key] = ConvertBenchmarks(OurBenchmarkPoints[key])
     #print key, BenchmarkPoints[key]
     # for plotting the mh1:
     TypeTag = ''.join([i for i in key if not i.isdigit()])
-    if TypeTag not in OneLoop_mh1.keys():
+    if TypeTag not in list(OneLoop_mh1.keys()):
         SIGN_SINTHETA_scatter[TypeTag] = []
         OneLoop_mh1[TypeTag] = []
         OneLoop_mh2[TypeTag] = []
@@ -458,7 +458,7 @@
 writemg5nevents = '1000'
 mg5print = open(writemg5file, 'w')
 if writemg5gen is True:
-    print 'Writing the mg5 runcard:', writemg5file ,'for the process:', writemg5process, 
+    print('Writing the mg5 runcard:', writemg5file ,'for the process:', writemg5process, end=' ') 
     mg5print.write('launch ' + writemg5process + ' -i\n\n')
 
 ###################################################
@@ -481,15 +481,15 @@
 #######################################################
 
 
-print 'Constraint calculator for SM+real singlet scalar'
-print '---'
+print('Constraint calculator for SM+real singlet scalar')
+print('---')
 
 
 
 
 # remove points outside the region of interest for mh2
 removed_point = 0
-for key in BenchmarkPoints.keys():
+for key in list(BenchmarkPoints.keys()):
     #print "benchmark point:", key
     rho_max = OurBenchmarkPoints[key][-3]
     rho_av = OurBenchmarkPoints[key][-2]
@@ -733,7 +733,7 @@
     printall = False
     #if (printall is True or key in BenchmarkSelection) and abs(l112) > 150. and abs(sintheta) < 0.06:
     if printall is True or key in BenchmarkSelection:
-        print "benchmark point:", key
+        print("benchmark point:", key)
         print_SARAH_params_and_masses(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, round_sig(mh1_1loop,5), round_sig(mh2_1loop,5))
         print_XSECS_AND_BRS(xs13_n3lo, xs27_n3lon3ll, xs100_n3lon3ll, BR_ZZ, BR_WW, BR_hh)
         print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy_withtripleHiggs, 'Heavy Higgs BRs & width')
@@ -753,7 +753,7 @@
         LumiFit = 1000.
         if do_invert_fit is True:
             Mfit, DMfit = get_m4l_fit(mh2, LumiFit, sintheta, l112, BR_interpolators_SM)
-            print 'mh1, mh2, sintheta, l112, signif100_ZZ_lumi[30000.], signif100_HH_lumi[30000.], key=', mh1, mh2, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key
+            print('mh1, mh2, sintheta, l112, signif100_ZZ_lumi[30000.], signif100_HH_lumi[30000.], key=', mh1, mh2, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key)
             if mh2 > 250. and signif100_ZZ_lumi[30000.] > 5. and signif100_HH_lumi[30000.] > 5.:
                 l112_min_fit, l112_max_fit, sintheta_min_fit, sintheta_max_fit = get_sintheta_l112_fit_fixedmass(mh1, mh2, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key)
                 l112_min_fit_errormass, l112_max_fit_errormass, sintheta_min_fit_errormass, sintheta_max_fit_errormass = get_sintheta_l112_fit_errormass(mh1, mh2, DMfit, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key)
@@ -768,45 +768,45 @@
         
         # read the h+eta0 cross section:
         sigma_heta0_real = read_file_single_heta0_norun(key, ProcLocation_heta0)
-        print 'mh1_tree_MRM, mh2_tree_MRM=', mh1_tree_MRM, mh2_tree_MRM
-        print 'tree-level fine tuning (m1)=', ft, '(m2)=', ft2, '(dekens,mu2,b2)=', ft3, 'delta_mh1_mzvar=',delta_mh1_mzvar
-        print 'one-loop fine tuning', ft_mh1_oneloop, ft_mh2_oneloop
-        print "rho_max/av/std=", rho_max, rho_av, rho_std
-        print "At One Loop (Sc=MZ): mh1, mh2=", mh1_1loop, mh2_1loop
-        print "At One Loop (Sc=MH): mh1, mh2=", mh1_1loop_atMH, mh2_1loop_atMH
-        print "At One Loop (Sc=MT): mh1, mh2=", mh1_1loop_atMt, mh2_1loop_atMt
-        print "At One Loop (Sc=v0): mh1, mh2=", mh1_1loop_atv0, mh2_1loop_atv0
+        print('mh1_tree_MRM, mh2_tree_MRM=', mh1_tree_MRM, mh2_tree_MRM)
+        print('tree-level fine tuning (m1)=', ft, '(m2)=', ft2, '(dekens,mu2,b2)=', ft3, 'delta_mh1_mzvar=',delta_mh1_mzvar)
+        print('one-loop fine tuning', ft_mh1_oneloop, ft_mh2_oneloop)
+        print("rho_max/av/std=", rho_max, rho_av, rho_std)
+        print("At One Loop (Sc=MZ): mh1, mh2=", mh1_1loop, mh2_1loop)
+        print("At One Loop (Sc=MH): mh1, mh2=", mh1_1loop_atMH, mh2_1loop_atMH)
+        print("At One Loop (Sc=MT): mh1, mh2=", mh1_1loop_atMt, mh2_1loop_atMt)
+        print("At One Loop (Sc=v0): mh1, mh2=", mh1_1loop_atv0, mh2_1loop_atv0)
         # print xs and xs constraints
-        print 'xs100_n3lon3ll, xs100_vbf_nlo', xs100_n3lon3ll, xs100_vbf_nlo
-        print 'xs13_HH, limit_ATLAS_HH_central=',xs13_HH, limit_ATLAS_HH_central
-        print 'xs13_HH, limit_CMS_HH_central=',xs13_HH, limit_CMS_HH_central
-        print 'xs13_ZZ, limit_HLLHC_ZZ_central=',xs13_ZZ, limit_HLLHC_ZZ_central
-        print 'xs13_WW, limit_HLLHC_WW_central=',xs13_WW, limit_HLLHC_WW_central
-        print 'xs13_ZZ, limit_CMS_ZZ_extrap=',xs13_ZZ, limit_CMS_ZZ_extrap
-        print 'xs13_WW, limit_ATLAS_WW_extrap=',xs13_WW, limit_ATLAS_WW_extrap
-        print 'xs100_WW, limit_FCC_WW_central, ratio=', xs100_WW, limit_FCC_WW_central, xs100_WW/limit_FCC_WW_central
-        print 'xs100_ZZ, limit_FCC_ZZ_central, ratio=', xs100_ZZ, limit_FCC_ZZ_central, xs100_ZZ/limit_FCC_ZZ_central
-        print 'xs100_HH, limit_FCC_HH_central, ratio=', xs100_HH, limit_FCC_HH_central, xs100_HH/limit_FCC_HH_central
-        print 'xs27_WW, limit_HELHC_WW_GGF_central, ratio=', xs27_WW, limit_HELHC_WW_GGF_central, xs27_WW/limit_HELHC_WW_GGF_central
-        print 'xs27_ZZ, limit_HELHC_ZZ_GGF_central, ratio=', xs27_ZZ, limit_HELHC_ZZ_GGF_central, xs27_ZZ/limit_HELHC_ZZ_GGF_central
-        print 'xs27_HH, limit_HELHC_HH_GGF_central, ratio=', xs27_HH, limit_HELHC_HH_GGF_central,  xs27_HH/limit_HELHC_HH_GGF_central
-        print 'xs27_WW, limit_HELHC_WW_QQ_central, ratio=', xs27_WW, limit_HELHC_WW_QQ_central, xs27_WW/limit_HELHC_WW_QQ_central
-        print 'xs27_ZZ, limit_HELHC_ZZ_QQ_central, ratio=', xs27_ZZ, limit_HELHC_ZZ_QQ_central, xs27_ZZ/limit_HELHC_ZZ_QQ_central
-        print 'xs27_HH, limit_HELHC_HH_QQ_central, ratio=', xs27_HH, limit_HELHC_HH_QQ_central,  xs27_HH/limit_HELHC_HH_QQ_central
-        print 'xs27/xs100=', xs27_HH/xs100_HH
-        print 'signif100_HH_lumi[Lumi]=', signif100_HH_lumi[Lumi]
-        print 'signif100_ZZ_lumi[Lumi]=', signif100_ZZ_lumi[Lumi]
-        print 'signif100_WW_lumi[Lumi]=', signif100_WW_lumi[Lumi]
-        print 'signif100_MAX_lumi[Lumi]=', signif100_MAX_lumi[Lumi]
-        print 'l111, l112, l122, l1112=', l111, l112, l122, l1112
-        print 'Mass fit=', Mfit, DMfit
-        print 'l112_prec, sintheta_prec=', l112_fit_prec, sintheta_fit_prec
+        print('xs100_n3lon3ll, xs100_vbf_nlo', xs100_n3lon3ll, xs100_vbf_nlo)
+        print('xs13_HH, limit_ATLAS_HH_central=',xs13_HH, limit_ATLAS_HH_central)
+        print('xs13_HH, limit_CMS_HH_central=',xs13_HH, limit_CMS_HH_central)
+        print('xs13_ZZ, limit_HLLHC_ZZ_central=',xs13_ZZ, limit_HLLHC_ZZ_central)
+        print('xs13_WW, limit_HLLHC_WW_central=',xs13_WW, limit_HLLHC_WW_central)
+        print('xs13_ZZ, limit_CMS_ZZ_extrap=',xs13_ZZ, limit_CMS_ZZ_extrap)
+        print('xs13_WW, limit_ATLAS_WW_extrap=',xs13_WW, limit_ATLAS_WW_extrap)
+        print('xs100_WW, limit_FCC_WW_central, ratio=', xs100_WW, limit_FCC_WW_central, xs100_WW/limit_FCC_WW_central)
+        print('xs100_ZZ, limit_FCC_ZZ_central, ratio=', xs100_ZZ, limit_FCC_ZZ_central, xs100_ZZ/limit_FCC_ZZ_central)
+        print('xs100_HH, limit_FCC_HH_central, ratio=', xs100_HH, limit_FCC_HH_central, xs100_HH/limit_FCC_HH_central)
+        print('xs27_WW, limit_HELHC_WW_GGF_central, ratio=', xs27_WW, limit_HELHC_WW_GGF_central, xs27_WW/limit_HELHC_WW_GGF_central)
+        print('xs27_ZZ, limit_HELHC_ZZ_GGF_central, ratio=', xs27_ZZ, limit_HELHC_ZZ_GGF_central, xs27_ZZ/limit_HELHC_ZZ_GGF_central)
+        print('xs27_HH, limit_HELHC_HH_GGF_central, ratio=', xs27_HH, limit_HELHC_HH_GGF_central,  xs27_HH/limit_HELHC_HH_GGF_central)
+        print('xs27_WW, limit_HELHC_WW_QQ_central, ratio=', xs27_WW, limit_HELHC_WW_QQ_central, xs27_WW/limit_HELHC_WW_QQ_central)
+        print('xs27_ZZ, limit_HELHC_ZZ_QQ_central, ratio=', xs27_ZZ, limit_HELHC_ZZ_QQ_central, xs27_ZZ/limit_HELHC_ZZ_QQ_central)
+        print('xs27_HH, limit_HELHC_HH_QQ_central, ratio=', xs27_HH, limit_HELHC_HH_QQ_central,  xs27_HH/limit_HELHC_HH_QQ_central)
+        print('xs27/xs100=', xs27_HH/xs100_HH)
+        print('signif100_HH_lumi[Lumi]=', signif100_HH_lumi[Lumi])
+        print('signif100_ZZ_lumi[Lumi]=', signif100_ZZ_lumi[Lumi])
+        print('signif100_WW_lumi[Lumi]=', signif100_WW_lumi[Lumi])
+        print('signif100_MAX_lumi[Lumi]=', signif100_MAX_lumi[Lumi])
+        print('l111, l112, l122, l1112=', l111, l112, l122, l1112)
+        print('Mass fit=', Mfit, DMfit)
+        print('l112_prec, sintheta_prec=', l112_fit_prec, sintheta_fit_prec)
         #print 'l112_min_fit, l112_max_fit, sintheta_min_fit, sintheta_max_fit=', l112_min_fit_errormass, l112_max_fit_errormass, sintheta_min_fit_errormass, sintheta_max_fit_errormass
-        print 'g g > h eta0 [QCD] from mg5 =', sigma_heta0_real
-        print 'g g > h eta0 [QCD] from fit =', sigma_heta0_fit
+        print('g g > h eta0 [QCD] from mg5 =', sigma_heta0_real)
+        print('g g > h eta0 [QCD] from fit =', sigma_heta0_fit)
         sigma_heta0_hZZ_fit = sigma_heta0_fit * BR_ZZ 
-        print 'g g > h (eta0 > ZZ) from fit =', sigma_heta0_hZZ_fit
-        print '\n'
+        print('g g > h (eta0 > ZZ) from fit =', sigma_heta0_hZZ_fit)
+        print('\n')
         if sigma_heta0_hZZ_fit_max < sigma_heta0_hZZ_fit:
             sigma_heta0_hZZ_fit_max = sigma_heta0_hZZ_fit
             sigma_heta0_hZZ_fit_max_key = key
@@ -860,7 +860,7 @@
             mg5print.write('set nevents ' + str(writemg5nevents) + '\n\n')
 
         #print 'l112, sintheta fits, fixed mass=', l112_min_fit, l112_max_fit, sintheta_min_fit, sintheta_max_fit
-        print 'l112, sintheta fit precisions=', l112_fit_prec, sintheta_fit_prec
+        print('l112, sintheta fit precisions=', l112_fit_prec, sintheta_fit_prec)
 
         
     # calculate the significance for this point at 100 TeV/30 inv.ab
@@ -1023,7 +1023,7 @@
     
     # debug signif100_MAX
     if signif100_MAX == 0.0:
-        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! signif100_MAX is ZERO'
+        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! signif100_MAX is ZERO')
     
     #HLLHC_Constraints[TypeTag].append()
     # use the information to write out the param_card.dat
@@ -1080,7 +1080,7 @@
 
 
 # print the maximum h eta0 > h Z Z cross section:
-print 'maximum g g > h eta0 [QCD] > h Z Z cross section=', sigma_heta0_hZZ_fit_max, 'for key', sigma_heta0_hZZ_fit_max_key
+print('maximum g g > h eta0 [QCD] > h Z Z cross section=', sigma_heta0_hZZ_fit_max, 'for key', sigma_heta0_hZZ_fit_max_key)
         
 mg5print.close()
 bprint.close()
@@ -1159,7 +1159,7 @@
         Current_Constraint_pass = 0
         if float(EWPOcurr) < 6.18 and float(ATLAS_HH) == 1 and float(CMS_HH) == 1 and float(HB) == 1 and float(HS) > 0.95 and float(couplstr_cur) ==1:
             Constraint_pass = marker_map['CURRENT']
-            print name, 'passed current constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, couplstr_cur, mh1, mh2
+            print(name, 'passed current constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, couplstr_cur, mh1, mh2)
             if float(mh2) < 1000. and float(mh2) > 200.:
                 CLIC_VV_scatter[TypeTag].append(float(CLIC_VV_pass))
                 CLIC14_HH_scatter[TypeTag].append(float(CLIC14_HH_pass))
@@ -1200,14 +1200,14 @@
                         SIGNIF27_WW_lumi_scatter[Lumi][TypeTag].append(signif27_WW_L[Lumi])
                         SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag].append(signif27_ZZ_L[Lumi])
                         SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag].append(signif27_MAX_L[Lumi])
-                print name, '\talso passed HL-LHC constraints', HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap
+                print(name, '\talso passed HL-LHC constraints', HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap)
                 if float(FCC_WW) == 1 and float(FCC_ZZ) == 1 and float(FCC_HH) == 1 and float(couplstr_fut) == 1:
-                    print name, '\t\talso passed FCC (ZZ+WW+HH) constraints', FCC_WW, FCC_ZZ, FCC_HH, couplstr_fut
+                    print(name, '\t\talso passed FCC (ZZ+WW+HH) constraints', FCC_WW, FCC_ZZ, FCC_HH, couplstr_fut)
                     xsm_point_info_FCCpass.append(xsm_point_info[p])
                     xsm_point_constraints_FCCpass.append(xsm_point_constraints[p])
         else:
             Constraint_pass = marker_map['EXCLUDED']
-            print name, 'did not pass HL-LHC constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap
+            print(name, 'did not pass HL-LHC constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap)
         
         Constraints[TypeTag].append(Constraint_pass)
 
@@ -1221,14 +1221,14 @@
 #print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy, 'Heavy Higgs BRs & width')
 #print '\n'
 
-print 'Removed points because they were outside the region of interest for mh2=',removed_point
+print('Removed points because they were outside the region of interest for mh2=',removed_point)
 
 # print info for points that pass FCC constraints:
-print '\nPoints that passed the FCC constraints:'
+print('\nPoints that passed the FCC constraints:')
 print_point_results(HB_results, HS_results, xsm_point_info_FCCpass, xsm_point_constraints_FCCpass)
 
 
-print 'FINISHED PROCESSING'
+print('FINISHED PROCESSING')
 
 ########################
 # PICKLING STARTS HERE #
@@ -1337,5 +1337,5 @@
     exit()
 
 
-execfile('singlet_constraint_plots.py')
-
+exec(compile(open('singlet_constraint_plots.py', "rb").read(), 'singlet_constraint_plots.py', 'exec'))
+
