#! /usr/bin/env python


import cmath
import math
import numpy as np
from numpy import linalg as LA

from singlet_oneloop_massmatrix import oneloop_massmatrix
from singlet_oneloop_massmatrix_scale import oneloop_massmatrix_scale

from singlet_EWPO import *

# function to convert the MRM parameters to the SARAH ones:
def param_MRM_to_SARAH(lam, v0, x0, a1, a2, b3, b4): 
    musqr_MRM = lam * v0**2 + (a1 + a2 * x0) * x0/2. # this parameter is constrained by the others, see https://arxiv.org/pdf/1605.06123.pdf eq. 2
    mu2 = - musqr_MRM
    b2 = -b3 * x0 - b4 * x0**2 - a1 * v0**2 / 4. / x0 - a2 * v0**2 /2. # this parameter is constrained by the others, see https://arxiv.org/pdf/1605.06123.pdf eq. 3
    MS = b2
    K1 = 0.5 * a1
    K2 = a2
    Lambda = 2. * lam
    LambdaS = 0.5 * b4
    Kappa = b3
    return v0, x0, mu2, MS, K1, K2, Kappa, LambdaS, Lambda

# function to convert the MRM parameters to the SARAH ones:
# param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)
def param_SARAH_to_MRM(v0, x0, mu2, MS, K1, K2, Kappa, LambdaS, Lambda): 
    b2 = MS
    a1 = 2*K1
    mu2_MRM = -mu2
    a2 = K2
    b3 = Kappa
    b4 = 2 * LambdaS
    lam = 0.5 * Lambda
    return lam, v0, x0, a1, a2, b2, b3, b4

# get the treel-level masses given the MRM parameters [eigenvalues from Mathematica]
def TreeLevel_masses(lam, v0, x0, a1, a2, b3, b4):
    # convert the MRM parameters to the SARAH ones:
    v0, x, mu2, MS, K1, K2, Kappa, LambdaS, Lambda = param_MRM_to_SARAH(lam, v0, x0, a1, a2, b3, b4)
    term1 = -2*MS  - 2*mu2  - 2*K1 *x - K2 *x**2 - 12*LambdaS *x**2 - 4*x*Kappa  - K2 *v0**2 - 3*Lambda *v0**2
    term2 = math.sqrt((2*MS  + 2*mu2  + 2*K1 *x + K2 *x**2 + 12*LambdaS *x**2 + 4*x*Kappa  + K2 *v0**2 + 3*Lambda *v0**2)**2 - 4*(4*MS *mu2  + 4*K1 *MS *x + 2*K2 *MS *x**2 + 24*LambdaS *mu2 *x**2 + 24*K1 *LambdaS *x**3 + 12*K2 *LambdaS *x**4 + 8*mu2 *x*Kappa  + 8*K1 *x**2*Kappa  + 4*K2 *x**3*Kappa  - 4*K1 **2*v0**2 + 2*K2 *mu2 *v0**2 - 6*K1 *K2 *x*v0**2 - 3*K2 **2*x**2*v0**2 + 6*MS *Lambda *v0**2 + 36*LambdaS *x**2*Lambda *v0**2 + 12*x*Kappa *Lambda *v0**2 + 3*K2 *Lambda *v0**4))
    # the eigenvalues of the mass matrix:
    TEigenValue1 = ( term1 - term2 )/4.
    TEigenValue2 = ( term1 + term2 )/4.
    return round_sig(math.sqrt(-TEigenValue1),5), round_sig(math.sqrt(-TEigenValue2),5)


# get the tree-level masses given the MRM parameters [eigenvalues calculated from double-derivative matrix]
def TreeLevel_masses_diag(lam, v0, x0, a1, a2, b3, b4): 
    # convert the MRM parameters to the SARAH ones:
    v0, x, mu2, MS, K1, K2, Kappa, LambdaS, Lambda = param_MRM_to_SARAH(lam, v0, x0, a1, a2, b3, b4)
    MASS = np.array( [[-mu2 - K1 *x - (K2 *x**2)/2. - (3*Lambda *v0**2)/2., -(K1 *v0) - K2 *x*v0], [-(K1 *v0) - K2 *x*v0, -MS - 6*LambdaS *x**2 - 2*x*Kappa  - (K2 *v0**2)/2.]] )
    # get eigenvalues
    w, v = LA.eig(MASS)
    mh2 = math.sqrt(-w[0])
    mh1 = math.sqrt(-w[1])
    return round_sig(mh1,5), round_sig(mh2,5)

# get the one-loop masses given the MRM parameters, g1, g2 and yt  [eigenvalues calculated from double-derivative matrix]
def OneLoop_masses_diag(lam, v0, x0, a1, a2, b3, b4, g1, g2, yt):
    # convert the MRM parameters to the SARAH ones:
    v0, x, mu2, MS, K1, K2, Kappa, LambdaS, Lambda = param_MRM_to_SARAH(lam, v0, x0, a1, a2, b3, b4)
    MASS = oneloop_massmatrix(v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    # get eigenvalues
    w, v = LA.eig(MASS)
    mh2 = cmath.sqrt(w[0]).real
    mh1 = cmath.sqrt(w[1]).real
    if mh1 > mh2:
        mh2_pr = mh2
        mh2 = mh1
        mh1 = mh2_pr
    return round_sig(mh1,5), round_sig(mh2, 5)

# get the one-loop masses given the MRM parameters, g1, g2 and yt  [eigenvalues calculated from double-derivative matrix]
# 
def OneLoop_masses_diag_scale(Sc, lam, v0, x0, a1, a2, b3, b4, g1, g2, yt):
    # convert the MRM parameters to the SARAH ones:
    v0, x, mu2, MS, K1, K2, Kappa, LambdaS, Lambda = param_MRM_to_SARAH(lam, v0, x0, a1, a2, b3, b4)
    # oneloop_massmatrix_scale(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    MASS = oneloop_massmatrix_scale(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    # get eigenvalues
    w, v = LA.eig(MASS)
    mh2 = cmath.sqrt(w[0])
    mh1 = cmath.sqrt(w[1])
    #print('mh1, mh2=', mh1, mh2)
    mh1 = mh1.real
    mh2 = mh2.real
    if mh1 > mh2:
        mh2_pr = mh2
        mh2 = mh1
        mh1 = mh2_pr
    return mh1, mh2
    #return round_sig(mh1,8), round_sig(mh2, 8)

def OneLoop_masses_diag_scale_SARAH(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt):
    # oneloop_massmatrix_scale(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    MASS = oneloop_massmatrix_scale(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    # get eigenvalues
    w, v = LA.eig(MASS)
    mh2 = cmath.sqrt(w[0])
    mh1 = cmath.sqrt(w[1])
    #print('mh1, mh2=', mh1, mh2)
    mh1 = mh1.real
    mh2 = mh2.real
    if mh1 > mh2:
        mh2_pr = mh2
        mh2 = mh1
        mh1 = mh2_pr
    return round_sig(mh1,8), round_sig(mh2, 8)

def finetuning_oneloop(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt):
    mh1, mh2 = OneLoop_masses_diag_scale_SARAH(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    # x derivatives:
    eps = 1E-6
    mh1_eps, mh2_eps = OneLoop_masses_diag_scale_SARAH(Sc, v0, x+eps, mu2, MS, K1, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    dmh1dx = (mh1_eps - mh1)/eps
    dmh2dx = (mh2_eps - mh2)/eps
    # K1 derivatives:
    eps = 1E-6
    mh1_eps, mh2_eps = OneLoop_masses_diag_scale_SARAH(Sc, v0, x, mu2, MS, K1+eps, K2, Lambda, LambdaS, Kappa, g1, g2, yt)
    dmh1dK1 = (mh1_eps - mh1)/eps
    dmh2dK1 = (mh2_eps - mh2)/eps
    # K2 derivatives:
    eps = 1E-6
    mh1_eps, mh2_eps = OneLoop_masses_diag_scale_SARAH(Sc, v0, x, mu2, MS, K1, K2+eps, Lambda, LambdaS, Kappa, g1, g2, yt)
    dmh1dK2 = (mh1_eps - mh1)/eps
    dmh2dK2 = (mh2_eps - mh2)/eps
    # Kappa derivatives
    eps = 1E-6
    mh1_eps, mh2_eps = OneLoop_masses_diag_scale_SARAH(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS, Kappa+eps, g1, g2, yt)    
    dmh1dKapp = (mh1_eps - mh1)/eps
    dmh2dKapp = (mh2_eps - mh2)/eps
    # LambdaS derivatives:
    eps = 1E-6
    mh1_eps, mh2_eps = OneLoop_masses_diag_scale_SARAH(Sc, v0, x, mu2, MS, K1, K2, Lambda, LambdaS+eps, Kappa, g1, g2, yt)    
    dmh1dLamS = (mh1_eps - mh1)/eps
    dmh2dLamS = (mh2_eps - mh2)/eps
    # get the fine tuning for each of the parameters:
    ftmh1x = (x/mh1) * dmh1dx
    ftmh2x = (x/mh2) * dmh2dx
    ftmh1K1 = (K1/mh1) * dmh1dK1
    ftmh2K1 = (K1/mh2) * dmh2dK1
    ftmh1K2 = (K2/mh1) * dmh1dK2
    ftmh2K2 = (K2/mh2) * dmh2dK2
    ftmh1Kapp = (Kappa/mh1) * dmh1dKapp
    ftmh2Kapp = (Kappa/mh2) * dmh2dKapp
    ftmh1LamS = (LambdaS/mh1) * dmh1dLamS
    ftmh2LamS= (LambdaS/mh2) * dmh2dLamS
    ftmh1 = max([abs(ftmh1x), abs(ftmh1K1), abs(ftmh1K2), abs(ftmh1Kapp), abs(ftmh1LamS)])
    ftmh2 = max([abs(ftmh2x), abs(ftmh2K1), abs(ftmh2K2), abs(ftmh2Kapp), abs(ftmh2LamS)])
    return ftmh1, ftmh2

def round_sig(x, sig=2):
    if x == 0.:
        return 0.
    return round(x, sig-int(math.floor(math.log10(abs(x))))-1)

def mass_and_mixing(lam, v0, x0, a1, a2, b3, b4):
    # diagonalise mass matrix:
    MASS = np.array( [[mh_sq(lam, v0), mhs_sq(a1, a2, x0, v0)], [mhs_sq(a1, a2, x0, v0), ms_sq(a1, b3, b4, x0, v0)]] )
    w, v = LA.eig(MASS)
    mh2 = math.sqrt(w[0])
    mh1 = math.sqrt(w[1])
    if mh1 > mh2:
        mh2_pr = mh2
        mh2 = mh1
        mh1 = mh2_pr
    #print "mh2 = ", mh2, " mh1=", mh1
    sin2theta = (a1 + 2 * a2 * x0) * v0/(w[0] - w[1])
    cos2theta = math.sqrt(1. - sin2theta**2)
    costheta = math.sqrt( (1. + cos2theta)/2.)
    sintheta = math.sqrt(1. - costheta**2)
    return round_sig(mh1,6), round_sig(mh2,6), round_sig(sintheta,5), round_sig(costheta,5)

def mh_sq(lam, v0):
    return round_sig(2 * lam * v0**2,5)

def ms_sq(a1, b3, b4, x0, v0):
    return round_sig(b3 * x0 + 2 * b4 * x0**2 - a1 * v0**2 / 4. / x0,5)

def mhs_sq(a1, a2, x0, v0):
    return round_sig((a1 + 2 * a2 * x0 ) * v0/2.,5)



Test = False
if Test != False:
    
    #######################
    # TESTING STARTS HERE #
    #######################


    ##BenchmarkPoints["NAME"] = [ costh, sinth, mass2, gam2, x0  , lamb, a_1, , a_2  , b_3   , b_4]
    #BenchmarkPoints["B1max"] = [ 0.976, 0.220, 341.0, 2.42, 257., 0.92, -377., 0.392, -403., 0.77 ]

    v0 = 246.
    x0 = 257.
    lam = 0.92
    a1 = -377.
    a2 = 0.392
    b3 = -403.
    b4 = 0.77


    print('yt=', yt)
    
    print('Tree level from the MRM formulae=', mass_and_mixing(lam, v0, x0, a1, a2, b3, b4))

    print('Tree level, eigenvalues (analytically) from Mathematica=', TreeLevel_masses(lam, v0, x0, a1, a2, b3, b4))
    print('Tree level, eigenvalues (numerically) in python=', TreeLevel_masses_diag(lam, v0, x0, a1, a2, b3, b4))
    print('One loop, eigenvalues (numerically) in python [REAL PART]=', OneLoop_masses_diag(lam, v0, x0, a1, a2, b3, b4, g1, g2, yt))

