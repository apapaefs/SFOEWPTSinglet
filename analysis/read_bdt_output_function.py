import ROOT
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from scipy.optimize import curve_fit
from lmfit.models import GaussianModel, VoigtModel, LorentzianModel, PseudoVoigtModel, MoffatModel, BreitWignerModel, SkewedVoigtModel, SkewedGaussianModel, DoniachModel, StepModel, ConstantModel, LinearModel, QuadraticModel, ExponentialModel, PowerLawModel
import scipy
from optparse import OptionParser
from math import log10, floor

#parser = OptionParser(usage="%prog [mode]")

#opts, args = parser.parse_args()

#if len(args) < 3:
#    print('usage: read_bdt_output_function.py [mass] [lumi] [xsec]')
#    exit()

#mass_in = float(args[0])
#lumi_in = float(args[1])
#xsec_in = float(args[2])

def round_sig(x, sig=2):
    if x == 0.:
        return 0.
    if math.isnan(x) is True:
        print('Warning, NaN!')
        return 0.
    return round(x, sig-int(floor(log10(abs(x))))-1)



def get_mass_fit(mass, lumi, xsec):
    colors = [ 'green', 'orange', 'red', 'magenta', 'blue', 'cyan', 'black', 'brown', 'violet', 'purple'] # 10 colours

    # read in the root file containing the BDT output
    rootfile = "./HZZ100_4l/Herwig/events/BDT_output_" + str(int(mass)) + ".root"
    #print('reading in', rootfile)
    f = ROOT.TFile.Open(rootfile)
    tree = f.Get("Data3")
    nev = tree.GetEntries()
    #print('number of entries = ', nev)

    # Whether to do the plots
    plotfits = False
    
    # if the bdt cut was found
    BDT_cut_set = False
    
    alpha = 0.05 # systematic uncertainty
    
    # loop over the events and apply the BDT cut 
    m4l = []
    weights = []
    m4l_dict = {}
    weights_dict = {}
    event_id_array = []
    pass_id = {}
    total_id = {}
    pass_weight_id = {}
    total_weight_id = {}
    total_absweight_id = {}
    for evt in tree:
        if BDT_cut_set == False:
            BDT_cut = evt.bdtcut
            fac_signal = evt.fac_signal
            #print('BDT cut =', BDT_cut, 'fac_signal=', fac_signal)
            BDT_cut_set = True
        #print(evt.event_id, evt.BDT_response, evt.m4l, evt.wgt)
        if evt.event_id not in list(total_id.keys()):
            total_id[evt.event_id] =[ np.sign(evt.wgt)]
            total_weight_id[evt.event_id] =[ evt.wgt ]
            total_absweight_id[evt.event_id] =[ abs(evt.wgt) ]
            #print((evt.event_id, evt.wgt))
        else:
            total_id[evt.event_id].append(np.sign(evt.wgt))
            total_weight_id[evt.event_id].append(evt.wgt)
            total_absweight_id[evt.event_id].append(abs(evt.wgt))
        if evt.BDT_response > BDT_cut:
            if evt.event_id not in list(pass_id.keys()):
                pass_id[evt.event_id] =[ np.sign(evt.wgt)]
                pass_weight_id[evt.event_id] =[evt.wgt]
            else:
                pass_id[evt.event_id].append(np.sign(evt.wgt))
                pass_weight_id[evt.event_id].append(evt.wgt)
            if evt.event_id not in event_id_array:
                event_id_array.append(evt.event_id)
            m4l.append(evt.m4l)
            if evt.event_id not in list(m4l_dict.keys()):
                m4l_dict[evt.event_id] = [evt.m4l]
                weights_dict[evt.event_id] = [evt.wgt]
            else:
                m4l_dict[evt.event_id].append(evt.m4l)
                weights_dict[evt.event_id].append(evt.wgt)
            weights.append(evt.wgt)



    # calculate the efficiency of the BDT cut as well as the total cross section for each sample
    efficiency_id = {}
    cross_section_id = {}
    cross_section_abs_id = {}

    # renormalize the weights to get the correct cross sections in the case of negative weights:
    negwF = {}
    cross_section_signal_sum = 0.
    for key in sorted(m4l_dict.keys()):
        #cross_section_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        cross_section_id[key] = np.sum(total_absweight_id[key])/len(total_absweight_id[key]) # note that the absolute value of the cross section has to be used
        # calculate the total signal xsec:
        if int(key) > 0:
            cross_section_signal_sum = cross_section_signal_sum + cross_section_id[key]
        negwF[key] = cross_section_id[key] * len(total_absweight_id[key])/np.sum(total_weight_id[key]) # the factor to reweigh the events with
        total_weight_id[key] = np.array(total_weight_id[key]) * negwF[key] # fix the weights so that the sum of the weights = the total cross section
        pass_weight_id[key] = np.array(pass_weight_id[key]) * negwF[key] # fix the weights of the events that passed the BDT cut as well

    #print(('total signal cross section from BDT=', cross_section_signal_sum))
    #print(('total signal cross section in=', xsec))
    signal_amplification = xsec/cross_section_signal_sum
    #print(('signal_amplification=', signal_amplification))

    # print the reweighting factor
    #print(('RW factors=', negwF))
        
    # check the sum of the renormalized weights:
    cross_section_renorm_id = {}
    for key in sorted(m4l_dict.keys()):
        cross_section_renorm_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        efficiency_id[key] = np.sum(pass_weight_id[key])/np.sum(total_weight_id[key])
        

    #print("total cross sections:", cross_section_id)
    #print("total cross sections (renormalization check):", cross_section_renorm_id)
    #print("total cross sections (from abs):", cross_section_abs_id)
    #print("efficiencies of BDT cut:", efficiency_id)


    # define a label map
    label_map = {}
    label_map[2] = '$ gg \\rightarrow h_2 \\rightarrow ZZ \\rightarrow 4 \\ell$'
    label_map[1] = '$ pp \\rightarrow h_2 jj \\rightarrow ZZ jj \\rightarrow 4 \\ell jj $'
    label_map[-1] = '$ gg \\rightarrow Z Z \\rightarrow 4 \\ell$'
    label_map[-2] = '$ pp \\rightarrow 4 \\ell$'

    # push the events that pass into arrays
    # counters for signal and background events:
    S = 0
    B = 0
    # arrays to store the events:
    m4l_arrays = []
    m4l_arrays_backgroundonly = []
    weights_arrays = []
    weights_arrays_backgroundonly = []
    color_map = {}
    colors_keys = []
    colors_keys_backgroundonly = []
    labels_keys = []
    labels_keys_backgroundonly = []
    Amplification = {} # contains the arrays of factors to modify the signal cross section
    n = 0
    #print('expected number of events at Lumi = ', lumi, 'fb^-1 after BDT cut for:')
    for key in sorted(m4l_dict.keys()):
        #print(key)
        if int(key) > 0: # multiply the signal by a factor 
            Amplification[key] = signal_amplification
        else:
            Amplification[key] = 1.
        m4l_arrays.append(m4l_dict[key])
        weights_arrays.append(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        #print('\t', key, '=', np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key])))
        color_map[key] = colors[n]
        colors_keys.append(colors[n])
        labels_keys.append(label_map[key])
        if int(key) < 0:
            m4l_arrays_backgroundonly.append(m4l_dict[key])
            weights_arrays_backgroundonly.append(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
            colors_keys_backgroundonly.append(colors[n])
            labels_keys_backgroundonly.append(label_map[key])
            B = B + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
        else:
            S = S + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        n = n+1
    
    Significance = S/math.sqrt(B + (alpha*B)**2)
    print(("Significance =", Significance, "Mass=", mass))
    if Significance < 0.1:
        return -2, -2, Significance
    
    # create total signal and background histograms
    nbins = 8
    xmin_h = mass - 20.0
    xmax_h = mass + 20.0
    m4l_background, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    m4l_signal, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    for key in sorted(m4l_dict.keys()):
        hist_key, edges = np.histogram(m4l_dict[key],nbins, weights=np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]), range=(xmin_h,xmax_h))
        # background
        if int(key) < 0:
            m4l_background = np.add( m4l_background, abs(hist_key)) # made absolute value here
        # signal
        else:
            m4l_signal = np.add( m4l_signal, hist_key)

    m4l_total = np.add( m4l_signal, m4l_background )

    #print(m4l_signal)
    #print(m4l_background)


    # calculate the bin mid-points from the edges
    bins = (edges[1:] + edges[:-1])/2
    #print('bins=', bins)
    
    ###################################
    # START PLOTTING AND FITTING HERE #
    ###################################


    #############################
    # TOTAL AND BACKGROUND PLOT #
    #############################
    if plotfits is True:
        plot_type = 'BDT_m4l_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        xmin = xmin_h
        xmax = xmax_h
        ylab = "Number of Events per bin"
        xlab = '$m_{4\ell}$ [GeV]'
        # number of bins
        n_bins = nbins

    
        # plot the total number of events with error bars
        ax.errorbar(bins, m4l_total, yerr=np.sqrt(m4l_signal), xerr=0., ms=5, capthick=20, fmt='bo', label='Signal+Background')
    
        # the colour map
        #print(color_map)

        # histogram the background
        ax.hist(m4l_arrays_backgroundonly, n_bins, weights=weights_arrays_backgroundonly, density=False, histtype='bar', stacked=True, color=colors_keys_backgroundonly, range=(xmin_h,xmax_h), label=labels_keys_backgroundonly)
        #plt.step(bins, m4l_background, ls='-', color='magenta', where='mid')

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
        #pl.rcParams.update({'font.size': 15})
        #pl.rcParams['figure.figsize'] = 12, 12

        # set axis ticks
        ax.xaxis.set_major_locator(MultipleLocator(100))
        ax.xaxis.set_minor_locator(MultipleLocator(20))
        #ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

        # set labels and limits
        ax.set_title('100 TeV/' + str(lumi) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)


    #############################
    # DO THE FIT                #
    #############################
    
    # calculate the poisson errors at 1 sigma
    def poisson_errors(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return up, down

    # calculate the poisson errors at 1 sigma and symmetrize
    def poisson_errors_symmetrize(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return (up+down)/2.


    def fit_poisson(Ntot, Nbkg, xbins, nfit):
        central_param_array = []
        out_array = []
        # do the fit nfit times:
        for ii in range(0,nfit):
            Nii_tot = []
            for it in range(0, len(Ntot)):
                Nii_tot.append(np.random.poisson(Ntot[it], 1)[0])
            Nii_bkg = []
            for it in range(0, len(Nbkg)):
                #print(Nbkg[it])
                Nii_bkg.append(np.random.poisson(Nbkg[it], 1)[0])
            Nii_tot = np.array(Nii_tot)
            Nii_bkg = np.array(Nii_bkg)
            Nii = np.subtract(Nii_tot, Nii_bkg)
            Nii[Nii < 0] = 0
            mod = VoigtModel()
            pars = mod.guess(Nii, x=bins)
            out = mod.fit(Nii, pars, x=bins)
            out_array.append(out)
            central_param_array.append(out.params['center'].value)

        n = len(central_param_array)
        index_med = n / 2 # median.
        index_84 = int(round(n * 0.84135)) # 84th percentile from median.
        index_16 = int(round(n * 0.15865))
        central_param_array = sorted(np.array(central_param_array))
        central_mean = np.mean(central_param_array)
        central_std = np.std(central_param_array)
        central_median = np.median(central_param_array)
        central_84 = central_param_array[index_84]
        central_16 = central_param_array[index_16]
        #return central_mean, central_std, central_median, central_84, central_16
        return central_median, central_84-central_median, central_median-central_16, central_mean, central_std, out_array
    
    # calculate the POISSON errors
    m4l_total_poisson_up, m4l_total_poisson_down = poisson_errors(np.add(m4l_total,m4l_background))
    m4l_background_poisson_up, m4l_background_poisson_down = poisson_errors(m4l_background)
    m4l_signal_poisson_up = np.sqrt( np.add(m4l_total_poisson_up**2, m4l_background_poisson_up**2))
    m4l_signal_poisson_down = np.sqrt( np.add(m4l_total_poisson_down**2, m4l_background_poisson_down**2))
    di = 0
    for d in m4l_signal_poisson_down:
        if m4l_signal[di] - d < 0:
            m4l_signal_poisson_down[di] = m4l_signal[di]
        di = di + 1

    m4l_signal_poisson_symmetric = 0.5 * np.add(m4l_signal_poisson_up, m4l_signal_poisson_down)
    
    # test the poisson fit:
    #print(m4l_total)
    #print(m4l_background)
    #central_median, bin_84, bin_16, central_mean, central_std, out_array = fit_poisson(m4l_total, m4l_background, bins, 100)
    #print('mass in', central_median, "+", bin_84, "-", bin_16)
    
    # do the fit 
    #Gaussian
    modg = GaussianModel()
    parsg = modg.guess(m4l_signal, x=bins)
    outg = modg.fit(m4l_signal, parsg, x=bins, scale_covar=False, weights=1/m4l_signal_poisson_symmetric)
    fitparamsg = outg.params
    #print(outg.fit_report(min_correl=0.25))
    #print(fitparamsg['center'].value, fitparamsg['sigma'].value)
    #print("reduced chisq Gauss =", outg.redchi)
    
    #Skewed Voigt
    modsv = SkewedVoigtModel()
    parssv = modsv.guess(m4l_signal, x=bins)
    outsv = modsv.fit(m4l_signal, parssv, x=bins, scale_covar=False, weights=1/m4l_signal_poisson_symmetric)
    fitparamssv = outsv.params
    #print(outsv.fit_report(min_correl=0.25))
    #print(fitparamssv['center'].value, fitparamssv['center'].stderr)
    #print("reduced chisq SV =", outsv.redchi)


    if outg.redchi < outsv.redchi and fitparamsg['center'].stderr != None:
        out = outg
        fitparams = fitparamsg
        fitkind = 'Gaussian'
    else:
        out = outsv
        fitparams = fitparamssv
        fitkind = 'SkewedVoigt'

    # fix for None error
    if fitparamssv['center'].stderr == None:
        out = outg
        fitparams = fitparamsg
        fitkind = 'Gaussian'


    #out = outg
    #fitparams = fitparamsg
    #fitkind = 'Gaussian'
        
    #print(("MASS FIT=", fitparams['center'].value, fitparams['center'].stderr))

    #print(popt)

    #############################
    # SIGNAL ONLY PLOT FOR FIT  #
    #############################
    if plotfits is True:
    
        plot_type = 'BDT_m4l_signal_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        # plot the signal with error bars
        ax.errorbar(bins, m4l_signal, yerr=[m4l_signal_poisson_down, m4l_signal_poisson_up], xerr=0., ms=5, capthick=20, fmt='bo', label='Signal after background subtraction')
        #ax.errorbar(bins, m4l_signal_norm, yerr=m4l_signal_poisson_symmetric_norm, xerr=0., ms=5, capthick=20, fmt='go', label='Signal after background subtraction')

        # plot the fit
        x = np.arange(xmin_h, xmax_h, 0.2)
        #for out_i in out_array:
        #    plt.plot(x, out_i.eval(x=x), '--', color='red', label=None)
    
        dely = out.eval_uncertainty(x=x)
        #plt.plot(x, out.eval(x=x), '--', color='green', label='Voigt fit with symm. Poisson')
        #plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)
    
        delyg = out.eval_uncertainty(x=x)
        plt.plot(x, out.eval(x=x), '--', color='red', label= fitkind + ' fit with symm. Poisson')
        plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)

        #plt.plot(bins, out.best_fit, '--', color='red', label='best fit')


        #insettext = "$pp \\rightarrow h_2 \\rightarrow ZZ \\rightarrow 4\\ell$,\n$m_2^\\mathrm{fit} = " + str(round_sig(fitparams['center'].value,5)) + "\pm" + str(round_sig(fitparams['center'].stderr,2)) + "$ GeV"
        insettext = "$pp \\rightarrow h_2 \\rightarrow ZZ \\rightarrow 4\\ell$,\n$m_2^\\mathrm{fit} = " + str(round_sig(fitparams['center'].value,5)) + "\pm" + str(round_sig(fitparams['sigma'].value,2)) + "$ GeV"
        plt.text(0.2, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=10)
    

        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # set labels and limits
        ax.set_title('100 TeV/' + str(int(lumi)) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)

    ####################
    ### RETURN HERE ####
    ####################    
    #return fitparams['center'].value, fitparams['center'].stderr, Significance
    return fitparams['center'].value, fitparams['sigma'].value, Significance

def get_mass_fit_HH(mass, lumi, xsec):
    colors = [ 'green', 'orange', 'red', 'magenta', 'blue', 'cyan', 'black', 'brown', 'violet', 'purple', 'olive'] # 10 colours
    # read in the root file containing the BDT output
    rootfile = "./HH100_BBGG/Herwig/events/BDT_output_" + str(int(mass)) + ".root"
    #print('reading in', rootfile)
    f = ROOT.TFile.Open(rootfile)
    tree = f.Get("Data3")
    nev = tree.GetEntries()
    #print('number of entries = ', nev)

    # Whether to do the plots
    plotfits = False
    
    # if the bdt cut was found
    BDT_cut_set = False
    
    alpha = 0.05 # systematic uncertainty
    
    # loop over the events and apply the BDT cut 
    mbbaa = []
    weights = []
    mbbaa_dict = {}
    weights_dict = {}
    event_id_array = []
    pass_id = {}
    total_id = {}
    pass_weight_id = {}
    total_weight_id = {}
    total_absweight_id = {}
    for evt in tree:
        if BDT_cut_set == False:
            BDT_cut = evt.bdtcut
            fac_signal = evt.fac_signal
            #print('BDT cut =', BDT_cut, 'fac_signal=', fac_signal)
            BDT_cut_set = True
        #print(evt.event_id, evt.BDT_response, evt.mbbaa, evt.wgt)
        if evt.event_id not in list(total_id.keys()):
            total_id[evt.event_id] =[ np.sign(evt.wgt)]
            total_weight_id[evt.event_id] =[ evt.wgt ]
            total_absweight_id[evt.event_id] =[ abs(evt.wgt) ]
            #print((evt.event_id, evt.wgt))
        else:
            total_id[evt.event_id].append(np.sign(evt.wgt))
            total_weight_id[evt.event_id].append(evt.wgt)
            total_absweight_id[evt.event_id].append(abs(evt.wgt))
        if evt.BDT_response > BDT_cut:
            if evt.event_id not in list(pass_id.keys()):
                pass_id[evt.event_id] =[ np.sign(evt.wgt)]
                pass_weight_id[evt.event_id] =[evt.wgt]
            else:
                pass_id[evt.event_id].append(np.sign(evt.wgt))
                pass_weight_id[evt.event_id].append(evt.wgt)
            if evt.event_id not in event_id_array:
                event_id_array.append(evt.event_id)
            mbbaa.append(evt.mbbaa)
            if evt.event_id not in list(mbbaa_dict.keys()):
                mbbaa_dict[evt.event_id] = [evt.mbbaa]
                weights_dict[evt.event_id] = [evt.wgt]
            else:
                mbbaa_dict[evt.event_id].append(evt.mbbaa)
                weights_dict[evt.event_id].append(evt.wgt)
            weights.append(evt.wgt)



    # calculate the efficiency of the BDT cut as well as the total cross section for each sample
    efficiency_id = {}
    cross_section_id = {}
    cross_section_abs_id = {}

    # renormalize the weights to get the correct cross sections in the case of negative weights:
    negwF = {}
    cross_section_signal_sum = 0.
    for key in sorted(mbbaa_dict.keys()):
        #cross_section_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        cross_section_id[key] = np.sum(total_absweight_id[key])/len(total_absweight_id[key]) # note that the absolute value of the cross section has to be used
        # calculate the total signal xsec:
        if int(key) > 0:
            cross_section_signal_sum = cross_section_signal_sum + cross_section_id[key]
        negwF[key] = cross_section_id[key] * len(total_absweight_id[key])/np.sum(total_weight_id[key]) # the factor to reweigh the events with
        total_weight_id[key] = np.array(total_weight_id[key]) * negwF[key] # fix the weights so that the sum of the weights = the total cross section
        pass_weight_id[key] = np.array(pass_weight_id[key]) * negwF[key] # fix the weights of the events that passed the BDT cut as well

    #print(('total signal cross section from BDT=', cross_section_signal_sum))
    #print(('total signal cross section in=', xsec))
    signal_amplification = xsec/cross_section_signal_sum
    #print(('signal_amplification=', signal_amplification))

    # print the reweighting factor
    #print(('RW factors=', negwF))
        
    # check the sum of the renormalized weights:
    cross_section_renorm_id = {}
    for key in sorted(mbbaa_dict.keys()):
        cross_section_renorm_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        efficiency_id[key] = np.sum(pass_weight_id[key])/np.sum(total_weight_id[key])
        

    #print("total cross sections:", cross_section_id)
    #print("total cross sections (renormalization check):", cross_section_renorm_id)
    #print("total cross sections (from abs):", cross_section_abs_id)
    #print("efficiencies of BDT cut:", efficiency_id)


    # define a label map
    label_map = {}
    label_map[2] = '$ gg \\rightarrow h_2 \\rightarrow h_1 h_1 \\rightarrow (b\\bar{b})(\\gamma \\gamma)$'
    label_map[1] = '$ pp \\rightarrow h_2 jj \\rightarrow h_1 h_1 jj \\rightarrow (b\\bar{b})(\\gamma \\gamma) jj $'
    label_map[-1] = '$ gg \\rightarrow Z h_1 $ (Loop)'
    label_map[-2] = '$ pp \\rightarrow \\gamma + \\mathrm{jets}$'
    label_map[-3] = '$ pp \\rightarrow t\\bar{t} \\gamma \\gamma$'
    label_map[-4] = '$ pp \\rightarrow \\gamma \\gamma + \\mathrm{jets}$'
    label_map[-5] = '$ gg \\rightarrow h_1 h_1$ SM'
    label_map[-6] = '$ pp \\rightarrow t\\bar{t} h_1$'
    label_map[-7] = '$ pp \\rightarrow b\\bar{b}\\gamma \\gamma$'
    label_map[-8] = '$ pp \\rightarrow Z h_1 $'
    label_map[-9] = '$ pp \\rightarrow b\\bar{b}\\gamma$+jets'
    label_map[-10] = '$ pp \\rightarrow b\\bar{b} h_1$'

    # push the events that pass into arrays
    # counters for signal and background events:
    S = 0
    B = 0
    # arrays to store the events:
    mbbaa_arrays = []
    mbbaa_arrays_backgroundonly = []
    weights_arrays = []
    weights_arrays_backgroundonly = []
    color_map = {}
    colors_keys = []
    colors_keys_backgroundonly = []
    labels_keys = []
    labels_keys_backgroundonly = []
    Amplification = {} # contains the arrays of factors to modify the signal cross section
    n = 0
    #print('expected number of events at Lumi = ', lumi, 'fb^-1 after BDT cut for:')
    for key in sorted(mbbaa_dict.keys()):
        #print(key)
        if int(key) > 0: # multiply the signal by a factor 
            Amplification[key] = signal_amplification
        else:
            Amplification[key] = 1.
        mbbaa_arrays.append(mbbaa_dict[key])
        weights_arrays.append(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        #print('\t', key, '=', np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key])))
        #print('n=', n)
        color_map[key] = colors[n]
        colors_keys.append(colors[n])
        labels_keys.append(label_map[key])
        if int(key) < 0:
            mbbaa_arrays_backgroundonly.append(mbbaa_dict[key])
            weights_arrays_backgroundonly.append(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
            colors_keys_backgroundonly.append(colors[n])
            labels_keys_backgroundonly.append(label_map[key])
            B = B + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
        else:
            S = S + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        n = n+1
    
    Significance = S/math.sqrt(B + (alpha*B)**2)
    print(("Significance =", Significance, "Mass=", mass))
    if Significance < 0.1:
        return -2, -2, Significance
    
    # create total signal and background histograms
    nbins = 8
    xmin_h = mass - 50.0
    xmax_h = mass + 50.0
    mbbaa_background, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    mbbaa_signal, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    for key in sorted(mbbaa_dict.keys()):
        hist_key, edges = np.histogram(mbbaa_dict[key],nbins, weights=np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]), range=(xmin_h,xmax_h))
        # background
        if int(key) < 0:
            mbbaa_background = np.add( mbbaa_background, hist_key)
        # signal
        else:
            mbbaa_signal = np.add( mbbaa_signal, abs(hist_key))

    mbbaa_total = np.add( mbbaa_signal, mbbaa_background )

    #print(mbbaa_signal)
    #print(mbbaa_background)


    # calculate the bin mid-points from the edges
    bins = (edges[1:] + edges[:-1])/2
    #print('bins=', bins)
    
    ###################################
    # START PLOTTING AND FITTING HERE #
    ###################################


    #############################
    # TOTAL AND BACKGROUND PLOT #
    #############################
    if plotfits is True:
        plot_type = 'BDT_mbbaa_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        xmin = xmin_h
        xmax = xmax_h
        ylab = "Number of Events per bin"
        xlab = '$m_{b\\bar{b}\\gamma \\gamma}$ [GeV]'
        # number of bins
        n_bins = nbins

    
        # plot the total number of events with error bars
        ax.errorbar(bins, mbbaa_total, yerr=np.sqrt(mbbaa_signal), xerr=0., ms=5, capthick=20, fmt='bo', label='Signal+Background')
    
        # the colour map
        #print(color_map)

        # histogram the background
        ax.hist(mbbaa_arrays_backgroundonly, n_bins, weights=weights_arrays_backgroundonly, density=False, histtype='bar', stacked=True, color=colors_keys_backgroundonly, range=(xmin_h,xmax_h), label=labels_keys_backgroundonly)
        #plt.step(bins, mbbaa_background, ls='-', color='magenta', where='mid')

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
        #pl.rcParams.update({'font.size': 15})
        #pl.rcParams['figure.figsize'] = 12, 12

        # set axis ticks
        ax.xaxis.set_major_locator(MultipleLocator(20))
        ax.xaxis.set_minor_locator(MultipleLocator(5))
        #ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

        # set labels and limits
        ax.set_title('100 TeV/' + str(lumi) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)


    #############################
    # DO THE FIT                #
    #############################
    
    # calculate the poisson errors at 1 sigma
    def poisson_errors(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return up, down

    # calculate the poisson errors at 1 sigma and symmetrize
    def poisson_errors_symmetrize(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return (up+down)/2.


    def fit_poisson(Ntot, Nbkg, xbins, nfit):
        central_param_array = []
        out_array = []
        # do the fit nfit times:
        for ii in range(0,nfit):
            Nii_tot = []
            for it in range(0, len(Ntot)):
                Nii_tot.append(np.random.poisson(Ntot[it], 1)[0])
            Nii_bkg = []
            for it in range(0, len(Nbkg)):
                #print(Nbkg[it])
                Nii_bkg.append(np.random.poisson(Nbkg[it], 1)[0])
            Nii_tot = np.array(Nii_tot)
            Nii_bkg = np.array(Nii_bkg)
            Nii = np.subtract(Nii_tot, Nii_bkg)
            Nii[Nii < 0] = 0
            mod = VoigtModel()
            pars = mod.guess(Nii, x=bins)
            out = mod.fit(Nii, pars, x=bins)
            out_array.append(out)
            central_param_array.append(out.params['center'].value)

        n = len(central_param_array)
        index_med = n / 2 # median.
        index_84 = int(round(n * 0.84135)) # 84th percentile from median.
        index_16 = int(round(n * 0.15865))
        central_param_array = sorted(np.array(central_param_array))
        central_mean = np.mean(central_param_array)
        central_std = np.std(central_param_array)
        central_median = np.median(central_param_array)
        central_84 = central_param_array[index_84]
        central_16 = central_param_array[index_16]
        #return central_mean, central_std, central_median, central_84, central_16
        return central_median, central_84-central_median, central_median-central_16, central_mean, central_std, out_array
    
    # calculate the POISSON errors
    mbbaa_total_poisson_up, mbbaa_total_poisson_down = poisson_errors(np.add(mbbaa_total,mbbaa_background))
    mbbaa_background_poisson_up, mbbaa_background_poisson_down = poisson_errors(mbbaa_background)
    mbbaa_signal_poisson_up = np.sqrt( np.add(mbbaa_total_poisson_up**2, mbbaa_background_poisson_up**2))
    mbbaa_signal_poisson_down = np.sqrt( np.add(mbbaa_total_poisson_down**2, mbbaa_background_poisson_down**2))
    di = 0
    for d in mbbaa_signal_poisson_down:
        if mbbaa_signal[di] - d < 0:
            mbbaa_signal_poisson_down[di] = mbbaa_signal[di]
        di = di + 1

    mbbaa_signal_poisson_symmetric = 0.5 * np.add(mbbaa_signal_poisson_up, mbbaa_signal_poisson_down)
    mbbaa_signal_poisson_symmetric = np.array([np.inf if e != e else e for e in mbbaa_signal_poisson_symmetric])
    # test the poisson fit:
    #print(mbbaa_total)
    #print(mbbaa_background)
    #central_median, bin_84, bin_16, central_mean, central_std, out_array = fit_poisson(mbbaa_total, mbbaa_background, bins, 100)
    #print('mass in', central_median, "+", bin_84, "-", bin_16)
    
    # do the fit 
    #Gaussian
    modg = GaussianModel()
    parsg = modg.guess(mbbaa_signal, x=bins)
    #print("mbbaa_signal=", mbbaa_signal)
    #print("mbbaa_signal_poisson_symmetric=", mbbaa_signal_poisson_symmetric)
    #print("parsg=", parsg)
    #print("bins=", bins)
    outg = modg.fit(mbbaa_signal, parsg, x=bins, scale_covar=False, weights=1./mbbaa_signal_poisson_symmetric)
    fitparamsg = outg.params
    #print(outg.fit_report(min_correl=0.25))
    #print(fitparamsg['center'].value, fitparamsg['sigma'].value)
    #print("reduced chisq Gauss =", outg.redchi)
    
    #Skewed Voigt
    modsv = SkewedVoigtModel()
    parssv = modsv.guess(mbbaa_signal, x=bins)
    outsv = modsv.fit(mbbaa_signal, parssv, x=bins, scale_covar=False, weights=1./mbbaa_signal_poisson_symmetric)
    fitparamssv = outsv.params
    #print(outsv.fit_report(min_correl=0.25))
    #print(fitparamssv['center'].value, fitparamssv['center'].stderr)
    #print("reduced chisq SV =", outsv.redchi)

    if outg.redchi < outsv.redchi and fitparamsg['center'].stderr != None:
        out = outg
        fitparams = fitparamsg
        fitkind = 'Gaussian'
    else:
        out = outsv
        fitparams = fitparamssv
        fitkind = 'SkewedVoigt'

    if plotfits is True:
        print('sigma=',fitparams['sigma'].value)
        print(out.fit_report(min_correl=0.25))

    if fitparams['center'].stderr == None:
        print('UNCERTAINTY CALCULATION FOR MASS FIT FAILED')
        return -2, -2, Significance
        
    #print(("MASS FIT=", fitparams['center'].value, fitparams['center'].stderr))

    #print(popt)

    #############################
    # SIGNAL ONLY PLOT FOR FIT  #
    #############################
    if plotfits is True:
    
        plot_type = 'BDT_mbbaa_signal_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        # plot the signal with error bars
        ax.errorbar(bins, mbbaa_signal, yerr=[mbbaa_signal_poisson_down, mbbaa_signal_poisson_up], xerr=0., ms=5, capthick=20, fmt='bo', label='Signal after background subtraction')
        #ax.errorbar(bins, mbbaa_signal_norm, yerr=mbbaa_signal_poisson_symmetric_norm, xerr=0., ms=5, capthick=20, fmt='go', label='Signal after background subtraction')

        # plot the fit
        x = np.arange(xmin_h, xmax_h, 0.2)
        #for out_i in out_array:
        #    plt.plot(x, out_i.eval(x=x), '--', color='red', label=None)
    
        dely = out.eval_uncertainty(x=x)
        #plt.plot(x, out.eval(x=x), '--', color='green', label='Voigt fit with symm. Poisson')
        #plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)
    
        delyg = out.eval_uncertainty(x=x)
        plt.plot(x, out.eval(x=x), '--', color='red', label= fitkind + ' fit with symm. Poisson')
        plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)

        #plt.plot(bins, out.best_fit, '--', color='red', label='best fit')


        #insettext = "$pp \\rightarrow h_2 \\rightarrow h_1 h_1 \\rightarrow (b\\bar{b})(\\gamma\\gamma)$,\n$m_2^\\mathrm{fit} = " + str(round_sig(fitparams['center'].value,5)) + "\pm" + str(round_sig(fitparams['center'].stderr,2)) + "$ GeV"
        insettext = "$pp \\rightarrow h_2 \\rightarrow h_1 h_1 \\rightarrow (b\\bar{b})(\\gamma\\gamma)$,\n$m_2^\\mathrm{fit} = " + str(round_sig(fitparams['center'].value,5)) + "\pm" + str(round_sig(fitparams['sigma'].value,2)) + "$ GeV"
        
        plt.text(0.2, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=10)
    

        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # set labels and limits
        ax.set_title('100 TeV/' + str(int(lumi)) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)

    ####################
    ### RETURN HERE ####
    ####################    
    #return fitparams['center'].value, fitparams['center'].stderr, Significance
    return fitparams['center'].value, fitparams['sigma'].value, Significance

def get_mass_fit_WW(mass, lumi, xsec):
    colors = [ 'green', 'orange', 'red', 'magenta', 'blue', 'cyan', 'black', 'brown', 'violet'] # 9 colours

    # read in the root file containing the BDT output
    rootfile = "./HWW100/Herwig/events/BDT_output_" + str(int(mass)) + ".root"
    #print('reading in', rootfile)
    f = ROOT.TFile.Open(rootfile)
    tree = f.Get("Data3")
    nev = tree.GetEntries()
    #print('number of entries = ', nev)

    # Whether to do the plots
    plotfits = False
    
    # if the bdt cut was found
    BDT_cut_set = False
    
    alpha = 0.05 # systematic uncertainty
    
    # loop over the events and apply the BDT cut 
    mT = []
    weights = []
    mT_dict = {}
    weights_dict = {}
    event_id_array = []
    pass_id = {}
    total_id = {}
    pass_weight_id = {}
    total_weight_id = {}
    total_absweight_id = {}
    for evt in tree:
        if BDT_cut_set == False:
            BDT_cut = evt.bdtcut
            fac_signal = evt.fac_signal
            print('BDT cut =', BDT_cut, 'fac_signal=', fac_signal)
            BDT_cut_set = True
        #print(evt.event_id, evt.BDT_response, evt.mT, evt.wgt)
        if evt.event_id not in list(total_id.keys()):
            total_id[evt.event_id] =[ np.sign(evt.wgt)]
            total_weight_id[evt.event_id] =[ evt.wgt ]
            total_absweight_id[evt.event_id] =[ abs(evt.wgt) ]
            #print((evt.event_id, evt.wgt))
        else:
            total_id[evt.event_id].append(np.sign(evt.wgt))
            total_weight_id[evt.event_id].append(evt.wgt)
            total_absweight_id[evt.event_id].append(abs(evt.wgt))
        if evt.BDT_response > BDT_cut:
            if evt.event_id not in list(pass_id.keys()):
                pass_id[evt.event_id] =[ np.sign(evt.wgt)]
                pass_weight_id[evt.event_id] =[evt.wgt]
            else:
                pass_id[evt.event_id].append(np.sign(evt.wgt))
                pass_weight_id[evt.event_id].append(evt.wgt)
            if evt.event_id not in event_id_array:
                event_id_array.append(evt.event_id)
            mT.append(evt.mT)
            if evt.event_id not in list(mT_dict.keys()):
                mT_dict[evt.event_id] = [evt.mT]
                weights_dict[evt.event_id] = [evt.wgt]
            else:
                mT_dict[evt.event_id].append(evt.mT)
                weights_dict[evt.event_id].append(evt.wgt)
            weights.append(evt.wgt)



    # calculate the efficiency of the BDT cut as well as the total cross section for each sample
    efficiency_id = {}
    cross_section_id = {}
    cross_section_abs_id = {}

    # renormalize the weights to get the correct cross sections in the case of negative weights:
    negwF = {}
    cross_section_signal_sum = 0.
    for key in sorted(mT_dict.keys()):
        #cross_section_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        cross_section_id[key] = np.sum(total_absweight_id[key])/len(total_absweight_id[key]) # note that the absolute value of the cross section has to be used
        # calculate the total signal xsec:
        if int(key) > 0:
            cross_section_signal_sum = cross_section_signal_sum + cross_section_id[key]
        negwF[key] = cross_section_id[key] * len(total_absweight_id[key])/np.sum(total_weight_id[key]) # the factor to reweigh the events with
        total_weight_id[key] = np.array(total_weight_id[key]) * negwF[key] # fix the weights so that the sum of the weights = the total cross section
        pass_weight_id[key] = np.array(pass_weight_id[key]) * negwF[key] # fix the weights of the events that passed the BDT cut as well

    #print(('total signal cross section from BDT=', cross_section_signal_sum))
    #print(('total signal cross section in=', xsec))
    signal_amplification = xsec/cross_section_signal_sum
    print(('signal_amplification=', signal_amplification))

    # print the reweighting factor
    #print(('RW factors=', negwF))
        
    # check the sum of the renormalized weights:
    cross_section_renorm_id = {}
    for key in sorted(mT_dict.keys()):
        cross_section_renorm_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        efficiency_id[key] = np.sum(pass_weight_id[key])/np.sum(total_weight_id[key])
        

    print("total cross sections:", cross_section_id)
    #print("total cross sections (renormalization check):", cross_section_renorm_id)
    #print("total cross sections (from abs):", cross_section_abs_id)
    print("efficiencies of BDT cut:", efficiency_id)


    # define a label map
    label_map = {}
    label_map[2] = '$ gg \\rightarrow h_2 \\rightarrow W^+W^-$'
    label_map[1] = '$ pp \\rightarrow h_2 jj \\rightarrow  W^+W^- jj \\rightarrow  W^+W^- jj $'
    label_map[-1] = '$ pp \\rightarrow W^+W^- $'
    label_map[-2] = '$ pp \\rightarrow t\\bar{t}$'
    label_map[-3] = '$ pp \\rightarrow ZZ$'
    label_map[-4] = '$ pp \\rightarrow W+ \\mathrm{jets}$'
    label_map[-5] = '$ gg \\rightarrow WZ$'
    # push the events that pass into arrays
    # counters for signal and background events:
    S = 0
    B = 0
    # arrays to store the events:
    mT_arrays = []
    mT_arrays_backgroundonly = []
    weights_arrays = []
    weights_arrays_backgroundonly = []
    color_map = {}
    colors_keys = []
    colors_keys_backgroundonly = []
    labels_keys = []
    labels_keys_backgroundonly = []
    Amplification = {} # contains the arrays of factors to modify the signal cross section
    n = 0
    #print('expected number of events at Lumi = ', lumi, 'fb^-1 after BDT cut for:')
    for key in sorted(mT_dict.keys()):
        #print(key)
        if int(key) > 0: # multiply the signal by a factor 
            Amplification[key] = signal_amplification
        else:
            Amplification[key] = 1.
        mT_arrays.append(mT_dict[key])
        weights_arrays.append(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        #print('\t', key, '=', np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key])))
        color_map[key] = colors[n]
        colors_keys.append(colors[n])
        labels_keys.append(label_map[key])
        if int(key) < 0:
            mT_arrays_backgroundonly.append(mT_dict[key])
            weights_arrays_backgroundonly.append(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
            colors_keys_backgroundonly.append(colors[n])
            labels_keys_backgroundonly.append(label_map[key])
            B = B + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
        else:
            S = S + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        n = n+1
    
    Significance = S/math.sqrt(B + (alpha*B)**2)
    print("S, B=", S, B)
    print(("Significance =", Significance, "Mass=", mass))
    if Significance < 0.1:
        return -2, -2, Significance
    
    # create total signal and background histograms
    nbins = 2
    xmin_h = mass - 200.0
    xmax_h = mass + 20.0
    mT_background, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    mT_signal, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    for key in sorted(mT_dict.keys()):
        hist_key, edges = np.histogram(mT_dict[key],nbins, weights=np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]), range=(xmin_h,xmax_h))
        # background
        if int(key) < 0:
            mT_background = np.add( mT_background, hist_key)
        # signal
        else:
            mT_signal = np.add( mT_signal, hist_key)

    mT_total = np.add( mT_signal, mT_background )

    
    #print('mT_signal=', mT_signal)
    #print(mT_background)


    # calculate the bin mid-points from the edges
    bins = (edges[1:] + edges[:-1])/2
    #print('bins=', bins)
    
    ###################################
    # START PLOTTING AND FITTING HERE #
    ###################################


    #############################
    # TOTAL AND BACKGROUND PLOT #
    #############################
    if plotfits is True:
        plot_type = 'BDT_mT_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        xmin = xmin_h
        xmax = xmax_h
        ylab = "Number of Events per bin"
        xlab = '$m_{T}$ [GeV]'
        # number of bins
        n_bins = nbins

    
        # plot the total number of events with error bars
        ax.errorbar(bins, mT_total, yerr=np.sqrt(mT_signal), xerr=0., ms=5, capthick=20, fmt='bo', label='Signal+Background')
    
        # the colour map
        #print(color_map)

        # histogram the background
        ax.hist(mT_arrays_backgroundonly, n_bins, weights=weights_arrays_backgroundonly, density=False, histtype='bar', stacked=True, color=colors_keys_backgroundonly, range=(xmin_h,xmax_h), label=labels_keys_backgroundonly)
        #plt.step(bins, mT_background, ls='-', color='magenta', where='mid')

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
        #pl.rcParams.update({'font.size': 15})
        #pl.rcParams['figure.figsize'] = 12, 12

        # set axis ticks
        ax.xaxis.set_major_locator(MultipleLocator(20))
        ax.xaxis.set_minor_locator(MultipleLocator(5))
        #ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

        # set labels and limits
        ax.set_title('100 TeV/' + str(lumi) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)


    #############################
    # DO THE FIT                #
    #############################
    
    # calculate the poisson errors at 1 sigma
    def poisson_errors(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return up, down

    # calculate the poisson errors at 1 sigma and symmetrize
    def poisson_errors_symmetrize(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return (up+down)/2.


    def fit_poisson(Ntot, Nbkg, xbins, nfit):
        central_param_array = []
        out_array = []
        # do the fit nfit times:
        for ii in range(0,nfit):
            Nii_tot = []
            for it in range(0, len(Ntot)):
                Nii_tot.append(np.random.poisson(Ntot[it], 1)[0])
            Nii_bkg = []
            for it in range(0, len(Nbkg)):
                #print(Nbkg[it])
                Nii_bkg.append(np.random.poisson(Nbkg[it], 1)[0])
            Nii_tot = np.array(Nii_tot)
            Nii_bkg = np.array(Nii_bkg)
            Nii = np.subtract(Nii_tot, Nii_bkg)
            Nii[Nii < 0] = 0
            mod = VoigtModel()
            pars = mod.guess(Nii, x=bins)
            out = mod.fit(Nii, pars, x=bins)
            out_array.append(out)
            central_param_array.append(out.params['center'].value)

        n = len(central_param_array)
        index_med = n / 2 # median.
        index_84 = int(round(n * 0.84135)) # 84th percentile from median.
        index_16 = int(round(n * 0.15865))
        central_param_array = sorted(np.array(central_param_array))
        central_mean = np.mean(central_param_array)
        central_std = np.std(central_param_array)
        central_median = np.median(central_param_array)
        central_84 = central_param_array[index_84]
        central_16 = central_param_array[index_16]
        #return central_mean, central_std, central_median, central_84, central_16
        return central_median, central_84-central_median, central_median-central_16, central_mean, central_std, out_array
    
    # calculate the POISSON errors
    mT_total_poisson_up, mT_total_poisson_down = poisson_errors(np.add(mT_total,mT_background))
    mT_background_poisson_up, mT_background_poisson_down = poisson_errors(mT_background)
    mT_signal_poisson_up = np.sqrt( np.add(mT_total_poisson_up**2, mT_background_poisson_up**2))
    mT_signal_poisson_down = np.sqrt( np.add(mT_total_poisson_down**2, mT_background_poisson_down**2))
    di = 0
    for d in mT_signal_poisson_down:
        if mT_signal[di] - d < 0:
            mT_signal_poisson_down[di] = mT_signal[di]
        di = di + 1

    mT_signal_poisson_symmetric = 0.5 * np.add(mT_signal_poisson_up, mT_signal_poisson_down)
    #print('mT_signal_poisson_symmetric=', mT_signal_poisson_symmetric)
    
    # test the poisson fit:
    #print(mT_total)
    #print(mT_background)
    #central_median, bin_84, bin_16, central_mean, central_std, out_array = fit_poisson(mT_total, mT_background, bins, 100)
    #print('mass in', central_median, "+", bin_84, "-", bin_16)
    
    # do the fit 
    #Gaussian
    #modg = GaussianModel()
    #parsg = modg.guess(mT_signal, x=bins)
    #outg = modg.fit(mT_signal, parsg, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    #fitparamsg = outg.params
    #print(outg.fit_report(min_correl=0.25))
    #print(fitparamsg['center'].value, fitparamsg['sigma'].value)
    #print("reduced chisq Gauss =", outg.redchi)

    #Step
    #modstep = ConstantModel() + StepModel(form='erf')
    #parssv = modsv.guess(mT_signal, x=bins)
    #parsstep = modstep.make_params(c=1000, amplitude=-1000, center=300, sigma=5)
    #outstep = modstep.fit(mT_signal, parsstep, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    #fitparamsstep = outstep.params
    #print(outstep.fit_report(min_correl=0.25))
    #print(fitparamssv['center'].value, fitparamssv['sigma'].value)
    #print("reduced chisq Gauss =", outg.redchi)

    #Skewed Voigt
    #modsv = SkewedVoigtModel()
    #parssv = modsv.guess(mT_signal, x=bins)
    #outsv = modsv.fit(mT_signal, parssv, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    #fitparamssv = outsv.params
    #print(outsv.fit_report(min_correl=0.25))
    #print(fitparamssv['center'].value, fitparamssv['center'].stderr)
    #print("reduced chisq SV =", outsv.redchi)

    #Linear model
    modlin = LinearModel()
    parslin = modlin.guess(mT_signal,x=bins)
    outlin = modlin.fit(mT_signal, parslin, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    fitparamslin = outlin.params
    #print(outlin.fit_report(min_correl=0.25))


    #if outg.redchi < outsv.redchi:
    #    out = outg
    #    fitparams = fitparamsg
    #    fitkind = 'Gaussian'
    #else:
    #    out = outsv
    #    fitparams = fitparamssv
    #    fitkind = 'SkewedVoigt'

    #out = outstep
    #fitparams = fitparamsstep
    #fitkind = 'Step'

    out = outlin
    fitparams = fitparamslin
    fitkind = 'Linear'

    x0 = -fitparams['intercept'].value/fitparams['slope'].value
    sigma_x0 = x0 * math.sqrt( (fitparams['intercept'].stderr/fitparams['intercept'].value)**2 + (fitparams['slope'].stderr/fitparams['slope'].value)**2)
    
    #print("MASS FIT=", x0, sigma_x0)

    #print(popt)

    #############################
    # SIGNAL ONLY PLOT FOR FIT  #
    #############################
    if plotfits is True:
    
        plot_type = 'BDT_mT_signal_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        # plot the signal with error bars
        ax.errorbar(bins, mT_signal, yerr=[mT_signal_poisson_down, mT_signal_poisson_up], xerr=0., ms=5, capthick=20, fmt='bo', label='Signal after background subtraction')
        #ax.errorbar(bins, mT_signal_norm, yerr=mT_signal_poisson_symmetric_norm, xerr=0., ms=5, capthick=20, fmt='go', label='Signal after background subtraction')

        # plot the fit
        x = np.arange(xmin_h, xmax_h, 0.2)
        #for out_i in out_array:
        #    plt.plot(x, out_i.eval(x=x), '--', color='red', label=None)
    
        dely = out.eval_uncertainty(x=x)
        #plt.plot(x, out.eval(x=x), '--', color='green', label='Voigt fit with symm. Poisson')
        #plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)
    
        delyg = out.eval_uncertainty(x=x)
        plt.plot(x, out.eval(x=x), '--', color='red', label= fitkind + ' fit with symm. Poisson')
        plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)

        #plt.plot(bins, out.best_fit, '--', color='red', label='best fit')


        #insettext = "$pp \\rightarrow h_2 \\rightarrow h_1 h_1 \\rightarrow (b\\bar{b})(\\gamma\\gamma)$,\n$m_2^\\mathrm{fit} = " + str(round_sig(fitparams['center'].value,5)) + "\pm" + str(round_sig(fitparams['center'].stderr,2)) + "$ GeV"
        #plt.text(0.2, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=10)
    

        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # set labels and limits
        ax.set_title('100 TeV/' + str(int(lumi)) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)

    ####################
    ### RETURN HERE ####
    ####################    
    return  x0, sigma_x0, Significance

# find the bins where the largest drop occurs and subtract one
def find_largest_drop(mtdata):
    ii_max = -1
    ratio_max = 0.
    for ii in range(len(mtdata)-1):
        ratio = 0.
        if mtdata[ii+1] != 0.:
            ratio = mtdata[ii]/mtdata[ii+1]
            #print(ii, "ratio=", ratio)
        if ratio > ratio_max:
            ratio_max = ratio
            ii_max = ii
    massfit = int(ii_max)#-len(mtdata)/4)
    #print("ratio_max=", ratio_max, "for ratio", ii_max, 'returning', massfit)
    return massfit

def find_dropbin_error(mtdata, mtbins):
    nps = 400 # number of pseudo-experiments
    # perform the pseudo-experiments with mean the mtdata
    #print('mdata centr=', mtdata)
    results = []
    for n in range(nps):
        mtdata_rand = np.random.poisson(mtdata)
        results.append(mtbins[find_largest_drop(mtdata_rand)])
        #print('mtdata_rand=', mtdata_rand)
        #print('largest drop=', find_largest_drop(mtdata_rand), 'corresp. to' , mtbins[find_largest_drop(mtdata_rand)])
    results = np.array(results)
    results_mean = np.mean(results)
    results_std = np.std(results)
    binwidth = (mtbins[-1]-mtbins[0])/len(mtbins)
    total_error = math.sqrt(results_std**2 + binwidth**2)
    #print('results_mean, results_std, binwidth=', results_mean, results_std, binwidth)
    return results_mean, total_error
    
def get_mass_fit_ZZ2l2v(mass, lumi, xsec):
    colors = [ 'green', 'orange', 'red', 'magenta', 'blue', 'cyan', 'black', 'brown', 'violet'] # 9 colours

    # read in the root file containing the BDT output
    rootfile = "./HZZ100_2l2v/Herwig/events/BDT_output_" + str(int(mass)) + ".root"
    #print('reading in', rootfile)
    f = ROOT.TFile.Open(rootfile)
    tree = f.Get("Data3")
    nev = tree.GetEntries()
    #print('number of entries = ', nev)

    # Whether to do the plots
    plotfits = False
    
    # if the bdt cut was found
    BDT_cut_set = False
    
    alpha = 0.05 # systematic uncertainty
    
    # loop over the events and apply the BDT cut 
    mT = []
    weights = []
    mT_dict = {}
    weights_dict = {}
    event_id_array = []
    pass_id = {}
    total_id = {}
    pass_weight_id = {}
    total_weight_id = {}
    total_absweight_id = {}
    for evt in tree:
        if BDT_cut_set == False:
            BDT_cut = evt.bdtcut
            fac_signal = evt.fac_signal
            print('BDT cut =', BDT_cut, 'fac_signal=', fac_signal)
            BDT_cut_set = True
        #print(evt.event_id, evt.BDT_response, evt.mT, evt.wgt)
        if evt.event_id not in list(total_id.keys()):
            total_id[evt.event_id] =[ np.sign(evt.wgt)]
            total_weight_id[evt.event_id] =[ evt.wgt ]
            total_absweight_id[evt.event_id] =[ abs(evt.wgt) ]
            #print((evt.event_id, evt.wgt))
        else:
            total_id[evt.event_id].append(np.sign(evt.wgt))
            total_weight_id[evt.event_id].append(evt.wgt)
            total_absweight_id[evt.event_id].append(abs(evt.wgt))
        if evt.BDT_response > BDT_cut:
            if evt.event_id not in list(pass_id.keys()):
                pass_id[evt.event_id] =[ np.sign(evt.wgt)]
                pass_weight_id[evt.event_id] =[evt.wgt]
            else:
                pass_id[evt.event_id].append(np.sign(evt.wgt))
                pass_weight_id[evt.event_id].append(evt.wgt)
            if evt.event_id not in event_id_array:
                event_id_array.append(evt.event_id)
            mT.append(evt.mT)
            if evt.event_id not in list(mT_dict.keys()):
                mT_dict[evt.event_id] = [evt.mT]
                weights_dict[evt.event_id] = [evt.wgt]
            else:
                mT_dict[evt.event_id].append(evt.mT)
                weights_dict[evt.event_id].append(evt.wgt)
            weights.append(evt.wgt)



    # calculate the efficiency of the BDT cut as well as the total cross section for each sample
    efficiency_id = {}
    cross_section_id = {}
    cross_section_abs_id = {}

    # renormalize the weights to get the correct cross sections in the case of negative weights:
    negwF = {}
    cross_section_signal_sum = 0.
    for key in sorted(mT_dict.keys()):
        #cross_section_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        cross_section_id[key] = np.sum(total_absweight_id[key])/len(total_absweight_id[key]) # note that the absolute value of the cross section has to be used
        # calculate the total signal xsec:
        if int(key) > 0:
            cross_section_signal_sum = cross_section_signal_sum + cross_section_id[key]
        negwF[key] = cross_section_id[key] * len(total_absweight_id[key])/np.sum(total_weight_id[key]) # the factor to reweigh the events with
        total_weight_id[key] = np.array(total_weight_id[key]) * negwF[key] # fix the weights so that the sum of the weights = the total cross section
        pass_weight_id[key] = np.array(pass_weight_id[key]) * negwF[key] # fix the weights of the events that passed the BDT cut as well

    #print(('total signal cross section from BDT=', cross_section_signal_sum))
    #print(('total signal cross section in=', xsec))
    signal_amplification = xsec/cross_section_signal_sum
    #print(('signal_amplification=', signal_amplification))

    # print the reweighting factor
    #print(('RW factors=', negwF))
        
    # check the sum of the renormalized weights:
    cross_section_renorm_id = {}
    for key in sorted(mT_dict.keys()):
        cross_section_renorm_id[key] = np.sum(total_weight_id[key])/len(total_weight_id[key])
        efficiency_id[key] = np.sum(pass_weight_id[key])/np.sum(total_weight_id[key])
        

    #print("total cross sections:", cross_section_id)
    #print("total cross sections (renormalization check):", cross_section_renorm_id)
    #print("total cross sections (from abs):", cross_section_abs_id)
    #print("efficiencies of BDT cut:", efficiency_id)


    # define a label map
    label_map = {}
    label_map[2] = '$ gg \\rightarrow h_2 \\rightarrow ZZ \rightarrow (2\\ell) (2\\nu)$'
    label_map[1] = '$ pp \\rightarrow h_2 jj \\rightarrow  ZZ jj \\rightarrow  (2\\ell) (2\\nu) jj $'
    label_map[-1] = '$ pp \\rightarrow WZ $'
    label_map[-2] = '$ pp \\rightarrow 2\\ell 2\\nu$'
    label_map[-3] = '$ pp \\rightarrow Z\\nu\\nu$'
    # push the events that pass into arrays
    # counters for signal and background events:
    S = 0
    B = 0
    # arrays to store the events:
    mT_arrays = []
    mT_arrays_backgroundonly = []
    weights_arrays = []
    weights_arrays_backgroundonly = []
    color_map = {}
    colors_keys = []
    colors_keys_backgroundonly = []
    labels_keys = []
    labels_keys_backgroundonly = []
    Amplification = {} # contains the arrays of factors to modify the signal cross section
    n = 0
    #print('expected number of events at Lumi = ', lumi, 'fb^-1 after BDT cut for:')
    for key in sorted(mT_dict.keys()):
        #print(key)
        if int(key) > 0: # multiply the signal by a factor 
            Amplification[key] = signal_amplification
        else:
            Amplification[key] = 1.
        mT_arrays.append(mT_dict[key])
        weights_arrays.append(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        #print('\t', key, '=', np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key])))
        color_map[key] = colors[n]
        colors_keys.append(colors[n])
        labels_keys.append(label_map[key])
        if int(key) < 0:
            mT_arrays_backgroundonly.append(mT_dict[key])
            weights_arrays_backgroundonly.append(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
            colors_keys_backgroundonly.append(colors[n])
            labels_keys_backgroundonly.append(label_map[key])
            B = B + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
        else:
            S = S + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
        n = n+1
    
    Significance = S/math.sqrt(B + (alpha*B)**2)
    #print("S, B=", S, B)
    print(("Significance =", Significance, "Mass=", mass))
    if Significance < 0.1:
        return -2, -2, Significance
    
    # create total signal and background histograms
    nbins = 16
    xmin_h = mass - 100.0
    xmax_h = mass + 50.0
    print("xmin_h, xmax_h=", xmin_h, xmax_h)
    mT_background, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    mT_signal, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
    for key in sorted(mT_dict.keys()):
        hist_key, edges = np.histogram(mT_dict[key],nbins, weights=np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]), range=(xmin_h,xmax_h))
        # background
        if int(key) < 0:
            mT_background = np.add( mT_background, hist_key)
        # signal
        else:
            mT_signal = np.add( mT_signal, hist_key)

    mT_total = np.add( mT_signal, mT_background )

    
    #print('mT_signal=', mT_signal)
    #print(mT_background)
    dropbin = find_largest_drop(mT_signal)
    #print('find_largest_drop(mT_signal)=', dropbin)

    # calculate the bin mid-points from the edges
    bins = (edges[1:] + edges[:-1])/2
    #print('bins=', bins)
    #print('bins[dropbin]=', bins[dropbin])

    # the pseudo-exp:
    fit, dfit = find_dropbin_error(mT_signal, bins)
    
    ###################################
    # START PLOTTING AND FITTING HERE #
    ###################################


    #############################
    # TOTAL AND BACKGROUND PLOT #
    #############################
    if plotfits is True:
        plot_type = 'BDT_mT_ZZ2l2v_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        xmin = xmin_h
        xmax = xmax_h
        ylab = "Number of Events per bin"
        xlab = '$m_{T}$ [GeV]'
        # number of bins
        n_bins = nbins

    
        # plot the total number of events with error bars
        ax.errorbar(bins, mT_total, yerr=np.sqrt(mT_signal), xerr=0., ms=5, capthick=20, fmt='bo', label='Signal+Background')
    
        # the colour map
        #print(color_map)

        # histogram the background
        ax.hist(mT_arrays_backgroundonly, n_bins, weights=weights_arrays_backgroundonly, density=False, histtype='bar', stacked=True, color=colors_keys_backgroundonly, range=(xmin_h,xmax_h), label=labels_keys_backgroundonly)
        #plt.step(bins, mT_background, ls='-', color='magenta', where='mid')
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
        #pl.rcParams.update({'font.size': 15})
        #pl.rcParams['figure.figsize'] = 12, 12

        # set axis ticks
        ax.xaxis.set_major_locator(MultipleLocator(20))
        ax.xaxis.set_minor_locator(MultipleLocator(5))
        #ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

        # set labels and limits
        ax.set_title('100 TeV/' + str(lumi) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)


    #############################
    # DO THE FIT                #
    #############################
    
    # calculate the poisson errors at 1 sigma
    def poisson_errors(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return up, down

    # calculate the poisson errors at 1 sigma and symmetrize
    def poisson_errors_symmetrize(N):
        y = (1 - scipy.special.ndtr(1))
        a =N+np.array(1)
        up = (N-scipy.special.gammaincinv(a,y))
        down = (scipy.special.gammaincinv(a,1-y)-N)
        di = 0
        for d in down:
            if N[di] - d < 0:
                down[di] = N[di]
            di = di + 1
        return (up+down)/2.


    def fit_poisson(Ntot, Nbkg, xbins, nfit):
        central_param_array = []
        out_array = []
        # do the fit nfit times:
        for ii in range(0,nfit):
            Nii_tot = []
            for it in range(0, len(Ntot)):
                Nii_tot.append(np.random.poisson(Ntot[it], 1)[0])
            Nii_bkg = []
            for it in range(0, len(Nbkg)):
                #print(Nbkg[it])
                Nii_bkg.append(np.random.poisson(Nbkg[it], 1)[0])
            Nii_tot = np.array(Nii_tot)
            Nii_bkg = np.array(Nii_bkg)
            Nii = np.subtract(Nii_tot, Nii_bkg)
            Nii[Nii < 0] = 0
            mod = VoigtModel()
            pars = mod.guess(Nii, x=bins)
            out = mod.fit(Nii, pars, x=bins)
            out_array.append(out)
            central_param_array.append(out.params['center'].value)

        n = len(central_param_array)
        index_med = n / 2 # median.
        index_84 = int(round(n * 0.84135)) # 84th percentile from median.
        index_16 = int(round(n * 0.15865))
        central_param_array = sorted(np.array(central_param_array))
        central_mean = np.mean(central_param_array)
        central_std = np.std(central_param_array)
        central_median = np.median(central_param_array)
        central_84 = central_param_array[index_84]
        central_16 = central_param_array[index_16]
        #return central_mean, central_std, central_median, central_84, central_16
        return central_median, central_84-central_median, central_median-central_16, central_mean, central_std, out_array
    
    # calculate the POISSON errors
    mT_total_poisson_up, mT_total_poisson_down = poisson_errors(np.add(mT_total,mT_background))
    mT_background_poisson_up, mT_background_poisson_down = poisson_errors(mT_background)
    mT_signal_poisson_up = np.sqrt( np.add(mT_total_poisson_up**2, mT_background_poisson_up**2))
    mT_signal_poisson_down = np.sqrt( np.add(mT_total_poisson_down**2, mT_background_poisson_down**2))
    di = 0
    for d in mT_signal_poisson_down:
        if mT_signal[di] - d < 0:
            mT_signal_poisson_down[di] = mT_signal[di]
        di = di + 1

    mT_signal_poisson_symmetric = 0.5 * np.add(mT_signal_poisson_up, mT_signal_poisson_down)
    #print('mT_signal_poisson_symmetric=', mT_signal_poisson_symmetric)
    
    # test the poisson fit:
    #print(mT_total)
    #print(mT_background)
    #central_median, bin_84, bin_16, central_mean, central_std, out_array = fit_poisson(mT_total, mT_background, bins, 100)
    #print('mass in', central_median, "+", bin_84, "-", bin_16)
    
    # do the fit 
    #Gaussian
    #modg = GaussianModel()
    #parsg = modg.guess(mT_signal, x=bins)
    #outg = modg.fit(mT_signal, parsg, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    #fitparamsg = outg.params
    #print(outg.fit_report(min_correl=0.25))
    #print(fitparamsg['center'].value, fitparamsg['sigma'].value)
    #print("reduced chisq Gauss =", outg.redchi)

    #Skewed Voigt
    #modsv = SkewedVoigtModel()
    #parssv = modsv.guess(mT_signal, x=bins)
    #outsv = modsv.fit(mT_signal, parssv, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    #fitparamssv = outsv.params
    #print(outsv.fit_report(min_correl=0.25))
    #print(fitparamssv['center'].value, fitparamssv['center'].stderr)
    #print("reduced chisq SV =", outsv.redchi)

    #Linear model
    #modlin = LinearModel()
    #parslin = modlin.guess(mT_signal,x=bins)
    #outlin = modlin.fit(mT_signal, parslin, x=bins, scale_covar=False, weights=1/mT_signal_poisson_symmetric)
    #fitparamslin = outlin.params
    #print(outlin.fit_report(min_correl=0.25))


    #if outg.redchi < outsv.redchi:
    #    out = outg
    #    fitparams = fitparamsg
    #    fitkind = 'Gaussian'
    #else:
    #    out = outsv
    #    fitparams = fitparamssv
    #    fitkind = 'SkewedVoigt'

    #out = outstep
    #fitparams = fitparamsstep
    #fitkind = 'Step'

    #out = outg
    #fitparams = fitparamsg
    #fitkind = 'Gaussian'

    #x0 = -fitparams['intercept'].value/fitparams['slope'].value
    #sigma_x0 = x0 * math.sqrt( (fitparams['intercept'].stderr/fitparams['intercept'].value)**2 + (fitparams['slope'].stderr/fitparams['slope'].value)**2)
    
    #print("MASS FIT=", x0, sigma_x0)

    #print(popt)

    #############################
    # SIGNAL ONLY PLOT FOR FIT  #
    #############################
    if plotfits is True:
    
        plot_type = 'BDT_mT_ZZ2l2v_signal_' + str(int(mass))
        outputdirectory = './'
        fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

        # plot the signal with error bars
        ax.errorbar(bins, mT_signal, yerr=[mT_signal_poisson_down, mT_signal_poisson_up], xerr=0., ms=5, capthick=20, fmt='bo', label='Signal after background subtraction')
        #ax.errorbar(bins, mT_signal_norm, yerr=mT_signal_poisson_symmetric_norm, xerr=0., ms=5, capthick=20, fmt='go', label='Signal after background subtraction')
        #ax.arrow(fit, max(mT_signal)/3.0, 0, -max(mT_signal)/3.4, head_width=1.5, head_length=1, lw=3, fc='r', ec='r', shape='full') # 600
        # ax.arrow(fit, 1.5*max(mT_signal), 0, -0.4*max(mT_signal), head_width=2, head_length=3, lw=3, fc='r', ec='r', shape='full')
        ax.arrow(fit, 280., 0, -240., head_width=1.5, head_length=25, lw=3, fc='r', ec='r', shape='full') # 600

        # plot the fit
        #x = np.arange(xmin_h, xmax_h, 0.2)
        #for out_i in out_array:
        #    plt.plot(x, out_i.eval(x=x), '--', color='red', label=None)
    
        #dely = out.eval_uncertainty(x=x)
        #plt.plot(x, out.eval(x=x), '--', color='green', label='Voigt fit with symm. Poisson')
        #plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)
    
        #delyg = out.eval_uncertainty(x=x)
        #plt.plot(x, out.eval(x=x), '--', color='red', label= fitkind + ' fit with symm. Poisson')
        #plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)

        #plt.plot(bins, out.best_fit, '--', color='red', label='best fit')


        insettext = "$pp \\rightarrow h_2 \\rightarrow ZZ \\rightarrow (2\\ell)(2\\nu)$,\n$m_2^\\mathrm{fit} = " + str(round_sig(fit,4)) + "\pm" + str(round_sig(dfit,2)) + "$ GeV"
        plt.text(0.8, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=10)
    

        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

        # set labels and limits
        ax.set_title('100 TeV/' + str(int(lumi)) + ' fb$^{-1}$')
        ax.set_xlabel(xlab, fontsize=20)
        ax.set_ylabel(ylab, fontsize=20)
        plt.xlim([xmin, xmax])

        # save the figure
        #print('---')
        #print('saving the figure')
        fig.tight_layout()
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
        plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
        plt.close(fig)

    ####################
    ### RETURN HERE ####
    ####################    
    return fit, dfit, Significance

########################################################
############### TEST BEGINS HERE #######################
########################################################

TEST = True
    
if TEST is True:
    mass_in = 1000. # the known mass
    lumi_in = 30000
    xsec_in = 10./83.48632488418798# bring the signal up to the desired cross section  

    # run the fitter
    print("FIT=", get_mass_fit(mass_in, lumi_in, xsec_in))

    #print("FIT=", get_mass_fit_HH(mass_in, lumi_in, xsec_in))

    #print("FIT=", get_mass_fit_WW(mass_in, lumi_in, xsec_in))

    #print("FIT=", get_mass_fit_ZZ2l2v(mass_in, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(200, lumi_in, xsec_in))#

    #print("FIT=", get_mass_fit_ZZ2l2v(300, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(350, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(400, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(450, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(500, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(550, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(400, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(650, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(700, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(750, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(800, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(850, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(900, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(950, lumi_in, xsec_in))
    #print("FIT=", get_mass_fit_ZZ2l2v(1000, lumi_in, xsec_in))
