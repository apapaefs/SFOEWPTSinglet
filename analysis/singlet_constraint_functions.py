#! /usr/bin/env python
import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
#from matplotlib.mlab import griddata
import matplotlib.font_manager as fm
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
from numpy import linalg as LA
import operator
import collections
from prettytable import PrettyTable
from math import log10, floor
from scipy.stats import gaussian_kde
from matplotlib.ticker import LogLocator, AutoLocator, AutoMinorLocator
#from matplotlib.font_manager import _rebuild; _rebuild()
import pickle
from datetime import date
from matplotlib.colors import to_rgb, to_rgba
from matplotlib import rc
from matplotlib import patches
from scipy.spatial import ConvexHull, convex_hull_plot_2d
from scipy.interpolate import splprep, splev
from read_bdt_output_function import *

fm = matplotlib.font_manager.json_load("/Users/apapaefs/.matplotlib/fontList.json")
#fm.findfont("serif", rebuild_if_missing=False)
#fm.findfont("serif", fontext="afm", rebuild_if_missing=False)
#rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
#rc('text', usetex=True)

plt.rcParams['font.family'] = 'Times New Roman'


from labellines import labelLine, labelLines

from singlet_h2toh1h1h1 import Width_h2h1h1h1

from singlet_check_higgs_sigbound import *

from singlet_constraint_from_plots import *

from singlet_mass_eigenvalues import OneLoop_masses_diag
from singlet_mass_eigenvalues import OneLoop_masses_diag_scale
from singlet_mass_eigenvalues import OneLoop_masses_diag_scale_SARAH
from singlet_mass_eigenvalues import param_SARAH_to_MRM
from singlet_mass_eigenvalues import param_MRM_to_SARAH
from singlet_mass_eigenvalues import finetuning_oneloop

from singlet_EWPO import *

def round_sig(x, sig=2):
    if x == 0.:
        return 0.
    if math.isnan(x) is True:
        print('Warning, NaN!')
        return 0.
    return round(x, sig-int(floor(log10(abs(x))))-1)

# choose the next colour -- for plotting
colors = [ 'green', 'orange', 'red', 'magenta', 'blue', 'cyan', 'black', 'brown', 'violet'] # 9 colours

ccount = 0
def next_color():
    global ccount
    color_chosen = colors[ccount]
    if ccount < 8:
        ccount = ccount + 1
    else:
        ccount = 0    
    return color_chosen

# do not increment colour in this case:
def same_color():
    global ccount
    color_chosen = colors[ccount-1]
    return color_chosen

# reset the colour counter
def reset_color():
    global ccount
    ccount = 0

# change the colour scheme   
def change_color_scheme():
    global colors
    colors = ['violet', 'blue', 'green', 'orange', 'red', 'black', 'magenta', 'brown', 'cyan']

# change the colour scheme   
def change_color_scheme2():
    global colors
    colors = ['brown', 'cyan', 'green', 'orange', 'red', 'black', 'magenta', 'brown', 'cyan']

# choose the next colour -- for plotting
cmcount = 0
def next_cmap():
    global cmcount
    cmaps = ['Greens', 'Blues', 'Reds', 'Greys', 'Purples', 'PuRd', 'OrRd', 'PuBu', 'PuBuGn'] # 9 colour maps
    cmap_chosen = cmaps[cmcount]
    if cmcount < 8:
        cmcount = cmcount + 1
    else:
        cmcount = 0    
    return cmap_chosen

# do not increment colour in this case:
def same_cmap():
    global cmcount
    cmaps = ['Greens', 'Blues', 'Reds', 'Greys', 'Purples', 'PuRd', 'OrRd', 'PuBu', 'PuBuGn'] # 9 colour maps
    cmap_chosen = cmaps[cmcount-1]
    return cmap_chosen

def reset_cmap():
    global cmcount
    cmcount = 0

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)
    
# calculation of singlet scalar couplings and masses:

def lambda112(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda112 = v0 * ( a2 - 3 * lam ) * ct**2 * st - 0.5 * a2 * v0 * st**3 + 0.5 * ct * st**2 * ( -a1 - 2 * a2 * x0 + 2 * b3 + 6 * b4 * x0 ) + 0.25 * (a1 + 2 * a2 * x0 ) * ct**3
    return round_sig(lambda112,5)

def lambda122(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda122 =  v0 * ( 3 * lam - a2 ) * st**2 * ct + 0.5 * a2 * v0 * ct**3 + (b3 + 3 * b4 * x0 - 0.5 * a1 - a2 * x0 ) * st * ct**2 + 0.25 * ( a1 + 2 * a2 * x0 ) * st**3 
    return round_sig(lambda122,5)

def lambda111(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda111 = lam * v0 * ct**3 + (0.25 * a1 + 0.5 * a2 * x0) * ct**2 * st + 0.5 * a2 * v0 * st**2 * ct + (b3/3 + b4 * x0) * st**3
    return round_sig(lambda111,5)

def lambda222(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda222 = (4 * (b3 + 3 *b4 *x0) * ct**3 - 6 * a2 * v0 *ct**2 * st + 3 *(a1 + 2 *a2 *x0)* ct* st**2 - 12 * lam * v0 * st**3)/12.
    return round_sig(lambda222,5)

def lambda1111(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1111 = (lam* ct**4 + a2 * ct**2 * st**2 + b4 * st**4)/4.
    return round_sig(lambda1111,5)

def lambda1112(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1112 = -(1./4.) * (-b4+lam+(-a2+b4+lam) * (2 * ct**2 - 1) ) *2*ct*st
    return round_sig(lambda1112,5)

def lambda1122(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1122 = (1./16.)* (a2+3*(b4+lam)+3*(a2-b4-lam) * ((ct**2-st**2)**2 - (st*ct)**2))
    return round_sig(lambda1122,5)

def lambda1222(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda1222 = (1./4.) * (b4-lam+(-a2+b4+lam) * (ct**2 - st**2)) * st*ct
    return round_sig(lambda1222,5)

def lambda2222(lam, a1, a2, x0, v0, ct, st, b3, b4):
    lambda2222= (1./4.)*(b4 *ct**4 + a2 * ct**2 * st**2 + lam*st**4)
    return round_sig(lambda2222,5)

def mh_sq(lam, v0):
    return round_sig(2 * lam * v0**2,10)

def ms_sq(a1, b3, b4, x0, v0):
    return round_sig(b3 * x0 + 2 * b4 * x0**2 - a1 * v0**2 / 4. / x0,10)

def mhs_sq(a1, a2, x0, v0):
    return round_sig((a1 + 2 * a2 * x0 ) * v0/2.,10)


# calculate the width h2 -> h1 h1, given the mass, the self-coupling l112 (in GeV) and the sin(mixing angle)
def Gam_h2_to_h1h1(m1, m2, l112, sth):
  if m2 < 2*m1:
    return 0.
  width_h2h1h1 = l112**2 * math.sqrt( 1 - 4 * m1**2 / m2**2 ) / 8 / math.pi / m2
  return width_h2h1h1

# branching ratio RESCALING for h2 -> xx given Gamma_SM, sintheta, m1, m2, l112:
def RES_BR_h2_to_xx(sth, Gam_SM, m1, m2, l112):
    Gam_h2h1h1 = Gam_h2_to_h1h1(m1, m2, l112, sth)
    RES_h2xx = sth**2 * Gam_SM/ ( Gam_SM * sth**2  + Gam_h2h1h1)
    return RES_h2xx

# the BR h2 -> h1 h1, given the m2, sintheta, l112, Gam_SM (total SM BR)
def BR_h2_to_h1h1(sth, m1, m2, l112, Gam_SM):
    BRh2h1h1 = Gam_h2_to_h1h1(m1, m2, l112, sth) /  ( Gam_SM * sth**2  + Gam_h2_to_h1h1(m1, m2, l112, sth))
    return BRh2h1h1

def width_h2(sth, m1, m2, l112, Gam_SM):
    total_width = Gam_SM * sth**2  + Gam_h2_to_h1h1(m1, m2, l112, sth)
    return total_width


def mass_and_mixing(lam, v0, x0, a1, a2, b3, b4):
    # diagonalise mass matrix:
    MASS = np.array( [[mh_sq(lam, v0), mhs_sq(a1, a2, x0, v0)], [mhs_sq(a1, a2, x0, v0), ms_sq(a1, b3, b4, x0, v0)]] )
    w, v = LA.eig(MASS)
    mh2 = cmath.sqrt(w[0]).real
    mh1 = cmath.sqrt(w[1]).real
    if mh1 > mh2:
        mh2_pr = mh2
        mh2 = mh1
        mh1 = mh2_pr
    #print "mh2 = ", mh2, " mh1=", mh1
    sin2theta = (a1 + 2 * a2 * x0) * v0/(w[0] - w[1])
    cos2theta = math.sqrt(1. - sin2theta**2)
    costheta = math.sqrt( (1. + cos2theta)/2.)
    sintheta = math.sqrt(1. - costheta**2)
    return round_sig(mh1,5), round_sig(mh2,5), round_sig(sintheta,5), round_sig(costheta,5)



# function to read in the branching ratios into a dictionary in the format:
# mass [GeV] | H -> bbbar | H -> tautau | H -> mumu | H -> cc | H -> ss | H -> tt | H -> gg | H -> gammagamma | H -> Zgamma | H -> WW | H -> ZZ | total width [GeV]
# see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageBR2014#SM_Higgs_Branching_Ratios_and_Pa
def read_higgsBR(brfile):
    higgsbrs = {}
    brstream = open(brfile, 'r')
    brarray = []
    for line in brstream:
        brarray = [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4]), float(line.split()[5]), float(line.split()[6]), float(line.split()[7]), float(line.split()[8]), float(line.split()[9]), float(line.split()[10]), float(line.split()[11]), float(line.split()[12])]
        higgsbrs[float(line.split()[0])] = brarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsbrs.items()), key=operator.itemgetter(0))
    sorted_higgsbrs = collections.OrderedDict(sorted_x)
    return sorted_higgsbrs

# function to read in the XS into a dictionary in the format:
# mH (GeV) |	Cross Section (pb) | +QCD Scale  | -QCD Scale | +-(PDF+alphaS) | +-PDF +| -alphaS | 1+delta EW
# see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG#BSM_Higgs
def read_higgsXS_N2LONNLL(xsfile):
    higgsxss = {}
    xsstream = open(xsfile, 'r')
    xsarray = []
    for line in xsstream:
        xsarray = [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4]), float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
        higgsxss[float(line.split()[0])] = xsarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsxss.items()), key=operator.itemgetter(0))
    sorted_higgsxss = collections.OrderedDict(sorted_x)
    return sorted_higgsxss

# function to read in the XS into a dictionary in the format:
# mS or mH (GeV) | Cross Section (pb) |	+Theory | -Theory |	TH Gaussian | -+(PDF+alphaS)
# see https://twiki.cern.ch/twiki/bin/view/LHCPhysics/LHCHXSWG#BSM_Higgs
def read_higgsXS_N3LO(xsfile):
    higgsxss = {}
    xsstream = open(xsfile, 'r')
    xsarray = []
    for line in xsstream:
        xsarray = [ float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4]), float(line.split()[5])]
        higgsxss[float(line.split()[0])] = xsarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsxss.items()), key=operator.itemgetter(0))
    sorted_higgsxss = collections.OrderedDict(sorted_x)
    return sorted_higgsxss

# function to read in the XS into a dictionary in the format:
# mS or mH (GeV) | Cross Section (pb)
def read_higgsXS_ihixs(xsfile):
    higgsxss = {}
    xsstream = open(xsfile, 'r')
    xsarray = []
    for line in xsstream:
        xsarray = [ float(line.split()[1])]
        higgsxss[float(line.split()[0])] = xsarray
    # sort by increasing value of HYmass
    sorted_x = sorted(list(higgsxss.items()), key=operator.itemgetter(0))
    sorted_higgsxss = collections.OrderedDict(sorted_x)
    return sorted_higgsxss

# print out the Higgs BRs and total width:
def print_HiggsBR(brdict):
    for key in list(brdict.keys()):
        print(key, brdict[key])
    return 1

# create interpolators for the various BRs and total width and return a dictionary
def interpolate_HiggsBR(brdict):
  # the kind of interpolation
  interpkind = 'cubic'

  # define an array of interpolators
  interp_higgsbrs = []

  # find out how many BRs+width we have:
  values_view = list(brdict.values())
  value_iterator = iter(values_view)
  first_value = next(value_iterator)
  NBRs = len(first_value)
  
  # push back all the values of the masses, brs and width into arrays
  mass_array = []
  br_array =[[] for yy in range(NBRs)]

  # get the mass and the corresponding BR arrays
  for key in list(brdict.keys()):
      mass_array.append(key)
      for ii in range(NBRs):
        br_array[ii].append(brdict[key][ii])

  # now create the interpolators and put them in the array:
  for ii in range(NBRs):
        interpolator = interp1d(mass_array, br_array[ii], kind=interpkind, bounds_error=False)
        interp_higgsbrs.append(interpolator)

  return interp_higgsbrs

# create interpolators for the XS and return a dictionary
def interpolate_HiggsXS(xsdict):
  # the kind of interpolation
  interpkind = 'linear'

  # define an array of interpolators
  interp_higgsxss = []

  # find out how many BRs+width we have:
  values_view = list(xsdict.values())
  value_iterator = iter(values_view)
  first_value = next(value_iterator)
  NXSs = len(first_value)
  
  # push back all the values of the masses, brs and width into arrays
  mass_array = []
  xs_array =[]

  # get the mass and the corresponding BR arrays
  for key in list(xsdict.keys()):
      mass_array.append(key)
      xs_array.append(xsdict[key][0])

  # now create the interpolators and put them in the array:
  interp_higgsxss = interp1d(mass_array, xs_array, kind=interpkind, bounds_error=False)

  return interp_higgsxss

# use the given interpolator array to print out the values for a chosen mass
def get_interpolated_HiggsBR(input_mass, interp_array):
    interpolatedbrs = []
    for bb in range(len(interp_array)):
      interpolatedbrs.append(interp_array[bb](input_mass))
    return interpolatedbrs

# create interpolators for the various BRs and total width and return a dictionary
# RETURN the array in the form appropriate for plotting
def interpolate_HiggsBR_plot(brdict, input_mass_array):
  # the kind of interpolation
  interpkind = 'cubic'

  # define an array of interpolators
  interp_higgsbrs = []

  # find out how many BRs+width we have:
  values_view = list(brdict.values())
  value_iterator = iter(values_view)
  first_value = next(value_iterator)
  NBRs = len(first_value)
  
  # push back all the values of the masses, brs and width into arrays
  mass_array = []
  br_array =[[] for yy in range(NBRs)]

  # get the mass and the corresponding BR arrays
  for key in list(brdict.keys()):
      mass_array.append(key)
      for ii in range(NBRs):
        br_array[ii].append(brdict[key][ii])

  # now create the interpolators and put them in the array:
  for ii in range(NBRs):
        interpolator = interp1d(mass_array, br_array[ii], kind=interpkind, bounds_error=False)
        interp_higgsbrs.append(interpolator(input_mass_array))
  return interp_higgsbrs


def convert_to_heavy(interpolators_SM, lam, v0, x0, a1, a2, b3, b4):
    heavyBRs = []
    # get the masses and mixing angle
    mh1, mh2, sintheta, costheta = mass_and_mixing(lam, v0, x0, a1, a2, b3, b4)
    #print 'Masses and mixing angles:'
    #print 'mh1, mh2=', mh1, mh2
    #print 'sintheta, costheta=', sintheta, costheta
    # get the corresponding SM width from the interpolator (this should be the last element):
    Gamma_SM = interpolators_SM[-1](mh2)
    # get the lambda112 coupling
    l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
    # loop over the SM BRs and rescale with the factor:
    for hh in range(len(interpolators_SM)-1):
        heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
    # add the h1h1 decay:
    heavyBRs.append(BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM))
    # add the total heavy Higgs width:
    heavyBRs.append(width_h2(sintheta, mh1, mh2, l112, Gamma_SM))
    return heavyBRs


def convert_to_heavy_withtripleHiggs(interpolators_SM, name, lam, v0, x0, a1, a2, b3, b4):
    heavyBRs = []
    # get the masses and mixing angle
    mh1, mh2, sintheta, costheta = mass_and_mixing(lam, v0, x0, a1, a2, b3, b4)
    # get the corresponding SM width from the interpolator (this should be the last element):
    if mh2 < 1000.: # 
        Gamma_SM = interpolators_SM[-1](mh2)
    else:
        Gamma_SM = interpolators_SM[-1](1000.)
        print("WARNING: mh2 > 1000.! (tree level)")
    # get the lambda couplings
    #print 'Masses and mixing angles:'
    #print 'mh1, mh2=', mh1, mh2
    #print 'sintheta, costheta=', sintheta, costheta
    #l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    #l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    #l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    #l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    #print 'l111, l112, l122, l1112=', l111, l112, l122, l1112

    # get the total heavy Higgs width excluding the triple Higgs mode:
    # note that this already implies that decay width into triple Higgs << total width
    if mh1 > 80.:
        Gam1 = costheta**2 * interpolators_SM[-1](mh1) # WARNING: this is the width of the SM Higgs!
    else:
        print('WARNING, mh1 < 80.! (tree level)')
        Gam1 = interpolators_SM[-1](80.0)
    
    Gam2 = width_h2(sintheta, mh1, mh2, l112, Gamma_SM)

    print('Gam1, Gam2=', round_sig(Gam1,5), round_sig(Gam2,5))
    
    width_hhh = Width_h2h1h1h1(l1112, l111, l112, l122, mh1, mh2, Gam1, Gam2)

    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
    # loop over the SM BRs and rescale with the factor:
    for hh in range(len(interpolators_SM)-1):
        heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
    # add the h1h1 decay:
    BR_hh = BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM)
    heavyBRs.append(BR_hh)

    # add the h1h1h1 decay (estimate!):
    BR_tripleHiggs = width_hhh/Gam2
    heavyBRs.append(BR_tripleHiggs)

    # add the total heavy Higgs width:
    heavyBRs.append(width_h2(sintheta, mh1, mh2, l112, Gamma_SM))
    
    return heavyBRs, round_sig(mh1,5), round_sig(mh2,2), round_sig(Gam1,5), round_sig(Gam2,5), sintheta, costheta, round_sig(BR_hh,5), name

def calculate_heavy_BRs_only(interpolators_SM, mh2, l112, sintheta):
    heavyBRs = []
    # fix the SM Higgs mass
    mh1 = 125.1
    if mh2 < 1000.: # 
        Gamma_SM = interpolators_SM[-1](mh2)
    else:
        Gamma_SM = interpolators_SM[-1](1000.)
        print("WARNING: mh2 > 1000.! (tree level)")
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
    # loop over the SM BRs and rescale with the factor:
    for hh in range(len(interpolators_SM)-1):
        if mh2 < 1000.:
            heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
        else:
            heavyBRs.append(interpolators_SM[hh](1000.) * rescale_fac)
    # add the h1h1 decay:
    BR_hh = BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM)
    heavyBRs.append(BR_hh)
    # add the h1h1h1 decay (DON'T DO THIS HERE):
    BR_tripleHiggs = 0.
    heavyBRs.append(BR_tripleHiggs)

    # add the total heavy Higgs width:
    heavyBRs.append(width_h2(sintheta, mh1, mh2, l112, Gamma_SM))
    
    return heavyBRs

#(BR_interpolators_SM, key, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta)
def convert_to_heavy_withtripleHiggs_OneLoop(interpolators_SM, name, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, costheta, calculate_triple=False):
    heavyBRs = []
    # get the masses and mixing angle

    #mh1, mh2, sintheta, costheta = mass_and_mixing(lam, v0, x0, a1, a2, b3, b4)
    lam, v0, x0, a1, a2, b2, b3, b4 = param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)

    mh1, mh2 = OneLoop_masses_diag_scale_SARAH(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    # get the corresponding SM width from the interpolator (this should be the last element):
    if mh2 < 1000.: # 
        Gamma_SM = interpolators_SM[-1](mh2)
    else:
        Gamma_SM = interpolators_SM[-1](1000.)
        print("WARNING: mh2 > 1000.! (tree level)")
    # get the lambda couplings
    #print 'Masses and mixing angles:'
    #print 'mh1, mh2=', mh1, mh2
    #print 'sintheta, costheta=', sintheta, costheta
    l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    #print 'l111, l112, l122, l1112=', l111, l112, l122, l1112

    # get the total heavy Higgs width excluding the triple Higgs mode:
    # note that this already implies that decay width into triple Higgs << total width
    if mh1 > 80.:
        Gam1 = costheta**2 * interpolators_SM[-1](mh1) # WARNING: this is the width of the SM Higgs!
    else:
        print('WARNING, mh1 < 80.! (tree level)')
        Gam1 = interpolators_SM[-1](80.0)
    
    Gam2 = width_h2(sintheta, mh1, mh2, l112, Gamma_SM)

    #print 'Gam1, Gam2=', round_sig(Gam1,5), round_sig(Gam2,5)

    # FOR NOW EXCLUDE
    if mh2 > 3*mh1 and calculate_triple is True:
        width_hhh = Width_h2h1h1h1(l1112, l111, l112, l122, mh1, mh2, Gam1, Gam2)
    else:
        width_hhh = 0.

    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
    # loop over the SM BRs and rescale with the factor:
    for hh in range(len(interpolators_SM)-1):
        if mh2 < 1000.:
            heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
        else:
            heavyBRs.append(interpolators_SM[hh](1000.) * rescale_fac)
    # add the h1h1 decay:
    BR_hh = BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM)
    heavyBRs.append(BR_hh)

    # add the h1h1h1 decay (estimate!):
    BR_tripleHiggs = width_hhh/Gam2
    heavyBRs.append(BR_tripleHiggs)

    # add the total heavy Higgs width:
    heavyBRs.append(width_h2(sintheta, mh1, mh2, l112, Gamma_SM))
    
    return heavyBRs, round_sig(mh1,5), round_sig(mh2,5), round_sig(Gam1,5), round_sig(Gam2,5), round_sig(BR_hh,5), name

# function that takes in the sintheta, lambda112, mh1 and an mh2 array and returns the array of BRs for the Heavy Higgs
def calc_HeavyHiggsBRs(interpolators_SM, mh1, mh2array, sintheta, l112):
    heavyBRs_array = []
    for mh2 in mh2array:
        heavyBRs = []
        # get the corresponding SM width from the interpolator (this should be the last element):
        if mh2 < 1000.: # 
            Gamma_SM = interpolators_SM[-1](mh2)
        else:
            Gamma_SM = interpolators_SM[-1](1000.)
            # get the rescaling factor of the SM BRs:
        rescale_fac = RES_BR_h2_to_xx(sintheta, Gamma_SM, mh1, mh2, l112)
        # loop over the SM BRs and rescale with the factor:
        for hh in range(len(interpolators_SM)-1):
            heavyBRs.append(interpolators_SM[hh](mh2) * rescale_fac)
        # add the h1h1 decay:
        BR_hh = BR_h2_to_h1h1(sintheta, mh1, mh2, l112, Gamma_SM)
        heavyBRs.append(BR_hh)
        heavyBRs_array.append(heavyBRs)
    # transpose the array to get it into the right form for plotting
    heavyBRs_array = np.transpose(heavyBRs_array)
    return heavyBRs_array

# set the parameters given the benchmark name and dictionary
def set_xsm_params(benchmark_name, benchmark_array):
    if benchmark_name in list(benchmark_array.keys()):
        x0 = benchmark_array[benchmark_name][4]
        lam = benchmark_array[benchmark_name][5]
        a1 = benchmark_array[benchmark_name][6]
        a2 = benchmark_array[benchmark_name][7]
        b3 = benchmark_array[benchmark_name][8]
        b4 = benchmark_array[benchmark_name][9]
    else:
        print(benchmark_name, 'is not in array of benchmarks')
        exit()
    return x0, lam, a1, a2, b3, b4

# set the parameters given the benchmark name and dictionary
def set_xsm_params_OneLoop(benchmark_name, benchmark_array):
    if benchmark_name in list(benchmark_array.keys()):
        sintheta = benchmark_array[benchmark_name][1]
        costheta = benchmark_array[benchmark_name][0]
        x0 = benchmark_array[benchmark_name][4]
        lam = benchmark_array[benchmark_name][5]
        a1 = benchmark_array[benchmark_name][6]
        a2 = benchmark_array[benchmark_name][7]
        b3 = benchmark_array[benchmark_name][8]
        b4 = benchmark_array[benchmark_name][9]
    else:
        print(benchmark_name, 'is not in array of benchmarks')
        exit()
    return x0, lam, a1, a2, b3, b4, sintheta, costheta


# print the heavy Higgs info:
def print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy_triple, textinfo):
    print(textinfo)
    tbl = PrettyTable(["process", "BR"])
    for idx in range(len(HeavyHiggsBRs)):
      tbl.add_row([BR_text_array_heavy_triple[idx].replace('$', ''), round_sig(HeavyHiggsBRs[idx],5)])
    print(tbl)
    BRsum_heavy = 0.000
    for bb in HeavyHiggsBRs:
        if HeavyHiggsBRs.index(bb) != len(HeavyHiggsBRs)-1:
            BRsum_heavy = BRsum_heavy + round_sig(bb, 3)
    print('consistency test: sum(BRs)=', round_sig(BRsum_heavy,5))
    print('\n')


# print the current parameter point info:
def print_xsm_params(v0, x0, a1, a2, b3, b4):
    print('\ntest point params:')
    param_names = [ 'v0', 'x0', 'a1', 'a2', 'b3', 'b4' ]
    tbl = PrettyTable(["param", "value"])
    tbl.add_row([param_names[0], v0])
    tbl.add_row([param_names[1], x0])
    tbl.add_row([param_names[2], a1])
    tbl.add_row([param_names[3], a2])
    tbl.add_row([param_names[4], b3])
    tbl.add_row([param_names[5], b4])
    print(tbl)
    print('\n')

# print the current parameter point info:
def print_xsm_params_and_masses(lam, v0, x0, a1, a2, b3, b4, mh1, mh2, mh1_1loop, mh2_1loop):
    print('\ntest point params:')
    param_names = ['lam', 'v0', 'x0', 'a1', 'a2', 'b3', 'b4', 'mh1(tree)', 'mh2(tree)', 'mh1(1loop)', 'mh2(1loop)' ]
    tbl = PrettyTable(["param", "value"])
    tbl.add_row([param_names[0], lam])
    tbl.add_row([param_names[1], v0])
    tbl.add_row([param_names[2], x0])
    tbl.add_row([param_names[3], a1])
    tbl.add_row([param_names[4], a2])
    tbl.add_row([param_names[5], b3])
    tbl.add_row([param_names[6], b4])

    tbl.add_row([param_names[7], mh1])
    tbl.add_row([param_names[8], mh2])
    tbl.add_row([param_names[9], mh1_1loop])
    tbl.add_row([param_names[10], mh2_1loop])
    print(tbl)
    print('\n')


# print the current parameter point info:
def print_SARAH_params_and_masses(v0, x0, mu2, MS, K1, K2, Kappa, LambdaS, Lambda, sintheta, mh1_1loop, mh2_1loop):
    print('\ntest point params (SARAH):')
    param_names = ['v0', 'x0', 'mu2', 'MS', 'K1', 'K2', 'Kappa', 'LambdaS', 'Lambda', 'sintheta', 'mh1(1loop)', 'mh2(1loop)' ]
    tbl = PrettyTable(["param", "value"])
    tbl.add_row([param_names[0], v0])
    tbl.add_row([param_names[1], x0])
    tbl.add_row([param_names[2], mu2])
    tbl.add_row([param_names[3], MS])
    tbl.add_row([param_names[4], K1])
    tbl.add_row([param_names[5], K2])
    tbl.add_row([param_names[6], Kappa])
    tbl.add_row([param_names[7], LambdaS])
    tbl.add_row([param_names[8], Lambda])
    tbl.add_row([param_names[9], sintheta])
    tbl.add_row([param_names[10], mh1_1loop])
    tbl.add_row([param_names[11], mh2_1loop])

    print(tbl)
    print('\n')

def print_XSECS_AND_BRS(xs13_n3lo, xs27_n3lon3ll, xs100_n3lon3ll, BR_ZZ, BR_WW, BR_hh):
    print('\nXS and BRs:')
    param_names = ['xs13', 'xs27', 'xs100', 'BR(ZZ)', 'BR(WW)', 'BR(hh)']
    tbl = PrettyTable(["param", "value"])
    tbl.add_row([param_names[0], xs13_n3lo])
    tbl.add_row([param_names[1], xs27_n3lon3ll])
    tbl.add_row([param_names[2], xs100_n3lon3ll])
    tbl.add_row([param_names[3], BR_ZZ])
    tbl.add_row([param_names[4], BR_WW])
    tbl.add_row([param_names[5], BR_hh])
    print(tbl)
    print('\n')


def print_point_results(HB_result_array, HS_result_array, xsm_point_info, xsm_point_constraints):
    #column_text = [ '#', 'name', 'mh1', 'mh2', 'Gh1', 'Gh2', 'stheta', 'ctheta', 'BR(h2->h1h1)', 'HB res.', 'HS res.', 'ATLAS+CMS mu', 'EWPO (curr.)', 'EWPO (fut.)', 'XS13(mh2)[pb]', 'XS14(mh2)[pb]', 'XS8(mh2)[pb]', 'XS7(mh2)[pb]' ]
    column_text = [ '#', 'name', 'mh1', 'mh2', 'Gh1', 'Gh2', 'stheta', 'ctheta', 'BR(h2->h1h1)', 'HB res.', 'HS res.', 'sth^2 < 0.05', 'sth^2 < 0.01', 'EWPO (curr.)', 'EWPO (fut.)', 'ATLAS HH', 'CMS HH', 'HL-LHC ZZ', 'HL-LHC WW', 'ATLAS HH XTR', 'CMS HH XTR', 'CLIC HH(1.4TeV)', 'CLIC VV(3TeV)', 'CLIC HH(3TeV)', 'FCC WW', 'FCC ZZ' ]
    tbl = PrettyTable(column_text)
    for p in range(len(xsm_point_info)):
        # xsm point info array
        name =  str(xsm_point_info[p][0])
        mh1 = str(xsm_point_info[p][1])
        mh2 = str(xsm_point_info[p][2])
        G1 = str(xsm_point_info[p][3])
        G2 = str(xsm_point_info[p][4])
        st = str(xsm_point_info[p][5])
        ct = str(xsm_point_info[p][6])
        BR_hh = str(xsm_point_info[p][7])
        xs13 = str(xsm_point_info[p][8])
        xs14 = str(xsm_point_info[p][9])
        xs8 = str(xsm_point_info[p][10])
        xs7 = str(xsm_point_info[p][11])

        # HiggsBounds/HiggsSignals results
        HB = str(HB_result_array[p])
        HS = str(HS_result_array[p])

        couplstr_cur = str(xsm_point_constraints[p][0])
        couplstr_fut = str(xsm_point_constraints[p][1])
        EWPOcurr = str( round_sig(xsm_point_constraints[p][2], 4))
        EWPOfut = str(round_sig(xsm_point_constraints[p][3], 5))
        ATLAS_HH = str(xsm_point_constraints[p][4])
        CMS_HH = str(xsm_point_constraints[p][5])
        HLLHC_ZZ = str(xsm_point_constraints[p][6])
        HLLHC_WW = str(xsm_point_constraints[p][7])
        ATLAS_HH_extrap = str(xsm_point_constraints[p][8])
        CMS_HH_extrap = str(xsm_point_constraints[p][9])
        CLIC14_HH_pass = str(xsm_point_constraints[p][10])
        CLIC_VV_pass = str(xsm_point_constraints[p][11])
        CLIC3_HH_pass = str(xsm_point_constraints[p][12])
        FCC_WW = str(xsm_point_constraints[p][13])
        FCC_ZZ = str(xsm_point_constraints[p][14])

        
        tbl.add_row([p+1, name, mh1, mh2, G1, G2, st, ct, BR_hh, HB, HS, couplstr_cur, couplstr_fut, EWPOcurr, EWPOfut, ATLAS_HH, CMS_HH, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap, CLIC14_HH_pass, CLIC_VV_pass, CLIC3_HH_pass, FCC_WW, FCC_ZZ])
    print(tbl)

# convert benchmarks to the right format
def ConvertBenchmarks(OurBenchmark):

    #{246, xsol, Re[mu2sol], Re[MStrial], Re[K1trial], Re[K2trial], Re[\[Kappa]trial], Re[LambdaStrial], Re[\[Lambda]sol], Re[sin\[Theta]]}]
    v0_SARAH = OurBenchmark[0]
    x0_SARAH = OurBenchmark[1]
    mu2_SARAH = OurBenchmark[2]
    MS_SARAH = OurBenchmark[3]
    K1_SARAH = OurBenchmark[4]
    K2_SARAH = OurBenchmark[5]
    Kappa_SARAH = OurBenchmark[6]
    LambdaS_SARAH = OurBenchmark[7]
    Lambda_SARAH = OurBenchmark[8]
    sintheta = OurBenchmark[9]
    #print 'sintheta =', sintheta
    costheta = math.sqrt(1 - sintheta**2)

    #lam, v0, x0, a1, a2, b3, b4
    
    lamConv, v0Conv, x0Conv, a1Conv, a2Conv, b2Conv, b3Conv, b4Conv = param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)

    # OneLoop_masses_diag_scale(Sc, lam, v0, x0, a1, a2, b3, b4, g1, g2, yt)
    mh1_1loop, mh2_1loop = OneLoop_masses_diag_scale_SARAH(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    
    #[ costh, sinth, mass2, gam2,  x0  , lamb, a_1, , a_2  , b_3   , b_4]
    ConvertedBenchmark = [costheta, sintheta, mh2_1loop, -1.0, x0Conv, lamConv, a1Conv, a2Conv, b3Conv, b4Conv]
    return ConvertedBenchmark

# get the SARAH parameters from the benchmark array
# and also the value of the rho(max) = Phi_c / T_c (max) 
def GetSARAHParams(OurBenchmark):
    v0_SARAH = OurBenchmark[0]
    x0_SARAH = OurBenchmark[1]
    mu2_SARAH = OurBenchmark[2]
    MS_SARAH = OurBenchmark[3]
    K1_SARAH = OurBenchmark[4]
    K2_SARAH = OurBenchmark[5]
    Kappa_SARAH = OurBenchmark[6]
    LambdaS_SARAH = OurBenchmark[7]
    Lambda_SARAH = OurBenchmark[8]
    sintheta = OurBenchmark[9]
    return v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta
    
def ReadGWBenchmarks(BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    BenchmarkDictionary = {}
    firstline = True
    counter = 0
    for line in BenchmarkFileStream:
        # skip the first line:
        if firstline is True:
            firstline = False
            continue
        previous_string = ""
        for el in line.split(','):
            # skip over empty strings:
            if len(el.strip()) == 0:
                continue
            current_string = el.replace('{', '').replace(',', '').replace('}','').replace('*I', 'j').replace('*^','E').replace(' ', '').strip()
            if current_string[-1] == "-" or current_string[-1] == "+":
                previous_sring = current_string
                continue
            if previous_string != "":
                current_string = previous_string + current_string
                #print "FOUND BREAKING LINE", current_string
                previous_string = ""
            entry = complex(current_string).real
            #print entry
            # start if there is a "{"
            if '{' in el:
                BenchmarkDictionary[Tag + str(counter)] = []
            #print 'appending', entry, 'to', Tag + str(counter)
            BenchmarkDictionary[Tag + str(counter)].append(entry)
            # stop if a "}" is found
            if '}' in el:
                counter = counter + 1
            #print el.replace('{', '').replace(',', '').replace('}',''),
            if counter == MaxPoints: # break at maximum points
                return BenchmarkDictionary
    return BenchmarkDictionary

# read the benchmarks processed by PhaseTracer
def ReadProcessedBenchmarks(BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    BenchmarkDictionary = {}
    counter = 0
    for line in BenchmarkFileStream:
        #print line
        point = [float(line.rstrip().split(',')[12]), float(line.rstrip().split(',')[13])] + [float(x) for x in line.rstrip().split(',')[:7]] + [float(line.rstrip().split(',')[14])] + [float(line.rstrip().split(',')[15])] + [float(line.rstrip().split(',')[16])] + [float(line.rstrip().split(',')[17])] 
        #print Tag + str(counter), point
        BenchmarkDictionary[Tag + str(counter)] = point
        counter = counter + 1
        if counter == MaxPoints: # break at maximum points
            BenchmarkFileStream.close()
            return BenchmarkDictionary
    BenchmarkFileStream.close()
    return BenchmarkDictionary

# add to the array the benchmarks processed by PhaseTracer
def AddProcessedBenchmarks(BenchmarkDictionary, BenchmarkFile, Tag, MaxPoints):
    BenchmarkFileStream = open(BenchmarkFile, "r")
    counter = int(len(BenchmarkDictionary))
    initial_counter = int(len(BenchmarkDictionary))
    for line in BenchmarkFileStream:
        #print line
        point = [float(line.rstrip().split(',')[12]), float(line.rstrip().split(',')[13])] + [float(x) for x in line.rstrip().split(',')[:7]] + [float(line.rstrip().split(',')[14])] + [float(line.rstrip().split(',')[15])] + [float(line.rstrip().split(',')[16])] + [float(line.rstrip().split(',')[17])] 
        #print Tag + str(counter), point
        BenchmarkDictionary[Tag + str(counter)] = point
        counter = counter + 1
        reduced_counter = counter-initial_counter
        if reduced_counter == MaxPoints: # break at maximum points
            BenchmarkFileStream.close()
            return BenchmarkDictionary
    BenchmarkFileStream.close()
    return BenchmarkDictionary

# scatter plot with changing markers
def mscatter(x,y,z=None,ax=None, m=None, plot_kind="scatter", colmap='magma', colmaplab='', colb=False, **kw):
    import matplotlib.markers as mmarkers
    if not ax: ax=plt.gca()
    if plot_kind == "scatter":
        sc = ax.scatter(x,y,rasterized=True, **kw)
        if (m is not None) and (len(m)==len(x)):
            paths = []
            for marker in m:
                if isinstance(marker, mmarkers.MarkerStyle):
                    marker_obj = marker
                else:
                    marker_obj = mmarkers.MarkerStyle(marker)
                path = marker_obj.get_path().transformed(
                    marker_obj.get_transform())
                paths.append(path)
            sc.set_paths(paths)
    elif plot_kind == "density":
        # Calculate the point density
        x = np.array(x)
        y = np.array(y)
        xy = np.vstack([x,y])
        z = gaussian_kde(xy)(xy)
        # Sort the points by density, so that the densest points are plotted last
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]
        sc = ax.scatter(x, y, c=z, s=50, edgecolor='none', cmap=colmap, rasterized=True)
        plt.colorbar(sc)
    elif plot_kind == "3D":
        if z == None:
            print('Error, z must be provided!')
            exit()
        else:
            sc = ax.scatter(x, y, c=z, s=50, edgecolor='none', cmap=colmap, rasterized=True)
            if colb is True:
                cbar = plt.colorbar(sc)
                cbar.set_label(colmaplab, rotation=270, fontsize=20, labelpad=20)
    return sc

# check the sign of sintheta: from the tree-level sign of sin2theta:
def tree_sign_sintheta(v0, x0, a1, a2, m1, m2):
    sin2theta = (a1 + 2 * a2 * x0) * v0/(m1**2 - m2**2)
    sign_sin2theta = np.sign(sin2theta)
    return sign_sin2theta

# write out data into a pickle file
def write_datfile(outputfile, dictionary):
    outputstream = open(outputfile, 'bw')
    pickle.dump(dictionary, outputstream, protocol=pickle.HIGHEST_PROTOCOL)
    outputstream.close()
    
# read in data from a pickle file
def read_datfile(inputfile):
    inputstream = open(inputfile, 'br')
    histogram = pickle.load(inputstream)
    return histogram

def ConvertMRMBenchmarkToGWAPBenchmark(Benchmark):
    #     0      1      2      3    4     5     6      7      8       9
    # [ costh, sinth, mass2, gam2, x0  , lamb, a_1, , a_2  , b_3   , b_4]
    sintheta = Benchmark[1]
    a1 = Benchmark[6]
    a2 = Benchmark[7]
    b3 = Benchmark[8]
    b4 = Benchmark[9]
    lam = Benchmark[5]
    x0 = Benchmark[4]
    musqr_MRM = lam * v0**2 + (a1 + a2 * x0) * x0/2. # this parameter is constrained by the others, see https://arxiv.org/pdf/1605.06123.pdf eq. 2
    mu2 = - musqr_MRM
    b2 = -b3 * x0 - b4 * x0**2 - a1 * v0**2 / 4. / x0 - a2 * v0**2 /2. # this parameter is constrained by the others, see https://arxiv.org/pdf/1605.06123.pdf eq. 3
    MS = b2
    K1 = 0.5 * a1
    K2 = a2
    Lambda = 2. * lam
    LambdaS = 0.5 * b4
    Kappa = b3
    return 246., x0, mu2, MS, K1, K2, Kappa, LambdaS, Lambda, sintheta, 1.0, 1.0, 1.0
    

#######################################################
# SETTINGS AND PARAMETERS #
#######################################################

# the SM value of the self-coupling and Higgs boson mass in [GeV]
v0 = 246.
MH = 125.0

BR_text_array = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$\\Gamma$' ]
BR_plot_or_not = [ True, True, False, False, False, True, False, True, False, True, True, False ]

BR_text_array_heavy = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$h_1 h_1$', '$\\Gamma$' ]
BR_plot_or_not_heavy = [ False,          False,           False,         False,         False,         True,          False,  False,               False,         True,   True,   True, False]


BR_text_array_heavy_withtripleHiggs = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$h_1 h1$', '$h_1 h_1 h_1$', '$\\Gamma$' ]

#######################################################
# SETUP THE INTERPOLATORS AND CONSTRAINT CALCULATORS  #
#######################################################

# the file containing the branching ratios for the SM Higgs boson:
BR_file = "higgsBR_YR4.txt"
# read the file:
print('reading in', BR_file)
HiggsBRs = read_higgsBR(BR_file)
# test: print the dictionary
# print_HiggsBR(HiggsBRs)
# first get the interpolated BRs and SM width 
BR_interpolators_SM = interpolate_HiggsBR(HiggsBRs)  # this returns the actual interpolators
# now for plotting over a range of values:
mass_plot_array = np.arange(80, 2000, 1.) 
BR_interpolation_array_for_plot = interpolate_HiggsBR_plot(HiggsBRs, mass_plot_array)

# the 13 TeV ggF cross sections at N^3LO
XS13_file = "higgsXS_YR4_13TeV_N3LO.txt"
print('reading in', XS13_file)
HiggsXS_13_N3LO = read_higgsXS_N3LO(XS13_file)
# get the interpolated XS
XS_interpolator_SM_13TeV_N3LO = interpolate_HiggsXS(HiggsXS_13_N3LO)

# the 13 TeV ggF cross sections at NNLO+NNLL
XS13_file = "higgsXS_YR4_13TeV_NNLONNLL.txt"
print('reading in', XS13_file)
HiggsXS_13_NNLONNLL = read_higgsXS_N3LO(XS13_file)
# get the interpolated XS
XS_interpolator_SM_13TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_13_NNLONNLL)

# the 14 TeV, 8 TeV and 7 TeV cross sections at NNLO+NNLL:
XS14_file = "higgsXS_YR4_14TeV_N2LONNLL.txt"
HiggsXS_14_N2LONNLL = read_higgsXS_N2LONNLL(XS14_file)
XS_interpolator_SM_14TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_14_N2LONNLL)
XS8_file = "higgsXS_YR4_8TeV_N2LONNLL.txt"
HiggsXS_8_N2LONNLL = read_higgsXS_N2LONNLL(XS8_file)
XS_interpolator_SM_8TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_8_N2LONNLL)
XS7_file = "higgsXS_YR4_7TeV_N2LONNLL.txt"
HiggsXS_7_N2LONNLL = read_higgsXS_N2LONNLL(XS7_file)
XS_interpolator_SM_7TeV_NNLONNLL = interpolate_HiggsXS(HiggsXS_7_N2LONNLL)

# the 100 TeV cross section from ihixs:
XS100_file = "higgsXS_ihixs_100TeV_N3LON3LL.txt"
HiggsXS_100_N3LON3LL = read_higgsXS_ihixs(XS100_file)
XS_interpolator_SM_100TeV_N3LON3LL = interpolate_HiggsXS(HiggsXS_100_N3LON3LL)

# 100 TeV VBF cross section at NLO from MadGraph:
XS100_VBF_file = "higgsXS_VBF_100TeV_NLO.txt"
HiggsXS_100_VBF_NLO = read_higgsXS_ihixs(XS100_VBF_file)
XS_interpolator_SM_100TeV_VBF_NLO = interpolate_HiggsXS(HiggsXS_100_VBF_NLO)

# 27 TeV cross section from ihixs:
XS27_file = "higgsXS_ihixs_27TeV_N3LON3LL.txt"
HiggsXS_27_N3LON3LL = read_higgsXS_ihixs(XS27_file)
XS_interpolator_SM_27TeV_N3LON3LL = interpolate_HiggsXS(HiggsXS_27_N3LON3LL)

# 27 TeV cross section to 100 TeV cross section RATIO from ihixs (GLUON FUSION):
XS27_RATIO_file = "higgsXS_RATIO_ihixs_27to100TeV_N3LON3LL.txt"
RATIO_27_N3LON3LL = read_higgsXS_ihixs(XS27_RATIO_file)
RATIO_interpolator_SM_27TeV_N3LON3LL = interpolate_HiggsXS(RATIO_27_N3LON3LL)

# 27 TeV cross section to 100 TeV cross section RATIO from ihixs (GLUON FUSION):
XS27_QQ_RATIO_file = "higgsXS_RATIO_QQ_27to100TeV_N3LON3LL.txt"
RATIO_QQ_27_N3LON3LL = read_higgsXS_ihixs(XS27_QQ_RATIO_file)
RATIO_QQ_interpolator_SM_27TeV_N3LON3LL = interpolate_HiggsXS(RATIO_QQ_27_N3LON3LL)


# consistency check for mh = 125.:
#BRsum_125 = 0.
#BR_125 = []
#for ii in range(len(BR_interpolators_SM)-1):
#    BRsum_125 = BRsum_125 + BR_interpolators_SM[ii](125.)
#    BR_125.append(BR_interpolators_SM[ii](125.))
#print 'BRs for mh=125 GeV=', BR_125
#print 'Sum(SM BRs at mh=125 GeV)=', BRsum_125

# test a mass:
#print 'BRs at 317.5', get_interpolated_HiggsBR(317.5, BR_interpolation_array)


# ELECTROWEAK PRECISION OBSERVABLES:
# the GFITTER CURRENT central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 11
# central values:
Delta_S_central = 0.06
Delta_T_central = 0.10
# errors:
errS = 0.09
errT = 0.07
# correlation (rho_12):
covST=0.91
# the GFITTER FUTURE central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 13, Table 3
# central values:
Delta_S_central_F = 0.00
Delta_T_central_F = 0.00
# errors:
errS_F = math.sqrt(0.017**2 + 0.006**2) # experimental + theoretical errors in quadrature
errT_F = math.sqrt(0.022**2 + 0.005**2) 
# correlation (rho_12):
covST_F=0.91
# get the functions required for the EWPO chi-squared
from singlet_EWPO import *


# PLOTS FOR EXCLUSION PLOTS FROM ANALYSES:
# get the interpolators

# ATLAS WW (36.1/fb):
ATLAS_WW_central, ATLAS_WW_1sigma, ATLAS_WW_2sigma, ATLAS_WW_process_tag = read_digit_plot("Fig5_1710.01123", "ATLAS-WW") # ok up to 4 TeV
# CMS ZZ (35.9/fb):
CMS_ZZ_central, CMS_ZZ_1sigma, CMS_ZZ_2sigma, CMS_ZZ_process_tag = read_digit_plot("Fig9_CMS-HIG-17-012-PAS", "CMS-ZZ") # ok up to 3 TeV
# HL-LHC ZZ:
HLLHC_ZZ_central, HLLHC_ZZ_1sigma, HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag = read_digit_plot("Fig173_1902.00134", "HLLHC-ZZ") # ok up to 3 TeV
# HL-LHC WW:
HLLHC_WW_central, HLLHC_WW_1sigma, HLLHC_WW_2sigma, HLLHC_WW_process_tag = read_digit_plot("Fig5_ATL-PHYS-PUB-2018-022", "HLLHC-WW") # ok up to 3 TeV
# CURRENT HH (ATLAS):
ATLAS_HH_central, ATLAS_HH_process_tag = read_digit_plot("Fig5a_1906.02025", "ATLAS-HH") # ok up to 3 TeV
# CURRENT HH (CMS):
CMS_HH_central, CMS_HH_process_tag = read_digit_plot("Fig3_1811.09689", "CMS-HH") # ok up to 3 TeV
# use these interpolators to get the limit for any given m2 as followS:
# limit_m2_test_central, limit_m2_test_1sigma, limit_m2_test_2sigma, process_tag = get_xsec_limit(m2_test, zz_central, zz_1sigma, zz_2sigma, process_tag)

# plots with kappa-m2 (kappa -> sintheta in the H -> VV case, kappa -> sintheta * BR(hh) in the H -> hh case)
# from 1807.04284
# CLIC H -> ZZ/WW (use BR=1):
CLIC_VV, CLIC_VV_tag = read_digit_plot_km2("Fig7_1807.04284", "CLIC-VV") # ok up to 1 TeV
# CLIC H -> hh 1.4 TeV (90% b-tagging):
CLIC14_HH, CLIC14_HH_tag = read_digit_plot_km2("Fig9orangedashed_1807.04284", "CLIC14-HH") # ok up to 1 TeV
# CLIC H -> hh 3 TeV (90% b-tagging):
CLIC3_HH, CLIC3_HH_tag = read_digit_plot_km2("Fig9bluedashed_1807.04284", "CLIC3-HH") # ok up to 1 TeV


SMEARTAG = True


# 100 TeV H -> WW
FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag = read_digit_plot("data_HWW100_L30000.0_signif2.0", "FCC-WW", smear=SMEARTAG)
# 100 TeV 30/ab DISCOVERY H -> WW
FCC_WW_DISC, FCC_WW_DISC_tag = read_digit_plot("data_dis_HWW100_L30000.0_signif5", "FCC-WW-5", smear=SMEARTAG)

# 100 TeV H -> ZZ
FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag = read_digit_plot_multichannel(["data_HZZ100_4l_L30000.0_signif2.0", "data_HZZ100_2l2v_L30000.0_signif2.0", "data_HZZ100_2l2q_L30000.0_signif2.0"], "FCC-ZZ", smear=SMEARTAG)
# 100 TeV 30/ab DISCOVERY H -> ZZ
FCC_ZZ_DISC, FCC_ZZ_DISC_tag = read_digit_plot_multichannel_nosigmas(["data_dis_HZZ100_4l_L30000.0_signif5", "data_dis_HZZ100_2l2v_L30000.0_signif5", "data_dis_HZZ100_2l2q_L30000.0_signif5"], "FCC-ZZ-DISC", smear=SMEARTAG)

# 100 TeV H -> hh
FCC_HH_central, FCC_HH_1sigma, FCC_HH_2sigma, FCC_HH_tag = read_digit_plot("data_HH100_BBGG_L30000.0_signif2.0", "FCC-HH", smear=SMEARTAG)
# 100 TeV 30/ab DISCOVERY H -> hh
FCC_HH_DISC, FCC_HH_DISC_tag = read_digit_plot("data_dis_HH100_BBGG_L30000.0_signif5", "FCC-HH-5", smear=SMEARTAG)


# FCC DIFFERENT LUMINOSITIES:
Lumis = [100., 200., 500., 1000., 2000., 5000., 10000., 15000., 20000., 25000., 30000.]
NSIGMA = 2.0 # EXCLUSION
FCC_WW_lumi = {}
FCC_WW_lumi_tag = {}
FCC_HH_lumi = {}
FCC_HH_lumi_tag = {}
FCC_ZZ_lumi = {}
FCC_ZZ_lumi_tag = {}
for Lumi in Lumis:
    # 100 TeV H -> WW
    CurrentAnalysis = "HWW100"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi] = read_digit_plot(lumifile, "FCC-WW-" + str(Lumi),  smear=SMEARTAG)
    # 100 TeV H -> hh
    CurrentAnalysis = "HH100_BBGG"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi] = read_digit_plot(lumifile, "FCC-HH-" + str(Lumi),  smear=SMEARTAG)
    # 100 TeV H -> ZZ
    lumifilez1 =  "data_HZZ100_4l_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    lumifilez2 =  "data_HZZ100_2l2q_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    lumifilez3 =  "data_HZZ100_2l2v_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar"
    FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi] = read_digit_plot_multichannel_nosigmas([lumifilez1, lumifilez2, lumifilez3], "FCC-ZZ" + str(Lumi),  smear=SMEARTAG)

# 27 TeV DIFFERENT LUMINOSITIES AND GGF/QQ:
NSIGMA = 2.0 # EXCLUSION
HE_WW_lumi_GGF = {}
HE_WW_lumi_GGF_tag = {}
HE_ZZ_lumi_GGF = {}
HE_ZZ_lumi_GGF_tag = {}
HE_HH_lumi_GGF = {}
HE_HH_lumi_GGF_tag = {}
HE_WW_lumi_QQ = {}
HE_WW_lumi_QQ_tag = {}
HE_ZZ_lumi_QQ = {}
HE_ZZ_lumi_QQ_tag = {}
HE_HH_lumi_QQ = {}
HE_HH_lumi_QQ_tag = {}

for Lumi in Lumis:
    # 27 TeV GGF H -> WW
    CurrentAnalysis = "HWW100"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVGGF"
    print(lumifile)
    HE_WW_lumi_GGF[Lumi], HE_WW_lumi_GGF_tag[Lumi] = read_digit_plot(lumifile, "HE-WW-" + str(Lumi),  smear=SMEARTAG)
    # 27 TeV GGF H -> hh
    CurrentAnalysis = "HH100_BBGG"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVGGF"
    print(lumifile)
    HE_HH_lumi_GGF[Lumi], HE_HH_lumi_GGF_tag[Lumi] = read_digit_plot(lumifile, "HE-HH-" + str(Lumi),  smear=SMEARTAG)
    # 27 TeV GGF H -> ZZ
    lumifilez1 =  "data_HZZ100_4l_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVGGF"
    lumifilez2 =  "data_HZZ100_2l2q_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVGGF"
    lumifilez3 =  "data_HZZ100_2l2v_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVGGF"
    HE_ZZ_lumi_GGF[Lumi], HE_ZZ_lumi_GGF_tag[Lumi] = read_digit_plot_multichannel_nosigmas([lumifilez1, lumifilez2, lumifilez3], "HE-ZZ" + str(Lumi),  smear=SMEARTAG)

    # 27 TeV QQ H -> WW
    CurrentAnalysis = "HWW100"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVQQ"
    print(lumifile)
    HE_WW_lumi_QQ[Lumi], HE_WW_lumi_QQ_tag[Lumi] = read_digit_plot(lumifile, "HE-WW-" + str(Lumi),  smear=SMEARTAG)
    # 27 TeV QQ H -> hh
    CurrentAnalysis = "HH100_BBGG"
    lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVQQ"
    print(lumifile)
    HE_HH_lumi_QQ[Lumi], HE_HH_lumi_QQ_tag[Lumi] = read_digit_plot(lumifile, "HE-HH-" + str(Lumi),  smear=SMEARTAG)
    # 27 TeV QQ H -> ZZ
    lumifilez1 =  "data_HZZ100_4l_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVQQ"
    lumifilez2 =  "data_HZZ100_2l2q_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVQQ"
    lumifilez3 =  "data_HZZ100_2l2v_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVQQ"
    HE_ZZ_lumi_QQ[Lumi], HE_ZZ_lumi_QQ_tag[Lumi] = read_digit_plot_multichannel_nosigmas([lumifilez1, lumifilez2, lumifilez3], "HE-ZZ" + str(Lumi),  smear=SMEARTAG)

# store the info for the constraints, including the HiggsSignals/HiggsBounds run:
xsm_point_info = []
xsm_point_constraints = []


# get g1 and g2 fro mz, mw and v0
#g2 = 2 * mw / v0
#g1 = 2 / v0 * math.sqrt(mz**2 - mw**2)
#print('g1, g2=', g1, g2)
mt = 172.44 # PDG as of 2019
#yt = mt*math.sqrt(2)/v0
Mw = 80.385
Mz = 91.1876
Mt = 172.44
Gf = 1.1663787E-5;
v0 = math.sqrt(1/math.sqrt(2.)*(1/Gf));
yt = mt/v0 * math.sqrt(2.);
g0sq = 4. * math.sqrt(2.) * Gf * Mw**2;
g1sq = g0sq * (Mz**2 - Mw**2)/Mw**2
g1 = math.sqrt(g1sq)
g2 = math.sqrt(g0sq)

print('g1, g2, yt', g1, g2, yt)

from singlet_finetuning import *

# get today's date
today = date.today()
todaysdate = today.strftime("%d%m%Y")

# prepare the mass fitting from m4l in HZZ100_4l:
# read in the available masses:
m4l_resultsFile = './HZZ100_4l/HZZ100_4l_results.smear.temp.txt'
m4l_resultsStream = open(m4l_resultsFile, 'r')
m4l_masses = []
for line in m4l_resultsStream:
    m4l_masses.append(float(line.split()[0]))
m4l_masses = sorted(m4l_masses) # sort
print('Available masses for m4l fitting:', m4l_masses)
m4l_resultsStream.close()
# get the total cross sections as they were written out (corresponding to the GGF and VBF processes for the masses above):
# !THESE ARE IN PICOBARN!
m4l_xsecsFile = 'HZZ100_4l/HZZ100_4l_InitialSignalXsecs.smear.temp.txt'
m4l_xsecsStream = open(m4l_xsecsFile, 'r')
m4l_xsecsdict = {}
for line in m4l_xsecsStream:
    m4l_xsecsdict[line.split()[0]] = float(line.split()[1])*1000 # !!!!convert to FB!!!!
print('Available process cross sections for m4l fitting:', m4l_xsecsdict)
# sum up the VBF and GGF cross sections and create a dictionary for the available masses:
m4l_xsecsummed_dict = {}
for key in m4l_xsecsdict:
    mkey = key.replace('pp-eta0jj-ZZ-4l-evmuv-M','').replace('gg-eta0-ZZ-4l-M','')
    if float(mkey) not in list(m4l_xsecsummed_dict.keys()):
        m4l_xsecsummed_dict[float(mkey)] = m4l_xsecsdict[key]
    else:
        m4l_xsecsummed_dict[float(mkey)] = m4l_xsecsummed_dict[float(mkey)] + m4l_xsecsdict[key]
print('summed up cross sections:', [ (key, m4l_xsecsummed_dict[key]) for key in sorted(m4l_xsecsummed_dict.keys())])

BRZZ4l = 4*3.3632E-2**2 # the BR(ZZ->4l)
# define the function for the m4l mass fitting:
def get_m4l_fit(m2tofit, lumi, sintheta_real, l112_real, signif_cutoff, BR_interpolators_SM):
    # determine where the M1 and M2 in the formula for the linear interpolation
    # i.e. in which between which two of the available masses does m2tofit fall according to m4l_masses:
    if m2tofit > m4l_masses[-1] or m2tofit < m4l_masses[0]:
        print('WARNING:', m2tofit, 'outside avalable masses range for m4l fit!')
        return -2, -2
    for im in range(0,len(m4l_masses)-1):
     if m2tofit >= m4l_masses[im] and m2tofit < m4l_masses[im+1]:
         Mlo = m4l_masses[im]
         Mhi = m4l_masses[im+1]
    #print 'm2 to fit falls within', Mlo, Mhi
    # calculate the X variable:
    X = (m2tofit - Mlo)/(Mhi-Mlo)
    #print 'X=', X
    heavyBRs_lo = calculate_heavy_BRs_only(BR_interpolators_SM, Mlo, l112_real, sintheta_real)
    heavyBRs_hi = calculate_heavy_BRs_only(BR_interpolators_SM, Mhi, l112_real, sintheta_real)

    BRZZ_hi = heavyBRs_hi[10]
    BRZZ_lo = heavyBRs_lo[10]

    xs100_n3lon3ll_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mhi),5)
    xs100_vbf_nlo_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mhi),5)
    xs100_n3lon3ll_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mlo),5)
    xs100_vbf_nlo_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mlo),5)

    xs100_hi_ZZ4l = (xs100_n3lon3ll_hi + xs100_vbf_nlo_hi)*BRZZ4l*BRZZ_hi*1000 # !!!!CONVERT TO FB!!!!
    xs100_lo_ZZ4l = (xs100_n3lon3ll_lo + xs100_vbf_nlo_lo)*BRZZ4l*BRZZ_lo*1000 # !!!!CONVERT TO FB!!!!
    #print '(xs100_lo, xs100_hi)*BRZZ*ZZ->4l=', xs100_lo*BRZZ*BRZZ4l, xs100_hi*BRZZ*BRZZ4l

    # call the python3 function:
    #read_bdt_output_call_lo = 'python3.9 read_bdt_output_function.py ' + str(Mlo) + ' ' + str(lumi) + ' ' + str(xs100_lo_ZZ4l)
    #print(read_bdt_output_call_lo)
    #p = subprocess.Popen(read_bdt_output_call_lo, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
        #print line
    #    if b'MASS FIT' in line:
    #        Mlo_fit = float(line.split()[2])
    #        DMlo_fit = float(line.split()[3])
        #if 'Significance' in line:
        #    print line
    Mlo_fit, DMlo_fit, Significance_lo = get_mass_fit(Mlo, lumi, xs100_lo_ZZ4l)
            
    # call the python3 function:
    #read_bdt_output_call_hi = 'python3.9 read_bdt_output_function.py ' + str(Mhi) + ' ' + str(lumi) + ' ' + str(xs100_hi_ZZ4l)
    #print(read_bdt_output_call_hi)
    #p = subprocess.Popen(read_bdt_output_call_hi, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
    #    #print line
    #    if b'MASS FIT' in line:
    #        Mhi_fit = float(line.split()[2])
    #        DMhi_fit = float(line.split()[3])
    #    if b'Significance' in line:
    #        print(line)
    Mhi_fit, DMhi_fit, Significance_hi = get_mass_fit(Mhi, lumi, xs100_hi_ZZ4l)

    print('Mhi_fit = ', Mhi_fit, '+-', DMhi_fit)
    print('Mlo_fit = ', Mlo_fit, '+-', DMlo_fit)
    
    if Significance_lo < signif_cutoff or Significance_hi < signif_cutoff:
        return -2, -2


    M_fit = X * (Mhi_fit - Mlo_fit) + Mlo_fit
    DM_fit = X * DMhi_fit + (1-X) * DMlo_fit
    #print('M_fit = ', M_fit, '+-', DM_fit)
    
    return M_fit, DM_fit

# prepare the mass fitting from mbbaa in HH100_BBGG:
# read in the available masses:
mbbaa_resultsFile = './HH100_BBGG/HH100_BBGG_results.smear.temp.txt'
mbbaa_resultsStream = open(mbbaa_resultsFile, 'r')
mbbaa_masses = []
for line in mbbaa_resultsStream:
    mbbaa_masses.append(float(line.split()[0]))
mbbaa_masses = sorted(mbbaa_masses) # sort
print('Available masses for mbbaa fitting:', mbbaa_masses)
mbbaa_resultsStream.close()
# get the total cross sections as they were written out (corresponding to the GGF and VBF processes for the masses above):
# !THESE ARE IN PICOBARN!
mbbaa_xsecsFile = 'HH100_BBGG/HH100_BBGG_InitialSignalXsecs.smear.temp.txt'
mbbaa_xsecsStream = open(mbbaa_xsecsFile, 'r')
mbbaa_xsecsdict = {}
for line in mbbaa_xsecsStream:
    mbbaa_xsecsdict[line.split()[0]] = float(line.split()[1])*1000 # !!!!convert to FB!!!!
print('Available process cross sections for mbbaa fitting:', mbbaa_xsecsdict)
# sum up the VBF and GGF cross sections and create a dictionary for the available masses:
mbbaa_xsecsummed_dict = {}
for key in mbbaa_xsecsdict:
    mkey = key.replace('pp-eta0jj-hh-bbaa-M','').replace('gg-eta0-hh-bbaa-M','')
    if float(mkey) not in list(mbbaa_xsecsummed_dict.keys()):
        mbbaa_xsecsummed_dict[float(mkey)] = mbbaa_xsecsdict[key]
    else:
        mbbaa_xsecsummed_dict[float(mkey)] = mbbaa_xsecsummed_dict[float(mkey)] + mbbaa_xsecsdict[key]
print('summed up cross sections:', [ (key, mbbaa_xsecsummed_dict[key]) for key in sorted(mbbaa_xsecsummed_dict.keys())])

BRhhbbaa = 2*0.569369*0.00229 # the BR(hh->bbaa)
# define the function for the mbbaa mass fitting:
def get_mbbaa_fit(m2tofit, lumi, sintheta_real, l112_real, signif_cutoff, BR_interpolators_SM):
    # determine where the M1 and M2 in the formula for the linear interpolation
    # i.e. in which between which two of the available masses does m2tofit fall according to mbbaa_masses:
    if m2tofit > mbbaa_masses[-1] or m2tofit < mbbaa_masses[0]:
        print('WARNING:', m2tofit, 'outside avalable masses range for mbbaa fit!')
        return -2, -2
    for im in range(0,len(mbbaa_masses)-1):
     if m2tofit >= mbbaa_masses[im] and m2tofit < mbbaa_masses[im+1]:
         Mlo = mbbaa_masses[im]
         Mhi = mbbaa_masses[im+1]
    #print 'm2 to fit falls within', Mlo, Mhi
    # calculate the X variable:
    X = (m2tofit - Mlo)/(Mhi-Mlo)
    #print 'X=', X
    Mlo_fix = Mlo
    if Mlo == 250:
        Mlo_fix = 251.
    heavyBRs_lo = calculate_heavy_BRs_only(BR_interpolators_SM, Mlo_fix, l112_real, sintheta_real)
    heavyBRs_hi = calculate_heavy_BRs_only(BR_interpolators_SM, Mhi, l112_real, sintheta_real)

    BRhh_hi = heavyBRs_hi[11]
    BRhh_lo = heavyBRs_lo[11]

    xs100_n3lon3ll_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mhi),5)
    xs100_vbf_nlo_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mhi),5)
    xs100_n3lon3ll_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mlo),5)
    xs100_vbf_nlo_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mlo),5)

    xs100_hi_hhbbaa = (xs100_n3lon3ll_hi + xs100_vbf_nlo_hi)*BRhhbbaa*BRhh_hi*1000 # !!!!CONVERT TO FB!!!!
    xs100_lo_hhbbaa = (xs100_n3lon3ll_lo + xs100_vbf_nlo_lo)*BRhhbbaa*BRhh_lo*1000 # !!!!CONVERT TO FB!!!!
    #print '(xs100_lo, xs100_hi)*BRZZ*ZZ->bbaa=', xs100_lo*BRZZ*BRZZbbaa, xs100_hi*BRZZ*BRZZbbaa

    # call the python3 function:
    #read_bdt_output_call_lo = 'python3.9 read_bdt_output_function.py ' + str(Mlo) + ' ' + str(lumi) + ' ' + str(xs100_lo_ZZbbaa)
    #print(read_bdt_output_call_lo)
    #p = subprocess.Popen(read_bdt_output_call_lo, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
        #print line
    #    if b'MASS FIT' in line:
    #        Mlo_fit = float(line.split()[2])
    #        DMlo_fit = float(line.split()[3])
        #if 'Significance' in line:
        #    print line
    Mlo_fit, DMlo_fit, Significance_lo = get_mass_fit_HH(Mlo, lumi, xs100_lo_hhbbaa)
            
    # call the python3 function:
    #read_bdt_output_call_hi = 'python3.9 read_bdt_output_function.py ' + str(Mhi) + ' ' + str(lumi) + ' ' + str(xs100_hi_ZZbbaa)
    #print(read_bdt_output_call_hi)
    #p = subprocess.Popen(read_bdt_output_call_hi, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
    #    #print line
    #    if b'MASS FIT' in line:
    #        Mhi_fit = float(line.split()[2])
    #        DMhi_fit = float(line.split()[3])
    #    if b'Significance' in line:
    #        print(line)
    Mhi_fit, DMhi_fit, Significance_hi = get_mass_fit_HH(Mhi, lumi, xs100_hi_hhbbaa)

    if Significance_lo < signif_cutoff or Significance_hi < signif_cutoff:
        return -2, -2

    print('Mhi_fit = ', Mhi_fit, '+-', DMhi_fit)
    print('Mlo_fit = ', Mlo_fit, '+-', DMlo_fit)

    M_fit = X * (Mhi_fit - Mlo_fit) + Mlo_fit
    DM_fit = X * DMhi_fit + (1-X) * DMlo_fit
    #print('M_fit = ', M_fit, '+-', DM_fit)
    
    return M_fit, DM_fit

# prepare the mass fitting from mT in HWW100:
# read in the available masses:
mT_resultsFile = './HWW100/HWW100_results.smear.temp.txt'
mT_resultsStream = open(mT_resultsFile, 'r')
mT_masses = []
for line in mT_resultsStream:
    mT_masses.append(float(line.split()[0]))
mT_masses = sorted(mT_masses) # sort
print('Available masses for mT fitting:', mT_masses)
mT_resultsStream.close()
# get the total cross sections as they were written out (corresponding to the GGF and VBF processes for the masses above):
# !THESE ARE IN PICOBARN!
mT_xsecsFile = 'HWW100/HWW100_InitialSignalXsecs.smear.temp.txt'
mT_xsecsStream = open(mT_xsecsFile, 'r')
mT_xsecsdict = {}
for line in mT_xsecsStream:
    mT_xsecsdict[line.split()[0]] = float(line.split()[1])*1000 # !!!!convert to FB!!!!
print('Available process cross sections for mT fitting:', mT_xsecsdict)
# sum up the VBF and GGF cross sections and create a dictionary for the available masses:
mT_xsecsummed_dict = {}
for key in mT_xsecsdict:
    mkey = key.replace('pp-eta0jj-WW-evmuv-M','').replace('gg-eta0-WW-evmuv-M','')
    if float(mkey) not in list(mT_xsecsummed_dict.keys()):
        mT_xsecsummed_dict[float(mkey)] = mT_xsecsdict[key]
    else:
        mT_xsecsummed_dict[float(mkey)] = mT_xsecsummed_dict[float(mkey)] + mT_xsecsdict[key]
print('summed up cross sections:', [ (key, mT_xsecsummed_dict[key]) for key in sorted(mT_xsecsummed_dict.keys())])

BRWWevmv = 2*(10.86E-2)**2# the BR(WW->evmuv)
# define the function for the mT mass fitting:
def get_mT_fit(m2tofit, lumi, sintheta_real, l112_real, signif_cutoff, BR_interpolators_SM):
    # determine where the M1 and M2 in the formula for the linear interpolation
    # i.e. in which between which two of the available masses does m2tofit fall according to mT_masses:
    if m2tofit > mT_masses[-1] or m2tofit < mT_masses[0]:
        print('WARNING:', m2tofit, 'outside avalable masses range for mT fit!')
        return -2, -2
    for im in range(0,len(mT_masses)-1):
     if m2tofit >= mT_masses[im] and m2tofit < mT_masses[im+1]:
         Mlo = mT_masses[im]
         Mhi = mT_masses[im+1]
    #print 'm2 to fit falls within', Mlo, Mhi
    # calculate the X variable:
    X = (m2tofit - Mlo)/(Mhi-Mlo)
    #print 'X=', X

    heavyBRs_lo = calculate_heavy_BRs_only(BR_interpolators_SM, Mlo, l112_real, sintheta_real)
    heavyBRs_hi = calculate_heavy_BRs_only(BR_interpolators_SM, Mhi, l112_real, sintheta_real)

    BRWW_hi = heavyBRs_hi[9]
    BRWW_lo = heavyBRs_lo[9]

    xs100_n3lon3ll_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mhi),5)
    xs100_vbf_nlo_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mhi),5)
    xs100_n3lon3ll_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mlo),5)
    xs100_vbf_nlo_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mlo),5)

    xs100_hi_WWevmv = (xs100_n3lon3ll_hi + xs100_vbf_nlo_hi)*BRWWevmv*BRWW_hi*1000 # !!!!CONVERT TO FB!!!!
    xs100_lo_WWevmv = (xs100_n3lon3ll_lo + xs100_vbf_nlo_lo)*BRWWevmv*BRWW_lo*1000 # !!!!CONVERT TO FB!!!!
    print('xs100_n3lon3ll_lo + xs100_vbf_nlo_lo, xs100_n3lon3ll_hi + xs100_vbf_nlo_hi=', xs100_n3lon3ll_lo + xs100_vbf_nlo_lo, xs100_n3lon3ll_hi + xs100_vbf_nlo_hi, 'pb')
    print('BRWW_lo, BRWW_hi=', BRWW_lo, BRWW_hi)
    print('xs100_lo, xs100_hi *BRWW*WW->evmuv=', xs100_lo_WWevmv, xs100_hi_WWevmv)

    # call the python3 function:
    #read_bdt_output_call_lo = 'python3.9 read_bdt_output_function.py ' + str(Mlo) + ' ' + str(lumi) + ' ' + str(xs100_lo_ZZbbaa)
    #print(read_bdt_output_call_lo)
    #p = subprocess.Popen(read_bdt_output_call_lo, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
        #print line
    #    if b'MASS FIT' in line:
    #        Mlo_fit = float(line.split()[2])
    #        DMlo_fit = float(line.split()[3])
        #if 'Significance' in line:
        #    print line
    Mlo_fit, DMlo_fit, Significance_lo = get_mass_fit_WW(Mlo, lumi, xs100_lo_WWevmv)
            
    # call the python3 function:
    #read_bdt_output_call_hi = 'python3.9 read_bdt_output_function.py ' + str(Mhi) + ' ' + str(lumi) + ' ' + str(xs100_hi_ZZbbaa)
    #print(read_bdt_output_call_hi)
    #p = subprocess.Popen(read_bdt_output_call_hi, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
    #    #print line
    #    if b'MASS FIT' in line:
    #        Mhi_fit = float(line.split()[2])
    #        DMhi_fit = float(line.split()[3])
    #    if b'Significance' in line:
    #        print(line)
    Mhi_fit, DMhi_fit, Significance_hi = get_mass_fit_WW(Mhi, lumi, xs100_hi_WWevmv)

    if Significance_lo < signif_cutoff or Significance_hi < signif_cutoff:
        return -2, -2

    #print('Mhi_fit = ', Mhi_fit, '+-', DMhi_fit)
    #print('Mlo_fit = ', Mlo_fit, '+-', DMlo_fit)

    M_fit = X * (Mhi_fit - Mlo_fit) + Mlo_fit
    DM_fit = X * DMhi_fit + (1-X) * DMlo_fit
    #print('M_fit = ', M_fit, '+-', DM_fit)
    
    return M_fit, DM_fit

# prepare the mass fitting from mT in HZZ100_2l2v:
# read in the available masses:
mT_resultsFile = './HZZ100_2l2v/HZZ100_2l2v_results.smear.temp.txt'
mT_resultsStream = open(mT_resultsFile, 'r')
mT_masses = []
for line in mT_resultsStream:
    mT_masses.append(float(line.split()[0]))
mT_masses = sorted(mT_masses) # sort
print('Available masses for mT fitting:', mT_masses)
mT_resultsStream.close()
# get the total cross sections as they were written out (corresponding to the GGF and VBF processes for the masses above):
# !THESE ARE IN PICOBARN!
mT_xsecsFile = 'HZZ100_2l2v/HZZ100_2l2v_InitialSignalXsecs.smear.temp.txt'
mT_xsecsStream = open(mT_xsecsFile, 'r')
mT_xsecsdict = {}
for line in mT_xsecsStream:
    mT_xsecsdict[line.split()[0]] = float(line.split()[1])*1000 # !!!!convert to FB!!!!
print('Available process cross sections for mT fitting:', mT_xsecsdict)
# sum up the VBF and GGF cross sections and create a dictionary for the available masses:
mT_xsecsummed_dict = {}
for key in mT_xsecsdict:
    mkey = key.replace('pp-eta0jj-ZZ-2l2v-evmuv-M','').replace('gg-eta0-ZZ-2l2v-M','')
    if float(mkey) not in list(mT_xsecsummed_dict.keys()):
        mT_xsecsummed_dict[float(mkey)] = mT_xsecsdict[key]
    else:
        mT_xsecsummed_dict[float(mkey)] = mT_xsecsummed_dict[float(mkey)] + mT_xsecsdict[key]
print('summed up cross sections:', [ (key, mT_xsecsummed_dict[key]) for key in sorted(mT_xsecsummed_dict.keys())])

BRZZ2l2v = (2*3.3632E-2 + 0.2)**2# the BR(ZZ->2l2v)
# define the function for the mT mass fitting:
def get_mT_fit_ZZ2l2v(m2tofit, lumi, sintheta_real, l112_real, signif_cutoff, BR_interpolators_SM):
    # determine where the M1 and M2 in the formula for the linear interpolation
    # i.e. in which between which two of the available masses does m2tofit fall according to mT_masses:
    if m2tofit > mT_masses[-1] or m2tofit < mT_masses[0]:
        print('WARNING:', m2tofit, 'outside avalable masses range for mT fit!')
        return -2, -2
    for im in range(0,len(mT_masses)-1):
     if m2tofit >= mT_masses[im] and m2tofit < mT_masses[im+1]:
         Mlo = mT_masses[im]
         Mhi = mT_masses[im+1]
    #print 'm2 to fit falls within', Mlo, Mhi
    # calculate the X variable:
    X = (m2tofit - Mlo)/(Mhi-Mlo)
    #print 'X=', X

    heavyBRs_lo = calculate_heavy_BRs_only(BR_interpolators_SM, Mlo, l112_real, sintheta_real)
    heavyBRs_hi = calculate_heavy_BRs_only(BR_interpolators_SM, Mhi, l112_real, sintheta_real)

    BRZZ_hi = heavyBRs_hi[10]
    BRZZ_lo = heavyBRs_lo[10]

    xs100_n3lon3ll_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mhi),5)
    xs100_vbf_nlo_hi = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mhi),5)
    xs100_n3lon3ll_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_N3LON3LL(Mlo),5)
    xs100_vbf_nlo_lo = round_sig(sintheta_real**2 * XS_interpolator_SM_100TeV_VBF_NLO(Mlo),5)

    xs100_hi_ZZ2l2v = (xs100_n3lon3ll_hi + xs100_vbf_nlo_hi)*BRZZ2l2v*BRZZ_hi*1000 # !!!!CONVERT TO FB!!!!
    xs100_lo_ZZ2l2v = (xs100_n3lon3ll_lo + xs100_vbf_nlo_lo)*BRZZ2l2v*BRZZ_lo*1000 # !!!!CONVERT TO FB!!!!
    #print('xs100_n3lon3ll_lo + xs100_vbf_nlo_lo, xs100_n3lon3ll_hi + xs100_vbf_nlo_hi=', xs100_n3lon3ll_lo + xs100_vbf_nlo_lo, xs100_n3lon3ll_hi + xs100_vbf_nlo_hi, 'pb')
    #print('BRWW_lo, BRWW_hi=', BRWW_lo, BRWW_hi)
    #print('xs100_lo, xs100_hi *BRWW*WW->evmuv=', xs100_lo_WWevmv, xs100_hi_WWevmv)

    # call the python3 function:
    #read_bdt_output_call_lo = 'python3.9 read_bdt_output_function.py ' + str(Mlo) + ' ' + str(lumi) + ' ' + str(xs100_lo_ZZbbaa)
    #print(read_bdt_output_call_lo)
    #p = subprocess.Popen(read_bdt_output_call_lo, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
        #print line
    #    if b'MASS FIT' in line:
    #        Mlo_fit = float(line.split()[2])
    #        DMlo_fit = float(line.split()[3])
        #if 'Significance' in line:
        #    print line
    Mlo_fit, DMlo_fit, Significance_lo = get_mass_fit_ZZ2l2v(Mlo, lumi, xs100_lo_ZZ2l2v)
            
    # call the python3 function:
    #read_bdt_output_call_hi = 'python3.9 read_bdt_output_function.py ' + str(Mhi) + ' ' + str(lumi) + ' ' + str(xs100_hi_ZZbbaa)
    #print(read_bdt_output_call_hi)
    #p = subprocess.Popen(read_bdt_output_call_hi, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    #for line in iter(p.stdout.readline, b''):
    #    #print line
    #    if b'MASS FIT' in line:
    #        Mhi_fit = float(line.split()[2])
    #        DMhi_fit = float(line.split()[3])
    #    if b'Significance' in line:
    #        print(line)
    Mhi_fit, DMhi_fit, Significance_hi = get_mass_fit_ZZ2l2v(Mhi, lumi, xs100_hi_ZZ2l2v)

    if Significance_lo < signif_cutoff or Significance_hi < signif_cutoff:
        return -2, -2

    #print('Mhi_fit = ', Mhi_fit, '+-', DMhi_fit)
    #print('Mlo_fit = ', Mlo_fit, '+-', DMlo_fit)

    M_fit = X * (Mhi_fit - Mlo_fit) + Mlo_fit
    DM_fit = X * DMhi_fit + (1-X) * DMlo_fit
    #print('M_fit = ', M_fit, '+-', DM_fit)
    
    return M_fit, DM_fit

# test:
#get_m4l_fit(700, 30000, 0.01, 0.1)

# the code for fitting l112 and sintheta
exec(compile(open('singlet_single_fit_mass.py', "rb").read(), 'singlet_single_fit_mass.py', 'exec'))

test_single_fit = False
if test_single_fit is True:
    # point parameters:
    mh1 = 125.1
    mh2 = 466.29
    dmh2 = 1.
    ctreal = 0.990457406445 # the true value of costheta for this point
    streal = math.sqrt(1-ctreal**2)
    k112real = 100.36 # true value of k112
    k122real = 502.56 # true value of k122
    RunNum = 4
    signif_ZZ = 100.
    signif_HH = 1.0001
    Tag = 'UCons30'
    print(get_sintheta_l112_fit_fixedmass(mh1, mh2, streal, k112real, signif_ZZ, signif_HH, Tag))
    # get the constraint on sintheta and l112 assuming no error on mass
    print(get_sintheta_l112_fit_errormass_curve(mh1, mh2, dmh2, streal, k112real, signif_ZZ, signif_HH, Tag))


def generate_spline(x, y, closed=False, steps=20):
    if closed:
        x = x.tolist()
        x.insert(0, x[-1])
        x.append(x[1])
        x.append(x[2])
 
        y = y.tolist()
        y.insert(0, y[-1])
        y.append(y[1])
        y.append(y[2])
 
    points = np.vstack((x,y)).T
    
    curve = []
 
    if not closed:
        curve.append(points[0])
    
    for j in range(1, len(points)-2):
        for s in range(steps):
            t = 1. * s / steps
            p0, p1, p2, p3 = points[j-1], points[j], points[j+1], points[j+2]
            pnew = 0.5 *((2 * p1) + (-p0 + p2) * t + (2*p0 - 5*p1 + 4*p2 - p3) * t**2 + (-p0 + 3*p1- 3*p2 + p3) * t**3)
            curve.append(pnew)
 
    if not closed:
        curve.append(points[-1])
 
    curve = np.array(curve)
    return curve[:, 0], curve[:, 1]
