############################
# H -> ZZ -> 2l2v ANALYSIS #
############################

# list of analysed processes
Analyses.append('HZZ100_2l2v')

# the latex name of the parent process (without the decay into the final state):
LatexNameParent['HZZ100_2l2v'] = '$h_2 \\rightarrow ZZ$' 

# the latex name of the analysis
LatexName['HZZ100_2l2v'] = '$h_2 \\rightarrow ZZ \\rightarrow 2 \\ell 2 \\nu$'

# the Branching ratio of the primary decay particles of the heavy particle into the observed final state:
FacFinalState['HZZ100_2l2v'] = (2*BR_z_ellell + BR_z_vv)**2

# the proton-proton energy for the analysis in TeV
Energy['HZZ100_2l2v'] = 100

# location of analyses' info
Location['HZZ100_2l2v'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HZZ100_2l2v/'

# analyses' executables (with respect to Location directory)
Executable['HZZ100_2l2v'] = 'HwSimPostAnalysis_2l2v'

# analyses' executables (with respect to Location directory)
ExecutableCuts['HZZ100_2l2v'] = 'HwSimPostAnalysis_2l2v'

# analyses' executables (with respect to Location directory)
ExecutableSmear['HZZ100_2l2v'] = 'HwSimPostAnalysis_2l2v_smear'

# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HZZ100_2l2v'] = {
                         'gg-eta0-ZZ-2l2v-M200':200.0,
                         #'gg-eta0-ZZ-2l2v-M250':250.0,
                         'gg-eta0-ZZ-2l2v-M300':300.0,
                         'gg-eta0-ZZ-2l2v-M350':350.0,
                         'gg-eta0-ZZ-2l2v-M400':400.0,
                         'gg-eta0-ZZ-2l2v-M450':450.0,
                         'gg-eta0-ZZ-2l2v-M500':500.0,
                         'gg-eta0-ZZ-2l2v-M550':550.0,
                        'gg-eta0-ZZ-2l2v-M600':600.0,
                         'gg-eta0-ZZ-2l2v-M650':650.0,
                         'gg-eta0-ZZ-2l2v-M700':700.0,
                         'gg-eta0-ZZ-2l2v-M750':750.0,
                         'gg-eta0-ZZ-2l2v-M800':800.0,
                        'gg-eta0-ZZ-2l2v-M850':850.0,
                        'gg-eta0-ZZ-2l2v-M900':900.0,
                        'gg-eta0-ZZ-2l2v-M950':950.0,
                         'gg-eta0-ZZ-2l2v-M1000':1000.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M200':200.0,
                         #'pp-eta0jj-ZZ-2l2v-evmuv-M250':250.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M300':300.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M350':350.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M400':400.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M450':450.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M500':500.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M550':550.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M600':600.0,
                        'pp-eta0jj-ZZ-2l2v-evmuv-M650':650.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M700':700.0,
                        'pp-eta0jj-ZZ-2l2v-evmuv-M750':750.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M800':800.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M850':850.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M900':900.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M950':950.0,
                         'pp-eta0jj-ZZ-2l2v-evmuv-M1000':1000.0,
                         'pp-2l2v':0.0,
                         'pp-wz-lvll':0.0,
#                         'pp-ttbar-lvlvbb':0.0,
                         'pp-zvv':0.0
                         }
                         
# the order of the processes
OrderProcesses['HZZ100_2l2v'] = {    'gg-eta0-ZZ-2l2v-M200':0,
    'gg-eta0-ZZ-2l2v-M250':0,
    'gg-eta0-ZZ-2l2v-M300':0,
    'gg-eta0-ZZ-2l2v-M350':0,
    'gg-eta0-ZZ-2l2v-M400':0,
    'gg-eta0-ZZ-2l2v-M450':0,
    'gg-eta0-ZZ-2l2v-M500':0,
    'gg-eta0-ZZ-2l2v-M550':0,
    'gg-eta0-ZZ-2l2v-M600':0,
    'gg-eta0-ZZ-2l2v-M650':0,
    'gg-eta0-ZZ-2l2v-M700':0,
    'gg-eta0-ZZ-2l2v-M750':0,
    'gg-eta0-ZZ-2l2v-M800':0,
    'gg-eta0-ZZ-2l2v-M850':0,
    'gg-eta0-ZZ-2l2v-M900':0,
    'gg-eta0-ZZ-2l2v-M950':0,
    'gg-eta0-ZZ-2l2v-M1000':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M200':0,                        
    'pp-eta0jj-ZZ-2l2v-evmuv-M250':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M300':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M350':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M400':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M450':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M500':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M550':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M600':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M650':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M700':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M750':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M800':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M850':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M900':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M950':0,
    'pp-eta0jj-ZZ-2l2v-evmuv-M1000':0,
                         'pp-2l2v':1,
                         'pp-wz-lvll':1,
                         'pp-ttbar-lvlvbb':1,
                         'pp-zvv':0
                         }

# K-factors for the processes: MUST INCLUDE SYMMETRY FACTORS (e.g. for the lepton flavours)
KFacsProcesses['HZZ100_2l2v'] = { 'gg-eta0-ZZ-2l2v-M200':2.153378605,
                                     'gg-eta0-ZZ-2l2v-M250':2.18073096985,
                                    'gg-eta0-ZZ-2l2v-M300':2.29775496014,
                                    'gg-eta0-ZZ-2l2v-M350':2.46589800207,
                                    'gg-eta0-ZZ-2l2v-M400':1.9707292403,
                                    'gg-eta0-ZZ-2l2v-M450':1.72468921497,
                                    'gg-eta0-ZZ-2l2v-M500':1.66261118224,
                                    'gg-eta0-ZZ-2l2v-M550':1.60230559544,
                                    'gg-eta0-ZZ-2l2v-M600':1.56017321503,
                                    'gg-eta0-ZZ-2l2v-M650':1.52629944125,
                                    'gg-eta0-ZZ-2l2v-M700':1.49797704809,
                                    'gg-eta0-ZZ-2l2v-M750':1.4753696585,
                                    'gg-eta0-ZZ-2l2v-M800':1.45478852862,
                                    'gg-eta0-ZZ-2l2v-M850':1.43632233525,
                                    'gg-eta0-ZZ-2l2v-M900':1.42118100769,
                                    'gg-eta0-ZZ-2l2v-M950':1.40459637177,
                                    'gg-eta0-ZZ-2l2v-M1000':1.39037544923,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M200':1.34144694309,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M250':1.31763219163,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M300': 1.33269322546,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M350': 1.33413720446,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M400': 1.35075506325,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M450': 1.34429175954,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M500': 1.36933619048,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M550': 1.36225180698,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M600': 1.3588164263,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M650': 1.34668089912,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M700': 1.33902198374,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M750': 1.35037833258,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M800': 1.35564696253,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M850': 1.34048099174,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M900': 1.33779585209,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M950': 1.34768444622,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M1000': 1.33078921944,
                         'pp-2l2v':1.0,
                         'pp-wz-lvll':1.0,
                         'pp-ttbar-lvlvbb':1.0,
                         'pp-zvv':2.0
         }


# K-factors to apply to processes on the TOTAL cross section -> NO symmetry factors from Decays.
TotalKFacsProcesses['HZZ100_2l2v'] = { 'gg-eta0-ZZ-2l2v-M200':2.153378605,
                                     'gg-eta0-ZZ-2l2v-M250':2.18073096985,
                                    'gg-eta0-ZZ-2l2v-M300':2.29775496014,
                                    'gg-eta0-ZZ-2l2v-M350':2.46589800207,
                                    'gg-eta0-ZZ-2l2v-M400':1.9707292403,
                                    'gg-eta0-ZZ-2l2v-M450':1.72468921497,
                                    'gg-eta0-ZZ-2l2v-M500':1.66261118224,
                                    'gg-eta0-ZZ-2l2v-M550':1.60230559544,
                                    'gg-eta0-ZZ-2l2v-M600':1.56017321503,
                                    'gg-eta0-ZZ-2l2v-M650':1.52629944125,
                                    'gg-eta0-ZZ-2l2v-M700':1.49797704809,
                                    'gg-eta0-ZZ-2l2v-M750':1.4753696585,
                                    'gg-eta0-ZZ-2l2v-M800':1.45478852862,
                                    'gg-eta0-ZZ-2l2v-M850':1.43632233525,
                                    'gg-eta0-ZZ-2l2v-M900':1.42118100769,
                                    'gg-eta0-ZZ-2l2v-M950':1.40459637177,
                                    'gg-eta0-ZZ-2l2v-M1000':1.39037544923,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M200':1.34144694309,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M250':1.31763219163,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M300': 1.33269322546,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M350': 1.33413720446,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M400': 1.35075506325,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M450': 1.34429175954,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M500': 1.36933619048,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M550': 1.36225180698,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M600': 1.3588164263,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M650': 1.34668089912,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M700': 1.33902198374,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M750': 1.35037833258,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M800': 1.35564696253,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M850': 1.34048099174,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M900': 1.33779585209,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M950': 1.34768444622,
                                    'pp-eta0jj-ZZ-2l2v-evmuv-M1000': 1.33078921944,
                                    'pp-2l2v':1.0,
                                    'pp-wz-lvll':1.0,
                                    'pp-ttbar-lvlvbb':1.0,
                                    'pp-zvv':2.0
                         }

# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HZZ100_2l2v'] = {
'gg-eta0-ZZ-2l2v-M200':['gg_eta0_ZZ_2l2v/Events/run0_200_decayed_1/unweighted_events.lhe.gz', 'gg_eta0_ZZ_2l2v/Events/run1_200_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M250':['gg_eta0_ZZ_2l2v/Events/run0_250_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M300':['gg_eta0_ZZ_2l2v/Events/run0_300_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M350':['gg_eta0_ZZ_2l2v/Events/run0_350_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M400':['gg_eta0_ZZ_2l2v/Events/run0_400_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M450':['gg_eta0_ZZ_2l2v/Events/run0_450_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M500':['gg_eta0_ZZ_2l2v/Events/run0_500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M550':['gg_eta0_ZZ_2l2v/Events/run0_550_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M600':['gg_eta0_ZZ_2l2v/Events/run0_600_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M650':['gg_eta0_ZZ_2l2v/Events/run0_650_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M700':['gg_eta0_ZZ_2l2v/Events/run0_700_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M750':['gg_eta0_ZZ_2l2v/Events/run0_750_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M800':['gg_eta0_ZZ_2l2v/Events/run0_800_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M850':['gg_eta0_ZZ_2l2v/Events/run0_850_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M900':['gg_eta0_ZZ_2l2v/Events/run0_900_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M950':['gg_eta0_ZZ_2l2v/Events/run0_950_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-2l2v-M1000':['gg_eta0_ZZ_2l2v/Events/run0_1000_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M200':['pp_eta0jj_zz2l2v/Events/run1_200_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M250':['pp_eta0jj_zz2l2v/Events/run1_250_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M300':['pp_eta0jj_zz2l2v/Events/run1_300_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M350':['pp_eta0jj_zz2l2v/Events/run1_350_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M400':['pp_eta0jj_zz2l2v/Events/run1_400_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M450':['pp_eta0jj_zz2l2v/Events/run1_450_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M500':['pp_eta0jj_zz2l2v/Events/run1_500_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M550':['pp_eta0jj_zz2l2v/Events/run1_550_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M600':['pp_eta0jj_zz2l2v/Events/run1_600_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M650':['pp_eta0jj_zz2l2v/Events/run1_650_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M700':['pp_eta0jj_zz2l2v/Events/run1_700_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M750':['pp_eta0jj_zz2l2v/Events/run1_750_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M800':['pp_eta0jj_zz2l2v/Events/run1_800_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M850':['pp_eta0jj_zz2l2v/Events/run1_850_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M900':['pp_eta0jj_zz2l2v/Events/run1_900_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M950':['pp_eta0jj_zz2l2v/Events/run1_950_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-2l2v-evmuv-M1000':['pp_eta0jj_zz2l2v/Events/run1_1000_decayed_1/unweighted_events.lhe.gz'],
'pp-2l2v':['pp_2l2v/Events/run_01/events.lhe.gz'],
'pp-wz-lvll':['pp_wz_lvll/Events/run_01_decayed_1/events.lhe.gz'],
'pp-ttbar-lvlvbb':['pp_ttbar_lvlvbb/Events/run_01_decayed_1/events.lhe.gz'],
'pp-zvv':['pp_zvv/Events/run_01_decayed_1/unweighted_events.lhe.gz']
    }
 
# Where to look for the UNDECAYED total cross section: Note the difference between LO and NLO!
MGProcessLocations_Undecayed['HZZ100_2l2v'] = {
'gg-eta0-ZZ-2l2v-M200':['gg_eta0_ZZ_2l2v/Events/run0_200/unweighted_events.lhe', 'gg_eta0_ZZ_2l2v/Events/run1_200/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M250':['gg_eta0_ZZ_2l2v/Events/run0_250/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M300':['gg_eta0_ZZ_2l2v/Events/run0_300/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M350':['gg_eta0_ZZ_2l2v/Events/run0_350/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M400':['gg_eta0_ZZ_2l2v/Events/run0_400/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M450':['gg_eta0_ZZ_2l2v/Events/run0_450/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M500':['gg_eta0_ZZ_2l2v/Events/run0_500/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M550':['gg_eta0_ZZ_2l2v/Events/run0_550/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M600':['gg_eta0_ZZ_2l2v/Events/run0_600/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M650':['gg_eta0_ZZ_2l2v/Events/run0_650/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M700':['gg_eta0_ZZ_2l2v/Events/run0_700/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M750':['gg_eta0_ZZ_2l2v/Events/run0_750/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M800':['gg_eta0_ZZ_2l2v/Events/run0_800/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M850':['gg_eta0_ZZ_2l2v/Events/run0_850/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M900':['gg_eta0_ZZ_2l2v/Events/run0_900/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M950':['gg_eta0_ZZ_2l2v/Events/run0_950/unweighted_events.lhe'],
'gg-eta0-ZZ-2l2v-M1000':['gg_eta0_ZZ_2l2v/Events/run0_1000/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M200':['pp_eta0jj_zz2l2v/Events/run1_200/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M250':['pp_eta0jj_zz2l2v/Events/run1_250/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M300':['pp_eta0jj_zz2l2v/Events/run1_300/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M350':['pp_eta0jj_zz2l2v/Events/run1_350/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M400':['pp_eta0jj_zz2l2v/Events/run1_400/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M450':['pp_eta0jj_zz2l2v/Events/run1_450/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M500':['pp_eta0jj_zz2l2v/Events/run1_500/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M550':['pp_eta0jj_zz2l2v/Events/run1_550/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M600':['pp_eta0jj_zz2l2v/Events/run1_600/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M650':['pp_eta0jj_zz2l2v/Events/run1_650/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M700':['pp_eta0jj_zz2l2v/Events/run1_700/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M750':['pp_eta0jj_zz2l2v/Events/run1_750/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M800':['pp_eta0jj_zz2l2v/Events/run1_800/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M850':['pp_eta0jj_zz2l2v/Events/run1_850/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M900':['pp_eta0jj_zz2l2v/Events/run1_900/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M950':['pp_eta0jj_zz2l2v/Events/run1_950/unweighted_events.lhe'],
'pp-eta0jj-ZZ-2l2v-evmuv-M1000':['pp_eta0jj_zz2l2v/Events/run1_1000/unweighted_events.lhe'],
'pp-2l2v':['pp_2l2v/Events/run_01/summary.txt'],
'pp-wz-lvll':['pp_wz_lvll/Events/run_01/summary.txt'],
'pp-ttbar-lvlvbb':['pp_ttbar_lvlvbb/Events/run_01/summary.txt'],
'pp-zvv':['pp_zvv/Events/run_01/unweighted_events.lhe.gz']
}

Luminosity['HZZ100_2l2v'] = 30000. # luminosity in inv fb.

NVariables['HZZ100_2l2v'] = 9 # the number of variables that go into the BDT

BRElement['HZZ100_2l2v'] = 10 # the element of the Higgs Branching Ratio array corresponding to this decay mode 

NBDTAnalysis['HZZ100_2l2v'] = 9#10# the number of BDT runs to search for the optimal solution

SignalFactorAnalysis['HZZ100_2l2v'] = 1.E-2 # the initial signal factor guess for the BDT

HwSimLibrary['HZZ100_2l2v'] = 'HwSim' # the name of the HwSim-type library to be loaded by Herwig 

FatAnalysis['HZZ100_2l2v'] = '#' # add a "#" to remove the HwSimFat stuff in the Herwig file

