import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
from matplotlib.mlab import griddata
import matplotlib.font_manager as fm
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, ScalarFormatter, FuncFormatter)
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
from scipy.optimize import fsolve
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
import pickle
import re
from matplotlib.colors import ListedColormap

# import the constraint calculator:
#from singlet_constraint_functions import *

####################
####################
# USEFUL FUNCTIONS #                 
####################
####################

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)


#####################################
#####################################
# GENERAL SETTINGS FOR THIS MACHINE #
#####################################
#####################################

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 1:
    print('usage: singlet_kfactors.py [options]')
    exit()

print("\nDriver script to calculate heavy Higgs boson k-factors")
print("---")

if len(args) > 1:
    writeout_tag = args[1] 


#####################
# ihixs information #
#####################

# the ihixs location (should contain the binary):
ihixs_location = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/ihixs/build/'

# the ihixs binary:
ihixs_bin = ihixs_location + 'ihixs'

# the ixixs template:
# REMOVE the .template extension
ihixs_template = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/ihixs/build/ihixs.card'

# the output file for the 100 TeV GGF cross sections
ihixs_output = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/higgsXS_ihixs_100TeV_N3LON3LL.txt'

# the output file for the 27 TeV GGF cross sections
ihixs27_output = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/higgsXS_ihixs_27TeV_N3LON3LL.txt'

# the output file for the ratio of 27 TeV cross section to 100 TeV cross section (GLUON FUSION)
ihixs27to100_output = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/higgsXS_RATIO_ihixs_27to100TeV_N3LON3LL.txt'

# the output file for the ratio of 27 TeV cross section to 100 TeV cross section (QQBAR)
qq_output = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/higgsXS_RATIO_QQ_27to100TeV_N3LON3LL.txt'

# the output for the 100 TeV VBF (NLO) cross sections:
vbf_output = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/higgsXS_VBF_100TeV_NLO.txt'

# 27 TeV ihixs template:
# REMOVE the .template extension
ihixs27_template = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/ihixs/build/ihixs27.card'


######################
# masses to consider #
######################

Masses = [200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1100, 1200, 1500, 2000]
#Masses = [200, 1100, 1200, 1500]
#Masses = [200, 1100, 1200]
#Masses = [2000]

# locations of the MG5 LO GLUON FUSION cross sections:
# Where to look for the UNDECAYED total cross section:
MGProcessLocations_Undecayed_GGF = {
200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0_2/Events/run0_200/unweighted_events.lhe.gz',
250:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_250/unweighted_events.lhe.gz',
300:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_300/unweighted_events.lhe.gz',
350:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_350/unweighted_events.lhe.gz',
400:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_400/unweighted_events.lhe.gz',
450:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_450/unweighted_events.lhe.gz',
500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_500/unweighted_events.lhe.gz',
550:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_550/unweighted_events.lhe.gz',
600:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_600/unweighted_events.lhe.gz',
650:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_650/unweighted_events.lhe.gz',
700:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_700/unweighted_events.lhe.gz',
750:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_750/unweighted_events.lhe.gz',
800:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_800/unweighted_events.lhe.gz',
850:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_850/unweighted_events.lhe.gz',
900:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_900/unweighted_events.lhe.gz',
950:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_950/unweighted_events.lhe.gz',
1000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0/Events/run0_1000/unweighted_events.lhe.gz',
1100:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0_2/Events/run0_1100/unweighted_events.lhe.gz',
1200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0_2/Events/run0_1200/unweighted_events.lhe.gz',
1500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0_2/Events/run0_1500/unweighted_events.lhe.gz',
2000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/gg_eta0_2/Events/run0_2000/unweighted_events.lhe.gz'
}

# locations of the MG5 NLO VBF cross sections:
# Where to look for the UNDECAYED total cross section:
MGProcessLocations_Undecayed_VBF_NLO = {
200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_200/summary.txt',
250:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_250/summary.txt',
300:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_300/summary.txt',
350:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_350/summary.txt',
400:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_400/summary.txt',
450:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_450/summary.txt',
500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_500/summary.txt',
550:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_550/summary.txt',
600:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_600/summary.txt',
650:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_650/summary.txt',
700:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_700/summary.txt',
750:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_750/summary.txt',
800:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_800/summary.txt',
850:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_850/summary.txt',
900:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_900/summary.txt',
950:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_950/summary.txt',
1000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_1000/summary.txt',
1100:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_1100/summary.txt',
1200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_1200/summary.txt',
1500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_1500/summary.txt',
2000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/pp_eta0jj/Events/run0_2000/summary.txt'
}

MGProcessLocations_Undecayed_VBF_LO = {
200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww2/Events/run0_200/unweighted_events.lhe',
250:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_250/unweighted_events.lhe',
300:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_300/unweighted_events.lhe',
350:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_350/unweighted_events.lhe',
400:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_400/unweighted_events.lhe',
450:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_450/unweighted_events.lhe',
500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_500/unweighted_events.lhe',
550:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_550/unweighted_events.lhe',
600:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_600/unweighted_events.lhe',
650:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_650/unweighted_events.lhe',
700:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_700/unweighted_events.lhe',
750:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_750/unweighted_events.lhe',
800:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_800/unweighted_events.lhe',
850:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_850/unweighted_events.lhe',
900:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_900/unweighted_events.lhe',
950:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww/Events/run0_950/unweighted_events.lhe',
1000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww2/Events/run0_1000/unweighted_events.lhe',
1100:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww2/Events/run0_1100/unweighted_events.lhe',
1200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww2/Events/run0_1200/unweighted_events.lhe',
1500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww2/Events/run0_1500/unweighted_events.lhe',
2000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HWW100/MG5_aMC_v2_7_2/pp_eta0jj_ww2/Events/run0_2000/unweighted_events.lhe'
}
    
MGProcessLocations_QQ_27 = {
200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_200/unweighted_events.lhe.gz',
250:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_250/unweighted_events.lhe.gz',
300:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_300/unweighted_events.lhe.gz',
350:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_350/unweighted_events.lhe.gz',
400:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_400/unweighted_events.lhe.gz',
450:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_450/unweighted_events.lhe.gz',
500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_500/unweighted_events.lhe.gz',
550:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_550/unweighted_events.lhe.gz',
600:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_600/unweighted_events.lhe.gz',
650:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_650/unweighted_events.lhe.gz',
700:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_700/unweighted_events.lhe.gz',
750:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_750/unweighted_events.lhe.gz',
800:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_800/unweighted_events.lhe.gz',
850:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_850/unweighted_events.lhe.gz',
900:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_900/unweighted_events.lhe.gz',
950:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_950/unweighted_events.lhe.gz',
1000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_1000/unweighted_events.lhe.gz',
1100:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_1100/unweighted_events.lhe.gz',
1200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_1200/unweighted_events.lhe.gz',
1500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_1500/unweighted_events.lhe.gz',
2000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run27_2000/unweighted_events.lhe.gz'
}
    
MGProcessLocations_QQ_100 = {
200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_200/unweighted_events.lhe.gz',
250:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_250/unweighted_events.lhe.gz',
300:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_300/unweighted_events.lhe.gz',
350:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_350/unweighted_events.lhe.gz',
400:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_400/unweighted_events.lhe.gz',
450:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_450/unweighted_events.lhe.gz',
500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_500/unweighted_events.lhe.gz',
550:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_550/unweighted_events.lhe.gz',
600:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_600/unweighted_events.lhe.gz',
650:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_650/unweighted_events.lhe.gz',
700:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_700/unweighted_events.lhe.gz',
750:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_750/unweighted_events.lhe.gz',
800:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_800/unweighted_events.lhe.gz',
850:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_850/unweighted_events.lhe.gz',
900:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_900/unweighted_events.lhe.gz',
950:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_950/unweighted_events.lhe.gz',
1000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_1000/unweighted_events.lhe.gz',
1100:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_1100/unweighted_events.lhe.gz',
1200:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_1200/unweighted_events.lhe.gz',
1500:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_1500/unweighted_events.lhe.gz',
2000:'/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/HZZ100_2l2v/MG5_aMC_v2_7_2/qq_eta0/Events/run100_2000/unweighted_events.lhe.gz'
}
    
#######################
# AVAILABLE RUN MODES #
#######################

genihixs = False # generate the ihixs input files
runihixs = False # run ihixs via the generated input files
collectihixs = False # collect the ihixs results and calculate the GGF K-factors
genihixs27 = False # generate the ihixs input files -> 27 TeV
runihixs27 = False # run ihixs via the generated input files -> 27 TeV
collectihixs27 = False # collect the ihixs results and calculate the GGF K-factors -> 27 TeV
collectvbf = False # collect the VBF NLO cross sections (pp -> eta0 j j)
collectqq = False # collect the ratio of LO qq -> eta0 processes for the rescaling of the backgrounds

########################################
########################################
# SET PARAMETERS AND EXECUTE MODE HERE #
########################################
########################################

# choose the mode from the command line
if 'genihixs' == args[0]:
    genihixs = True

if 'runihixs' == args[0]:
    runihixs = True

if 'collectihixs' == args[0]:
    collectihixs = True

if 'genihixs27' == args[0]:
    genihixs27 = True

if 'runihixs27' == args[0]:
    runihixs27 = True

if 'collectihixs27' == args[0]:
    collectihixs27 = True

if 'collectvbf' == args[0]:
    collectvbf = True

if 'collectqq' == args[0]:
    collectqq = True

if len(args) > 1:
    processtag = args[1]

if genihixs == True:
    print('generating ihixs input cards from template', ihixs_template + '.template')
    # load the template
    ihixsInputTemplate = getTemplate(ihixs_template)
    for mass in Masses:
        tag = 'ihixs.' + str(mass)
        ihixsinputfile = ihixs_location + tag + '.card'
        print('\t\twriting', ihixsinputfile)
        if mass < 340.:
            mtexp = 'true'
        else:
            mtexp = 'false'
        parmtextsubs = {
                'TAG' : tag,
                'MH' : mass,
                'MHover2' : mass/2.,
                'MTEXP': mtexp
            }
        writeFile(ihixsinputfile, ihixsInputTemplate.substitute(parmtextsubs) )

if runihixs == True:
    print('running ihixs')
    for mass in Masses:
        tag = 'ihixs.' + str(mass)
        ihixsinputfile = ihixs_location + tag + '.card'
        # check if input file exists and run if so
        if os.path.exists(ihixsinputfile):
              ihixscommand = './ihixs -i ' + ihixsinputfile
              print(ihixscommand)
              p = subprocess.Popen(ihixscommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=ihixs_location)
              for line in iter(p.stdout.readline, b''):
                  print(line)
        else:
            print(ihixsinputfile, 'does not exist, skipping')
            continue

if collectihixs is True:
    print('collecting ihixs results and calculating K-factors')
    ihixs_xsecs = {}
    LO_xsecs = {}
    for mass in Masses:
        tag = 'ihixs.' + str(mass)
        ihixsoutputfile = ihixs_location + tag + '.output'
        print('checking', MGProcessLocations_Undecayed_GGF[mass], 'and', ihixsoutputfile)
        if os.path.exists(ihixsoutputfile) and os.path.exists(MGProcessLocations_Undecayed_GGF[mass]):
            ihixsstream = open(ihixsoutputfile, 'r')
            for line in ihixsstream:
                if 'Higgs XS                  =' in line:
                    #print line.split()
                    ihixs_xsecs[mass] = float(line.split()[3])
                    print('mass=', mass, 'ihixs xsec=', ihixs_xsecs[mass])
            # collect the corresponding LO cross section:
            zgrepcommand = 'zgrep "Integrated weight" ' + MGProcessLocations_Undecayed_GGF[mass]
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=ihixs_location)
            for line in iter(p.stdout.readline, b''):
                LO_xsecs[mass] = float(line.split()[5])
        else:
            print('one of output files does not exist, skipping:', os.path.exists(ihixsoutputfile), os.path.exists(MGProcessLocations_Undecayed_GGF[mass]))
            continue
    print("K-factors:")
    for mass in sorted(ihixs_xsecs.keys()):
        print(mass, 'sigma(ihixs)/sigma(LO)=', ihixs_xsecs[mass]/LO_xsecs[mass])
    # write out the cross sections
    xs_stream = open(ihixs_output, "w")
    print('writing xsecs in', ihixs_output)
    for mass in sorted(ihixs_xsecs.keys()):
        xs_stream.write(str(mass) + "\t" + str(ihixs_xsecs[mass]) + "\n")
    xs_stream.close()
        

if genihixs27 == True:
    print('generating ihixs input cards from template', ihixs_template + '.template for pp@27 TeV')
    # load the template
    ihixsInputTemplate = getTemplate(ihixs27_template)
    for mass in Masses:
        tag = 'ihixs27.' + str(mass)
        ihixsinputfile = ihixs_location + tag + '.card'
        print('\t\twriting', ihixsinputfile)
        if mass < 340.:
            mtexp = 'true'
        else:
            mtexp = 'false'
        parmtextsubs = {
                'TAG' : tag,
                'MH' : mass,
                'MHover2' : mass/2.,
                'MTEXP': mtexp
            }
        writeFile(ihixsinputfile, ihixsInputTemplate.substitute(parmtextsubs) )

if runihixs27 == True:
    print('running ihixs for pp@27 TeV')
    for mass in Masses:
        tag = 'ihixs27.' + str(mass)
        ihixsinputfile = ihixs_location + tag + '.card'
        # check if input file exists and run if so
        if os.path.exists(ihixsinputfile):
              ihixscommand = './ihixs -i ' + ihixsinputfile
              print(ihixscommand)
              p = subprocess.Popen(ihixscommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=ihixs_location)
              for line in iter(p.stdout.readline, b''):
                  print(line)
        else:
            print(ihixsinputfile, 'does not exist, skipping')
            continue

if collectihixs27 is True:
    print('collecting ihixs results and calculating DOWNSCALING factors for 100 -> 27 TeV')
    ihixs100_xsecs = {}
    ihixs27_xsecs = {}
    ratio27_to_100 = {}
    for mass in Masses:
        tag = 'ihixs.' + str(mass)
        ihixsoutputfile = ihixs_location + tag + '.output'
        tag27 = 'ihixs27.' + str(mass)
        ihixsoutputfile27 = ihixs_location + tag27 + '.output'
        if os.path.exists(ihixsoutputfile) and os.path.exists(ihixsoutputfile27):
            ihixsstream = open(ihixsoutputfile, 'r')
            for line in ihixsstream:
                if 'Higgs XS                  =' in line:
                    #print line.split()
                    ihixs100_xsecs[mass] = float(line.split()[3])
                    print('mass=', mass, 'ihixs 100 TeV xsec=', ihixs100_xsecs[mass])
            ihixsstream27 = open(ihixsoutputfile27, 'r')
            for line in ihixsstream27:
                if 'Higgs XS                  =' in line:
                    #print line.split()
                    ihixs27_xsecs[mass] = float(line.split()[3])
                    print('mass=', mass, 'ihixs 27 TeV xsec=', ihixs27_xsecs[mass])            
        else:
            print('one of output files does not exist, skipping:', os.path.exists(ihixsoutputfile), os.path.exists(ihixsoutputfile27))
            continue
    print("K-factors:")
    for mass in sorted(ihixs100_xsecs.keys()):
        print(mass, 'sigma(ihixs)/sigma(LO)=', ihixs27_xsecs[mass]/ihixs100_xsecs[mass])
        ratio27_to_100[mass] = ihixs27_xsecs[mass]/ihixs100_xsecs[mass]
    # write out the cross sections
    xs_ratio_stream = open(ihixs27to100_output, "w")
    xs_stream = open(ihixs27_output, "w")
    print('writing xsecs in', ihixs_output)
    for mass in sorted(ihixs100_xsecs.keys()):
        xs_ratio_stream.write(str(mass) + "\t" + str(ratio27_to_100[mass]) + "\n")
        xs_stream.write(str(mass) + "\t" + str(ihixs27_xsecs[mass]) + "\n")
    xs_ratio_stream.close()

    
if collectvbf is True:
    print('collecting VBF cross sections and calculating K-factors')
    LO_xsecs = {}
    NLO_xsecs = {}
    for mass in Masses:
        print('checking NLO VBF', MGProcessLocations_Undecayed_VBF_NLO[mass], 'and LO VBF', MGProcessLocations_Undecayed_VBF_LO[mass])
        if os.path.exists(MGProcessLocations_Undecayed_VBF_LO[mass]) and os.path.exists(MGProcessLocations_Undecayed_VBF_NLO[mass]):
            # get the corresponding NLO cross section:
            zgrepcommand = 'zgrep "Total cross section" ' + MGProcessLocations_Undecayed_VBF_NLO[mass]
            #print zgrepcommand
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
            for line in iter(p.stdout.readline, b''):
                NLO_xsecs[mass] = float(line.split()[3])
            # collect the corresponding LO cross section:
            zgrepcommand = 'zgrep "Integrated weight" ' + MGProcessLocations_Undecayed_VBF_LO[mass]
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
            for line in iter(p.stdout.readline, b''):
                LO_xsecs[mass] = float(line.split()[5])
        else:
            print('one of output files does not exist, skipping:', os.path.exists(MGProcessLocations_Undecayed_VBF_NLO[mass]), os.path.exists(MGProcessLocations_Undecayed_VBF_LO[mass]))
            continue
    print("K-factors:")
    for mass in sorted(NLO_xsecs.keys()):
        processtag_mass = "'" + processtag + "-M" + str(mass) + "':" 
        print(processtag_mass, NLO_xsecs[mass]/LO_xsecs[mass],",")
    # write out the cross sections
    xs_stream = open(vbf_output, "w")
    print('writing xsecs in', vbf_output)
    for mass in sorted(NLO_xsecs.keys()):
        xs_stream.write(str(mass) + "\t" + str(NLO_xsecs[mass]) + "\n")
    xs_stream.close()        


if collectqq is True:
    print('collecting the qq -> eta0 results')
    LO27_xsecs = {}
    LO100_xsecs = {}
    ratio_qq = {}
    for mass in Masses:
        print('checking', MGProcessLocations_QQ_27[mass], 'and', MGProcessLocations_QQ_100[mass])
        if os.path.exists(MGProcessLocations_QQ_100[mass]) and os.path.exists(MGProcessLocations_QQ_27[mass]):
            zgrepcommand = 'zgrep "Integrated weight" ' + MGProcessLocations_QQ_100[mass]
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=ihixs_location)
            for line in iter(p.stdout.readline, b''):
                LO100_xsecs[mass] = float(line.split()[5])
            zgrepcommand = 'zgrep "Integrated weight" ' + MGProcessLocations_QQ_27[mass]
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=ihixs_location)
            for line in iter(p.stdout.readline, b''):
                LO27_xsecs[mass] = float(line.split()[5])
        else:
            print('one of output files does not exist, skipping:', os.path.exists(MGProcessLocations_QQ_27[mass]), os.path.exists(MGProcessLocations_QQ_100[mass]))
            continue
    print("K-factors:")
    for mass in sorted(LO27_xsecs.keys()):
        print(mass, 'sigma(27)/sigma(100)=', LO27_xsecs[mass]/LO100_xsecs[mass])
        ratio_qq[mass] = LO27_xsecs[mass]/LO100_xsecs[mass]
    # write out the cross sections
    xs_stream = open(qq_output, "w")
    print('writing xsecs in', qq_output)
    for mass in sorted(LO27_xsecs.keys()):
        xs_stream.write(str(mass) + "\t" + str(ratio_qq[mass]) + "\n")
    xs_stream.close()
            
