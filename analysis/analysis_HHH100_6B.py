########################################
# Hh -> hhh -> bbar-bbbar-bbbar ANALYSIS #
########################################

# list of analysed processes
Analyses.append('HHH100_6B')

# the latex name of the parent process (without the decay into the final state):
LatexNameParent['HHH100_6B'] = '$h_2 h_1$'

# the latex name of the analysis
LatexName['HHH100_6B'] = '$h_2 h_1 \\rightarrow  (b\\bar{b})  (b\\bar{b})  (b\\bar{b})$'

# the Branching ratio of the primary decay particles of the heavy particle into the observed final state:
FacFinalState['HHH100_6B'] = 1.

# the proton-proton energy for the analysis in TeV
Energy['HHH100_6B'] = 100

# location of analyses' info
Location['HHH100_6B'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HHH100_6B/'

# analyses' executables (with respect to Location directory)
ExecutableSmear['HHH100_6B'] = 'HwSimPostAnalysis-xSM'

# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HHH100_6B'] = { 'gg_heta0_6b_450.0_100_100':(450.0,100,100),
                         'gg-6b':0,
                            } # M ! =0 stands for signal, 0 for backgrounds
                            
# the order of the processes
OrderProcesses['HHH100_6B'] = {'gg_heta0_6b_450.0_100_100':0,
                                   'gg-6b':0
        } # 0 stands for LO and 1 for NLO (i.e. MC@NLO), 2 for FxFx
        
# K-factors for the processes: MUST INCLUDE SYMMETRY FACTORS (e.g. for the lepton flavours)
KFacsProcesses['HHH100_6B'] = {  # 1.080683 comes from the MG5 BRs being wrong for hh > (bb~)(aa)
                        'gg_heta0_6b_450.0_100_100':2*1.080683,
                        'gg-6b':2
                        } # any K-factors to apply to processes

# K-factors to apply to processes on the TOTAL cross section -> NO symmetry factors from Decays.
TotalKFacsProcesses['HHH100_6B'] = { 'gg_heta0_6b_450.0_100_100':2,
                                         'gg-6b':2
                        } # any K-factors to apply to processes on the TOTAL cross section


# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HHH100_6B'] = { 'gg_heta0_6b_450.0_100_100':['gg_heta0_6b/Events/run4_m450.0_100_100_decayed_1/unweighted_events.lhe.gz'],
                                        'gg-6b':['gg_6b_gilberto/6b_all_events_gilberto270519.lhe.gz']
}

# Where to look for the UNDECAYED total cross section: Note the difference between LO and NLO!
MGProcessLocations_Undecayed['HHH100_6B'] = {
'gg-eta0-hh-bbaa-M200':['gg_heta0_6b/Events/run4_m450.0_100_100/unweighted_events.lhe'],
'gg-6b':['gg_6b_gilberto/6b_all_events_gilberto270519.lhe.gz']
}

Luminosity['HHH100_6B'] = 30000. # luminosity in /fb

NVariables['HHH100_6B'] = 1 # the number of variables that go into the BDT

BRElement['HHH100_6B'] = -1 # the element of the Higgs Branching Ratio array corresponding to this decay mode
 
NBDTAnalysis['HHH100_6B'] = -1 # the number of BDT runs to search for the optimal solution

SignalFactorAnalysis['HHH100_6B'] = 1000. # the initial signal factor guess for the BDT

HwSimLibrary['HHH100_6B'] = 'HwSim' # the name of the HwSim-type library to be loaded by Herwig 

FatAnalysis['HHH100_6B'] = '#' # add a "#" to remove the HwSimFat stuff in the Herwig file
