 
#from singlet_constraint_functions import *
import numpy as np
import math
from matplotlib.patches import Rectangle

# Get the single h2 cross section from XS_interpolator_SM_100TeV_N3LON3LL for a given mass:


def line(p1, p2):
    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0]*p2[1] - p2[0]*p1[1])
    return A, B, -C

def intersection(L1, L2):
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x,y
    else:
        return False

def manual_intersection(x1, y1, x2, y2):
    #print('x1=',x1)
    interp1 = scipy.interpolate.InterpolatedUnivariateSpline(x1, y1, ext=3)
    interp2 = scipy.interpolate.InterpolatedUnivariateSpline(x2, y2, ext=3)
    max_x = max(x1.max(), x2.max())
    min_x = min(x1.min(), x2.min())
    #print('min_x, max_x=', min_x, max_x)
    nxpoints = 10000
    new_x = np.linspace(min_x, max_x, nxpoints)
    new_y1 = interp1(new_x)
    new_y2 = interp2(new_x)
    idx = np.argwhere(np.diff(np.sign(new_y1 - new_y2)) != 0)
    return float(new_x[idx][0]), float(new_y1[idx][0])

def sigma_pp_h2_to_ZZ_or_hh(mh1, mh2, stval, l112val):
    if mh2 > 1000.:
        mh2 = 999.9999
    stval = np.array(stval)
    l112val = np.array(l112val)
    # get the cross section at N^3LO+N3LL @ 100 TeV (ihixs):
    
    ggF_XS_mh2 = stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2)

    #print(ggF_XS_mh2)
    #print 'gg -> h2 xsec, (mh2,stval) = (', mh2, stval, ')=', ggF_XS_mh2, 'pb'

    # get the BR to h1h1 and to ZZ:
    if mh2 < 1000.: # 
        Gamma_SM = BR_interpolators_SM[-1](mh2)
    else:
        Gamma_SM = BR_interpolators_SM[-1](1000.)
        
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(stval, Gamma_SM, mh1, mh2, l112val)
    BR_hh = BR_h2_to_h1h1(stval, mh1, mh2, l112val, Gamma_SM)
    BR_ZZ = BR_interpolators_SM[10](mh2) * rescale_fac

    #print rescale_fac
    
    #print BR_ZZ
     
    
    ggF_XS_mh2_ZZ = ggF_XS_mh2 * BR_ZZ
    ggF_XS_mh2_hh = ggF_XS_mh2 * BR_hh

    return ggF_XS_mh2_ZZ, ggF_XS_mh2_hh

def BR_h2_to_ZZ_or_hh(mh1, mh2, stval, l112val):
    stval = np.array(stval)
    l112val = np.array(l112val)
    # get the cross section at N^3LO+N3LL @ 100 TeV (ihixs):
    
    ggF_XS_mh2 = stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2)

    #print(ggF_XS_mh2)
    #print 'gg -> h2 xsec, (mh2,stval) = (', mh2, stval, ')=', ggF_XS_mh2, 'pb'

    # get the BR to h1h1 and to ZZ:
    if mh2 < 1000.: # 
        Gamma_SM = BR_interpolators_SM[-1](mh2)
    else:
        Gamma_SM = BR_interpolators_SM[-1](1000.)
        
    # get the rescaling factor of the SM BRs:
    rescale_fac = RES_BR_h2_to_xx(stval, Gamma_SM, mh1, mh2, l112val)
    BR_hh = BR_h2_to_h1h1(stval, mh1, mh2, l112val, Gamma_SM)
    BR_ZZ = BR_interpolators_SM[10](mh2) * rescale_fac

    return BR_ZZ, BR_hh

def sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2, st, l112val):
    if mh2 > 1000.:
        mh2 = 999.9999
    l112val = np.array(l112val)
    ggF_XS_mh2_ZZ = []
    ggF_XS_mh2_hh = []
    for stval in st:
            ggF_XS_mh2_ZZ.append(stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2) * BR_interpolators_SM[10](mh2) * RES_BR_h2_to_xx(stval, BR_interpolators_SM[-1](mh2), mh1, mh2, l112val))
            ggF_XS_mh2_hh.append(stval**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2) * BR_h2_to_h1h1(stval, mh1, mh2, l112val, BR_interpolators_SM[-1](mh2)))
    return ggF_XS_mh2_ZZ, ggF_XS_mh2_hh

# get the constraint on sintheta and l112 assuming no error on mass
def get_sintheta_l112_fit_fixedmass(mh1, mh2, streal, k112real, signif_zz, signif_hh, Tag):

    # whether to make the plots
    plotlamsin = False

    ##### the real cross sections:
    sigma_zz_real, sigma_hh_real = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2, streal, k112real)
    print('real cross sections, ZZ, hh=', sigma_zz_real, sigma_hh_real)
    BR_ZZ_real, BR_hh_real = BR_h2_to_ZZ_or_hh(mh1, mh2, streal, k112real)
    print('real BRs, ZZ, hh=', BR_ZZ_real, BR_hh_real)
    
    #######################
    # THE FIT STARTS HERE #
    #######################

    outputdirectory = './hSstudy/'
    
    gs = gridspec.GridSpec(6,6)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)
    
    prec_sigma_zz = 1/signif_zz
    prec_sigma_hh = 1/signif_hh

    xmin = k112real * (1 + 5*prec_sigma_hh)
    xmax = k112real * (1 - 5*prec_sigma_hh)
    ymin = streal * (1 + 2*prec_sigma_zz)
    ymax = streal * (1 - 2*prec_sigma_zz)
    #print('xmin, xmax, ymin, ymax=', xmin, xmax, ymin, ymax) 

    #yminp = 0.05
    #ymaxp = 0.2
    #xminp = -250
    #xmaxp = 250.

    yminp = ymin
    ymaxp = ymax
    xminp = xmin
    xmaxp = xmax

    xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
    yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
    x1, y1 = np.meshgrid(xi,yi)

    zi_zz, zi_hh = sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2, yi, xi)

    #print zi_zz

    #print sigma

    #zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
    #ZZ
    cs = plt.contour(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], extend='both', colors='k')
    cs2 = plt.contourf(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

    #hh
    cs3 = plt.contour(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], extend='both', colors='k')
    cs4 = plt.contourf(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

    # get the paths for the zz curve
    p = cs.collections[0].get_paths()[0]
    v = p.vertices
    xm_zz = v[:,0]
    ym_zz = v[:,1]
    p = cs.collections[1].get_paths()[0]
    v = p.vertices
    xp_zz = v[:,0]
    yp_zz = v[:,1]

    # get the paths for the hh curve
    p = cs3.collections[0].get_paths()[0]
    v = p.vertices
    xm_hh = v[:,0]
    ym_hh = v[:,1]
    p = cs3.collections[1].get_paths()[0]
    v = p.vertices
    xp_hh = v[:,0]
    yp_hh = v[:,1]

    #print('X LENGTHS=', len(xm_zz), len(xp_zz), len(xm_hh), len(xp_hh))

    L1_zz = line([xm_zz[0],ym_zz[0]], [xm_zz[-1],ym_zz[-1]])
    L2_zz = line([xp_zz[0],yp_zz[0]], [xp_zz[-1],yp_zz[-1]])
    L1_hh = line([xm_hh[0],ym_hh[0]], [xm_hh[-1],ym_hh[-1]])
    L2_hh = line([xp_hh[0],yp_hh[0]], [xp_hh[-1],yp_hh[-1]])

    R11 = intersection(L1_zz, L1_hh)
    R12 = intersection(L1_zz, L2_hh)
    R22 = intersection(L2_zz, L2_hh)
    R21 = intersection(L2_zz, L1_hh)
    #print 'intersections:', R11, R12, R22, R21

    #print 'limits:'
    #print 'lambda112 in [', min(R11[0], R12[0], R22[0], R21[0]), max(R11[0], R12[0], R22[0], R21[0]), ']', 'precision=', 2*(max(R11[0], R12[0], R22[0], R21[0])-min(R11[0], R12[0], R22[0], R21[0]))/(max(R11[0], R12[0], R22[0], R21[0])+min(R11[0], R12[0], R22[0], R21[0]))

    #print 'sintheta in [', min(R11[1], R12[1], R22[1], R21[1]), max(R11[1], R12[1], R22[1], R21[1]), ']', 'precision=', 2*(max(R11[1], R12[1], R22[1], R21[1])-min(R11[1], R12[1], R22[1], R21[1]))/(max(R11[1], R12[1], R22[1], R21[1])+min(R11[1], R12[1], R22[1], R21[1]))

    l112_min = min(abs(R11[0]), abs(R12[0]), abs(R22[0]), abs(R21[0]))
    l112_max = max(abs(R11[0]), abs(R12[0]), abs(R22[0]), abs(R21[0]))
  
    sintheta_min = min(abs(R11[1]), abs(R12[1]), abs(R22[1]), abs(R21[1]))
    sintheta_max = max(abs(R11[1]), abs(R12[1]), abs(R22[1]), abs(R21[1]))

    #print('limits:',  l112_min, l112_max, sintheta_min, sintheta_max)
    
    ################### CONTOUR PLOT STARTS HERE #################################################
    
    #####################    
    # plot zoomed in    #
    #####################
    if plotlamsin is True:
        
    
        manual_locations = [(500, -2000), (1000, -4000)]
        
        strs = ['$0.5\\times$', '$1.5\\times$']
        fmt = {}
        for l, s in zip(cs.levels, strs):
            fmt[l] = s
            
        star = plt.plot( k112real, streal, marker='*', color='red', ms=10, label='Truth', lw=0)
        
        #ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
        ax.set_ylabel('$\\sin \\theta$', fontsize=20)
        ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
        titletext = 'pp@100 TeV/30000 fb$^{-1}$, UCons1'
        ax.set_title(titletext)
        ax.set_xlim(xminp,xmaxp)
        ax.set_ylim(yminp,ymaxp)

        #ax.xaxis.set_major_locator(MultipleLocator(0.5))
        #ax.xaxis.set_minor_locator(MultipleLocator(0.1))
    
        #ax.yaxis.set_major_locator(MultipleLocator(0.0005))
        #ax.yaxis.set_minor_locator(MultipleLocator(0.0001))
        ax.yaxis.tick_left()
        ax.yaxis.set_ticks_position('both')

  
        for pc in cs2.collections:
            fc2 = pc.get_facecolor()[0]
        for pc in cs4.collections:
            fc4 = pc.get_facecolor()[0]

        hh_patch = mpatches.Patch(color=fc4, label='$h_1 h_1$')        
        zz_patch = mpatches.Patch(color=fc2, label='$ZZ$')        

        # create legend and plot/font size
        handles, labels = plt.gca().get_legend_handles_labels()
        ax.legend(handles=handles + [hh_patch] + [zz_patch], loc="upper left", numpoints=1, frameon=False, prop={'size':10})

        plot_type = 'xx_sigma_eta0_'+ Tag + '_2D'
        # save the figure
        print('saving the figure')
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print('---')
        print('output in', outputdirectory + infile.replace('.dat','.pdf'))
        pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
        pl.close(fig)

        #####################    
        # plot zoomed out    #
        #####################

        outputdirectory = './hSstudy/'

        gs = gridspec.GridSpec(6,6)
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ax.grid(False)
        xmin = k112real * (1 + 50*prec_sigma_hh)
        xmax = k112real * (1 - 50*prec_sigma_hh)
        ymin = streal * (1 + 100*prec_sigma_zz)
        ymax = streal * (1 - 100*prec_sigma_zz)

        yminp = ymin
        ymaxp = ymax
        xminp = xmin
        xmaxp = xmax

        xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
        yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
        x1, y1 = np.meshgrid(xi,yi)


        zi_zz, zi_hh = sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2, yi, xi)

        #print zi_zz

        #zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
        #ZZ
        cs = plt.contour(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], extend='both', colors='k')#, linestyles='--')
        cs2 = plt.contourf(x1, y1, zi_zz, levels=[sigma_zz_real*(1-prec_sigma_zz), sigma_zz_real*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

        #hh
        cs3 = plt.contour(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], extend='both', colors='k')
        cs4 = plt.contourf(x1, y1, zi_hh, levels=[sigma_hh_real*(1-prec_sigma_hh), sigma_hh_real*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

        manual_locations = [(500, -2000), (1000, -4000)]

        strs = ['$0.5\\times$', '$1.5\\times$']
        fmt = {}
        for l, s in zip(cs.levels, strs):
            fmt[l] = s

        hh_patch = mpatches.Patch(color=fc4, label='$h_1 h_1$')        
        zz_patch = mpatches.Patch(color=fc2, label='$ZZ$')        
            
        plt.plot( k112real, streal, marker='*', color='red', ms=11, label='Truth', lw=0)
        for pc in cs2.collections:
            fc2 = pc.get_facecolor()[0]
        for pc in cs4.collections:
            fc4 = pc.get_facecolor()[0]


    
        #plt.plot( np.nan, np.nan, marker='', color='black', ms=0, label='$h_1 h_1$', lw=2)
        #plt.plot( np.nan, np.nan, marker='', color='black', ms=0, label='$ZZ$', lw=2, ls='--')
        #ax.add_patch(Rectangle((k112real, streal), k112real*100*prec_sigma_hh, streal*40*prec_sigma_zz,alpha=0.5, color='blue'))

        #ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
        ax.set_ylabel('$\\sin \\theta$', fontsize=20)
        ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
        titletext = 'pp@100 TeV/30000 fb$^{-1}$, UCons1'
        ax.set_title(titletext)
        ax.set_xlim(xminp,xmaxp)
        ax.set_ylim(yminp,ymaxp)

        #ax.xaxis.set_major_locator(MultipleLocator(5))
        #ax.xaxis.set_minor_locator(MultipleLocator(1))
    
        #ax.yaxis.set_major_locator(MultipleLocator(0.005))
        #ax.yaxis.set_minor_locator(MultipleLocator(0.001))

        ax.yaxis.tick_left()
        ax.yaxis.set_ticks_position('both')

        # create legend and plot/font size
        ax.legend()
        ax.legend(handles=handles + [hh_patch] + [zz_patch], loc="upper left", numpoints=1, frameon=False, prop={'size':10})


        plot_type = 'xx_sigma_eta0_' + Tag + '_2D_out'
        # save the figure
        print('saving the figure')
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print('---')
        print('output in', outputdirectory + infile.replace('.dat','.pdf'))
        pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
        pl.close(fig)



        ################### cross section versus lambda112 plot START HERE #################################################
    
        # generate sigmas for multiple values:
    
        xmin = -100
        xmax = 100.
    
        lambda112_x = np.arange(xmin, xmax, (xmax-xmin)/1000)
    
        sigma_zz, sigma_hh = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2, streal, lambda112_x)
    
        outputdirectory = './hSstudy/'
    
        gs = gridspec.GridSpec(6,6)
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ax.grid(False)
    
        # range of figure
        xminp = -100
        xmaxp = 100.
        
        plt.plot( lambda112_x, sigma_hh, marker='', color='green', lw=3, ms=9, label='$h_1 h_1$')
        plt.plot( lambda112_x, sigma_zz, marker='', color='red', lw=3, ms=9, label='$ZZ$')

        ax.set_ylabel('$\\sigma(gg \\rightarrow h_2 \\rightarrow xx)$ [pb]', fontsize=20)
        ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
        titletext = '$\\sigma(gg \\rightarrow h_2 \\rightarrow xx)$ @ 100 TeV, UCons1, fixed $\\sin \\theta =' + str(round_sig(streal,3)) + '$'
        ax.set_title(titletext)
        ax.set_xlim(xminp,xmaxp)
    
        #ax.xaxis.set_major_locator(MultipleLocator(25))
        #ax.xaxis.set_minor_locator(MultipleLocator(5))
    
        #ax.yaxis.set_major_locator(MultipleLocator(0.1))
        #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
        #ax.set_yscale('log')
        plt.axvline(k112real, ls='--', label='True $\lambda_{112}$')
        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="center left", numpoints=1, frameon=False, prop={'size':10})
    
        plot_type = 'xx_sigma_heta0_' + Tag
        # save the figure
        print('saving the figure')
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print('---')
        print('output in', outputdirectory + infile.replace('.dat','.pdf'))
        pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
        pl.close(fig)

        ################### cross section versus sintheta plot START HERE #################################################
    
        # generate sigmas for multiple values:
    
        xmin = -0.05
        xmax =  0.05
    
        sintheta_x = np.arange(xmin, xmax, (xmax-xmin)/1000)
    
        sigma_zz, sigma_hh = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2, sintheta_x, k112real)
    
        outputdirectory = './hSstudy/'
    
        gs = gridspec.GridSpec(6,6)
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ax.grid(False)
    
        # range of figure
        xminp = -0.05
        xmaxp = 0.05
        
        plt.plot( sintheta_x, sigma_hh, marker='', color='green', lw=3, ms=9, label='$h_1 h_1$')
        plt.plot( sintheta_x, sigma_zz, marker='', color='red', lw=3, ms=9, label='$ZZ$')

        ax.set_ylabel('$\\sigma(gg \\rightarrow h_2 \\rightarrow xx)$ [pb]', fontsize=20)
        ax.set_xlabel('$\\sin \\theta$', fontsize=20)
        titletext = '$\\sigma(gg \\rightarrow h_2 \\rightarrow xx)$ @ 100 TeV, UCons1, fixed $\\lambda_{112} =' + str(round_sig(k112real,3)) + '$ GeV'
        ax.set_title(titletext)
        ax.set_xlim(xminp,xmaxp)
    
        #ax.xaxis.set_major_locator(MultipleLocator(0.01))
        #ax.xaxis.set_minor_locator(MultipleLocator(0.002))
    
        #ax.yaxis.set_major_locator(MultipleLocator(0.1))
        #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
        #ax.set_yscale('log')
        plt.axvline(streal, ls='--', label='True $\\sin \\theta$')
        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="center", numpoints=1, frameon=False, prop={'size':10})
    
        plot_type = 'xx_sigma_heta0_sintheta_' + Tag
        # save the figure
        print('saving the figure')
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print('---')
        print('output in', outputdirectory + infile.replace('.dat','.pdf'))
        pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
        pl.close(fig)
    
    return l112_min, l112_max, sintheta_min, sintheta_max


def get_sintheta_l112_fit_errormass(mh1, mh2, dmh2, streal, k112real, signif_zz, signif_hh, Tag):

    # whether to make the plots
    plotlamsin = False

    ##### the real cross sections:
    sigma_zz_real_up, sigma_hh_real_up = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2+dmh2, streal, k112real)
    #print 'real cross sections (up), ZZ, hh=', sigma_zz_real_up , sigma_hh_real_up 
    BR_ZZ_real_up, BR_hh_real_up = BR_h2_to_ZZ_or_hh(mh1, mh2+dmh2, streal, k112real)
    #print 'real BRs, ZZ, hh=', BR_ZZ_real_up, BR_hh_real_up

    sigma_zz_real_down, sigma_hh_real_down = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2-dmh2, streal, k112real)
    #print 'real cross sections (down), ZZ, hh=', sigma_zz_real, sigma_hh_real
    BR_ZZ_real_down, BR_hh_real_down = BR_h2_to_ZZ_or_hh(mh1, mh2-dmh2, streal, k112real)
    #print 'real BRs, ZZ, hh=', BR_ZZ_real_down, BR_hh_real_down
    
    #######################
    # THE FIT STARTS HERE #
    #######################

    outputdirectory = './hSstudy/'
    
    gs = gridspec.GridSpec(6,6)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    prec_sigma_zz = 1/signif_zz
    prec_sigma_hh = 1/signif_hh

    xmin = k112real * (1 - prec_sigma_hh)
    xmax = k112real * (1 + prec_sigma_hh)
    ymin = streal * (1 - 10*prec_sigma_zz)
    ymax = streal * (1 + 10*prec_sigma_zz)

    #print 'xmin, xmax, ymin, ymax=', xmin, xmax, ymin, ymax 

    #yminp = 0.05
    #ymaxp = 0.2
    #xminp = -250
    #xmaxp = 250.

    yminp = ymin
    ymaxp = ymax
    xminp = xmin
    xmaxp = xmax

    xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
    yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
    x1, y1 = np.meshgrid(xi,yi)

    zi_zz_up, zi_hh_up = sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2+dmh2, yi, xi)
    zi_zz_down, zi_hh_down = sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2-dmh2, yi, xi)

    #print zi_zz

    #print sigma

    #zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
    #ZZ (up)
    cs_up = plt.contour(x1, y1, zi_zz_up, levels=[sigma_zz_real_up*(1-prec_sigma_zz), sigma_zz_real_up*(1+prec_sigma_zz)], extend='both', colors='k')
    cs2_up = plt.contourf(x1, y1, zi_zz_up, levels=[sigma_zz_real_up*(1-prec_sigma_zz), sigma_zz_real_up*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

    #hh (up)
    cs3_up = plt.contour(x1, y1, zi_hh_up, levels=[sigma_hh_real_up*(1-prec_sigma_hh), sigma_hh_real_up*(1+prec_sigma_hh)], extend='both', colors='k')
    cs4_up = plt.contourf(x1, y1, zi_hh_up, levels=[sigma_hh_real_up*(1-prec_sigma_hh), sigma_hh_real_up*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

    # get the paths for the zz curve (up)
    p_up = cs_up.collections[0].get_paths()[0]
    v_up = p_up.vertices
    xm_zz_up = v_up[:,0]
    ym_zz_up = v_up[:,1]
    p_up = cs_up.collections[1].get_paths()[0]
    v_up = p_up.vertices
    xp_zz_up = v_up[:,0]
    yp_zz_up = v_up[:,1]

    # get the paths for the hh curve (up)
    p_up = cs3_up.collections[0].get_paths()[0]
    v_up = p_up.vertices
    xm_hh_up = v_up[:,0]
    ym_hh_up = v_up[:,1]
    p_up = cs3_up.collections[1].get_paths()[0]
    v_up = p_up.vertices
    xp_hh_up = v_up[:,0]
    yp_hh_up = v_up[:,1]

    L1_zz_up = line([xm_zz_up[0],ym_zz_up[0]], [xm_zz_up[-1],ym_zz_up[-1]])
    L2_zz_up = line([xp_zz_up[0],yp_zz_up[0]], [xp_zz_up[-1],yp_zz_up[-1]])
    L1_hh_up = line([xm_hh_up[0],ym_hh_up[0]], [xm_hh_up[-1],ym_hh_up[-1]])
    L2_hh_up = line([xp_hh_up[0],yp_hh_up[0]], [xp_hh_up[-1],yp_hh_up[-1]])

    #ZZ (down)
    cs_down = plt.contour(x1, y1, zi_zz_down, levels=[sigma_zz_real_down*(1-prec_sigma_zz), sigma_zz_real_down*(1+prec_sigma_zz)], extend='both', colors='k')
    cs2_down = plt.contourf(x1, y1, zi_zz_down, levels=[sigma_zz_real_down*(1-prec_sigma_zz), sigma_zz_real_down*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

    #hh (down)
    cs3_down = plt.contour(x1, y1, zi_hh_down, levels=[sigma_hh_real_down*(1-prec_sigma_hh), sigma_hh_real_down*(1+prec_sigma_hh)], extend='both', colors='k')
    cs4_down = plt.contourf(x1, y1, zi_hh_down, levels=[sigma_hh_real_down*(1-prec_sigma_hh), sigma_hh_real_down*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

    # get the paths for the zz curve (down)
    p_down = cs_down.collections[0].get_paths()[0]
    v_down = p_down.vertices
    xm_zz_down = v_down[:,0]
    ym_zz_down = v_down[:,1]
    p_down = cs_down.collections[1].get_paths()[0]
    v_down = p_down.vertices
    xp_zz_down = v_down[:,0]
    yp_zz_down = v_down[:,1]

    # get the paths for the hh curve (down)
    p_down = cs3_down.collections[0].get_paths()[0]
    v_down = p_down.vertices
    xm_hh_down = v_down[:,0]
    ym_hh_down = v_down[:,1]
    p_down = cs3_down.collections[1].get_paths()[0]
    v_down = p_down.vertices
    xp_hh_down = v_down[:,0]
    yp_hh_down = v_down[:,1]

    L1_zz_down = line([xm_zz_down[0],ym_zz_down[0]], [xm_zz_down[-1],ym_zz_down[-1]])
    L2_zz_down = line([xp_zz_down[0],yp_zz_down[0]], [xp_zz_down[-1],yp_zz_down[-1]])
    L1_hh_down = line([xm_hh_down[0],ym_hh_down[0]], [xm_hh_down[-1],ym_hh_down[-1]])
    L2_hh_down = line([xp_hh_down[0],yp_hh_down[0]], [xp_hh_down[-1],yp_hh_down[-1]])

    R11_upup = intersection(L1_zz_up, L1_hh_up)
    R12_upup = intersection(L1_zz_up, L2_hh_up)
    R22_upup = intersection(L2_zz_up, L2_hh_up)
    R21_upup = intersection(L2_zz_up, L1_hh_up)

    R11_downdown = intersection(L1_zz_down, L1_hh_down)
    R12_downdown = intersection(L1_zz_down, L2_hh_down)
    R22_downdown = intersection(L2_zz_down, L2_hh_down)
    R21_downdown = intersection(L2_zz_down, L1_hh_down)

    R11_updown = intersection(L1_zz_up, L1_hh_down)
    R12_updown = intersection(L1_zz_up, L2_hh_down)
    R22_updown = intersection(L2_zz_up, L2_hh_down)
    R21_updown = intersection(L2_zz_up, L1_hh_down)

    R11_downup = intersection(L1_zz_down, L1_hh_up)
    R12_downup = intersection(L1_zz_down, L2_hh_up)
    R22_downup = intersection(L2_zz_down, L2_hh_up)
    R21_downup = intersection(L2_zz_down, L1_hh_up)
    
    #print 'intersections (up-up):', R11_upup, R12_upup, R22_upup, R21_upup
    #print 'intersections (down-down):', R11_downdown, R12_downdown, R22_downdown, R21_downdown
    #print 'intersections (down-up):', R11_downup, R12_downup, R22_downup, R21_downup
    #print 'intersections (up-down):', R11_updown, R12_updown, R22_updown, R21_updown

    #print 'limits:'
    #print 'lambda112 in [', min(R11[0], R12[0], R22[0], R21[0]), max(R11[0], R12[0], R22[0], R21[0]), ']', 'precision=', 2*(max(R11[0], R12[0], R22[0], R21[0])-min(R11[0], R12[0], R22[0], R21[0]))/(max(R11[0], R12[0], R22[0], R21[0])+min(R11[0], R12[0], R22[0], R21[0]))

    #print 'sintheta in [', min(R11[1], R12[1], R22[1], R21[1]), max(R11[1], R12[1], R22[1], R21[1]), ']', 'precision=', 2*(max(R11[1], R12[1], R22[1], R21[1])-min(R11[1], R12[1], R22[1], R21[1]))/(max(R11[1], R12[1], R22[1], R21[1])+min(R11[1], R12[1], R22[1], R21[1]))

    l112_min = min(abs(R11_upup[0]), abs(R12_upup[0]), abs(R22_upup[0]), abs(R21_upup[0]), abs(R11_downdown[0]), abs(R12_downdown[0]), abs(R22_downdown[0]), abs(R21_downdown[0]), abs(R11_updown[0]), abs(R12_updown[0]), abs(R22_updown[0]), abs(R21_updown[0]), abs(R11_downup[0]), abs(R12_downup[0]), abs(R22_downup[0]), abs(R21_downup[0]))
    l112_max = max(abs(R11_upup[0]), abs(R12_upup[0]), abs(R22_upup[0]), abs(R21_upup[0]), abs(R11_downdown[0]), abs(R12_downdown[0]), abs(R22_downdown[0]), abs(R21_downdown[0]), abs(R11_updown[0]), abs(R12_updown[0]), abs(R22_updown[0]), abs(R21_updown[0]), abs(R11_downup[0]), abs(R12_downup[0]), abs(R22_downup[0]), abs(R21_downup[0]))
    sintheta_min = min(abs(R11_upup[1]), abs(R12_upup[1]), abs(R22_upup[1]), abs(R21_upup[1]), abs(R11_downdown[1]), abs(R12_downdown[1]), abs(R22_downdown[1]), abs(R21_downdown[1]), abs(R11_updown[1]), abs(R12_updown[1]), abs(R22_updown[1]), abs(R21_updown[1]), abs(R11_downup[1]), abs(R12_downup[1]), abs(R22_downup[1]), abs(R21_downup[1]))
    sintheta_max = max(abs(R11_upup[1]), abs(R12_upup[1]), abs(R22_upup[1]), abs(R21_upup[1]), abs(R11_downdown[1]), abs(R12_downdown[1]), abs(R22_downdown[1]), abs(R21_downdown[1]), abs(R11_updown[1]), abs(R12_updown[1]), abs(R22_updown[1]), abs(R21_updown[1]), abs(R11_downup[1]), abs(R12_downup[1]), abs(R22_downup[1]), abs(R21_downup[1]))



    #print 'lambda112 in [', l112_min, l112_max']'
    #print 'sintheta in [', sintheta_min, sintheta_max, ']'
    
    return l112_min, l112_max, sintheta_min, sintheta_max

def get_sintheta_l112_fit_errormass_curve(mh1, mh2, dmh2, streal, k112real, signif_zz, signif_hh, Tag):

    # whether to make the plots
    plotlamsin = False

    # look at the upper-right quadrant:
    streal = abs(streal)
    k112real = abs(k112real)

    ##### the real cross sections:
    sigma_zz_real_up, sigma_hh_real_up = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2+dmh2, streal, k112real)
    #print 'real cross sections (up), ZZ, hh=', sigma_zz_real_up , sigma_hh_real_up 
    BR_ZZ_real_up, BR_hh_real_up = BR_h2_to_ZZ_or_hh(mh1, mh2+dmh2, streal, k112real)
    #print 'real BRs, ZZ, hh=', BR_ZZ_real_up, BR_hh_real_up

    sigma_zz_real_down, sigma_hh_real_down = sigma_pp_h2_to_ZZ_or_hh(mh1, mh2-dmh2, streal, k112real)
    #print 'real cross sections (down), ZZ, hh=', sigma_zz_real, sigma_hh_real
    BR_ZZ_real_down, BR_hh_real_down = BR_h2_to_ZZ_or_hh(mh1, mh2-dmh2, streal, k112real)
    #print 'real BRs, ZZ, hh=', BR_ZZ_real_down, BR_hh_real_down
    
    #######################
    # THE FIT STARTS HERE #
    #######################

    outputdirectory = './hSstudy/'

    # factors that determine the ranges
    precfac_zz = 100.
    precfac_hh = 20.
    
    if signif_hh < 1.0001:
        signif_hh = 1.0001
        
    prec_sigma_hh = 1/signif_hh
    xmin = 0.
    xmax = k112real * (1 + precfac_hh*prec_sigma_hh)

    prec_sigma_zz = 1/signif_zz
    ymin = 0.
    #ymin = streal * (1 - precfac_zz*prec_sigma_zz)
    ymax = streal * (1 + precfac_zz*prec_sigma_zz)

    #print('xmin, xmax, ymin, ymax=', xmin, xmax, ymin, ymax)
    #print('prec_sigma_zz=', prec_sigma_zz)
    
    #yminp = 0.05
    #ymaxp = 0.2
    #xminp = -250
    #xmaxp = 250.

    yminp = ymin
    ymaxp = ymax
    xminp = xmin
    xmaxp = xmax

    xi = np.arange(xmin, xmax, (xmax-xmin)/1000)
    yi = np.arange(ymin, ymax, (ymax-ymin)/1000)
    x1, y1 = np.meshgrid(xi,yi)

    zi_zz_up, zi_hh_up = sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2+dmh2, yi, xi)
    zi_zz_down, zi_hh_down = sigma_pp_h2_to_ZZ_or_hh_plot(mh1, mh2-dmh2, yi, xi)

    #print(zi_zz_up)
    #print(zi_hh_up)

    #print sigma

    #zi = matplotlib.mlab.griddata(sigma, xi, yi, xi, yi, interp='linear')
    #ZZ (up)
    cs_up = plt.contour(x1, y1, zi_zz_up, levels=[sigma_zz_real_up*(1-prec_sigma_zz), sigma_zz_real_up*(1+prec_sigma_zz)], extend='both', colors='k')
    cs2_up = plt.contourf(x1, y1, zi_zz_up, levels=[sigma_zz_real_up*(1-prec_sigma_zz), sigma_zz_real_up*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

    #hh (up)
    cs3_up = plt.contour(x1, y1, zi_hh_up, levels=[sigma_hh_real_up*(1-prec_sigma_hh), sigma_hh_real_up*(1+prec_sigma_hh)], extend='both', colors='k')
    cs4_up = plt.contourf(x1, y1, zi_hh_up, levels=[sigma_hh_real_up*(1-prec_sigma_hh), sigma_hh_real_up*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

    plot_new = False
    if plot_new:
            
        gs = gridspec.GridSpec(6,6)
        fig = pl.figure()
        ax = fig.add_subplot(111)
        ax.grid(False)
        
        #plt.plot( np.nan, np.nan, marker='', color='black', ms=0, label='$h_1 h_1$', lw=2)
        #plt.plot( np.nan, np.nan, marker='', color='black', ms=0, label='$ZZ$', lw=2, ls='--')
        #ax.add_patch(Rectangle((k112real, streal), k112real*100*prec_sigma_hh, streal*40*prec_sigma_zz,alpha=0.5, color='blue'))
     
        #ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
        ax.set_ylabel('$\\sin \\theta$', fontsize=20)
        ax.set_xlabel('$\\lambda_{112}$ [GeV]', fontsize=20)
        titletext = 'pp@100 TeV/30000 fb$^{-1}$, UCons1'
        ax.set_title(titletext)
        ax.set_xlim(xminp,xmaxp)
        ax.set_ylim(yminp,ymaxp)
        
        #ax.xaxis.set_major_locator(MultipleLocator(5))
        #ax.xaxis.set_minor_locator(MultipleLocator(1))
        
        #ax.yaxis.set_major_locator(MultipleLocator(0.005))
        #ax.yaxis.set_minor_locator(MultipleLocator(0.001))
        
        ax.yaxis.tick_left()
        ax.yaxis.set_ticks_position('both')
        
        # create legend and plot/font size
        ax.legend()
        ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':10})
        
    
        plot_type = 'xx_sigma_eta0_' + Tag + '_2D_out_NEW'
        # save the figure
        print('saving the figure')
        # save the figure in PDF format
        infile = plot_type + '.dat'
        print('---')
        print('output in', outputdirectory + infile.replace('.dat','.pdf'))
        pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
        pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
        pl.close(fig)


    # get the paths for the zz curve (up)
    p_up = cs_up.collections[0].get_paths()[0]
    v_up = p_up.vertices
    xm_zz_up = v_up[:,0]
    ym_zz_up = v_up[:,1]
    p_up = cs_up.collections[1].get_paths()[0]
    v_up = p_up.vertices
    xp_zz_up = v_up[:,0]
    yp_zz_up = v_up[:,1]

    # get the paths for the hh curve (up)
    p_up = cs3_up.collections[0].get_paths()[0]
    v_up = p_up.vertices
    xm_hh_up = v_up[:,0]
    ym_hh_up = v_up[:,1]
    p_up = cs3_up.collections[1].get_paths()[0]
    v_up = p_up.vertices
    xp_hh_up = v_up[:,0]
    yp_hh_up = v_up[:,1]

    L1_zz_up = line([xm_zz_up[0],ym_zz_up[0]], [xm_zz_up[-1],ym_zz_up[-1]])
    L2_zz_up = line([xp_zz_up[0],yp_zz_up[0]], [xp_zz_up[-1],yp_zz_up[-1]])
    L1_hh_up = line([xm_hh_up[0],ym_hh_up[0]], [xm_hh_up[-1],ym_hh_up[-1]])
    L2_hh_up = line([xp_hh_up[0],yp_hh_up[0]], [xp_hh_up[-1],yp_hh_up[-1]])

    #ZZ (down)
    cs_down = plt.contour(x1, y1, zi_zz_down, levels=[sigma_zz_real_down*(1-prec_sigma_zz), sigma_zz_real_down*(1+prec_sigma_zz)], extend='both', colors='k')
    cs2_down = plt.contourf(x1, y1, zi_zz_down, levels=[sigma_zz_real_down*(1-prec_sigma_zz), sigma_zz_real_down*(1+prec_sigma_zz)], cmap='inferno', alpha=0.4)

    #hh (down)
    cs3_down = plt.contour(x1, y1, zi_hh_down, levels=[sigma_hh_real_down*(1-prec_sigma_hh), sigma_hh_real_down*(1+prec_sigma_hh)], extend='both', colors='k')
    cs4_down = plt.contourf(x1, y1, zi_hh_down, levels=[sigma_hh_real_down*(1-prec_sigma_hh), sigma_hh_real_down*(1+prec_sigma_hh)], cmap='Greens', alpha=0.4)

    # get the paths for the zz curve (down)
    p_down = cs_down.collections[0].get_paths()[0]
    v_down = p_down.vertices
    xm_zz_down = v_down[:,0]
    ym_zz_down = v_down[:,1]
    p_down = cs_down.collections[1].get_paths()[0]
    v_down = p_down.vertices
    xp_zz_down = v_down[:,0]
    yp_zz_down = v_down[:,1]

    # get the paths for the hh curve (down)
    p_down = cs3_down.collections[0].get_paths()[0]
    v_down = p_down.vertices
    xm_hh_down = v_down[:,0]
    ym_hh_down = v_down[:,1]
    p_down = cs3_down.collections[1].get_paths()[0]
    v_down = p_down.vertices
    xp_hh_down = v_down[:,0]
    yp_hh_down = v_down[:,1]

    #print('manual intersection 11 upup =', manual_intersection(xm_zz_up, ym_zz_up, xp_hh_up, yp_hh_up))
    #print('manual intersection 12 upup =', manual_intersection(xm_zz_up, ym_zz_up, xm_hh_up, ym_hh_up))
    #print('manual intersection 21 upup =', manual_intersection(xp_zz_up, yp_zz_up, xm_hh_up, ym_hh_up))
    #print('manual intersection 22 upup =', manual_intersection(xp_zz_up, yp_zz_up, xp_hh_up, yp_hh_up))

    #print('manual intersection 11 downdown =', manual_intersection(xm_zz_down, ym_zz_down, xp_hh_down, yp_hh_down))
    #print('manual intersection 12 downdown =', manual_intersection(xm_zz_down, ym_zz_down, xm_hh_down, ym_hh_down))
    #print('manual intersection 21 downdown =', manual_intersection(xp_zz_down, yp_zz_down, xm_hh_down, ym_hh_down))
    #print('manual intersection 22 downdown =', manual_intersection(xp_zz_down, yp_zz_down, xp_hh_down, yp_hh_down))


    #print('manual intersection 11 downup =', manual_intersection(xm_zz_down, ym_zz_down, xp_hh_up, yp_hh_up))
    #print('manual intersection 12 downup =', manual_intersection(xm_zz_down, ym_zz_down, xm_hh_up, ym_hh_up))
    #print('manual intersection 21 downup =', manual_intersection(xp_zz_down, yp_zz_down, xm_hh_up, ym_hh_up))
    #print('manual intersection 22 downup =', manual_intersection(xp_zz_down, yp_zz_down, xp_hh_up, yp_hh_up))

    #print('manual intersection 11 updown =', manual_intersection(xm_zz_up, ym_zz_up, xp_hh_down, yp_hh_down))
    #print('manual intersection 12 updown =', manual_intersection(xm_zz_up, ym_zz_up, xm_hh_down, ym_hh_down))
    #print('manual intersection 21 updown =', manual_intersection(xp_zz_up, yp_zz_up, xm_hh_down, ym_hh_down))
    #print('manual intersection 22 updown =', manual_intersection(xp_zz_up, yp_zz_up, xp_hh_down, yp_hh_down))


    MR11_upup = manual_intersection(xm_zz_up, ym_zz_up, xp_hh_up, yp_hh_up)
    MR12_upup = manual_intersection(xm_zz_up, ym_zz_up, xm_hh_up, ym_hh_up)
    MR21_upup = manual_intersection(xp_zz_up, yp_zz_up, xm_hh_up, ym_hh_up)
    MR22_upup = manual_intersection(xp_zz_up, yp_zz_up, xp_hh_up, yp_hh_up)

    MR11_downdown = manual_intersection(xm_zz_down, ym_zz_down, xp_hh_down, yp_hh_down)
    MR12_downdown = manual_intersection(xm_zz_down, ym_zz_down, xm_hh_down, ym_hh_down)
    MR21_downdown = manual_intersection(xp_zz_down, yp_zz_down, xm_hh_down, ym_hh_down)
    MR22_downdown = manual_intersection(xp_zz_down, yp_zz_down, xp_hh_down, yp_hh_down)

    MR11_downup = manual_intersection(xm_zz_down, ym_zz_down, xp_hh_up, yp_hh_up)
    MR12_downup = manual_intersection(xm_zz_down, ym_zz_down, xm_hh_up, ym_hh_up)
    MR21_downup = manual_intersection(xp_zz_down, yp_zz_down, xm_hh_up, ym_hh_up)
    MR22_downup = manual_intersection(xp_zz_down, yp_zz_down, xp_hh_up, yp_hh_up)

    MR11_updown = manual_intersection(xm_zz_up, ym_zz_up, xp_hh_down, yp_hh_down)
    MR12_updown = manual_intersection(xm_zz_up, ym_zz_up, xm_hh_down, ym_hh_down)
    MR21_updown = manual_intersection(xp_zz_up, yp_zz_up, xm_hh_down, ym_hh_down)
    MR22_updown = manual_intersection(xp_zz_up, yp_zz_up, xp_hh_down, yp_hh_down)

    L1_zz_down = line([xm_zz_down[0],ym_zz_down[0]], [xm_zz_down[-1],ym_zz_down[-1]])
    L2_zz_down = line([xp_zz_down[0],yp_zz_down[0]], [xp_zz_down[-1],yp_zz_down[-1]])
    L1_hh_down = line([xm_hh_down[0],ym_hh_down[0]], [xm_hh_down[-1],ym_hh_down[-1]])
    L2_hh_down = line([xp_hh_down[0],yp_hh_down[0]], [xp_hh_down[-1],yp_hh_down[-1]])

    R11_upup = intersection(L1_zz_up, L1_hh_up)
    R12_upup = intersection(L1_zz_up, L2_hh_up)
    R22_upup = intersection(L2_zz_up, L2_hh_up)
    R21_upup = intersection(L2_zz_up, L1_hh_up)

    R11_downdown = intersection(L1_zz_down, L1_hh_down)
    R12_downdown = intersection(L1_zz_down, L2_hh_down)
    R22_downdown = intersection(L2_zz_down, L2_hh_down)
    R21_downdown = intersection(L2_zz_down, L1_hh_down)

    R11_updown = intersection(L1_zz_up, L1_hh_down)
    R12_updown = intersection(L1_zz_up, L2_hh_down)
    R22_updown = intersection(L2_zz_up, L2_hh_down)
    R21_updown = intersection(L2_zz_up, L1_hh_down)

    R11_downup = intersection(L1_zz_down, L1_hh_up)
    R12_downup = intersection(L1_zz_down, L2_hh_up)
    R22_downup = intersection(L2_zz_down, L2_hh_up)
    R21_downup = intersection(L2_zz_down, L1_hh_up)
    
    #print 'intersections (up-up):', R11_upup, R12_upup, R22_upup, R21_upup
    #print 'intersections (down-down):', R11_downdown, R12_downdown, R22_downdown, R21_downdown
    #print 'intersections (down-up):', R11_downup, R12_downup, R22_downup, R21_downup
    #print 'intersections (up-down):', R11_updown, R12_updown, R22_updown, R21_updown

    #print 'limits:'
    #print 'lambda112 in [', min(R11[0], R12[0], R22[0], R21[0]), max(R11[0], R12[0], R22[0], R21[0]), ']', 'precision=', 2*(max(R11[0], R12[0], R22[0], R21[0])-min(R11[0], R12[0], R22[0], R21[0]))/(max(R11[0], R12[0], R22[0], R21[0])+min(R11[0], R12[0], R22[0], R21[0]))

    #print 'sintheta in [', min(R11[1], R12[1], R22[1], R21[1]), max(R11[1], R12[1], R22[1], R21[1]), ']', 'precision=', 2*(max(R11[1], R12[1], R22[1], R21[1])-min(R11[1], R12[1], R22[1], R21[1]))/(max(R11[1], R12[1], R22[1], R21[1])+min(R11[1], R12[1], R22[1], R21[1]))

    l112_min = min(abs(R11_upup[0]), abs(R12_upup[0]), abs(R22_upup[0]), abs(R21_upup[0]), abs(R11_downdown[0]), abs(R12_downdown[0]), abs(R22_downdown[0]), abs(R21_downdown[0]), abs(R11_updown[0]), abs(R12_updown[0]), abs(R22_updown[0]), abs(R21_updown[0]), abs(R11_downup[0]), abs(R12_downup[0]), abs(R22_downup[0]), abs(R21_downup[0]))
    l112_max = max(abs(R11_upup[0]), abs(R12_upup[0]), abs(R22_upup[0]), abs(R21_upup[0]), abs(R11_downdown[0]), abs(R12_downdown[0]), abs(R22_downdown[0]), abs(R21_downdown[0]), abs(R11_updown[0]), abs(R12_updown[0]), abs(R22_updown[0]), abs(R21_updown[0]), abs(R11_downup[0]), abs(R12_downup[0]), abs(R22_downup[0]), abs(R21_downup[0]))
    sintheta_min = min(abs(R11_upup[1]), abs(R12_upup[1]), abs(R22_upup[1]), abs(R21_upup[1]), abs(R11_downdown[1]), abs(R12_downdown[1]), abs(R22_downdown[1]), abs(R21_downdown[1]), abs(R11_updown[1]), abs(R12_updown[1]), abs(R22_updown[1]), abs(R21_updown[1]), abs(R11_downup[1]), abs(R12_downup[1]), abs(R22_downup[1]), abs(R21_downup[1]))
    sintheta_max = max(abs(R11_upup[1]), abs(R12_upup[1]), abs(R22_upup[1]), abs(R21_upup[1]), abs(R11_downdown[1]), abs(R12_downdown[1]), abs(R22_downdown[1]), abs(R21_downdown[1]), abs(R11_updown[1]), abs(R12_updown[1]), abs(R22_updown[1]), abs(R21_updown[1]), abs(R11_downup[1]), abs(R12_downup[1]), abs(R22_downup[1]), abs(R21_downup[1]))

    M_l112_min = min(abs(MR11_upup[0]), abs(MR12_upup[0]), abs(MR22_upup[0]), abs(MR21_upup[0]), abs(MR11_downdown[0]), abs(MR12_downdown[0]), abs(MR22_downdown[0]), abs(MR21_downdown[0]), abs(MR11_updown[0]), abs(MR12_updown[0]), abs(MR22_updown[0]), abs(MR21_updown[0]), abs(MR11_downup[0]), abs(MR12_downup[0]), abs(MR22_downup[0]), abs(MR21_downup[0]))
    M_l112_max = max(abs(MR11_upup[0]), abs(MR12_upup[0]), abs(MR22_upup[0]), abs(MR21_upup[0]), abs(MR11_downdown[0]), abs(MR12_downdown[0]), abs(MR22_downdown[0]), abs(MR21_downdown[0]), abs(MR11_updown[0]), abs(MR12_updown[0]), abs(MR22_updown[0]), abs(MR21_updown[0]), abs(MR11_downup[0]), abs(MR12_downup[0]), abs(MR22_downup[0]), abs(MR21_downup[0]))
    M_sintheta_min = min(abs(MR11_upup[1]), abs(MR12_upup[1]), abs(MR22_upup[1]), abs(MR21_upup[1]), abs(MR11_downdown[1]), abs(MR12_downdown[1]), abs(MR22_downdown[1]), abs(MR21_downdown[1]), abs(MR11_updown[1]), abs(MR12_updown[1]), abs(MR22_updown[1]), abs(MR21_updown[1]), abs(MR11_downup[1]), abs(MR12_downup[1]), abs(MR22_downup[1]), abs(MR21_downup[1]))
    M_sintheta_max = max(abs(MR11_upup[1]), abs(MR12_upup[1]), abs(MR22_upup[1]), abs(MR21_upup[1]), abs(MR11_downdown[1]), abs(MR12_downdown[1]), abs(MR22_downdown[1]), abs(MR21_downdown[1]), abs(MR11_updown[1]), abs(MR12_updown[1]), abs(MR22_updown[1]), abs(MR21_updown[1]), abs(MR11_downup[1]), abs(MR12_downup[1]), abs(MR22_downup[1]), abs(MR21_downup[1]))

    #print('lambda112 in [', l112_min, l112_max, ']')
    #print('sintheta in [', sintheta_min, sintheta_max, ']')

    print('lambda112 in [', M_l112_min, M_l112_max, '] (curve intercept)')
    print('sintheta in [', M_sintheta_min, M_sintheta_max, '] (curve intersept)')
    
    return M_l112_min, M_l112_max, M_sintheta_min, M_sintheta_max

