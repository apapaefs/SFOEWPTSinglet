#! /usr/bin/env python

import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
import matplotlib.font_manager as fm
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, ScalarFormatter, FuncFormatter)
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
from scipy.optimize import fsolve
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
import pickle
import re
from matplotlib.colors import ListedColormap
from past.builtins import execfile

####################
####################
# USEFUL FUNCTIONS #                 
####################
####################

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)

# choose the next colour -- for plotting
ccount = 0
def next_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount]
    if ccount < 8:
        ccount = ccount + 1
    else:
        ccount = 0    
    return color_chosen

# do not increment colour in this case:
def same_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount-1]
    return color_chosen

#####################################
#####################################
# GENERAL SETTINGS FOR THIS MACHINE #
#####################################
#####################################

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 2:
    print('usage: run_singlet_analysis.py [mode] [analysis]')
    exit()

print("\nDriver script to run and collect results for the SM+Singlet analyses")
print("---")

# Herwig installation location on system:
HerwigInstallation = '/Users/apapaefs/Documents/Projects/Herwig-stable/'
HerwigSourceCmd = 'source ' + HerwigInstallation + 'bin/activate;'

# Herwig input file sub-dir and output for the events
HerwigLocation = 'Herwig/'
HerwigOutputLocation = HerwigLocation + 'events/'

# Input file templates for LO, MC@NLO and FxFx:
# the real files have a .in.template extension
HW_template = ['','', '']
HW_template[0] = 'Templates/HW-LO.in' # 0th element is LO
HW_template[1] = 'Templates/HW-MCatNLO.in' # element 1 is NLO
HW_template[2] = 'Templates/HW-FxFx.in' # element 2 is FxFx

# The reduction factor of the number of events between the LHE file and the actual HW run for each process:
Reduction_Fac = [ '', '', '' ]
Reduction_Fac[0] = 0.995
Reduction_Fac[1] = 0.995
Reduction_Fac[2] = 0.7 # The FxFx runs have some vetoing so we expect fewer events

# assumed systematic uncertainties on signal and background predictions (fractional):
aB = 0.05

# Branching ratios:
BR_z_ellell = 3.3632E-2 #  Z -> lepton lepton (one flavour)
BR_w_ellnu = 10.86E-2 # W -> lepton+neutrino (one flavour)
BR_z_vv = 0.2 # Z -> neutrino neutrino (all flavours)
BR_z_qq = 0.116 + 0.156 + 0.1203 + 0.1512 # Z -> qq
BR_h_bb = 0.569369
BR_h_gamgam = 0.00229

# MG5/aMC sub-dir
MGLocation = 'MG5_aMC_v2_9_7/'
# the run cards for LO and NLO MG5/aMC runs:
MG_runcard = ['','']
MG_runcard[0] = 'run_card-LO.dat' # 0th element is LO
MG_runcard[1] = 'run_card-NLO.dat' # element 1 is NLO

# location of digitization plots:
digit_plot_location = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/digitizationplots/' # SLASH AT END IMPORTANT!

# available modes:
genherwig = False # generate the Herwig input files
runherwig = False # run Herwig via the generated input files
runanalysis = False # run the analysis on the generated HwSim root files

# re-run?
ReRun = False
if len(args) > 2:
    if args[2] == "rerun":
        ReRun = True
        print('WARNING: Re-running everything forced! Even existing files')


############################
############################
# PROCESS SPECIFIC SECTION #
############################
############################

# dictionaries and lists for all processes
Analyses = [] # the list of valid analyses. The analysis must be added to this list if it is to be executable
Location = {} # the locaiton of the analysis directory (must include Herwig, Herwig/events and MG5 directory given by MGLocation
Executable = {} # the executable for the analysis
ExecutableCuts = {} # the executable for the analysis for the cuts run
ExecutableSmear = {} # the executable for the analysis for smearing
Processes = {}  # the processes to be considered, along with their "tag". In this case the mass of h2
OrderProcesses = {} # the order of the process: 0 for LO, 1 for NLO (relevant for the Herwig input files)
KFacsProcesses = {} # K-factors and other SYMMETRY factors for the processes
TotalKFacsProcesses = {} # K-factors on the *TOTAL* cross sections
FacFinalState = {} # the BR that gives you the final state i.e. in the case of h2 -> xx -> FS, the BR(xx -> FS)
MGProcessLocations = {} # includes the decays of events
MGProcessLocations_Undecayed = {} # the total cross sections (no decays)
HwSimLibrary = {} # the name of the HwSim library to be used in the Herwig runs (for saving the .root files) -> to be replaced in the templates
FatAnalysis = {} # add a "#" to remove the HwSimFat stuff in the Herwig file
Luminosity = {} # the luminosity in inverse femtobarn
NVariables = {} # the number of variables that go into the BDT -> needs to correspond to the ones in the analysis
Energy = {} # the energy for the analysis 
LatexName = {} # the LaTeX name for the analysis (includes the $$) for plotting
LatexNameParent = {} # the LaTeX name for the analysis (includes the $$) for plotting: for the "Parent" process. e.g. if we are looking at pp -> h2 -> ZZ -> 4ell, the parent process is pp -> h2 -> ZZ
BRElement = {} # the element of the Higgs Branching Ratio array corresponding to this decay mode
NBDTAnalysis = {} # the number of BDT runs to search for the optimal analysis
SignalFactorAnalysis = {} # The "initial guess" for the signal cross section multiplier used in the BDT solver


###################################
# IMPORT THE ANALYSIS INFORMATION #
###################################

# WARNING! MAKE SURE THAT THE SUBDIRECTORIES EXIST! Anaklysis_name/Herwig/events and the MG5 installation 

#######################################
# H ETA -> BB ZZ -> BB LL VV ANALYSIS #
#######################################

exec(compile(open('analysis_HETA_BBZZ_BB2l2v.py', "rb").read(), 'analysis_HETA_BBZZ_BB2l2v.py', 'exec'))

###################
###################
# SET PARAMETERS  #
###################
###################

# choose the mode from the command line
if 'genherwig' == args[0]:
    genherwig = True

if 'runherwig' == args[0]:
    runherwig = True

if 'runanalysis' == args[0]:
    runanalysis = True

# the current analysis (e.g. 'HW100')
CurrentAnalysis = args[1]


#############
#############
# EXECUTE!  #
#############
#############

# generate the Herwig input files from the templates:
if genherwig == True:
    print('Generating Herwig input files for the analysis:', CurrentAnalysis)
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        print('\tfor Process:', Process, 'order:', OrderProcesses[CurrentAnalysis][Process], 'S or B:', Processes[CurrentAnalysis][Process])
        # choose template according to LO or MC@NLO:
        HerwigInputTemplate = getTemplate(HW_template[OrderProcesses[CurrentAnalysis][Process]])
        # loop over the LHE files and write the HW inputs for each, increment a counter
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            hwinputfile = Location[CurrentAnalysis] + HerwigLocation + processname + '.in'
            parmtextsubs = {
                'PROCESSNAME' : processname, 
                'LHEFILE' : lhefile,
                'OUTPUTLOCATION' : outputlocation,
                'FatAnalysis' : FatAnalysis[CurrentAnalysis],
                'HwSimLibrary' : HwSimLibrary[CurrentAnalysis]
            }
            counter = counter + 1
            print('\t\twriting', hwinputfile)
            writeFile(hwinputfile, HerwigInputTemplate.substitute(parmtextsubs) )


if runherwig == True:
    print('Running Herwig from the input files previously generated, for:', CurrentAnalysis)
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            processname = 'HW-' + Process + '-' + str(counter)
            hwinputfile = processname + '.in'
            hwrunfile = processname + '.run'
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            if os.path.exists(rootfile) is True:
                 print('File', rootfile, 'exists')
            if os.path.exists(rootfile) is False or (os.path.exists(rootfile) is True and ReRun is True): # if the root file exists, do not proceed except if ReRun is true
                if os.path.exists(rootfile) is True and ReRun is True:
                    print('File', rootfile, 'exists, but have chosen to re-run!')
                # get the number of events in the corresponding lhe file:
                zgrepcommand = 'zgrep "= nevents" ' + lhefile
                print(zgrepcommand)
                p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    nevents = float(line.split()[0])
                print('\t\tHerwig reading:', hwinputfile)
                readcommand = HerwigSourceCmd + 'Herwig read ' + hwinputfile
                p = subprocess.Popen(readcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                #print out, err
                print('\t\tHerwig running:', hwrunfile, 'for', nevents, 'events')
                runcommand = HerwigSourceCmd + 'Herwig run ' + hwrunfile + ' -N' + str(int(nevents*Reduction_Fac[OrderProcesses[CurrentAnalysis][Process]]))
                p = subprocess.Popen(runcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                #print out, err
            counter = counter + 1
