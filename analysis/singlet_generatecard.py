from singlet_constraint_functions import *

# the dictionary of benchmark points
BenchmarkPoints = {}
# element                = [ 0    , 1    , 2    , 3   , 4   , 5   , 6    , 7    , 8     , 9  ] 
#BenchmarkPoints["NAME"] = [ costh, sinth, mass2, gam2, x0  , lamb, a_1, , a_2  , b_3   , b_4]
#BenchmarkPoints["B1max"] = [ 0.976, 0.220, 341.0, 2.42, 257., 0.92, -377., 0.392, -403., 0.77 ]
#BenchmarkPoints["B2max"]= [ 0.982, 0.188, 353.0, 2.17, 265., 0.99, -400., 0.446, -378., 0.69 ]
#BenchmarkPoints["B3max"] = [ 0.983, 0.181, 415.0, 1.59, 54.6, 0.17, -642., 3.80, -214., 0.16 ]
#BenchmarkPoints["B4max"] = [ 0.984, 0.176, 455.0, 2.08, 47.4, 0.18, -707., 4.63, -607., 0.85 ]
#BenchmarkPoints["B5max"] = [ 0.986, 0.164, 511.0, 2.44, 40.7, 0.18, -744., 5.17, -618., 0.82 ]
#BenchmarkPoints["B6max"] = [ 0.988, 0.153, 563.0, 2.92, 40.5, 0.19, -844., 5., -151., 0.083 ]
#BenchmarkPoints["B7max"] = [ 0.992, 0.129, 604.0, 2.82, 36.4, 0.18, -898., 7.36, -424., 0.28 ]
#BenchmarkPoints["B8max"] = [ 0.994, 0.113, 662.0, 2.97, 32.9, 0.17, -976., 8.98, -542., 0.53 ]
#BenchmarkPoints["B9max"] = [ 0.993, 0.115, 714.0, 3.27, 29.2, 0.18, -941., 8.28, 497., 0.38 ]
#BenchmarkPoints["B10max"] = [ 0.996, 0.094, 767.0, 2.83, 24.5, 0.17, -920., 9.87, 575., 0.41 ]
#BenchmarkPoints["B11max"] = [ 0.994, 0.105, 840.0, 4.03, 21.7, 0.19, -988., 9.22, 356., 0.83 ]
#BenchmarkPoints["NAME"] = [ costh, sinth, mass2, gam2,  x0  , lamb, a_1, , a_2  , b_3   , b_4]
#BenchmarkPoints["B1min"] = [ 0.999, 0.029, 343.0, 0.041, 105., 0.13, -850., 3.91, -106., 0.29 ]
#BenchmarkPoints["B2min"] = [ 0.973, 0.231, 350.0, 0.777, 225., 0.18, -639., 0.986,-111., 0.97 ]
#BenchmarkPoints["B3min"] = [ 0.980, 0.197, 419.0, 1.32,  234., 0.18, -981., 1.56, 0.42, 0.96 ]
#BenchmarkPoints["B4min"] = [ 0.999, 0.026, 463.0, 0.086, 56.8, 0.13, -763., 6.35, 113., 0.73 ]
#BenchmarkPoints["B5min"] = [ 0.999, 0.035, 545.0, 0.278, 50.2, 0.13, -949., 8.64, 151., 0.57 ]
#BenchmarkPoints["B6min"] = [ 0.999, 0.043, 563.0, 0.459, 33.0, 0.13, -716., 9.25, -448., 0.96 ]
#BenchmarkPoints["B7min"] = [ 0.984, 0.180, 609.0, 4.03,  34.2, 0.22, -822., 4.53, -183., 0.57 ]
#BenchmarkPoints["B8min"] = [ 0.987, 0.161, 676.0, 4.47,  30.3, 0.22, -931., 5.96, -680., 0.43 ]
#BenchmarkPoints["B9min"] = [ 0.990, 0.138, 729.0, 4.22,  27.3, 0.21, -909., 6.15, 603., 0.93 ]
#BenchmarkPoints["B10min"] = [ 0.995, 0.104, 792.0, 3.36, 22.2, 0.18, -936., 9.47, -848., 0.66 ]
#BenchmarkPoints["B11min"] = [ 0.994, 0.105, 841.0, 3.95, 21.2, 0.19, -955., 8.69, 684., 0.53 ]

#BenchmarkPoints["1701.04442_B6max"] = [0.986, math.sqrt(1-0.986**2), 511, 2.44, 40.7, 0.18, -744, 5.17, -618, 0.82]
#BenchmarkPoints["1701.04442_B8max"] = [0.992, math.sqrt(1-0.992**2), 604, 2.82, 36.4, 0.18, -898, 7.36, -424, 0.28]
#BenchmarkPoints["1701.04442_B12max"] = [0.994, math.sqrt(1-0.994**2), 840, 4.03, 21.7, 0.19, -988, 9.22, 356, 0.83]


# Our own benchmark points:
OurBenchmarkPoints = {}
#OurBenchmarkPoints["Test1"] = [246., 28.73104102624717, -8502.853839569823, 82218.30712566273,  -90.01898849641022, 0.9630232407603962, -256.63996730948486, 0.8036145723919945, 0.3496081158172681, 0.19551182514516174]
#OurBenchmarkPoints["Test2"] = [246., 38.33684785322958, -6587.397065540476, -3691.5394814469805,-87.080774337433, 2.4012672347502297, -48.86493693457646,  0.9858633109040484, 0.26570885900263347, 0.005376582674682184]
#OurBenchmarkPoints["Test3"] = [246., 5.65890561098029, -12106.838691305065, 329482.4124451493, -135.29550565863224, 0.8298442357440221, 347.28060190349277, 0.9422293921652389, 0.3388917549894227, 0.09094003321761433]
#OurBenchmarkPoints["Test4"] = [246., 27.23549843333949, -10223.662333809232, 172406.00093719576, -222.71088105447677, 1.5357924759301493, 248.84527364483927, 0.41163663024272745, 0.4236174932730022, 0.20078798963249367]
#OurBenchmarkPoints["Test5"] = [246., 13.93050742349821, -19442.79727868895, 460920.08832155436, -126.24641642233114, 2.5649942281785147, -314.8808356947973, 0.08440290415257423, 0.2482519295016067, 0.05680079520654735]
#OurBenchmarkPoints["Test6"] = [246., 14.968096932303679, -16062.496157302381, 743164.2490181373, -178.20737042777145, 0.9781188521256698, -346.94200265031577, 0.8090942816781423, 0.3314247301956522, 0.059555193128321283]

MaxPoints = 10000 # the maximum number of points to read in

# read the benchmarks from GW's files:
#BenchmarkDir = "GWpoints/"
#BenchmarkFile_CorrectMass = "parameters_with_correct_mass.m"
#OurBenchmarkPoints_CorrectMass = ReadGWBenchmarks(BenchmarkDir + BenchmarkFile_CorrectMass, "GWAP", MaxPoints)
#OurBenchmarkPoints.update(OurBenchmarkPoints_CorrectMass)


# CHOOSE CATEGORY
category = 1

# read the benchmarks from the processed files:
# NOTE THAT THE ORDER IS DIFFERENT THAN THE .m FILES ABOVE BUT THIS IS TAKEN CARE OF BY THE
# READING FUNCTION!
ProcessedBenchmarkDir = "../pointgenerator"

#OurBenchmarkPoints_PROCESSED_CENTR = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
#OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)

#OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_CENTR)

#OurBenchmarkPoints_PROCESSED_LIB = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
#OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_liberal.txt', "GWAPLib", MaxPoints)

#OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_LIB)

#OurBenchmarkPoints_PROCESSED_CONS = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
#OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_conservative.txt', "GWAPCons", MaxPoints)

#OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_CONS)
    
OurBenchmarkPoints_PROCESSED_UCONS = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_UCONS)



# PLOT OUTPUT DIRECTORY
addtag = ''
outputdirectory = 'plots_' + str(todaysdate) + '_cat' + str(category) + addtag + '/'
pickledir = 'data_' + str(todaysdate) + '_cat' + str(category) + addtag + '/'
# check if the directories exist and create them if not
if not os.path.exists(outputdirectory):
    os.makedirs(outputdirectory)
if not os.path.exists(pickledir):
    os.makedirs(pickledir)


Constraints = {}
for key in list(OurBenchmarkPoints.keys()):
    BenchmarkPoints[key] = ConvertBenchmarks(OurBenchmarkPoints[key])
    print(key, BenchmarkPoints[key])
    TypeTag = ''.join([i for i in key if not i.isdigit()])
    if TypeTag not in list(Constraints.keys()):
        Constraints[TypeTag] = []
        
# Select only one benchmark point to write out:
WriteBenchmarkSelection = 'GWAPUCons30'#'GWAPCons846' # #'GWAPCons1381' #'GWAPCons1278' #'GWAPCons639' #'GWAPCons1459' #'GWAPCons1129'

# get template for param card:                
param_card_template = getTemplate("param_card.dat")

# Luminosities in /fb
Lumis = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
Lumis27 = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]

#######################################################
# CALCULATION FOR heavy scalar parameters starts here #
#######################################################


print('Constraint calculator for SM+real singlet scalar')
print('---')

# remove points outside the region of interest for mh2
removed_point = 0

for key in list(BenchmarkPoints.keys()):
    print("benchmark point:", key)
    rho_max = OurBenchmarkPoints[key][-3]
    rho_av = OurBenchmarkPoints[key][-2]
    rho_std = OurBenchmarkPoints[key][-1]
    # Set the parameters via a function
    # get the parameters from our benchmark in the SARAH format:
    v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta = GetSARAHParams(OurBenchmarkPoints[key])
    
    # get the cosine:
    costheta = math.sqrt(1-sintheta**2)
    
    # convert the SARAH parameters to MRM: 
    lam, v0, x0, a1, a2, b2, b3, b4 = param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)

    # TESTING THE ONE-LOOP MASSES:
    mh1_1loop, mh2_1loop = OneLoop_masses_diag_scale_SARAH(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    # scale variation of the one-loop massses:
    mh1_1loop_at2mz, mh2_1loop_at2mz = OneLoop_masses_diag_scale_SARAH(2*91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    mh1_1loop_athalfmz, mh2_1loop_athalfmz = OneLoop_masses_diag_scale_SARAH(0.5*91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    # calculate the difference beteween mh1(2*mz) and mh1(0.5*mz)
    delta_mh1_mzvar = abs(mh1_1loop_athalfmz-mh1_1loop_at2mz)

    
    
    # check the sign of sintheta
    tree_sign = tree_sign_sintheta(v0, x0, a1, a2, mh1_1loop, mh2_1loop)
    #print 'tree_sign_sintheta=', tree_sign

    # change the sign of sintheta:
    sintheta = sintheta*tree_sign

    # calculate the fine tuning for the mass m1, m2:
    ft = finetuning_m1(lam, v0, x0, a1, a2, b3, b4)
    ft2 = finetuning_m2(lam, v0, x0, a1, a2, b3, b4)
    ft3 = finetuning_mu2b2(lam, v0, x0, a1, a2, b3, b4)

    # one-loop fine tuning:
    ft_mh1_oneloop, ft_mh2_oneloop = finetuning_oneloop(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)


    # get the tree-level 1-1-2 coupling:
    l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    
    # print the parameters
    #print_xsm_params(v0, x0, a1, a2, b3, b4)
    # convert the SM BRs and width to Heavy Higgs BRs and width for the model parameters:
    # BR positions: = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$h_1 h_1$', '$h_1 h_1 h_1$', '$\\Gamma$' ]
    
    calculate_tripe_Higgs_width = False # whether to calculate the triple Higgs h2 -> h1 h1 h1 width (SLOW!)
    HeavyHiggsBRs, mh1, mh2, Gam1, Gam2, BR_hh, name = convert_to_heavy_withtripleHiggs_OneLoop(BR_interpolators_SM, key, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, costheta, calculate_tripe_Higgs_width)
    
   
    mh1_1loop_atMH, mh2_1loop_atMH = OneLoop_masses_diag_scale_SARAH(125., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    mh1_1loop_atMt, mh2_1loop_atMt = OneLoop_masses_diag_scale_SARAH(mt, v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt) 
    mh1_1loop_atv0, mh2_1loop_atv0 = OneLoop_masses_diag_scale_SARAH(v0, v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)

    # remove points outside certain range of interest. 
    if mh2 >= 2000. or mh2 < 200.:
        removed_point = removed_point+1
        continue

    # get the tree-level masses for comparison
    mh1_tree, mh2_tree, sintheta_tree, costheta_tree = mass_and_mixing(lam, v0, x0, a1, a2, b3, b4)
    mh1_tree_MRM, mh2_tree_MRM = mass_and_mixing_MRM(lam, v0, x0, a1, a2, b3, b4)
    delta_mh2_treevsloop = abs(mh2_1loop-mh2_tree)
        
    # print the parameters and masses
    #print_xsm_params_and_masses(lam, v0, x0, a1, a2, b3, b4, round_sig(mh1_tree,5), round_sig(mh2_tree,5), round_sig(mh1_1loop,5), round_sig(mh2_1loop,5))
    # WARNING! WARNING!
    # RESET THE HIGGS MASS AND WIDTH TO THE SM!
    # WARNING! WARNING!
    mh1 = 125.1
    #Gam1 = 0.00411

    # get the light Higgs BRs as well -- rescaled by costheta:
    LightHiggsBRs = []
    for hh in range(len(BR_interpolators_SM)):
        LightHiggsBRs.append(BR_interpolators_SM[hh](mh1) * costheta**2)
        
    # get the 13 TeV N3LO cross section for h2:
    #xs13_n3lo = round_sig(sintheta**2 * XS_interpolator_SM_13TeV_N3LO(mh2),5)
    xs13_n3lo = round_sig(sintheta**2 * XS_interpolator_SM_13TeV_NNLONNLL(mh2),5)
    
    # get the 14, 8, 7 TeV NNLO+NNLL cross sections for h2:
    xs14_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_14TeV_NNLONNLL(mh2),5)
    xs8_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_8TeV_NNLONNLL(mh2),5)
    xs7_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_7TeV_NNLONNLL(mh2),5)
    # get the 100 TeV cross sections:
    xs100_n3lon3ll = round_sig(sintheta**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2),5)
    xs100_vbf_nlo = round_sig(sintheta**2 * XS_interpolator_SM_100TeV_VBF_NLO(mh2),5)
    # get the 27 TeV cross sections:
    xs27_n3lon3ll = round_sig(sintheta**2 * XS_interpolator_SM_27TeV_N3LON3LL(mh2),5)

    # get the EWPO constraints:
    chisq_EWPO_cur = get_chisq_EWPO(mh1, mh2, sintheta, mz, mw, Delta_S_central, Delta_T_central, errS, errT, covST)
    chisq_EWPO_fut = get_chisq_EWPO(mh1, mh2, sintheta, mz, mw, Delta_S_central_F, Delta_T_central_F, errS_F, errT_F, covST_F)
    # check the "current" ATLAS+CMS constraints (1909.02845 and CMS HIG-19-005-PAS):
    # the central value is above mu = 1 so the current constraint is stronger than expected.
    # for HL-LHC studies see: ATL-PHYS-PUB-2014-016, CMS-FTR-011-PAS
    if sintheta**2 > 0.05:
        couplstr_cur = 0
    else:
        couplstr_cur = 1
    # FCC-hh and FCC-ee give similar constraints (see FCC Physics p. 53, table 4.4)    
    if sintheta**2 > 0.01:
        couplstr_fut = 0
    else:
        couplstr_fut = 1
    
    # get the sigma(h2) * BR for the processes to check:
    BR_ZZ = HeavyHiggsBRs[10]
    BR_WW = HeavyHiggsBRs[9]
    BR_hhh = HeavyHiggsBRs[12]
    xs13_ZZ = xs13_n3lo * BR_ZZ
    xs13_WW = xs13_n3lo * BR_WW
    xs14_WW = xs14_nnlonnll * BR_WW
    xs13_HH = xs13_n3lo * BR_hh
    xs100_WW = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_WW
    xs100_ZZ = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_ZZ
    xs100_HH = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_hh

    # get the sigma(h2) * BR (GGF only) for 27 TeV:
    xs27_WW = (xs27_n3lon3ll) * BR_WW
    xs27_ZZ = (xs27_n3lon3ll) * BR_ZZ
    xs27_HH = (xs27_n3lon3ll) * BR_hh



    # get the HL-LHC projections:
    limit_HLLHC_ZZ_central, limit_HLLHC_ZZ_1sigma, limit_HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag = get_xsec_limit_future(mh2, HLLHC_ZZ_central, HLLHC_ZZ_1sigma, HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag)
    limit_HLLHC_WW_central, limit_HLLHC_WW_1sigma, limit_HLLHC_WW_2sigma, HLLHC_WW_process_tag = get_xsec_limit_future(mh2, HLLHC_WW_central, HLLHC_WW_1sigma, HLLHC_WW_2sigma, HLLHC_WW_process_tag)
    
    # current ATLAS/CMS HH limits
    limit_ATLAS_HH_central, ATLAS_HH_process_tag = get_xsec_limit_current(mh2, ATLAS_HH_central, ATLAS_HH_process_tag) # 27.5-36.1/fb
    limit_CMS_HH_central, CMS_HH_process_tag = get_xsec_limit_current(mh2, CMS_HH_central, CMS_HH_process_tag) # 35.9/fb

    # current ATLAS WW/CMS ZZ limits
    limit_ATLAS_WW_central, ATLAS_WW_process_tag = get_xsec_limit_current(mh2, ATLAS_WW_central, ATLAS_WW_process_tag) # 36.1/fb
    limit_CMS_ZZ_central, CMS_ZZ_process_tag = get_xsec_limit_current(mh2, CMS_ZZ_central, CMS_ZZ_process_tag) # 35.9/fb

    # extrapolate naively to 3000/fb
    limit_ATLAS_HH_extrap = limit_ATLAS_HH_central * math.sqrt(36.1/3000.)
    limit_CMS_HH_extrap = limit_CMS_HH_central * math.sqrt(35.9/3000.)
    limit_ATLAS_WW_extrap = limit_ATLAS_WW_central * math.sqrt(36.1/3000.)
    limit_CMS_ZZ_extrap = limit_CMS_ZZ_central * math.sqrt(35.9/3000.)

    # get the FCC 100 TeV limits (30/ab):
    limit_FCC_WW_central, limit_FCC_WW_1sigma, limit_FCC_WW_2sigma, FCC_WW_process_tag = get_xsec_limit_future(mh2, FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag)
    limit_FCC_ZZ_central, limit_FCC_ZZ_1sigma, limit_FCC_ZZ_2sigma, FCC_ZZ_process_tag = get_xsec_limit_future(mh2, FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag)
    limit_FCC_HH_central, limit_FCC_HH_1sigma, limit_FCC_HH_2sigma, FCC_HH_process_tag = get_xsec_limit_future(mh2, FCC_HH_central, FCC_HH_1sigma, FCC_HH_2sigma, FCC_HH_tag)
    # and at different luminosities:
    signif100_ZZ_lumi = {}
    signif100_HH_lumi = {}
    signif100_WW_lumi = {}
    signif100_MAX_lumi = {}
    limit_FCC_HH_lumi_array = {}
    limit_FCC_HH_lumi_tag = {}
    limit_FCC_WW_lumi_array = {}
    limit_FCC_WW_lumi_tag = {}
    limit_FCC_ZZ_lumi_array = {}
    limit_FCC_ZZ_lumi_tag = {}
    for Lumi in Lumis:
        limit_FCC_HH_lumi_array[Lumi], limit_FCC_HH_lumi_tag[Lumi] = get_xsec_limit_current(mh2, FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi])
        signif100_HH_lumi[Lumi] = xs100_HH * 2 / limit_FCC_HH_lumi_array[Lumi]
        limit_FCC_WW_lumi_array[Lumi], limit_FCC_WW_lumi_tag[Lumi] = get_xsec_limit_current(mh2, FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi])
        signif100_WW_lumi[Lumi] = xs100_WW * 2 / limit_FCC_WW_lumi_array[Lumi]
        limit_FCC_ZZ_lumi_array[Lumi], limit_FCC_ZZ_lumi_tag[Lumi] = get_xsec_limit_current(mh2, FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi])
        signif100_ZZ_lumi[Lumi] = xs100_ZZ * 2 / limit_FCC_ZZ_lumi_array[Lumi]
        # get the maximum significance for each point at each lumi
        signif100_MAX_lumi[Lumi] = max([signif100_ZZ_lumi[Lumi], signif100_HH_lumi[Lumi], signif100_WW_lumi[Lumi]])
    # 27 TeV limits (15/ab):
    Lumi27 = 15000.
    limit_HLLHC_HH_GGF_central, HLLHC_HH_GGF_process_tag = get_xsec_limit_current(mh2, HE_HH_lumi_GGF[Lumi27], HE_HH_lumi_GGF_tag[Lumi27])
    limit_HLLHC_HH_QQ_central, HLLHC_HH_QQ_process_tag = get_xsec_limit_current(mh2, HE_HH_lumi_QQ[Lumi27], HE_HH_lumi_QQ_tag[Lumi27])
    limit_HLLHC_ZZ_GGF_central, HLLHC_ZZ_GGF_process_tag = get_xsec_limit_current(mh2, HE_ZZ_lumi_GGF[Lumi27], HE_ZZ_lumi_GGF_tag[Lumi27])
    limit_HLLHC_ZZ_QQ_central, HLLHC_ZZ_QQ_process_tag = get_xsec_limit_current(mh2, HE_ZZ_lumi_QQ[Lumi27], HE_ZZ_lumi_QQ_tag[Lumi27])
    limit_HLLHC_WW_GGF_central, HLLHC_WW_GGF_process_tag = get_xsec_limit_current(mh2, HE_WW_lumi_GGF[Lumi27], HE_WW_lumi_GGF_tag[Lumi27])
    limit_HLLHC_WW_QQ_central, HLLHC_WW_QQ_process_tag = get_xsec_limit_current(mh2, HE_WW_lumi_QQ[Lumi27], HE_WW_lumi_QQ_tag[Lumi27])
    
    # and at different luminosities:
    limit_HLLHC_HH_GGF_lumi = {}
    HLLHC_HH_GGF_process_tag_lumi = {}
    limit_HLLHC_HH_QQ_lumi = {}
    HLLHC_HH_QQ_process_tag_lumi = {}
    limit_HLLHC_ZZ_GGF_lumi = {}
    HLLHC_ZZ_GGF_process_tag_lumi = {}
    limit_HLLHC_ZZ_QQ_lumi = {}
    HLLHC_ZZ_QQ_process_tag_lumi = {}
    limit_HLLHC_WW_GGF_lumi = {}
    HLLHC_WW_GGF_process_tag_lumi = {}
    limit_HLLHC_WW_QQ_lumi = {}
    HLLHC_WW_QQ_process_tag_lumi = {}
    signif27_ZZ_lumi = {}
    signif27_WW_lumi = {}
    signif27_HH_lumi = {}
    signif27_MAX_lumi = {}
    for Lumi27 in Lumis27:
        limit_HLLHC_HH_GGF_lumi[Lumi27], HLLHC_HH_GGF_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_HH_lumi_GGF[Lumi27], HE_HH_lumi_GGF_tag[Lumi27])
        limit_HLLHC_HH_QQ_lumi[Lumi27], HLLHC_HH_QQ_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_HH_lumi_QQ[Lumi27], HE_HH_lumi_QQ_tag[Lumi27])
        limit_HLLHC_ZZ_GGF_lumi[Lumi27], HLLHC_ZZ_GGF_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_ZZ_lumi_GGF[Lumi27], HE_ZZ_lumi_GGF_tag[Lumi27])
        limit_HLLHC_ZZ_QQ_lumi[Lumi27], HLLHC_ZZ_QQ_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_ZZ_lumi_QQ[Lumi27], HE_ZZ_lumi_QQ_tag[Lumi27])
        limit_HLLHC_WW_GGF_lumi[Lumi27], HLLHC_WW_GGF_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_WW_lumi_GGF[Lumi27], HE_WW_lumi_GGF_tag[Lumi27])
        limit_HLLHC_WW_QQ_lumi[Lumi27], HLLHC_WW_QQ_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_WW_lumi_QQ[Lumi27], HE_WW_lumi_QQ_tag[Lumi27])
        signif27_ZZ_lumi[Lumi27] = max([xs27_ZZ * 2 / limit_HLLHC_ZZ_GGF_lumi[Lumi27], xs27_ZZ * 2 / limit_HLLHC_ZZ_QQ_lumi[Lumi27]])
        signif27_WW_lumi[Lumi27] = max([xs27_WW * 2 / limit_HLLHC_WW_GGF_lumi[Lumi27], xs27_WW * 2 / limit_HLLHC_WW_QQ_lumi[Lumi27]])
        signif27_HH_lumi[Lumi27] = max([xs27_HH * 2 / limit_HLLHC_HH_GGF_lumi[Lumi27], xs27_HH * 2 / limit_HLLHC_HH_QQ_lumi[Lumi27]])
        signif27_MAX_lumi[Lumi27] = max([signif27_ZZ_lumi[Lumi27], signif27_HH_lumi[Lumi27], signif27_WW_lumi[Lumi27]])

    # print info:
    #if abs(sintheta) < 0.1 and mh2_1loop > 600. and mh2_1loop < 800.:
    printall = False
    if printall is True:
        print_SARAH_params_and_masses(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, round_sig(mh1_1loop,5), round_sig(mh2_1loop,5))
        print_XSECS_AND_BRS(xs13_n3lo, xs27_n3lon3ll, xs100_n3lon3ll, BR_ZZ, BR_WW, BR_hh)
        print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy_withtripleHiggs, 'Heavy Higgs BRs & width')
        print('mh1_tree_MRM, mh2_tree_MRM=', mh1_tree_MRM, mh2_tree_MRM)
        print('tree-level fine tuning (m1)=', ft, '(m2)=', ft2, '(dekens,mu2,b2)=', ft3, 'delta_mh1_mzvar=',delta_mh1_mzvar)
        print('one-loop fine tuning', ft_mh1_oneloop, ft_mh2_oneloop)
        print("rho_max/av/std=", rho_max, rho_av, rho_std)
        print("At One Loop (Sc=MZ): mh1, mh2=", mh1_1loop, mh2_1loop)
        print("At One Loop (Sc=MH): mh1, mh2=", mh1_1loop_atMH, mh2_1loop_atMH)
        print("At One Loop (Sc=MT): mh1, mh2=", mh1_1loop_atMt, mh2_1loop_atMt)
        print("At One Loop (Sc=v0): mh1, mh2=", mh1_1loop_atv0, mh2_1loop_atv0)

        # print xs and xs constraints
        print('xs13_HH, limit_ATLAS_HH_central=',xs13_HH, limit_ATLAS_HH_central)
        print('xs13_HH, limit_CMS_HH_central=',xs13_HH, limit_CMS_HH_central)
        print('xs13_ZZ, limit_HLLHC_ZZ_central=',xs13_ZZ, limit_HLLHC_ZZ_central)
        print('xs13_WW, limit_HLLHC_WW_central=',xs13_WW, limit_HLLHC_WW_central)
        print('xs13_ZZ, limit_CMS_ZZ_extrap=',xs13_ZZ, limit_CMS_ZZ_extrap)
        print('xs13_WW, limit_ATLAS_WW_extrap=',xs13_WW, limit_ATLAS_WW_extrap)
        print('xs100_WW, limit_FCC_WW_central=', xs100_WW, limit_FCC_WW_central)
        print('xs100_ZZ, limit_FCC_ZZ_central=', xs100_ZZ, limit_FCC_ZZ_central)
        print('xs100_HH, limit_FCC_HH_central=', xs100_HH, limit_FCC_HH_central)

        #l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        print('l111, l112, l122, l1112=', l111, l112, l122, l1112)
        

    # calculate the significance for this point at 100 TeV/30 inv.ab
    signif100_ZZ = xs100_ZZ * 2 / limit_FCC_ZZ_central
    signif100_WW = xs100_WW * 2 / limit_FCC_WW_central
    signif100_HH = xs100_HH * 2 / limit_FCC_HH_central
    signif100_MAX = max([signif100_ZZ, signif100_HH, signif100_WW])

    # calculate the maximum significance for this point at 27 TeV/15 inv.ab
    signif27_ZZ = max([xs27_ZZ * 2 / limit_HLLHC_ZZ_GGF_central, xs27_ZZ * 2 / limit_HLLHC_ZZ_QQ_central])
    signif27_WW = max([xs27_WW * 2 / limit_HLLHC_WW_GGF_central, xs27_WW * 2 / limit_HLLHC_WW_QQ_central])
    signif27_HH = max([xs27_HH * 2 / limit_HLLHC_HH_GGF_central, xs27_HH * 2 / limit_HLLHC_HH_QQ_central])
    signif27_MAX = max([signif27_ZZ, signif27_HH, signif27_WW])
    
    # check cross sections against these limits
    
    # CHECK CURRENT: 
    # ATLAS CURRENT HH:
    if xs13_HH > limit_ATLAS_HH_central:
        ATLAS_HH = 0
    else:
        ATLAS_HH = 1
    # CMS CURRENT HH:
    if xs13_HH > limit_CMS_HH_central:
        CMS_HH = 0
    else:
        CMS_HH = 1

    # CHECK HL-LHC:
    # HL-LHC ZZ:
    if xs13_ZZ > limit_HLLHC_ZZ_central:
        HLLHC_ZZ = 0
    else:
        HLLHC_ZZ = 1
    # HL-LHC WW:
    if xs14_WW > limit_HLLHC_WW_central:
        HLLHC_WW = 0
    else:
        HLLHC_WW = 1
    # ATLAS HH HL-LHC extrap.:
    if xs13_HH > limit_ATLAS_HH_extrap:
        ATLAS_HH_extrap = 0
    else:
        ATLAS_HH_extrap = 1
    # CMS HH HL-LHC extrap.:
    if xs13_HH > limit_CMS_HH_extrap:
        CMS_HH_extrap = 0
    else:
        CMS_HH_extrap = 1
    # ATLAS WW HL-LHC extrap., if mh2 < 550 GeV:
    if mh2 < 550.:
        if xs13_WW > limit_ATLAS_WW_extrap:
            ATLAS_WW_extrap = 0
        else:
            ATLAS_WW_extrap = 1
        # CMS ZZ HL-LHC extrap.:
        if xs13_ZZ > limit_CMS_ZZ_extrap:
            CMS_ZZ_extrap = 0
        else:
            CMS_ZZ_extrap = 1
        HLLHC_ZZ = CMS_ZZ_extrap
        HLLHC_WW = ATLAS_WW_extrap

    # CHECK THE FCC CONSTRAINTS
    if xs100_WW > limit_FCC_WW_central:
        FCC_WW_pass = 0
    else:
        FCC_WW_pass = 1
    if xs100_HH > limit_FCC_HH_central:
        FCC_HH_pass = 0
    else:
        FCC_HH_pass = 1
    if xs100_ZZ > limit_FCC_ZZ_central:
        FCC_ZZ_pass = 0
    else:
        FCC_ZZ_pass = 1
    

    # check the CLIC constraints:
    GammaSM = BR_interpolators_SM[-1](mh2) 
    #check_sthetam2_limit(stheta, m1, m2, l112, GammaSM, limtype, finterp, process_tag)
    CLIC_VV_pass = check_sthetam2_limit(v0, abs(sintheta), mh1, mh2, l112, GammaSM, "VV", CLIC_VV, CLIC_VV_tag) # this is also 3 TeV
    CLIC3_HH_pass = check_sthetam2_limit(v0, abs(sintheta), mh1, mh2, l112, GammaSM, "hh", CLIC3_HH, CLIC3_HH_tag)
    CLIC14_HH_pass = check_sthetam2_limit(v0, abs(sintheta), mh1, mh2, l112, GammaSM, "hh", CLIC14_HH, CLIC3_HH_tag)


    # the point info
    xsm_point = [name, mh1, mh2, Gam1, Gam2, sintheta, costheta, BR_hh, xs13_n3lo, xs14_nnlonnll, xs8_nnlonnll, xs7_nnlonnll, LightHiggsBRs, HeavyHiggsBRs]
    xsm_constraints = [couplstr_cur, couplstr_fut, chisq_EWPO_cur, chisq_EWPO_fut, ATLAS_HH, CMS_HH, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap, CLIC14_HH_pass, CLIC_VV_pass, CLIC3_HH_pass, FCC_WW_pass, FCC_ZZ_pass, FCC_HH_pass, signif100_HH_lumi, signif100_WW_lumi, signif100_ZZ_lumi, signif100_MAX_lumi,  signif27_HH_lumi, signif27_WW_lumi, signif27_ZZ_lumi, signif27_MAX_lumi, signif100_HH, signif100_WW, signif100_ZZ, signif100_MAX, ft]
    
    # add the info to an array:
    xsm_point_info.append(xsm_point)
    xsm_point_constraints.append(xsm_constraints)

    # save the one loop h1 mass/ h2 mass/sintheta and tree-level l112 in a dictionary:
    TypeTag = ''.join([i for i in key if not i.isdigit()])
    
    # debug signif100_MAX
    if signif100_MAX == 0.0:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! signif100_MAX is ZERO')


    # use the information to write out the param_card.dat
    if key == WriteBenchmarkSelection:
        # write out the parameter card:
        def write_param_card(paramfile, paramsubs):
            writeFile('param_card.dat', param_card_template.substitute(paramsubs) )
        l2222 = lambda2222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1222 = lambda1222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1122 = lambda1122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1111 = lambda1111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l222 = lambda222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        paramsubs = {
            'CT' : costheta, 
            'ST' : sintheta,
            'M1' : mh1,
            'M2' : mh2,
            'width1': Gam1,
            'width2': Gam2,
            'K111': l111,
            'K112': l112,
            'K122': l122,
            'K222': l222,
            'K1111' : l1111,
            'K1112' : l1112,
            'K1122' : l1122,
            'K1222' : l1222, 
            'K2222' : l2222,
            'lam' : lam,
            'v0' : v0,
            'a1' : a1,
            'a2' : a2,
            'b2' : b2,
            'b3' : b3,
            'b4' : b4,
            'bb' : HeavyHiggsBRs[0],
            'tautau' : HeavyHiggsBRs[1],
            'mumu' : HeavyHiggsBRs[2],
            'cc' : HeavyHiggsBRs[3],
            'ss' : HeavyHiggsBRs[4],
            'tt' : HeavyHiggsBRs[5],
            'gg' : HeavyHiggsBRs[6],
            'gammagamma' : HeavyHiggsBRs[7],
            'Zgamma' : HeavyHiggsBRs[8],
            'WW' : HeavyHiggsBRs[9],
            'ZZ' : HeavyHiggsBRs[10],
            'h1h1' : HeavyHiggsBRs[11],
            'POINTNAME' : key
            }
        write_param_card('param_card.dat', paramsubs)



# run and retrieve the HiggsBounds/HiggsSignals:
HB_results, HS_results = get_HiggsBoundsSignals_results('.', 'XSM_1605.06123', xsm_point_info)


# add the info to an array for points that pass the FCC constraints:
xsm_point_info_FCCpass = []
xsm_point_constraints_FCCpass = []


# DEFINE THE MARKER MAPS FOR VARIOUS COLLIDER SCENARIOS
marker_map = {}
marker_map['HL-LHC'] = 'o'
marker_map['HL-LHC-CS'] = '^'
marker_map['CURRENT'] = 'o' #'o'
marker_map['EXCLUDED'] = '' #'x'

# now check all constraints together
for p in range(len(xsm_point_info)):
        # xsm point info array
        name =  str(xsm_point_info[p][0])
        mh1 = str(xsm_point_info[p][1])
        mh2 = str(xsm_point_info[p][2])
        G1 = str(xsm_point_info[p][3])
        G2 = str(xsm_point_info[p][4])
        st = str(xsm_point_info[p][5])
        ct = str(xsm_point_info[p][6])
        BR_hh = str(xsm_point_info[p][7])
        xs13 = str(xsm_point_info[p][8])
        xs14 = str(xsm_point_info[p][9])
        xs8 = str(xsm_point_info[p][10])
        xs7 = str(xsm_point_info[p][11])

        # HiggsBounds/HiggsSignals results
        HB = str(HB_results[p])
        HS = str(HS_results[p])

        couplstr_cur = str(xsm_point_constraints[p][0])
        couplstr_fut = str(xsm_point_constraints[p][1])
        EWPOcurr = str( round_sig(xsm_point_constraints[p][2], 4))
        EWPOfut = str(round_sig(xsm_point_constraints[p][3], 5))
        ATLAS_HH = str(xsm_point_constraints[p][4])
        CMS_HH = str(xsm_point_constraints[p][5])
        HLLHC_ZZ = str(xsm_point_constraints[p][6])
        HLLHC_WW = str(xsm_point_constraints[p][7])
        ATLAS_HH_extrap = str(xsm_point_constraints[p][8])
        CMS_HH_extrap = str(xsm_point_constraints[p][9])
        CLIC14_HH_pass = str(xsm_point_constraints[p][10])
        CLIC_VV_pass = str(xsm_point_constraints[p][11])
        CLIC3_HH_pass = str(xsm_point_constraints[p][12])
        FCC_WW = str(xsm_point_constraints[p][13])
        FCC_ZZ = str(xsm_point_constraints[p][14])
        FCC_HH = str(xsm_point_constraints[p][15])
        signif100_HH_L = xsm_point_constraints[p][16]
        signif100_WW_L = xsm_point_constraints[p][17]
        signif100_ZZ_L = xsm_point_constraints[p][18]
        signif100_MAX_L = xsm_point_constraints[p][19]
        signif27_HH_L = xsm_point_constraints[p][20]
        signif27_WW_L = xsm_point_constraints[p][21]
        signif27_ZZ_L = xsm_point_constraints[p][22]
        signif27_MAX_L = xsm_point_constraints[p][23]

        signif100_HH_30 = xsm_point_constraints[p][24]
        signif100_WW_30 = xsm_point_constraints[p][25]
        signif100_ZZ_30 = xsm_point_constraints[p][26]
        signif100_MAX_30 = xsm_point_constraints[p][27]

        finetuning = xsm_point_constraints[p][28]


        TypeTag = ''.join([i for i in name if not i.isdigit()])
        Current_Constraint_pass = 0
        if float(EWPOcurr) < 6.18 and float(ATLAS_HH) == 1 and float(CMS_HH) == 1 and float(HB) == 1 and float(HS) > 0.95 and float(couplstr_cur) ==1:
            Constraint_pass = marker_map['CURRENT']
            print(name, 'passed current constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, couplstr_cur, mh1, mh2)
            if float(HLLHC_ZZ) ==1 and float(HLLHC_WW)==1 and float(ATLAS_HH_extrap)==1 and float(CMS_HH_extrap)==1:
                Constraint_pass = marker_map['HL-LHC']
                if float(couplstr_fut) == 1:
                    Constraint_pass = marker_map['HL-LHC-CS']
                print(name, '\talso passed HL-LHC constraints', HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap)
                if float(FCC_WW) == 1 and float(FCC_ZZ) == 1 and float(FCC_HH) == 1 and float(couplstr_fut) == 1:
                    print(name, '\t\talso passed FCC (ZZ+WW+HH) constraints', FCC_WW, FCC_ZZ, FCC_HH, couplstr_fut)
                    xsm_point_info_FCCpass.append(xsm_point_info[p])
                    xsm_point_constraints_FCCpass.append(xsm_point_constraints[p])
        else:
            Constraint_pass = marker_map['EXCLUDED']
            print(name, 'did not pass HL-LHC constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap)
        
        Constraints[TypeTag].append(Constraint_pass)

        

# print all the info:
print_point_results(HB_results, HS_results, xsm_point_info, xsm_point_constraints)

# convert the SM BRs and width to Heavy Higgs BRs and width for the model parameters:
#HeavyHiggsBRs = convert_to_heavy(BR_interpolators_SM, lam, v0, x0, a1, a2, b3, b4)
#print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy, 'Heavy Higgs BRs & width')
#print '\n'

#print 'Removed points because they were outside the region of interest for mh2=',removed_point

# print info for points that pass FCC constraints:
#print '\nPoints that passed the FCC constraints:'
#print_point_results(HB_results, HS_results, xsm_point_info_FCCpass, xsm_point_constraints_FCCpass)


print('FINISHED PROCESSING')
