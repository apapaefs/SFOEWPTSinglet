########################################
# H -> hh -> bbar-gamma-gamma ANALYSIS #
########################################

# list of analysed processes
Analyses.append('HH100_BBGG')

# the latex name of the parent process (without the decay into the final state):
LatexNameParent['HH100_BBGG'] = '$h_2 \\rightarrow h_1 h_1$'

# the latex name of the analysis
LatexName['HH100_BBGG'] = '$h_2 \\rightarrow h_1 h_1 \\rightarrow (b\\bar{b}) (\\gamma \\gamma)$'

# the Branching ratio of the primary decay particles of the heavy particle into the observed final state:
FacFinalState['HH100_BBGG'] = 2*BR_h_bb*BR_h_gamgam

# the proton-proton energy for the analysis in TeV
Energy['HH100_BBGG'] = 100

# location of analyses' info
Location['HH100_BBGG'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HH100_BBGG/'

# analyses' executables (with respect to Location directory)
Executable['HH100_BBGG'] = 'HwSimPostAnalysis'

# analyses' executables (with respect to Location directory)
ExecutableCuts['HH100_BBGG'] = 'HwSimPostAnalysis'

# analyses' executables with smearing (with respect to Location directory)
ExecutableSmear['HH100_BBGG'] = 'HwSimPostAnalysis_smear'

# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HH100_BBGG'] = { 'gg-eta0-hh-bbaa-M250':250.0,
                         ##'gg-eta0-hh-bbaa-M300':300.0,
                         'gg-eta0-hh-bbaa-M350':350.0,
                         'gg-eta0-hh-bbaa-M400':400.0,
                         ##'gg-eta0-hh-bbaa-M450':450.0,
                         'gg-eta0-hh-bbaa-M500':500.0,
                         ##'gg-eta0-hh-bbaa-M550':550.0,
                         'gg-eta0-hh-bbaa-M600':600.0,
                         ##'gg-eta0-hh-bbaa-M650':650.0,
                         'gg-eta0-hh-bbaa-M700':700.0,
                         ##'gg-eta0-hh-bbaa-M750':750.0,
                         'gg-eta0-hh-bbaa-M800':800.0,
                         ##'gg-eta0-hh-bbaa-M850':850.0,
                         'gg-eta0-hh-bbaa-M900':900.0,
                         ##'gg-eta0-hh-bbaa-M950':950.0,
                         'gg-eta0-hh-bbaa-M1000':1000.0,
                         ##'gg-eta0-hh-bbaa-M1100':1100.0,
                         ##'gg-eta0-hh-bbaa-M1200':1200.0,
                         ##'gg-eta0-hh-bbaa-M1500':1500.0,
                         ##'gg-eta0-hh-bbaa-M2000':2000.0,
                         'pp-eta0jj-hh-bbaa-M250':250.0,
                         ##'pp-eta0jj-hh-bbaa-M300':300.0,
                         'pp-eta0jj-hh-bbaa-M350':350.0,
                         'pp-eta0jj-hh-bbaa-M400':400.0,
                         ##'pp-eta0jj-hh-bbaa-M450':450.0,
                         'pp-eta0jj-hh-bbaa-M500':500.0,
                         ##'pp-eta0jj-hh-bbaa-M550':550.0,
                         'pp-eta0jj-hh-bbaa-M600':600.0,
                         ##'pp-eta0jj-hh-bbaa-M650':650.0,
                         'pp-eta0jj-hh-bbaa-M700':700.0,
                         ##'pp-eta0jj-hh-bbaa-M750':750.0,
                         'pp-eta0jj-hh-bbaa-M800':800.0,
                         ##'pp-eta0jj-hh-bbaa-M850':850.0,
                         'pp-eta0jj-hh-bbaa-M900':900.0,
                         ##'pp-eta0jj-hh-bbaa-M950':950.0,
                         'pp-eta0jj-hh-bbaa-M1000':1000.0,
                         ##'pp-eta0jj-hh-bbaa-M1100':1100.0,
                         ##'pp-eta0jj-hh-bbaa-M1200':1200.0,
                         ##'pp-eta0jj-hh-bbaa-M1500':1500.0,
                         ##'pp-eta0jj-hh-bbaa-M2000':2000.0,
                         'pp-aajets':0.,
                         'pp-ajjets':0.,
                         'pp-bbaa':0.,
                         'pp-bbaj':0.,
                         'pp-bbh':0.,
                         'pp-ttaa':0.,
                         'pp-zh':0.,
                         'gg-zh':0.,
                         'pp-tth':0.,
                         'gg-hh':0.
                            } # M ! =0 stands for signal, 0 for backgrounds
                            
# the order of the processes
OrderProcesses['HH100_BBGG'] = {'gg-eta0-hh-bbaa-M250':0,
                         'gg-eta0-hh-bbaa-M300':0,
                         'gg-eta0-hh-bbaa-M350':0,
                         'gg-eta0-hh-bbaa-M400':0,
                         'gg-eta0-hh-bbaa-M450':0,
                         'gg-eta0-hh-bbaa-M500':0,
                         'gg-eta0-hh-bbaa-M550':0,
                         'gg-eta0-hh-bbaa-M600':0,
                         'gg-eta0-hh-bbaa-M650':0,
                         'gg-eta0-hh-bbaa-M700':0,
                         'gg-eta0-hh-bbaa-M750':0,
                         'gg-eta0-hh-bbaa-M800':0,
                         'gg-eta0-hh-bbaa-M850':0,
                         'gg-eta0-hh-bbaa-M900':0,
                         'gg-eta0-hh-bbaa-M950':0,
                         'gg-eta0-hh-bbaa-M1000':0,
                         'gg-eta0-hh-bbaa-M1100':0,
                         'gg-eta0-hh-bbaa-M1200':0,
                         'gg-eta0-hh-bbaa-M1500':0,
                         'gg-eta0-hh-bbaa-M2000':0,
                         'pp-eta0jj-hh-bbaa-M250':0,
                         'pp-eta0jj-hh-bbaa-M300':0,
                         'pp-eta0jj-hh-bbaa-M350':0,
                         'pp-eta0jj-hh-bbaa-M400':0,
                         'pp-eta0jj-hh-bbaa-M450':0,
                         'pp-eta0jj-hh-bbaa-M500':0,
                         'pp-eta0jj-hh-bbaa-M550':0,
                         'pp-eta0jj-hh-bbaa-M600':0,
                         'pp-eta0jj-hh-bbaa-M650':0,
                         'pp-eta0jj-hh-bbaa-M700':0,
                         'pp-eta0jj-hh-bbaa-M750':0,
                         'pp-eta0jj-hh-bbaa-M800':0,
                         'pp-eta0jj-hh-bbaa-M850':0,
                         'pp-eta0jj-hh-bbaa-M900':0,
                         'pp-eta0jj-hh-bbaa-M950':0,
                         'pp-eta0jj-hh-bbaa-M1000':0,
                         'pp-eta0jj-hh-bbaa-M1100':0,
                         'pp-eta0jj-hh-bbaa-M1200':0,
                         'pp-eta0jj-hh-bbaa-M1500':0,
                         'pp-eta0jj-hh-bbaa-M2000':0,
                         'pp-aajets':1,
                         'pp-ajjets':1,
                         'pp-bbaa':0,
                         'pp-bbaj':0,
                         'pp-bbh':1,
                         'pp-ttaa':1,
                         'pp-zh':1,
                         'gg-zh':0,
                         'pp-tth':1,
                         'gg-hh':0
        } # 0 stands for LO and 1 for NLO (i.e. MC@NLO), 2 for FxFx
        
# K-factors for the processes: MUST INCLUDE SYMMETRY FACTORS (e.g. for the lepton flavours)
KFacsProcesses['HH100_BBGG'] = {  # 1.080683 comes from the MG5 BRs being wrong for hh > (bb~)(aa)
                        'gg-eta0-hh-bbaa-M200':2.153378605**1.080683,
                        'gg-eta0-hh-bbaa-M250':2.18073096985*1.080683,
                        'gg-eta0-hh-bbaa-M300':2.29775496014*1.080683,
                        'gg-eta0-hh-bbaa-M350':2.46589800207*1.080683,
                        'gg-eta0-hh-bbaa-M400':1.9707292403*1.080683,
                        'gg-eta0-hh-bbaa-M450':1.72468921497*1.080683,
                        'gg-eta0-hh-bbaa-M500':1.66261118224*1.080683,
                        'gg-eta0-hh-bbaa-M550':1.60230559544*1.080683,
                        'gg-eta0-hh-bbaa-M600':1.56017321503*1.080683,
                        'gg-eta0-hh-bbaa-M650':1.52629944125*1.080683,
                        'gg-eta0-hh-bbaa-M700':1.49797704809*1.080683,
                        'gg-eta0-hh-bbaa-M750':1.4753696585*1.080683,
                        'gg-eta0-hh-bbaa-M800':1.45478852862*1.080683,
                        'gg-eta0-hh-bbaa-M850':1.43632233525*1.080683,
                        'gg-eta0-hh-bbaa-M900':1.42118100769*1.080683,
                        'gg-eta0-hh-bbaa-M950':1.40459637177*1.080683,
                        'gg-eta0-hh-bbaa-M1000':1.39037544923*1.080683,
                        'gg-eta0-hh-bbaa-M1100':1.36671510558*1.080683,
                        'gg-eta0-hh-bbaa-M1200':1.34539183607*1.080683,
                        'gg-eta0-hh-bbaa-M1500':1.28924838934*1.080683,
                        'gg-eta0-hh-bbaa-M2000':1.22343037638*1.080683,
                         'pp-eta0jj-hh-bbaa-M200':1.34144694309*1.080683,
                         'pp-eta0jj-hh-bbaa-M250':1.31763219163*1.080683,
                         'pp-eta0jj-hh-bbaa-M300':1.33269322546*1.080683,
                         'pp-eta0jj-hh-bbaa-M350':1.33413720446*1.080683,
                         'pp-eta0jj-hh-bbaa-M400':1.35075506325*1.080683,
                         'pp-eta0jj-hh-bbaa-M450':1.34429175954*1.080683,
                         'pp-eta0jj-hh-bbaa-M500':1.36933619048*1.080683,
                         'pp-eta0jj-hh-bbaa-M550':1.36225180698*1.080683,
                         'pp-eta0jj-hh-bbaa-M600':1.3588164263*1.080683,
                         'pp-eta0jj-hh-bbaa-M650':1.34668089912*1.080683,
                         'pp-eta0jj-hh-bbaa-M700':1.33902198374*1.080683,
                         'pp-eta0jj-hh-bbaa-M750':1.35037833258*1.080683,
                         'pp-eta0jj-hh-bbaa-M800':1.35564696253*1.080683,
                         'pp-eta0jj-hh-bbaa-M850':1.34048099174*1.080683,
                         'pp-eta0jj-hh-bbaa-M900':1.33779585209*1.080683,
                         'pp-eta0jj-hh-bbaa-M950':1.34768444622*1.080683,
                         'pp-eta0jj-hh-bbaa-M1000':1.33078921944*1.080683,
                         'pp-eta0jj-hh-bbaa-M1000':1.34140978335*1.080683,
                         'pp-eta0jj-hh-bbaa-M1100':1.3385351023*1.080683,
                         'pp-eta0jj-hh-bbaa-M1200':1.31389948565*1.080683,
                         'pp-eta0jj-hh-bbaa-M1500':1.31595998714*1.080683,
                         'pp-eta0jj-hh-bbaa-M2000':1.28087286701*1.080683,
                         'pp-aajets':1.,
                         'pp-ajjets':1.,
                         'pp-bbaa':2.,
                         'pp-bbaj':2.,
                         'pp-bbh':1.49295251,
                         'pp-ttaa':1.,
                         'pp-zh':1.49295251,
                         'gg-zh':1.49295251*2,
                         'pp-tth':1.49295251,
                         'gg-hh':2*1.080683/2. # dividing by two since the "trick" does not know that eta0 and h are in fact identical particles 
                        } # any K-factors to apply to processes

# K-factors to apply to processes on the TOTAL cross section -> NO symmetry factors from Decays.
TotalKFacsProcesses['HH100_BBGG'] = { 'gg-eta0-hh-bbaa-M250':2.18073096985,
                                      'gg-eta0-hh-bbaa-M300':2.29775496014,
                                      'gg-eta0-hh-bbaa-M350':2.46589800207,
                                      'gg-eta0-hh-bbaa-M400':1.9707292403,
                                      'gg-eta0-hh-bbaa-M450':1.72468921497,
                                      'gg-eta0-hh-bbaa-M500':1.66261118224,
                                      'gg-eta0-hh-bbaa-M550':1.60230559544,
                                      'gg-eta0-hh-bbaa-M600':1.56017321503,
                                      'gg-eta0-hh-bbaa-M650':1.52629944125,
                                      'gg-eta0-hh-bbaa-M700':1.49797704809,
                                      'gg-eta0-hh-bbaa-M750':1.4753696585,
                                      'gg-eta0-hh-bbaa-M800':1.45478852862,
                                      'gg-eta0-hh-bbaa-M850':1.43632233525,
                                      'gg-eta0-hh-bbaa-M900':1.42118100769,
                                      'gg-eta0-hh-bbaa-M950':1.40459637177,
                                      'gg-eta0-hh-bbaa-M1000':1.39037544923,
                                      'gg-eta0-hh-bbaa-M1100':1.36671510558,
                                      'gg-eta0-hh-bbaa-M1200':1.34539183607,
                                      'gg-eta0-hh-bbaa-M1500':1.28924838934,
                                      'gg-eta0-hh-bbaa-M2000':1.22343037638,
                                      'pp-eta0jj-hh-bbaa-M200':1.34144694309,
                                      'pp-eta0jj-hh-bbaa-M250':1.31763219163,
                                      'pp-eta0jj-hh-bbaa-M300':1.33269322546,
                                      'pp-eta0jj-hh-bbaa-M350':1.33413720446,
                                      'pp-eta0jj-hh-bbaa-M400':1.35075506325,
                                      'pp-eta0jj-hh-bbaa-M450':1.34429175954,
                                      'pp-eta0jj-hh-bbaa-M500':1.36933619048,
                                      'pp-eta0jj-hh-bbaa-M550':1.36225180698,
                                      'pp-eta0jj-hh-bbaa-M600':1.3588164263,
                                      'pp-eta0jj-hh-bbaa-M650':1.34668089912,
                                      'pp-eta0jj-hh-bbaa-M700':1.33902198374,
                                      'pp-eta0jj-hh-bbaa-M750':1.35037833258,
                                      'pp-eta0jj-hh-bbaa-M800':1.35564696253,
                                      'pp-eta0jj-hh-bbaa-M850':1.34048099174,
                                      'pp-eta0jj-hh-bbaa-M900':1.33779585209,
                                      'pp-eta0jj-hh-bbaa-M950':1.34768444622,
                                      'pp-eta0jj-hh-bbaa-M1000':1.34140978335,
                                      'pp-eta0jj-hh-bbaa-M1100':1.3385351023,
                                      'pp-eta0jj-hh-bbaa-M1200':1.31389948565,
                                      'pp-eta0jj-hh-bbaa-M1500':1.31595998714,
                                      'pp-eta0jj-hh-bbaa-M2000':1.28087286701,
                                      'pp-aajets':1.,
                                      'pp-ajjets':1.,
                                      'pp-bbaa':2.,
                                      'pp-bbaj':2.,
                                      'pp-bbh':1.,
                                      'pp-ttaa':1.,
                                      'pp-zh':1.,
                                      'gg-zh':2.,
                                      'pp-tth':1.,
                                      'gg-hh':2./2. # dividing by two since the "trick" does not know that eta0 and h are in fact identical particles
                        } # any K-factors to apply to processes on the TOTAL cross section



# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HH100_BBGG'] = { #'gg-eta0-hh-bbaa-M400':['gg_eta0_hh/Events/run_05_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M250':['gg_eta0_hh/Events/run0_250_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M300':['gg_eta0_hh/Events/run0_300_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M350':['gg_eta0_hh/Events/run0_350_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M400':['gg_eta0_hh/Events/run0_400_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M450':['gg_eta0_hh/Events/run0_450_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M500':['gg_eta0_hh/Events/run0_500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M550':['gg_eta0_hh/Events/run0_550_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M600':['gg_eta0_hh/Events/run0_600_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M650':['gg_eta0_hh/Events/run0_650_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M700':['gg_eta0_hh/Events/run0_700_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M750':['gg_eta0_hh/Events/run0_750_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M800':['gg_eta0_hh/Events/run0_800_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M850':['gg_eta0_hh/Events/run0_850_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M900':['gg_eta0_hh/Events/run0_900_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M950':['gg_eta0_hh/Events/run0_950_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M1000':['gg_eta0_hh/Events/run0_1000_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M1100':['gg_eta0_hh/Events/run0_1100_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M1200':['gg_eta0_hh/Events/run0_1200_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M1500':['gg_eta0_hh/Events/run0_1500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-hh-bbaa-M2000':['gg_eta0_hh/Events/run0_2000_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M250':['pp_eta0jj_hh/Events/run0_250_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M300':['pp_eta0jj_hh/Events/run0_300_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M350':['pp_eta0jj_hh/Events/run0_350_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M400':['pp_eta0jj_hh/Events/run0_400_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M450':['pp_eta0jj_hh/Events/run0_450_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M500':['pp_eta0jj_hh/Events/run0_500_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M550':['pp_eta0jj_hh/Events/run0_550_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M600':['pp_eta0jj_hh/Events/run0_600_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M650':['pp_eta0jj_hh/Events/run0_650_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M700':['pp_eta0jj_hh/Events/run0_700_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M750':['pp_eta0jj_hh/Events/run0_750_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M800':['pp_eta0jj_hh/Events/run0_800_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M850':['pp_eta0jj_hh/Events/run0_850_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M900':['pp_eta0jj_hh/Events/run0_900_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M950':['pp_eta0jj_hh/Events/run0_950_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M1000':['pp_eta0jj_hh/Events/run0_1000_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M1100':['pp_eta0jj_hh/Events/run0_1100_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M1200':['pp_eta0jj_hh/Events/run0_1200_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M1500':['pp_eta0jj_hh/Events/run0_1500_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-hh-bbaa-M2000':['pp_eta0jj_hh/Events/run0_2000_decayed_1/unweighted_events.lhe.gz'],
'pp-aajets':['pp_aajets/Events/run_02/events.lhe.gz', 'pp_aajets/Events/run_03/events.lhe.gz', 'pp_aajets/Events/run_04/events.lhe.gz'],
'pp-ajjets':['pp_ajjets/Events/run_01/events.lhe.gz', 'pp_ajjets/Events/run_02/events.lhe.gz', 'pp_ajjets/Events/run_03/events.lhe.gz', 'pp_ajjets/Events/run_04/events.lhe.gz', 'pp_ajjets/Events/run_05/events.lhe.gz', 'pp_ajjets/Events/run_06/events.lhe.gz'],
'pp-bbaa':['pp_bbaa/Events/run_01/unweighted_events.lhe.gz', 'pp_bbaa/Events/run_02/unweighted_events.lhe.gz'],
'pp-bbaj':['pp_bbaj/Events/run_01/unweighted_events.lhe.gz','pp_bbaj/Events/run_02/unweighted_events.lhe.gz'],
'pp-bbh':['pp_bbh/Events/run_01_decayed_1/events.lhe.gz','pp_bbh/Events/run_03_decayed_1/events.lhe.gz'],
'pp-ttaa':['pp_ttaa/Events/run_01_decayed_1/events.lhe.gz', 'pp_ttaa/Events/run_02_decayed_1/events.lhe.gz'],
'pp-zh':['pp_zh/Events/run_01_decayed_1/events.lhe.gz'],
'gg-zh':['gg_zh/Events/run_01_decayed_1/unweighted_events.lhe.gz'],
'pp-tth':['pp_tth/Events/run_01_decayed_1/events.lhe.gz'],
'gg-hh':['gg_hh_trick/Events/run_01_decayed_1/unweighted_events.lhe.gz'],
}

# Where to look for the UNDECAYED total cross section: Note the difference between LO and NLO!
MGProcessLocations_Undecayed['HH100_BBGG'] = {
'gg-eta0-hh-bbaa-M200':['gg_eta0_hh/Events/run0_200/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M250':['gg_eta0_hh/Events/run0_250/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M300':['gg_eta0_hh/Events/run0_300/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M350':['gg_eta0_hh/Events/run0_350/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M400':['gg_eta0_hh/Events/run0_400/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M450':['gg_eta0_hh/Events/run0_450/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M500':['gg_eta0_hh/Events/run0_500/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M550':['gg_eta0_hh/Events/run0_550/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M600':['gg_eta0_hh/Events/run0_600/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M650':['gg_eta0_hh/Events/run0_650/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M700':['gg_eta0_hh/Events/run0_700/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M750':['gg_eta0_hh/Events/run0_750/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M800':['gg_eta0_hh/Events/run0_800/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M850':['gg_eta0_hh/Events/run0_850/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M900':['gg_eta0_hh/Events/run0_900/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M950':['gg_eta0_hh/Events/run0_950/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M1000':['gg_eta0_hh/Events/run0_1000/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M1100':['gg_eta0_hh/Events/run0_1100/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M1200':['gg_eta0_hh/Events/run0_1200/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M1500':['gg_eta0_hh/Events/run0_1500/unweighted_events.lhe'],
'gg-eta0-hh-bbaa-M2000':['gg_eta0_hh/Events/run0_2000/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M200':['pp_eta0jj_hh/Events/run0_200/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M250':['pp_eta0jj_hh/Events/run0_250/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M300':['pp_eta0jj_hh/Events/run0_300/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M350':['pp_eta0jj_hh/Events/run0_350/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M400':['pp_eta0jj_hh/Events/run0_400/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M450':['pp_eta0jj_hh/Events/run0_450/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M500':['pp_eta0jj_hh/Events/run0_500/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M550':['pp_eta0jj_hh/Events/run0_550/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M600':['pp_eta0jj_hh/Events/run0_600/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M650':['pp_eta0jj_hh/Events/run0_650/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M700':['pp_eta0jj_hh/Events/run0_700/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M750':['pp_eta0jj_hh/Events/run0_750/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M800':['pp_eta0jj_hh/Events/run0_800/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M850':['pp_eta0jj_hh/Events/run0_850/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M900':['pp_eta0jj_hh/Events/run0_900/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M950':['pp_eta0jj_hh/Events/run0_950/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M1000':['pp_eta0jj_hh/Events/run0_1000/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M1100':['pp_eta0jj_hh/Events/run0_1100/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M1200':['pp_eta0jj_hh/Events/run0_1200/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M1500':['pp_eta0jj_hh/Events/run0_1500/unweighted_events.lhe'],
'pp-eta0jj-hh-bbaa-M2000':['pp_eta0jj_hh/Events/run0_2000/unweighted_events.lhe'],
'gg-zh':['gg_zh/Events/run_01/unweighted_events.lhe'],
'pp-aajets':['pp_aajets/Events/run_02/summary.txt','pp_aajets/Events/run_03/summary.txt', 'pp_aajets/Events/run_04/summary.txt'],
'pp-ajjets':['pp_ajjets/Events/run_01/summary.txt', 'pp_ajjets/Events/run_02/summary.txt', 'pp_ajjets/Events/run_03/summary.txt', 'pp_ajjets/Events/run_04/summary.txt', 'pp_ajjets/Events/run_05/summary.txt', 'pp_ajjets/Events/run_06/summary.txt'],
'pp-bbaa':['pp_bbaa/Events/run_01/unweighted_events.lhe.gz', 'pp_bbaa/Events/run_02/unweighted_events.lhe.gz'],
'pp-bbaj':['pp_bbaj/Events/run_01/unweighted_events.lhe.gz', 'pp_bbaj/Events/run_02/unweighted_events.lhe.gz'],
'pp-bbh':['pp_bbh/Events/run_01/summary.txt', 'pp_bbh/Events/run_03/summary.txt'],
'pp-ttaa':['pp_ttaa/Events/run_01/summary.txt', 'pp_ttaa/Events/run_01/summary.txt'],
'pp-zh':['pp_zh/Events/run_01/summary.txt'],
'pp-tth':['pp_tth/Events/run_01/summary.txt'],
'gg-hh':['gg_hh_trick/Events/run_01/unweighted_events.lhe']
}

Luminosity['HH100_BBGG'] = 30000. # luminosity in /fb

NVariables['HH100_BBGG'] = 17 # the number of variables that go into the BDT

BRElement['HH100_BBGG'] = 11 # the element of the Higgs Branching Ratio array corresponding to this decay mode
 
NBDTAnalysis['HH100_BBGG'] = 10# 3 # the number of BDT runs to search for the optimal solution

SignalFactorAnalysis['HH100_BBGG'] = 1000. # the initial signal factor guess for the BDT

HwSimLibrary['HH100_BBGG'] = 'HwSim' # the name of the HwSim-type library to be loaded by Herwig 

FatAnalysis['HH100_BBGG'] = '#' # add a "#" to remove the HwSimFat stuff in the Herwig file
