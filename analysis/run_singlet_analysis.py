#! /usr/bin/env python

import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
import matplotlib.font_manager as fm
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, ScalarFormatter, FuncFormatter)
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
from scipy.optimize import fsolve
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
import pickle
import re
from matplotlib.colors import ListedColormap
from past.builtins import execfile

# import the constraint calculator:
from singlet_constraint_functions import *

####################
####################
# USEFUL FUNCTIONS #                 
####################
####################

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)

# choose the next colour -- for plotting
ccount = 0
def next_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount]
    if ccount < 8:
        ccount = ccount + 1
    else:
        ccount = 0    
    return color_chosen

# do not increment colour in this case:
def same_color():
    global ccount
    colors = ['green', 'orange', 'red', 'blue', 'black', 'cyan', 'magenta', 'brown', 'violet'] # 9 colours
    color_chosen = colors[ccount-1]
    return color_chosen

#####################################
#####################################
# GENERAL SETTINGS FOR THIS MACHINE #
#####################################
#####################################

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 2:
    print('usage: run_singlet_analysis.py [mode] [analysis]')
    exit()

print("\nDriver script to run and collect results for the SM+Singlet analyses")
print("---")

# Herwig installation location on system:
HerwigInstallation = '/Users/apapaefs/Documents/Projects/Herwig-stable/'
HerwigSourceCmd = 'source ' + HerwigInstallation + 'bin/activate;'

# Herwig input file sub-dir and output for the events
HerwigLocation = 'Herwig/'
HerwigOutputLocation = HerwigLocation + 'events/'

# Input file templates for LO, MC@NLO and FxFx:
# the real files have a .in.template extension
HW_template = ['','', '']
HW_template[0] = 'Templates/HW-LO.in' # 0th element is LO
HW_template[1] = 'Templates/HW-MCatNLO.in' # element 1 is NLO
HW_template[2] = 'Templates/HW-FxFx.in' # element 2 is FxFx

# The reduction factor of the number of events between the LHE file and the actual HW run for each process:
Reduction_Fac = [ '', '', '' ]
Reduction_Fac[0] = 0.995
Reduction_Fac[1] = 0.995
Reduction_Fac[2] = 0.7 # The FxFx runs have some vetoing so we expect fewer events

# assumed systematic uncertainties on signal and background predictions (fractional):
aB = 0.05

# Branching ratios:
BR_z_ellell = 3.3632E-2 #  Z -> lepton lepton (one flavour)
BR_w_ellnu = 10.86E-2 # W -> lepton+neutrino (one flavour)
BR_z_vv = 0.2 # Z -> neutrino neutrino (all flavours)
BR_z_qq = 0.116 + 0.156 + 0.1203 + 0.1512 # Z -> qq
BR_h_bb = 0.569369
BR_h_gamgam = 0.00229

# MG5/aMC sub-dir
MGLocation = 'MG5_aMC_v2_7_2/'
# the run cards for LO and NLO MG5/aMC runs:
MG_runcard = ['','']
MG_runcard[0] = 'run_card-LO.dat' # 0th element is LO
MG_runcard[1] = 'run_card-NLO.dat' # element 1 is NLO

# ROOT TMVA location and file
TMVALocation = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/TMVA/'
TMVAFile = 'TMVAClassification_singlet_multi.C' # the BDT file, run with: root -q -b -l 'TMVAClassification_singlet_multi.C(30000, "/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/TMVA/HWW100_signals.txt", "/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/TMVA/HWW100_backgrounds.txt", 7)'
TMVABinary = 'TMVAClassification_singlet_multi'
TMVABinaryCuts = 'TMVAClassification_singlet_Cuts'
TMVAFileCuts = 'TMVAClassification_singlet_Cuts.C'
TMVABinOrScript = 0 # 0 for Binary, 1 for script

# the binaries for the construction of the m4l distributions in HZZ100_4l
TMVABinary_output = 'TMVAClassification_singlet_multi_output'
TMVABinary_output_application = 'TMVAClassification_singlet_multi_output_application'

# the binaries for the construction of the mbbaa distributions in HH1000
TMVABinary_output_HH = 'TMVAClassification_singlet_multi_output_HH'
TMVABinary_output_application_HH = 'TMVAClassification_singlet_multi_output_application_HH'

# the binaries for the construction of the mT distributions in HWW100
TMVABinary_output_WW = 'TMVAClassification_singlet_multi_output_WW'
TMVABinary_output_application_WW = 'TMVAClassification_singlet_multi_output_application_WW'

# the binaries for the construction of the mT distributions in HZZ100_2l2v
TMVABinary_output_ZZ2l2v = 'TMVAClassification_singlet_multi_output_ZZ2l2v'
TMVABinary_output_application_ZZ2l2v = 'TMVAClassification_singlet_multi_output_application_ZZ2l2v'


# location of digitization plots:
digit_plot_location = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/digitizationplots/' # SLASH AT END IMPORTANT!


# available modes:
genherwig = False # generate the Herwig input files
runherwig = False # run Herwig via the generated input files
runanalysis = False # run the analysis on the generated HwSim root files
collectanalysis = False # collect the analysis results
runanalysiscuts = False # run the analysis on the generated HwSim root files (cuts)
collectanalysiscuts = False # collect the analysis results (cuts)
runanalysissmear = False # run the analysis on the generated HwSim root files (smear)
collectanalysissmear = False # collect the analysis results (smear)
runbdt = False # run the BDT
runcuts = False # run the Cuts (doesn't work)
runbdtsmear = False # run the BDT with smearing
runbdtsolve = False # run the BDT and find the minimum cross section for given values of the significance
runbdtmulti = False # run the BDT N times and calculate the mean, 1 sigma and 2 sigma intervals on the cross section limits for each mass
runbdtmultismear = False # run the BDT N times and calculate the mean, 1 sigma and 2 sigma intervals on the cross section limits for each mass (WITH SMEARING)

plotexclusion = False # plot the 95% C.L. exclusions after the "runbdtsolve" was ran
plotexclusionsmear = False # plot the 95% C.L. exclusions after the "runbdtsolve" was ran (smear version)

checkMCevents = False # get the number of events in the lhe files, after analysis and after BDT
checkMCeventssmear = False # get the number of events in the lhe files, after analysis and after BDT (smear)

runbdtm4l = False # # run the BDT N times, calculate the 1 sigma and 2 sigma intervals, get the information necessary for the mass fitting in m4l

runbdtmbbaa = False # # run the BDT N times, calculate the 1 sigma and 2 sigma intervals, get the information necessary for the mass fitting in mbbaa

runbdtmT = False # # run the BDT N times, calculate the 1 sigma and 2 sigma intervals, get the information necessary for the mass fitting in mT from the WW analysis

runbdtmT_ZZ2l2v = False # # run the BDT N times, calculate the 1 sigma and 2 sigma intervals, get the information necessary for the mass fitting in mT from the ZZ 2l2v analysis

# re-run?
ReRun = False
if len(args) > 2:
    if args[2] == "rerun":
        ReRun = True
        print('WARNING: Re-running everything forced! Even existing files')

############################
############################
# PROCESS SPECIFIC SECTION #
############################
############################

# dictionaries and lists for all processes
Analyses = [] # the list of valid analyses. The analysis must be added to this list if it is to be executable
Location = {} # the locaiton of the analysis directory (must include Herwig, Herwig/events and MG5 directory given by MGLocation
Executable = {} # the executable for the analysis
ExecutableCuts = {} # the executable for the analysis for the cuts run
ExecutableSmear = {} # the executable for the analysis for smearing
Processes = {}  # the processes to be considered, along with their "tag". In this case the mass of h2
OrderProcesses = {} # the order of the process: 0 for LO, 1 for NLO (relevant for the Herwig input files)
KFacsProcesses = {} # K-factors and other SYMMETRY factors for the processes
TotalKFacsProcesses = {} # K-factors on the *TOTAL* cross sections
FacFinalState = {} # the BR that gives you the final state i.e. in the case of h2 -> xx -> FS, the BR(xx -> FS)
MGProcessLocations = {} # includes the decays of events
MGProcessLocations_Undecayed = {} # the total cross sections (no decays)
HwSimLibrary = {} # the name of the HwSim library to be used in the Herwig runs (for saving the .root files) -> to be replaced in the templates
FatAnalysis = {} # add a "#" to remove the HwSimFat stuff in the Herwig file
Luminosity = {} # the luminosity in inverse femtobarn
NVariables = {} # the number of variables that go into the BDT -> needs to correspond to the ones in the analysis
Energy = {} # the energy for the analysis 
LatexName = {} # the LaTeX name for the analysis (includes the $$) for plotting
LatexNameParent = {} # the LaTeX name for the analysis (includes the $$) for plotting: for the "Parent" process. e.g. if we are looking at pp -> h2 -> ZZ -> 4ell, the parent process is pp -> h2 -> ZZ
BRElement = {} # the element of the Higgs Branching Ratio array corresponding to this decay mode
NBDTAnalysis = {} # the number of BDT runs to search for the optimal analysis
SignalFactorAnalysis = {} # The "initial guess" for the signal cross section multiplier used in the BDT solver

###################################
# IMPORT THE ANALYSIS INFORMATION #
###################################

# WARNING! MAKE SURE THAT THE SUBDIRECTORIES EXIST! Anaklysis_name/Herwig/events and the MG5 installation 

####################
# H -> WW ANALYSIS #
####################
    
exec(compile(open('analysis_HWW100.py', "rb").read(), 'analysis_HWW100.py', 'exec'))
    
##########################
# H -> ZZ -> 4l ANALYSIS #
##########################

exec(compile(open('analysis_HZZ100_4l.py', "rb").read(), 'analysis_HZZ100_4l.py', 'exec'))

############################
# H -> ZZ -> 2l2v ANALYSIS #
############################

exec(compile(open('analysis_HZZ100_2l2v.py', "rb").read(), 'analysis_HZZ100_2l2v.py', 'exec'))

############################
# H -> ZZ -> 2l2q ANALYSIS #
############################

exec(compile(open('analysis_HZZ100_2l2q.py', "rb").read(), 'analysis_HZZ100_2l2q.py', 'exec'))

##########################################
# H -> hh -> b-bbar-gamma-gamma ANALYSIS #
#########################################

exec(compile(open('analysis_HH100_BBGG.py', "rb").read(), 'analysis_HH100_BBGG.py', 'exec'))


###################
###################
# SET PARAMETERS  #
###################
###################

# choose the mode from the command line
if 'genherwig' == args[0]:
    genherwig = True

if 'runherwig' == args[0]:
    runherwig = True

if 'runanalysis' == args[0]:
    runanalysis = True

if 'collectanalysis' == args[0]:
    collectanalysis = True

if 'runanalysiscuts' == args[0]:
    runanalysiscuts = True

if 'collectanalysiscuts' == args[0]:
    collectanalysiscuts = True

if 'runanalysissmear' == args[0]:
    runanalysissmear = True

if 'collectanalysissmear' == args[0]:
    collectanalysissmear = True
    
if 'runbdt' == args[0]:
    runbdt = True
    
if 'runbdtsmear' == args[0]:
    runbdtsmear = True

if 'runcuts' == args[0]:
    runcuts = True

runbdtdebug = False
if 'runbdtsolve' == args[0]:
    runbdtsolve = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0
    # debugging option
    if len(args) > 3:
        if args[3] == "debug":
            runbdtdebug = True

if 'runbdtmulti' == args[0]:
    runbdtmulti = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0

if 'runbdtmultismear' == args[0]:
    runbdtmultismear = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0

if 'plotexclusion' == args[0]:
    plotexclusion = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0

if 'plotexclusionsmear' == args[0]:
    plotexclusionsmear = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0

if 'checkMCevents' == args[0]:
    checkMCevents = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0

if 'checkMCeventssmear' == args[0]:
    checkMCeventssmear = True
    if len(args) > 2:
        SignifTarget = float(args[2])
    else:
        SignifTarget = 2.0
        
if 'runbdtm4l' == args[0]:
    runbdtm4l = True

if 'runbdtmbbaa' == args[0]:
    runbdtmbbaa = True

if 'runbdtmT' == args[0]:
    runbdtmT = True

if 'runbdtmT_ZZ2l2v' == args[0]:
    runbdtmT_ZZ2l2v = True


# the current analysis (e.g. 'HW100')
CurrentAnalysis = args[1]


#############
#############
# EXECUTE!  #
#############
#############

# generate the Herwig input files from the templates:
if genherwig == True:
    print('Generating Herwig input files for the analysis:', CurrentAnalysis)
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        print('\tfor Process:', Process, 'order:', OrderProcesses[CurrentAnalysis][Process], 'S or B:', Processes[CurrentAnalysis][Process])
        # choose template according to LO or MC@NLO:
        HerwigInputTemplate = getTemplate(HW_template[OrderProcesses[CurrentAnalysis][Process]])
        # loop over the LHE files and write the HW inputs for each, increment a counter
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            hwinputfile = Location[CurrentAnalysis] + HerwigLocation + processname + '.in'
            parmtextsubs = {
                'PROCESSNAME' : processname, 
                'LHEFILE' : lhefile,
                'OUTPUTLOCATION' : outputlocation,
                'FatAnalysis' : FatAnalysis[CurrentAnalysis],
                'HwSimLibrary' : HwSimLibrary[CurrentAnalysis]
            }
            counter = counter + 1
            print('\t\twriting', hwinputfile)
            writeFile(hwinputfile, HerwigInputTemplate.substitute(parmtextsubs) )

if runherwig == True:
    print('Running Herwig from the input files previously generated, for:', CurrentAnalysis)
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            processname = 'HW-' + Process + '-' + str(counter)
            hwinputfile = processname + '.in'
            hwrunfile = processname + '.run'
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            if os.path.exists(rootfile) is True:
                 print('File', rootfile, 'exists')
            if os.path.exists(rootfile) is False or (os.path.exists(rootfile) is True and ReRun is True): # if the root file exists, do not proceed except if ReRun is true
                if os.path.exists(rootfile) is True and ReRun is True:
                    print('File', rootfile, 'exists, but have chosen to re-run!')
                # get the number of events in the corresponding lhe file:
                zgrepcommand = 'zgrep "= nevents" ' + lhefile
                print(zgrepcommand)
                p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    nevents = float(line.split()[0])
                print('\t\tHerwig reading:', hwinputfile)
                readcommand = HerwigSourceCmd + 'Herwig read ' + hwinputfile
                p = subprocess.Popen(readcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                #print out, err
                print('\t\tHerwig running:', hwrunfile, 'for', nevents, 'events')
                runcommand = HerwigSourceCmd + 'Herwig run ' + hwrunfile + ' -N' + str(int(nevents*Reduction_Fac[OrderProcesses[CurrentAnalysis][Process]]))
                p = subprocess.Popen(runcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                #print out, err
            counter = counter + 1
            
# check the number of MC events after the analysis cuts and BDT have been applied:
if checkMCevents == True:
    print('Checking the number of MC events after each stage of analysis')
    NEventsProcess = {}
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            # get the number of events in the corresponding lhe file:
            zgrepcommand = 'zgrep "= nevents" ' + lhefile
            #print zgrepcommand
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
            for line in iter(p.stdout.readline, b''):
                nevents = float(line.split()[0])
            print("LHE Events in process", Process, "=", nevents)
        if Process in NEventsProcess:
            NEventsProcess[Process] = NeventsProcess[Process] + nevents*Reduction_Fac[OrderProcesses[CurrentAnalysis][Process]] # factor to take into account possible discrepancy in the HW events
        else:
            NEventsProcess[Process] = nevents*Reduction_Fac[OrderProcesses[CurrentAnalysis][Process]]
        counter = counter + 1
    print('NEventsProcess=', NEventsProcess)
    # now get the efficiency from the analysis .dat file for each process
    Efficiencies = {}
    print('\tGetting the efficiencies of the analysis for each process')
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        print('\t\tAnalysis efficiency for:', Process, '=', efficiency)
        Efficiencies[Process] = efficiency
    # get the total number of events before the BDT for signal and for background:
    NEventsSignal_BeforeBDT = {} # this needs to be a dictionary since it varies for each signal point
    NEventsBackground_BeforeBDT = 0. # at this stage, this does not vary for each signal point
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            if Processes[CurrentAnalysis][Process] not in NEventsSignal_BeforeBDT:
                NEventsSignal_BeforeBDT[Processes[CurrentAnalysis][Process]] = Efficiencies[Process] * NEventsProcess[Process]
            else:
                NEventsSignal_BeforeBDT[Processes[CurrentAnalysis][Process]] = NEventsSignal_BeforeBDT[Process] + Efficiencies[Process] * NEventsProcess[Process]
        else: #sum over backgrounds
            NEventsBackground_BeforeBDT = NEventsBackground_BeforeBDT + Efficiencies[Process] * NEventsProcess[Process]
    print("Before BDT: MC events (S,B)=", NEventsSignal_BeforeBDT, NEventsBackground_BeforeBDT)
    # and get the efficiency for the BDT value corresponding to the Significance target:
    EfficiencyFile = Location[CurrentAnalysis] + CurrentAnalysis + '_EFF_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '.txt'
    EfficiencyStream = open(EfficiencyFile, 'r')
    print('Generating efficiencies from:', EfficiencyFile)
    BDTEfficiency = {}
    # get efficiency for each of signals and the corresponding background efficiency:
    for line in EfficiencyStream:
        BDTEfficiency[float(line.split()[0])] = [ float(line.split()[1]), float(line.split()[2]) ]
    # now calculate and print the total number of events for a given mass:
    for key in BDTEfficiency:
        print(key, 'MC events (S,B)=', BDTEfficiency[key][0] * NEventsSignal_BeforeBDT[key], BDTEfficiency[key][1] * NEventsBackground_BeforeBDT)


# check the number of MC events after the analysis cuts and BDT have been applied:
if checkMCeventssmear == True:
    print('Checking the number of MC events after each stage of analysis')
    NEventsProcess = {}
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            # get the number of events in the corresponding lhe file:
            zgrepcommand = 'zgrep "= nevents" ' + lhefile
            #print zgrepcommand
            p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
            for line in iter(p.stdout.readline, b''):
                #print line
                nevents = float(line.split()[0])
            print("LHE Events in process", Process, "=", nevents)
        if Process in NEventsProcess:
            NEventsProcess[Process] = NeventsProcess[Process] + nevents*Reduction_Fac[OrderProcesses[CurrentAnalysis][Process]] # factor to take into account possible discrepancy in the HW events
        else:
            NEventsProcess[Process] = nevents*Reduction_Fac[OrderProcesses[CurrentAnalysis][Process]]
        counter = counter + 1
    print('NEventsProcess=', NEventsProcess)
    # now get the efficiency from the analysis .dat file for each process
    Efficiencies = {}
    print('\tGetting the efficiencies of the analysis for each process')
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.smear.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        print('\t\tAnalysis efficiency for:', Process, '=', efficiency)
        Efficiencies[Process] = efficiency
    # get the total number of events before the BDT for signal and for background:
    NEventsSignal_BeforeBDT = {} # this needs to be a dictionary since it varies for each signal point
    NEventsBackground_BeforeBDT = 0. # at this stage, this does not vary for each signal point
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            if Processes[CurrentAnalysis][Process] not in NEventsSignal_BeforeBDT:
                NEventsSignal_BeforeBDT[Processes[CurrentAnalysis][Process]] = Efficiencies[Process] * NEventsProcess[Process]
            else:
                NEventsSignal_BeforeBDT[Processes[CurrentAnalysis][Process]] = NEventsSignal_BeforeBDT[Processes[CurrentAnalysis][Process]] + Efficiencies[Process] * NEventsProcess[Process]
        else: #sum over backgrounds
            NEventsBackground_BeforeBDT = NEventsBackground_BeforeBDT + Efficiencies[Process] * NEventsProcess[Process]
    print("Before BDT: MC events (S,B)=", NEventsSignal_BeforeBDT, NEventsBackground_BeforeBDT)
    # and get the efficiency for the BDT value corresponding to the Significance target:
    EfficiencyFile = Location[CurrentAnalysis] + CurrentAnalysis + '_EFF_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '.smear.txt'
    EfficiencyStream = open(EfficiencyFile, 'r')
    print('Generating efficiencies from:', EfficiencyFile)
    BDTEfficiency = {}
    # get efficiency for each of signals and the corresponding background efficiency:
    for line in EfficiencyStream:
        BDTEfficiency[float(line.split()[0])] = [ float(line.split()[1]), float(line.split()[2]) ]
    # now calculate and print the total number of events for a given mass:
    for key in BDTEfficiency:
        print(key, 'MC events (S,B)=', BDTEfficiency[key][0] * NEventsSignal_BeforeBDT[key], BDTEfficiency[key][1] * NEventsBackground_BeforeBDT)

if runanalysis == True:
    print('Running analysis from the HwSim .root files generated, for:', CurrentAnalysis)
    # LOOP over the processes and write an .input file:
    for Process in list(Processes[CurrentAnalysis].keys()):
        # open the input file for all the root files in a given process:
        analysisInputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.input'
        analysisInputstream = open(analysisInputfile,'w') 
        counter = 0
        print('\twriting the .input file for the analysis')
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
            print(rootfile)
            if os.path.exists(analysisOutputfile) is True:
                    print(analysisOutputfile, 'exists')
            if os.path.exists(analysisOutputfile) is False or (os.path.exists(analysisOutputfile) is True and ReRun is True):
                if os.path.exists(analysisOutputfile) is True and ReRun is True:
                    print(analysisOutputfile, 'exists but re-running anyway')
                # check if .root file exists:
                if os.path.exists(rootfile) is False:
                    print('Error: ROOT file:', rootfile, 'does not exist!')
                    exit()
                elif os.path.exists(rootfile) is True:
                    analysisInputstream.write(rootfile + '\n')
            counter = counter + 1
        analysisInputstream.close()
        print('running the analysis', Executable[CurrentAnalysis], 'on the input file', analysisInputfile)
        analysiscommand = HerwigSourceCmd + Location[CurrentAnalysis] + Executable[CurrentAnalysis] + ' ' + analysisInputfile
        p = subprocess.Popen(analysiscommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            print('\t\t', line, end=' ')
        out, err = p.communicate()
        print('\n')

if runanalysissmear == True:
    print('Running analysis from the HwSim .root files generated, for:', CurrentAnalysis)
    # LOOP over the processes and write an .input file:
    for Process in list(Processes[CurrentAnalysis].keys()):
        # open the input file for all the root files in a given process:
        analysisInputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.input'
        analysisInputstream = open(analysisInputfile,'w') 
        counter = 0
        print('\twriting the .input file for the analysis')
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.smear.dat'
            print(rootfile)
            if os.path.exists(analysisOutputfile) is True:
                    print(analysisOutputfile, 'exists')
            if os.path.exists(analysisOutputfile) is False or (os.path.exists(analysisOutputfile) is True and ReRun is True):
                if os.path.exists(analysisOutputfile) is True and ReRun is True:
                    print(analysisOutputfile, 'exists but re-running anyway')
                # check if .root file exists:
                if os.path.exists(rootfile) is False:
                    print('Error: ROOT file:', rootfile, 'does not exist!')
                    exit()
                elif os.path.exists(rootfile) is True:
                    analysisInputstream.write(rootfile + '\n')
            counter = counter + 1
        analysisInputstream.close()
        print('running the analysis', ExecutableSmear[CurrentAnalysis], 'on the input file', analysisInputfile)
        analysiscommand = HerwigSourceCmd + Location[CurrentAnalysis] + ExecutableSmear[CurrentAnalysis] + ' ' + analysisInputfile
        p = subprocess.Popen(analysiscommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            print('\t\t', line, end=' ')
        out, err = p.communicate()
        print('\n')

if runanalysiscuts == True:
    print('Running analysis from the HwSim .root files generated, for:', CurrentAnalysis)
    # LOOP over the processes and write an .input file:
    for Process in list(Processes[CurrentAnalysis].keys()):
        # open the input file for all the root files in a given process:
        analysisInputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.input'
        analysisInputstream = open(analysisInputfile,'w') 
        counter = 0
        print('\twriting the .input file for the analysis')
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.cuts.dat'
            print(rootfile)
            if os.path.exists(analysisOutputfile) is True:
                    print(analysisOutputfile, 'exists')
            if os.path.exists(analysisOutputfile) is False or (os.path.exists(analysisOutputfile) is True and ReRun is True):
                if os.path.exists(analysisOutputfile) is True and ReRun is True:
                    print(analysisOutputfile, 'exists but re-running anyway')
                # check if .root file exists:
                if os.path.exists(rootfile) is False:
                    print('Error: ROOT file:', rootfile, 'does not exist!')
                    exit()
                elif os.path.exists(rootfile) is True:
                    analysisInputstream.write(rootfile + '\n')
            counter = counter + 1
        analysisInputstream.close()
        print('running the analysis', ExecutableCuts[CurrentAnalysis], 'on the input file', analysisInputfile)
        analysiscommand = HerwigSourceCmd + Location[CurrentAnalysis] + ExecutableCuts[CurrentAnalysis] + ' ' + analysisInputfile
        p = subprocess.Popen(analysiscommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            print('\t\t', line, end=' ')
        out, err = p.communicate()
        print('\n')


if collectanalysiscuts == True:
    print('Collecting analysis results for', end=' ') 
    # get the *TOTAL* cross sections before decays
    TotalUndecayedCrossSection = {}
    for Process in list(Processes[CurrentAnalysis].keys()):            
        AverageTotalCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations_Undecayed[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            if os.path.exists(lhefile) is False:
                print('Error, lhe file or summary file:', lhefile, 'does not exist!')
                exit()
            else:
                #  LO processes
                if OrderProcesses[CurrentAnalysis][Process] == 0:
                    zgrepcommand = 'zgrep "Integrated weight" ' + lhefile
                    p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                    for line in iter(p.stdout.readline, b''):
                        xsec = float(line.split()[5])
                # NLO processes:
                if OrderProcesses[CurrentAnalysis][Process] == 1 or OrderProcesses[CurrentAnalysis][Process] == 2:
                    zgrepcommand = 'zgrep "Total cross section" ' + lhefile
                    #print zgrepcommand
                    p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                    for line in iter(p.stdout.readline, b''):
                        xsec = float(line.split()[3])
                AverageTotalCrossSection = AverageTotalCrossSection + xsec
            counter = counter + 1
        TotalUndecayedCrossSection[Process] = AverageTotalCrossSection / float(counter)
        print(Process, 'Total UNDECAYED h2 cross section=', TotalUndecayedCrossSection[Process])
    # write out the LO cross section for the SIGNAL processes:
    TotalCrossSectionOutputFile = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_totalxsec.txt'
    TotalCrossSectionOutputStream = open(TotalCrossSectionOutputFile,'w')
    print('\twriting out the undecayed cross sections to', TotalCrossSectionOutputFile)
    for Process in list(Processes[CurrentAnalysis].keys()):
        TotalCrossSectionOutputStream.write(Process + "\t" + str(TotalUndecayedCrossSection[Process]) + "\n")
    TotalCrossSectionOutputStream.close()
    
    # Cross section dictionary:
    InitialCrossSection = {}
    # LOOP over the processes and calculate the cross section before the analysis:
    print('\tcalculating the cross sections for each process from Herwig .out files')
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "LHReader " ' + outfile
                print(grepcommand)
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec)*1000. # convert to pb
                print('\t\tCross section in', outfile, '=', xsec, 'pb')
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    # now get the efficiency from the analysis .dat file for each process
    Efficiencies = {}
    print('\tGetting the efficiencies of the analysis for each process')
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.cuts.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .cuts.dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        print('\t\tAnalysis efficiency for:', Process, '=', efficiency)
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    print('\tResulting cross sections after analysis * K-factors:')
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process] * 1000. # CONVER TO FB
        print('\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'fb')
    # get an estimate of the significance given the above output cross sections at Luminosity
    sum_signals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. # reset the signal sum for the given mass
    sum_backgrounds = 0.
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] == 0: # backgrounds
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
        else: # Processes[CurrentAnalysis][Process] == 1: # signal
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. + sum_signals[Processes[CurrentAnalysis][Process]] + OutputCrossSection[Process]
    significance = {}
    signalfactor = 150.
    for Masses in list(Processes[CurrentAnalysis].values()):
        if Masses != 0.0:
            significance[Masses] = sum_signals[Masses] * signalfactor*Luminosity[CurrentAnalysis] / math.sqrt(sum_backgrounds*Luminosity[CurrentAnalysis] + (0.05*sum_backgrounds*Luminosity[CurrentAnalysis])**2 )
            
    print('\tCurrent significance estimate for signal factor=', signalfactor, significance)
  
       

if collectanalysis == True:
    print('Collecting analysis results for', end=' ') 
    # get the *TOTAL* cross sections before decays
    TotalUndecayedCrossSection = {}
    for Process in list(Processes[CurrentAnalysis].keys()):            
        AverageTotalCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations_Undecayed[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            if os.path.exists(lhefile) is False:
                print('Error, lhe file or summary file:', lhefile, 'does not exist!')
                exit()
            else:
                #  LO processes
                if OrderProcesses[CurrentAnalysis][Process] == 0:
                    zgrepcommand = 'zgrep "Integrated weight" ' + lhefile
                    p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                    for line in iter(p.stdout.readline, b''):
                        xsec = float(line.split()[5])
                # NLO processes:
                if OrderProcesses[CurrentAnalysis][Process] == 1 or OrderProcesses[CurrentAnalysis][Process] == 2:
                    zgrepcommand = 'zgrep "Total cross section" ' + lhefile
                    #print zgrepcommand
                    p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                    for line in iter(p.stdout.readline, b''):
                        xsec = float(line.split()[3])
                AverageTotalCrossSection = AverageTotalCrossSection + xsec
            counter = counter + 1
        TotalUndecayedCrossSection[Process] = AverageTotalCrossSection / float(counter)
        print(Process, 'Total UNDECAYED h2 cross section=', TotalUndecayedCrossSection[Process])
    # write out the LO cross section for the SIGNAL processes:
    TotalCrossSectionOutputFile = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_totalxsec.txt'
    TotalCrossSectionOutputStream = open(TotalCrossSectionOutputFile,'w')
    print('\twriting out the undecayed cross sections to', TotalCrossSectionOutputFile)
    for Process in list(Processes[CurrentAnalysis].keys()):
        TotalCrossSectionOutputStream.write(Process + "\t" + str(TotalUndecayedCrossSection[Process]) + "\n")
    TotalCrossSectionOutputStream.close()
    
    # Cross section dictionary:
    InitialCrossSection = {}
    # LOOP over the processes and calculate the cross section before the analysis:
    print('\tcalculating the cross sections for each process from Herwig .out files')
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "LHReader " ' + outfile
                print(grepcommand)
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec)*1000. # convert to pb
                print('\t\tCross section in', outfile, '=', xsec, 'pb')
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    # now get the efficiency from the analysis .dat file for each process
    Efficiencies = {}
    print('\tGetting the efficiencies of the analysis for each process')
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        print('\t\tAnalysis efficiency for:', Process, '=', efficiency)
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    print('\tResulting cross sections after analysis * K-factors:')
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process] * 1000. # CONVER TO FB
        print('\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'fb')
    # get an estimate of the significance given the above output cross sections at Luminosity
    sum_signals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. # reset the signal sum for the given mass
    sum_backgrounds = 0.
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] == 0: # backgrounds
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
        else: # Processes[CurrentAnalysis][Process] == 1: # signal
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. + sum_signals[Processes[CurrentAnalysis][Process]] + OutputCrossSection[Process]
    significance = {}
    for Masses in list(Processes[CurrentAnalysis].values()):
        if Masses != 0.0:
            significance[Masses] = sum_signals[Masses] * math.sqrt(Luminosity[CurrentAnalysis]) / math.sqrt(sum_backgrounds)
            
    print('\tCurrent significance estimate=', significance)
    # write output files for the BDT
    print('\tWriting the BDT txt files:')
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    BDTOutputfileSignals = {}
    BDTSignals_Stream = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt'
            BDTSignals_Stream[Processes[CurrentAnalysis][Process]] = open(BDTOutputfileSignals[Processes[CurrentAnalysis][Process]],'w')
    print('\t\twriting signal results in:', BDTOutputfileSignals)
    print('\t\twriting background results in:', BDTOutputfileBackgrounds)
    BDTBackgrounds_Stream = open(BDTOutputfileBackgrounds, 'w')
    for Process in list(Processes[CurrentAnalysis].keys()):
        processname = Process
        outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
        varrootfile = outputlocation + processname + '_var.root'
        if Processes[CurrentAnalysis][Process] == 0: # background 
            BDTBackgrounds_Stream.write(varrootfile + "\t" + str(OutputCrossSection[Process]) + "\n")
        else: # signal
            BDTSignals_Stream[Processes[CurrentAnalysis][Process]].write(varrootfile + "\t" + str(OutputCrossSection[Process]) + "\n")

if collectanalysissmear == True:
    print('Collecting analysis results for smearing', end=' ') 
    # get the *TOTAL* cross sections before decays
    TotalUndecayedCrossSection = {}
    for Process in list(Processes[CurrentAnalysis].keys()):            
        AverageTotalCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations_Undecayed[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            if os.path.exists(lhefile) is False:
                print('Error, lhe file or summary file:', lhefile, 'does not exist!')
                exit()
            else:
                #  LO processes
                if OrderProcesses[CurrentAnalysis][Process] == 0:
                    zgrepcommand = 'zgrep "Integrated weight" ' + lhefile
                    p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                    for line in iter(p.stdout.readline, b''):
                        xsec = float(line.split()[5])
                # NLO processes:
                if OrderProcesses[CurrentAnalysis][Process] == 1 or OrderProcesses[CurrentAnalysis][Process] == 2:
                    zgrepcommand = 'zgrep "Total cross section" ' + lhefile
                    #print zgrepcommand
                    p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                    for line in iter(p.stdout.readline, b''):
                        xsec = float(line.split()[3])
                AverageTotalCrossSection = AverageTotalCrossSection + xsec
            counter = counter + 1
        TotalUndecayedCrossSection[Process] = AverageTotalCrossSection / float(counter)
        print(Process, 'Total UNDECAYED h2 cross section=', TotalUndecayedCrossSection[Process])
    # write out the LO cross section for the SIGNAL processes:
    TotalCrossSectionOutputFile = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_totalxsec.smear.txt'
    TotalCrossSectionOutputStream = open(TotalCrossSectionOutputFile,'w')
    print('\twriting out the undecayed cross sections to', TotalCrossSectionOutputFile)
    for Process in list(Processes[CurrentAnalysis].keys()):
        TotalCrossSectionOutputStream.write(Process + "\t" + str(TotalUndecayedCrossSection[Process]) + "\n")
    TotalCrossSectionOutputStream.close()
    
    # Cross section dictionary:
    InitialCrossSection = {}
    # LOOP over the processes and calculate the cross section before the analysis:
    print('\tcalculating the cross sections for each process from Herwig .out files')
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "LHReader " ' + outfile
                print(grepcommand)
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline,b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec.replace("b","").replace("'", ""))*1000. # convert to pb
                print('\t\tCross section in', outfile, '=', xsec, 'pb')
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    # now get the efficiency from the analysis .dat file for each process
    Efficiencies = {}
    print('\tGetting the efficiencies of the analysis for each process')
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.smear.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        print('\t\tAnalysis efficiency for:', Process, '=', efficiency)
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    print('\tResulting cross sections after analysis * K-factors:')
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process] * 1000. # CONVER TO FB
        print('\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'fb')
    # get an estimate of the significance given the above output cross sections at Luminosity
    sum_signals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. # reset the signal sum for the given mass
    sum_backgrounds = 0.
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] == 0: # backgrounds
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
        else: # Processes[CurrentAnalysis][Process] == 1: # signal
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. + sum_signals[Processes[CurrentAnalysis][Process]] + OutputCrossSection[Process]
    significance = {}
    signalfactor = 150.
    for Masses in list(Processes[CurrentAnalysis].values()):
        if Masses != 0.0:
            significance[Masses] = sum_signals[Masses] * signalfactor*Luminosity[CurrentAnalysis] / math.sqrt(sum_backgrounds*Luminosity[CurrentAnalysis] + (0.05*sum_backgrounds*Luminosity[CurrentAnalysis])**2 )
            
    print('\tCurrent significance estimate for signal factor=', signalfactor, significance)
            
    print('\tCurrent significance estimate=', significance)
    # write output files for the BDT
    print('\tWriting the BDT txt files:')
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    BDTOutputfileSignals = {}
    BDTSignals_Stream = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
            BDTSignals_Stream[Processes[CurrentAnalysis][Process]] = open(BDTOutputfileSignals[Processes[CurrentAnalysis][Process]],'w')
    print('\t\twriting signal results in:', BDTOutputfileSignals)
    print('\t\twriting background results in:', BDTOutputfileBackgrounds)
    BDTBackgrounds_Stream = open(BDTOutputfileBackgrounds, 'w')
    for Process in list(Processes[CurrentAnalysis].keys()):
        processname = Process
        outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
        varrootfile = outputlocation + processname + '_var.smear.root'
        if Processes[CurrentAnalysis][Process] == 0: # background 
            BDTBackgrounds_Stream.write(varrootfile + "\t" + str(OutputCrossSection[Process]) + "\n")
        else: # signal
            BDTSignals_Stream[Processes[CurrentAnalysis][Process]].write(varrootfile + "\t" + str(OutputCrossSection[Process]) + "\n")
       

# run the BDT:
if runbdt == True:
    print('Running the BDT for:', CurrentAnalysis)
    # the BDT files:
    BDTOutputfileSignals = {}
    BDTSignals_Stream = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]
    SignalFactor = SignalFactorAnalysis[CurrentAnalysis]
    RSEED = 100
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            if TMVABinOrScript == 1:
                rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(SignalFactor) + ", " + str(NVariables[CurrentAnalysis]) + ", " + str(RSEED) + ")'"
            elif TMVABinOrScript == 0:
                rootcommand = TMVALocation + TMVABinary + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + '" "' + BDTOutputfileBackgrounds + '" ' + str(SignalFactor) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED)
            print(rootcommand)
            p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
            for line in iter(p.stdout.readline, b''):
                if "Maximum" in line:
                    signif = line.split()[3] # Note that the the significance is given by the 4th word in the line
                    print('Maximum significance for Signal factor:', SignalFactor, signif)
            out, err = p.communicate()

# run the BDT:
if runbdtsmear == True:
    print('Running the BDT for:', CurrentAnalysis)
    # the BDT files:
    BDTOutputfileSignals = {}
    BDTSignals_Stream = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]
    SignalFactor = SignalFactorAnalysis[CurrentAnalysis]
    RSEED = 100
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            if TMVABinOrScript == 1:
                rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(SignalFactor) + ", " + str(NVariables[CurrentAnalysis]) + ", " + str(RSEED) + ")'"
            elif TMVABinOrScript == 0:
                rootcommand = TMVALocation + TMVABinary + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + '" "' + BDTOutputfileBackgrounds + '" ' + str(SignalFactor) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED)
            print(rootcommand)
            p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
            for line in iter(p.stdout.readline, b''):
                if "Maximum" in line:
                    signif = line.split()[3] # Note that the the significance is given by the 4th word in the line
                    print(line)
                    print('Maximum significance for Signal factor:', SignalFactor, signif)
            out, err = p.communicate()


# run the Cuts:
if runcuts == True:
    print('Running the Cuts for:', CurrentAnalysis)
    # the BDT files:
    BDTOutputfileSignals = {}
    BDTSignals_Stream = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]
    SignalFactor = SignalFactorAnalysis[CurrentAnalysis]
    RSEED = 100
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            if TMVABinOrScript == 1:
                rootcommand = "root -l '" + TMVALocation + TMVAFileCuts + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(SignalFactor) + ", " + str(NVariables[CurrentAnalysis]) + ", " + str(RSEED) + ")'"
                print(runcuts)
            elif TMVABinOrScript == 0:
                rootcommand = TMVALocation + TMVABinaryCuts + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + '" "' + BDTOutputfileBackgrounds + '" ' + str(SignalFactor) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED)
            print(rootcommand)
            p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
            for line in iter(p.stdout.readline, b''):
                if "Maximum" in line:
                    signif = line.split()[3] # Note that the the significance is given by the 4th word in the line
                    print('Maximum significance for Signal factor:', SignalFactor, signif)
            out, err = p.communicate()



            
# run the BDT and find solution:
if runbdtsolve == True:
    print('Running the BDT for:', CurrentAnalysis, 'finding the cross section for the given significance', SignifTarget)
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]
    SolutionEfficiencyS = {}
    SolutionEfficiencyB = {}
    SolutionNeventsS = {}
    SolutionNeventsB = {}
    # function to run the BDT -- to assist solution:
    RSEED = 100 # the random seed
    def FuncBDT(SignalFactor, BDTOutputfileSignals_Mass):
        #print SignalFactor
        if TMVABinOrScript == 1:
            rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(abs(SignalFactor)) + ", " + str(NVariables[CurrentAnalysis]) + ", " + str(RSEED) + ")'"
        elif TMVABinOrScript == 0:
            rootcommand = TMVALocation + TMVABinary + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + '" "' + BDTOutputfileBackgrounds + '" ' + str(abs(SignalFactor[0])) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED)
        #print rootcommand
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        signif = 0.
        for line in iter(p.stdout.readline, b''):
            if "Maximum" in line:
                signif = line.split()[3] # Note that the the significance is give by the 4th word in the line
                epsS = float(line.split()[9])
                epsB = float(line.split()[10])
                NS = float(line.split()[15])
                NB = float(line.split()[16])
                print(SignalFactor, signif)
        #out, err = p.communicate()
        return float(signif), epsS, epsB, NS, NB
    # get the solution for the signal cross section for a given significance target:
    SolutionFactor = {} # array contains the solution factor for each mass
    #SignifTarget = 2.0
    if len(args) > 2:
        SignifTarget = float(args[2])
        print("Significance target is ", SignifTarget, " standard deviations")
    for Process in sorted(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0 and Processes[CurrentAnalysis][Process] not in list(SolutionNeventsS.keys()): # not a background and not already looked at (to avoid running twice over the masses)
            print('considering:', Processes[CurrentAnalysis][Process])
            popt = [Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt']
            func_arg = lambda f : FuncBDT(f, *popt)[0] - SignifTarget
            f_initial_guess = SignalFactorAnalysis[CurrentAnalysis]
            if runbdtdebug == True:
                print('DEBUG=', Processes[CurrentAnalysis][Process], FuncBDT([f_initial_guess], *popt))
            elif runbdtdebug == False:
                f_solution = scipy.optimize.fsolve(func_arg, x0=f_initial_guess, xtol=0.01)#, options={'maxfev':1000}) #options={'xtol':0.01, 'maxfev':1000})
                SolutionFactor[Processes[CurrentAnalysis][Process]] = f_solution[0]
                sig, SolutionEfficiencyS[Processes[CurrentAnalysis][Process]],  SolutionEfficiencyB[Processes[CurrentAnalysis][Process]], SolutionNeventsS[Processes[CurrentAnalysis][Process]], SolutionNeventsB[Processes[CurrentAnalysis][Process]] = FuncBDT(f_solution, *popt)
                print("M, f_solution, sig, effS, effB, NS(L), NB(L)=", Processes[CurrentAnalysis][Process], SolutionFactor[Processes[CurrentAnalysis][Process]], sig, SolutionEfficiencyS[Processes[CurrentAnalysis][Process]],  SolutionEfficiencyB[Processes[CurrentAnalysis][Process]], SolutionNeventsS[Processes[CurrentAnalysis][Process]], SolutionNeventsB[Processes[CurrentAnalysis][Process]])
    if runbdtdebug is True:
        print('DEBUGGING FINISHED, EXITING')
        exit()
    # BDT SOLUTION AVAILABLE AT THIS POINT
    # now use the rescaling factor to calculate the cross section for the signal corresponding to the significance target
    # repeating code here to get the output cross section
    # LOOP over the processes and calculate the cross section BEFORE the analysis:
    #print '\tcalculating the cross sections for each process from Herwig .out files'
    InitialCrossSection = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "LHReader " ' + outfile
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec)*1000. # convert to pb
                #print '\t\tCross section in', outfile, '=', xsec, 'pb'
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    Efficiencies = {}
    #print '\tGetting the efficiencies of the analysis for each process'
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        #print '\t\tAnalysis efficiency for:', Process, '=', efficiency
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    #print '\tResulting cross sections after analysis * K-factors:'
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process] * 1000. # CONVERT TO FB
        #print '\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'fb'
    # sum the cross sections for all the signals and all the backgrounds
    sum_signals = {}
    sum_backgrounds = 0.
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. # reset the signal sum for the given mass
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = 0. + sum_signals[Processes[CurrentAnalysis][Process]] + OutputCrossSection[Process] * SolutionFactor[Processes[CurrentAnalysis][Process]]
        elif Processes[CurrentAnalysis][Process] == 0: # backgrounds
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
    
    # calculate the limit:
    sum_signals_limit = {}
    sum_backgrounds_limit = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals_limit[Processes[CurrentAnalysis][Process]] = sum_signals[Processes[CurrentAnalysis][Process]] * SolutionEfficiencyS[Processes[CurrentAnalysis][Process]]
            sum_backgrounds_limit[Processes[CurrentAnalysis][Process]] = sum_backgrounds * SolutionEfficiencyB[Processes[CurrentAnalysis][Process]] 
    print('Signal cross sections corresponding to a significance target:', SignifTarget, ' (in fb):', sum_signals_limit)
    # write out the results in a file for plotting:
    SignifFile = Location[CurrentAnalysis] + CurrentAnalysis + '_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '.txt'
    SignifOutputStream = open(SignifFile, 'w')
    print('writing out cross section file:', SignifFile)
    for Process in list(sum_signals_limit.keys()):            
        SignifOutputStream.write(str(Process) + "\t" + str(sum_signals_limit[Process]) + "\t" + str(sum_backgrounds_limit[Process]) + "\n")
    SignifOutputStream.close()
    # write out a file with the signal and background efficiencies at each signal point:
    EfficiencyFile = Location[CurrentAnalysis] + CurrentAnalysis + '_EFF_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '.txt'
    print('writing out efficiency file:', EfficiencyFile)
    EfficiencyStream = open(EfficiencyFile, 'w')
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background 
            EfficiencyStream.write(str(Processes[CurrentAnalysis][Process]) + "\t" + str(SolutionEfficiencyS[Processes[CurrentAnalysis][Process]]) + "\t" + str(SolutionEfficiencyB[Processes[CurrentAnalysis][Process]]) + "\n")
    EfficiencyStream.close()


# run the BDT N times, calculate the 1 sigma and 2 sigma intervals:
if runbdtmulti == True:
    NBDT = NBDTAnalysis[CurrentAnalysis]
    print('Running the BDT for:', CurrentAnalysis, 'finding the cross section for the given significance', SignifTarget, 'for N=', NBDT)
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]
    SolutionEfficiencyS = {}
    SolutionEfficiencyB = {}
    SolutionNeventsS = {}
    SolutionNeventsB = {}
    # function to run the BDT -- to assist solution:
    def FuncBDT(SignalFactor, BDTOutputfileSignals_Mass, RSEED):
        #print SignalFactor
        if TMVABinOrScript == 1:
            rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(abs(SignalFactor)) + ", " + str(NVariables[CurrentAnalysis]) + ", " + str(RSEED) + ")'"
        elif TMVABinOrScript == 0:
            rootcommand = TMVALocation + TMVABinary + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + '" "' + BDTOutputfileBackgrounds + '" ' + str(abs(SignalFactor[0])) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED)
        #print rootcommand
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        signif = 0.
        epsS = 0
        epsB = 0
        NS = 0
        NB = 0
        for line in iter(p.stdout.readline, b''):
            if "Maximum" in line:
                signif = line.split()[3] # Note that the the significance is give by the 4th word in the line
                epsS = float(line.split()[9])
                epsB = float(line.split()[10])
                NS = float(line.split()[15])
                NB = float(line.split()[16])
                print(SignalFactor, signif)
        #out, err = p.communicate()
        return float(signif), epsS, epsB, NS, NB
    # get the solution for the signal cross section for a given significance target:
    SolutionFactor = {} # array contains the solution factor for each mass
    #SignifTarget = 2.0
    RSEED_ZERO = 100
    if len(args) > 2:
        SignifTarget = float(args[2])
        print("Significance target is ", SignifTarget, " standard deviations")
    for Process in sorted(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0 and Processes[CurrentAnalysis][Process] not in list(SolutionNeventsS.keys()): # not a background and not already looked at (to avoid running twice over the masses)
            print('considering:', Processes[CurrentAnalysis][Process], end=' ')
            Ni = 0
            while Ni < NBDT:
                print('Ni=', Ni)
                RSEED_i = Ni + RSEED_ZERO
                popt = [Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.txt', RSEED_i]
                func_arg = lambda f : FuncBDT(f, *popt)[0] - SignifTarget
                f_initial_guess = SignalFactorAnalysis[CurrentAnalysis]
                f_solution = scipy.optimize.fsolve(func_arg, x0=f_initial_guess, xtol=0.01)
                sig, SolutionEfficiencyS_i,  SolutionEfficiencyB_i, SolutionNeventsS_i, SolutionNeventsB_i = FuncBDT(f_solution, *popt)
                if sig != SignifTarget:
                    RSEED_ZERO = random.randint(0,999)
                    print('WARNING: Significance target', SignifTarget, 'not reached:', sig, 'retrying with new seed', Ni+RSEED_ZERO)
                    continue
                if Processes[CurrentAnalysis][Process] not in list(SolutionFactor.keys()):
                    SolutionFactor[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionEfficiencyS[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionEfficiencyB[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionNeventsS[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionNeventsB[Processes[CurrentAnalysis][Process]] = [0.] * NBDT                   
                    SolutionFactor[Processes[CurrentAnalysis][Process]][Ni] = f_solution[0]
                    SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyS_i
                    SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyB_i
                    SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsS_i
                    SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsB_i
                else:
                    SolutionFactor[Processes[CurrentAnalysis][Process]][Ni] = f_solution[0]
                    SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyS_i
                    SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyB_i
                    SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsS_i
                    SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsB_i
                print("M, Ni, RSEED_i, f_solution, sig, effS, effB, NS(L), NB(L)=", Processes[CurrentAnalysis][Process], Ni, RSEED_i, SolutionFactor[Processes[CurrentAnalysis][Process]][Ni], sig, SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni],  SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni], SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni], SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni])
                Ni = Ni + 1
            
    # BDT SOLUTION(S) AVAILABLE AT THIS POINT
    # now use the rescaling factor to calculate the cross section for the signal corresponding to the significance target
    # repeating code here to get the output cross section
    # LOOP over the processes and calculate the cross section BEFORE the analysis:
    #print '\tcalculating the cross sections for each process from Herwig .out files'
    InitialCrossSection = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "LHReader " ' + outfile
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec)*1000. # convert to pb
                #print '\t\tCross section in', outfile, '=', xsec, 'pb'
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    Efficiencies = {}
    #print '\tGetting the efficiencies of the analysis for each process'
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        #print '\t\tAnalysis efficiency for:', Process, '=', efficiency
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    #print '\tResulting cross sections after analysis * K-factors:'
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process] * 1000. # CONVERT TO FB
        #print '\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'fb'
    # sum the cross sections for all the signals and all the backgrounds
    sum_signals = {}
    sum_backgrounds = 0.

    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = [0.]*NBDT # reset the signal sum for the given mass
            
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            for Ni in range(NBDT):
                sum_signals[Processes[CurrentAnalysis][Process]][Ni] = sum_signals[Processes[CurrentAnalysis][Process]][Ni] + OutputCrossSection[Process] * SolutionFactor[Processes[CurrentAnalysis][Process]][Ni]
        elif Processes[CurrentAnalysis][Process] == 0: # backgrounds
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
    
    # calculate the limit:
    sum_signals_limit = {}
    sum_backgrounds_limit = {}
    sum_backgrounds_limit_mean = {} 
    sum_signals_limit_mean = {}
    sum_signals_limit_error = {}
    sum_signals_limit_min = {}

    for Process in list(Processes[CurrentAnalysis].keys()):
        sum_signals_limit[Processes[CurrentAnalysis][Process]] = [0.]*NBDT
        sum_backgrounds_limit[Processes[CurrentAnalysis][Process]] = [0.]*NBDT

    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            for Ni in range(NBDT):
                sum_signals_limit[Processes[CurrentAnalysis][Process]][Ni] = sum_signals[Processes[CurrentAnalysis][Process]][Ni] * SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni]
                sum_backgrounds_limit[Processes[CurrentAnalysis][Process]][Ni] = sum_backgrounds * SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni]
            sum_backgrounds_limit_mean[Processes[CurrentAnalysis][Process]] = np.mean(sum_backgrounds_limit[Processes[CurrentAnalysis][Process]])
            sum_signals_limit_mean[Processes[CurrentAnalysis][Process]] = np.mean(sum_signals_limit[Processes[CurrentAnalysis][Process]])
            sum_signals_limit_error[Processes[CurrentAnalysis][Process]] = np.std(sum_signals_limit[Processes[CurrentAnalysis][Process]])
            sum_signals_limit_min[Processes[CurrentAnalysis][Process]] = np.min(sum_signals_limit[Processes[CurrentAnalysis][Process]])

    print('Mean Signal cross sections corresponding to a significance target:', SignifTarget, ' (in fb):', sum_signals_limit_mean, 'average over NBDT=', NBDT)
    # write out the results in a file for plotting:
    SignifFile = Location[CurrentAnalysis] + CurrentAnalysis + '_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '_NBDT' + str(NBDT) + '.txt'
    SignifOutputStream = open(SignifFile, 'w')
    print('writing out cross section file:', SignifFile)
    for Process in list(sum_signals_limit_mean.keys()):            
        SignifOutputStream.write(str(Process) + "\t" + str(sum_signals_limit_mean[Process]) + "\t" + str(sum_backgrounds_limit_mean[Process]) + "\t" + str(sum_signals_limit_error[Process]) + "\t" + str(sum_signals_limit_min[Process]) + "\n")
    SignifOutputStream.close()
    # write out a file with the signal and background efficiencies at each signal point:
    #EfficiencyFile = Location[CurrentAnalysis] + CurrentAnalysis + '_EFFN_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '.txt'
    #print 'writing out efficiency file:', EfficiencyFile
    #EfficiencyStream = open(EfficiencyFile, 'w')
    #for Process in Processes[CurrentAnalysis].keys():
    #    if Processes[CurrentAnalysis][Process] != 0: # not a background 
    #        EfficiencyStream.write(str(Processes[CurrentAnalysis][Process]) + "\t" + str(SolutionEfficiencyS[Processes[CurrentAnalysis][Process]]) + "\t" + str(SolutionEfficiencyB[Processes[CurrentAnalysis][Process]]) + "\n")
    #EfficiencyStream.close()


# run the BDT N times, calculate the 1 sigma and 2 sigma intervals:
if runbdtmultismear == True:
    NBDT = NBDTAnalysis[CurrentAnalysis]
    print('Running the BDT for:', CurrentAnalysis, 'finding the cross section for the given significance', SignifTarget, 'for N=', NBDT)
    print('WITH SMEARING')
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]
    SolutionEfficiencyS = {}
    SolutionEfficiencyB = {}
    SolutionNeventsS = {}
    SolutionNeventsB = {}
    # function to run the BDT -- to assist solution:
    def FuncBDT(SignalFactor, BDTOutputfileSignals_Mass, RSEED):
        #print SignalFactor
        if TMVABinOrScript == 1:
            rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(abs(SignalFactor)) + ", " + str(NVariables[CurrentAnalysis]) + ", " + str(RSEED) + ")'"
        elif TMVABinOrScript == 0:
            rootcommand = TMVALocation + TMVABinary + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] + '" "' + BDTOutputfileBackgrounds + '" ' + str(abs(SignalFactor[0])) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED)
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        signif = 0.
        epsS = 0
        epsB = 0
        NS = 0
        NB = 0
        for line in iter(p.stdout.readline, b''):
            #print line
            if b"Maximum" in line:
                signif = line.split()[3] # Note that the the significance is give by the 4th word in the line
                epsS = float(line.split()[9])
                epsB = float(line.split()[10])
                NS = float(line.split()[15])
                NB = float(line.split()[16])
                print(SignalFactor, signif)
            if b"Kolmogorov" in line:
                if b"Signal" in line:
                    Klmgv_S = float(line.split()[4])
                if b"Background" in line:
                    Klmgv_B = float(line.split()[4])
        #out, err = p.communicate()
        return float(signif), epsS, epsB, NS, NB
    # get the solution for the signal cross section for a given significance target:
    SolutionFactor = {} # array contains the solution factor for each mass
    #SignifTarget = 2.0
    # open a file to write out the results for the BDT run
    BDTResultsFile = Location[CurrentAnalysis] + CurrentAnalysis + '_results.smear.txt'
    BDTResultsStream = open(BDTResultsFile, 'w')
    RSEED_ZERO = 100
    if len(args) > 2:
        SignifTarget = float(args[2])
        print("Significance target is ", SignifTarget, " standard deviations")
    for Process in sorted(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0 and Processes[CurrentAnalysis][Process] not in list(SolutionNeventsS.keys()): # not a background and not already looked at (to avoid running twice over the masses)
            print('considering:', Processes[CurrentAnalysis][Process], end=' ')
            Ni = 0
            while Ni < NBDT:
                print('Ni=', Ni)
                RSEED_i = Ni + RSEED_ZERO
                popt = [Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt', RSEED_i]
                f_initial_guess = SignalFactorAnalysis[CurrentAnalysis]
                f_initial_guess_test = [SignalFactorAnalysis[CurrentAnalysis]]
                # run once to test Kolmogorov test
                print('Kolmogorov test for initial random seed')
                if FuncBDT(f_initial_guess_test, *popt)[0] == -1:
                    print('Kolmogorov test failed, changing random seed')
                    RSEED_ZERO = random.randint(0,999)
                    continue
                print('Kolmogorov test passed, continuing')
                func_arg = lambda f : FuncBDT(f, *popt)[0] - SignifTarget
                f_solution = scipy.optimize.fsolve(func_arg, x0=f_initial_guess, xtol=0.01)
                sig, SolutionEfficiencyS_i,  SolutionEfficiencyB_i, SolutionNeventsS_i, SolutionNeventsB_i = FuncBDT(f_solution, *popt)
                if sig != SignifTarget:
                    RSEED_ZERO = random.randint(0,999)
                    print('WARNING: Significance target', SignifTarget, 'not reached:', sig, 'retrying with new seed', Ni+RSEED_ZERO)
                    continue
                if Processes[CurrentAnalysis][Process] not in list(SolutionFactor.keys()):
                    SolutionFactor[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionEfficiencyS[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionEfficiencyB[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionNeventsS[Processes[CurrentAnalysis][Process]] = [0.] * NBDT
                    SolutionNeventsB[Processes[CurrentAnalysis][Process]] = [0.] * NBDT                   
                    SolutionFactor[Processes[CurrentAnalysis][Process]][Ni] = f_solution[0]
                    SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyS_i
                    SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyB_i
                    SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsS_i
                    SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsB_i
                else:
                    SolutionFactor[Processes[CurrentAnalysis][Process]][Ni] = f_solution[0]
                    SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyS_i
                    SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni] = SolutionEfficiencyB_i
                    SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsS_i
                    SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni] = SolutionNeventsB_i
                print("M, Ni, RSEED_i, f_solution, sig, effS, effB, NS(L), NB(L)=", Processes[CurrentAnalysis][Process], Ni, RSEED_i, SolutionFactor[Processes[CurrentAnalysis][Process]][Ni], sig, SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni],  SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni], SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni], SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni])

                BDTResultsStream.write(str(Processes[CurrentAnalysis][Process]) + "\t" + str(Ni) + "\t" + str(RSEED_i) + "\t" + str(SolutionFactor[Processes[CurrentAnalysis][Process]][Ni]) + "\t" + str(sig) + "\t" + str(SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni]) + "\t" + str(SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni]) + "\t" + str(SolutionNeventsS[Processes[CurrentAnalysis][Process]][Ni]) + "\t" + str(SolutionNeventsB[Processes[CurrentAnalysis][Process]][Ni]) + "\n")
                Ni = Ni + 1
    BDTResultsStream.close()
    # BDT SOLUTION(S) AVAILABLE AT THIS POINT
    # now use the rescaling factor to calculate the cross section for the signal corresponding to the significance target
    # repeating code here to get the output cross section
    # LOOP over the processes and calculate the cross section BEFORE the analysis:
    #print '\tcalculating the cross sections for each process from Herwig .out files'
    InitialCrossSection = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "LHReader " ' + outfile
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec.replace("b","").replace("'",""))*1000. # convert to pb
                #print '\t\tCross section in', outfile, '=', xsec, 'pb'
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    # write out the total cross sections for the signals
    InitialCrossSectionFile =  Location[CurrentAnalysis] + CurrentAnalysis + '_InitialSignalXsecs.smear.txt' # THESE ARE IN PB
    InitialCrossSectionStream = open(InitialCrossSectionFile, 'w')
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            InitialCrossSectionStream.write(str(Process) + "\t" + str(InitialCrossSection[Process]) + "\n")
    InitialCrossSectionStream.close()
    Efficiencies = {}
    #print '\tGetting the efficiencies of the analysis for each process'
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.smear.dat'
        if os.path.exists(analysisOutputfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(analysisOutputfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        #print '\t\tAnalysis efficiency for:', Process, '=', efficiency
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    #print '\tResulting cross sections after analysis * K-factors:'
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process] * 1000. # CONVERT TO FB
        #print '\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'fb'
    # sum the cross sections for all the signals and all the backgrounds
    sum_signals = {}
    sum_backgrounds = 0.

    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            sum_signals[Processes[CurrentAnalysis][Process]] = [0.]*NBDT # reset the signal sum for the given mass
            
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            for Ni in range(NBDT):
                sum_signals[Processes[CurrentAnalysis][Process]][Ni] = sum_signals[Processes[CurrentAnalysis][Process]][Ni] + OutputCrossSection[Process] * SolutionFactor[Processes[CurrentAnalysis][Process]][Ni]
        elif Processes[CurrentAnalysis][Process] == 0: # backgrounds
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
    
    # calculate the limit:
    sum_signals_limit = {}
    sum_backgrounds_limit = {}
    sum_backgrounds_limit_mean = {} 
    sum_signals_limit_mean = {}
    sum_signals_limit_error = {}
    sum_signals_limit_min = {}

    for Process in list(Processes[CurrentAnalysis].keys()):
        sum_signals_limit[Processes[CurrentAnalysis][Process]] = [0.]*NBDT
        sum_backgrounds_limit[Processes[CurrentAnalysis][Process]] = [0.]*NBDT

    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            for Ni in range(NBDT):
                sum_signals_limit[Processes[CurrentAnalysis][Process]][Ni] = sum_signals[Processes[CurrentAnalysis][Process]][Ni] * SolutionEfficiencyS[Processes[CurrentAnalysis][Process]][Ni]
                sum_backgrounds_limit[Processes[CurrentAnalysis][Process]][Ni] = sum_backgrounds * SolutionEfficiencyB[Processes[CurrentAnalysis][Process]][Ni]
            sum_backgrounds_limit_mean[Processes[CurrentAnalysis][Process]] = np.mean(sum_backgrounds_limit[Processes[CurrentAnalysis][Process]])
            sum_signals_limit_mean[Processes[CurrentAnalysis][Process]] = np.mean(sum_signals_limit[Processes[CurrentAnalysis][Process]])
            sum_signals_limit_error[Processes[CurrentAnalysis][Process]] = np.std(sum_signals_limit[Processes[CurrentAnalysis][Process]])
            sum_signals_limit_min[Processes[CurrentAnalysis][Process]] = np.max(sum_signals_limit[Processes[CurrentAnalysis][Process]]) # CHANGED TO MAX 22.03.2021

    print('Mean Signal cross sections corresponding to a significance target:', SignifTarget, ' (in fb):', sum_signals_limit_mean, 'average over NBDT=', NBDT)
    # write out the results in a file for plotting:
    SignifFile = Location[CurrentAnalysis] + CurrentAnalysis + '_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '_NBDT' + str(NBDT) + '.smear.txt'
    SignifOutputStream = open(SignifFile, 'w')
    print('writing out cross section file:', SignifFile)
    for Process in list(sum_signals_limit_mean.keys()):            
        SignifOutputStream.write(str(Process) + "\t" + str(sum_signals_limit_mean[Process]) + "\t" + str(sum_backgrounds_limit_mean[Process]) + "\t" + str(sum_signals_limit_error[Process]) + "\t" + str(sum_signals_limit_min[Process]) + "\n")
    SignifOutputStream.close()
    # write out a file with the signal and background efficiencies at each signal point:
    #EfficiencyFile = Location[CurrentAnalysis] + CurrentAnalysis + '_EFFN_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '.txt'
    #print 'writing out efficiency file:', EfficiencyFile
    #EfficiencyStream = open(EfficiencyFile, 'w')
    #for Process in Processes[CurrentAnalysis].keys():
    #    if Processes[CurrentAnalysis][Process] != 0: # not a background 
    #        EfficiencyStream.write(str(Processes[CurrentAnalysis][Process]) + "\t" + str(SolutionEfficiencyS[Processes[CurrentAnalysis][Process]]) + "\t" + str(SolutionEfficiencyB[Processes[CurrentAnalysis][Process]]) + "\n")
    #EfficiencyStream.close()


# Generate the BDT files necessary for the m4l mass plots
if runbdtm4l is True:
    print('generating the BDT results for the m4l mass plots')
    if CurrentAnalysis != 'HZZ100_4l':
        print('\tThis only works for HZZ100_4l! Exiting...')
        exit()
    print('reading in the BDT results (NBDT=1)')
    BDTResultsFile = Location[CurrentAnalysis] + CurrentAnalysis + '_results.smear.temp.txt'
    BDTResultsStream = open(BDTResultsFile, 'r')
    Ni = {}
    RSEED_i = {}
    f_solution = {}
    sig = {}
    effS = {}
    effB = {}
    NS = {}
    NB = {}
    for line in BDTResultsStream:
        M = float(line.split()[0])
        Ni[M] = line.split()[1]
        RSEED_i[M] = line.split()[2]
        f_solution[M] = line.split()[3]
        sig[M] = line.split()[4]
        effS[M] = line.split()[5]
        effB[M] = line.split()[6]
        NS[M] = line.split()[7]
        NB[M] = line.split()[8]
    for key in list(Ni.keys()):
        print("M, Ni, RSEED_i, f_solution, sig, effS, effB, NS(L), NB(L)=", key, Ni[key], RSEED_i[key], f_solution[key], sig[key], effS[key], effB[key], NS[key], NB[key])
    LumiFB = Luminosity[CurrentAnalysis]
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        #print Process, Processes[CurrentAnalysis][Process]
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    # run the first step ("output")
    print('running the output step')
    effS_BDT = {}
    BDT_score = {}
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process)
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            if b"BDT score" in line:
                effS_BDT[Process] = line.split()[2].decode().replace("b","").replace("'","") # Note that the the significance is given by the 4th word in the line
                BDT_score[Process] = line.split()[5].decode().replace("b","").replace("'","")
                print(' signal efficiency = ', effS_BDT[Process], 'for BDT score=', BDT_score[Process])
        out, err = p.communicate()
    print('running the application step')
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_application + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process) + ' ' + str(BDT_score[Process])
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        out, err = p.communicate()
        print(out)

# Generate the BDT files necessary for the m4l mass plots
if runbdtmbbaa is True:
    print('generating the BDT results for the mbbaa mass plots')
    if CurrentAnalysis != 'HH100_BBGG':
        print('\tThis only works for HH100_BBGG Exiting...')
        exit()
    print('reading in the BDT results (NBDT=1)')
    BDTResultsFile = Location[CurrentAnalysis] + CurrentAnalysis + '_results.smear.temp.txt'
    BDTResultsStream = open(BDTResultsFile, 'r')
    Ni = {}
    RSEED_i = {}
    f_solution = {}
    sig = {}
    effS = {}
    effB = {}
    NS = {}
    NB = {}
    for line in BDTResultsStream:
        M = float(line.split()[0])
        Ni[M] = line.split()[1]
        RSEED_i[M] = line.split()[2]
        f_solution[M] = line.split()[3]
        sig[M] = line.split()[4]
        effS[M] = line.split()[5]
        effB[M] = line.split()[6]
        NS[M] = line.split()[7]
        NB[M] = line.split()[8]
    for key in list(Ni.keys()):
        print("M, Ni, RSEED_i, f_solution, sig, effS, effB, NS(L), NB(L)=", key, Ni[key], RSEED_i[key], f_solution[key], sig[key], effS[key], effB[key], NS[key], NB[key])
    LumiFB = Luminosity[CurrentAnalysis]
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        #print Process, Processes[CurrentAnalysis][Process]
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    # run the first step ("output")
    print('running the output step')
    effS_BDT = {}
    BDT_score = {}
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_HH + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process)
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            if b"BDT score" in line:
                effS_BDT[Process] = line.split()[2].decode().replace("b","").replace("'","") # Note that the the significance is given by the 4th word in the line
                BDT_score[Process] = line.split()[5].decode().replace("b","").replace("'","")
                print(' signal efficiency = ', effS_BDT[Process], 'for BDT score=', BDT_score[Process])
        out, err = p.communicate()
    print('running the application step')
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_application_HH + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process) + ' ' + str(BDT_score[Process])
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        out, err = p.communicate()
        print(out)

# Generate the BDT files necessary for the WW mT plots
if runbdtmT is True:
    print('generating the BDT results for the mT ww mass plots')
    if CurrentAnalysis != 'HWW100':
        print('\tThis only works for HW100 Exiting...')
        exit()
    print('reading in the BDT results (NBDT=1)')
    BDTResultsFile = Location[CurrentAnalysis] + CurrentAnalysis + '_results.smear.temp.txt'
    BDTResultsStream = open(BDTResultsFile, 'r')
    Ni = {}
    RSEED_i = {}
    f_solution = {}
    sig = {}
    effS = {}
    effB = {}
    NS = {}
    NB = {}
    for line in BDTResultsStream:
        M = float(line.split()[0])
        Ni[M] = line.split()[1]
        RSEED_i[M] = line.split()[2]
        f_solution[M] = line.split()[3]
        sig[M] = line.split()[4]
        effS[M] = line.split()[5]
        effB[M] = line.split()[6]
        NS[M] = line.split()[7]
        NB[M] = line.split()[8]
    for key in list(Ni.keys()):
        print("M, Ni, RSEED_i, f_solution, sig, effS, effB, NS(L), NB(L)=", key, Ni[key], RSEED_i[key], f_solution[key], sig[key], effS[key], effB[key], NS[key], NB[key])
    LumiFB = Luminosity[CurrentAnalysis]
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        #print Process, Processes[CurrentAnalysis][Process]
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    # run the first step ("output")
    print('running the output step')
    effS_BDT = {}
    BDT_score = {}
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_WW + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process)
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            #print(line)
            if b"BDT score" in line:
                effS_BDT[Process] = line.split()[2].decode().replace("b","").replace("'","") # Note that the the significance is given by the 4th word in the line
                BDT_score[Process] = line.split()[5].decode().replace("b","").replace("'","")
                print(' signal efficiency = ', effS_BDT[Process], 'for BDT score=', BDT_score[Process])
        out, err = p.communicate()
    print('running the application step')
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_application_WW + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process) + ' ' + str(BDT_score[Process])
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        out, err = p.communicate()

# Generate the BDT files necessary for the WW mT plots
if runbdtmT_ZZ2l2v is True:
    print('generating the BDT results for the mT mass plots')
    if CurrentAnalysis != 'HZZ100_2l2v':
        print('\tThis only works for HZZ100_2l2v Exiting...')
        exit()
    print('reading in the BDT results (NBDT=1)')
    BDTResultsFile = Location[CurrentAnalysis] + CurrentAnalysis + '_results.smear.temp.txt'
    BDTResultsStream = open(BDTResultsFile, 'r')
    Ni = {}
    RSEED_i = {}
    f_solution = {}
    sig = {}
    effS = {}
    effB = {}
    NS = {}
    NB = {}
    for line in BDTResultsStream:
        M = float(line.split()[0])
        Ni[M] = line.split()[1]
        RSEED_i[M] = line.split()[2]
        f_solution[M] = line.split()[3]
        sig[M] = line.split()[4]
        effS[M] = line.split()[5]
        effB[M] = line.split()[6]
        NS[M] = line.split()[7]
        NB[M] = line.split()[8]
    for key in list(Ni.keys()):
        print("M, Ni, RSEED_i, f_solution, sig, effS, effB, NS(L), NB(L)=", key, Ni[key], RSEED_i[key], f_solution[key], sig[key], effS[key], effB[key], NS[key], NB[key])
    LumiFB = Luminosity[CurrentAnalysis]
    # the BDT files:
    BDTOutputfileSignals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        #print Process, Processes[CurrentAnalysis][Process]
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            BDTOutputfileSignals[Processes[CurrentAnalysis][Process]] = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_' + str(Processes[CurrentAnalysis][Process]) + '_signals.smear.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.smear.txt'
    # run the first step ("output")
    print('running the output step')
    effS_BDT = {}
    BDT_score = {}
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_ZZ2l2v + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process)
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        for line in iter(p.stdout.readline, b''):
            #print(line)
            if b"BDT score" in line:
                effS_BDT[Process] = line.split()[2].decode().replace("b","").replace("'","") # Note that the the significance is given by the 4th word in the line
                BDT_score[Process] = line.split()[5].decode().replace("b","").replace("'","")
                print(' signal efficiency = ', effS_BDT[Process], 'for BDT score=', BDT_score[Process])
        out, err = p.communicate()
    print('running the application step')
    for Process in list(Ni.keys()):
        rootcommand = TMVALocation + TMVABinary_output_application_ZZ2l2v + ' ' + str(LumiFB) + ' ' + '"' + BDTOutputfileSignals[Process] + '" "' + BDTOutputfileBackgrounds + '" ' + str(f_solution[Process]) + ' ' + str(NVariables[CurrentAnalysis]) + ' ' + str(RSEED_i[Process]) + ' ' + str(Process) + ' ' + str(BDT_score[Process])
        print(rootcommand)
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        out, err = p.communicate()

# Make the exclusion plots
if plotexclusion is True or plotexclusionsmear is True:
    smear_tag = ''
    smear_text = ''
    if plotexclusionsmear is True:
        smear_tag = '.smear'
        smear_text = '\nwith smearing'
    
    ######################
    # the mass exclusion #
    ######################
    NBDT = NBDTAnalysis[CurrentAnalysis]
    SignifFile = Location[CurrentAnalysis] + CurrentAnalysis + '_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + '_NBDT' + str(NBDT) + smear_tag + '.txt'
    print('Generating exclusion plot from', SignifFile)
    SignifInputStream = open(SignifFile, 'r')
    xsec_limit = {}
    # get sigma_S limit, S_limit and B_limit for each mass
    for line in SignifInputStream:
        print(line)
        xsec_limit[float(line.split()[0])] = [  float(line.split()[1]), float(line.split()[2]), float(line.split()[3]), float(line.split()[4])  ]
        #print xsec_limit[float(line.split()[0])], float(line.split()[1])
    # make the plot x, y, dy
    x = []
    y = []
    dy = []
    y1p = []
    y1m = []
    y2p = []
    y2m = []
    
    print('key, S, B, signif, sigmaB')

    sigmaB_dictionary = {}

    for key in sorted(xsec_limit):
        
        sigma = xsec_limit[key][0] # the mean cross section limit

        #sigma = xsec_limit[key][3] # the maximum cross section limit found for the BDT scan
        
        
        S = sigma *Luminosity[CurrentAnalysis]

        a = 1
        b = 1/aB**2
        c = - ( sigma * Luminosity[CurrentAnalysis] / aB / SignifTarget )**2
        d = (b**2) - (4*a*c)
        # find two solutions
        sol1 = (-b-np.sqrt(d))/(2*a)
        sol2 = (-b+np.sqrt(d))/(2*a)
        B = sol2
        sigmaB = B/Luminosity[CurrentAnalysis]
        print(key, S, B, S/math.sqrt(B+(aB*B)**2), sigmaB)

        sigma_dis = sigma * 5./2.
        print('discovery for sigma(S)=', sigma_dis)
        sigmaB_dictionary[key] = sigmaB
        
        #S = abs(xsec_limit[key][1])
        #B = xsec_limit[key][2]
        x.append(key)        
        y.append(sigma)

        # calculate the error:
        
        #deltaB = math.sqrt(B + (aB*B)**2)

        #y1p_i = (SignifTarget / Luminosity[CurrentAnalysis]) * math.sqrt(B + (aB*B)**2 + deltaB)
        #y1m_i = (SignifTarget / Luminosity[CurrentAnalysis]) * math.sqrt(B + (aB*B)**2 - deltaB)

        #y2p_i = (SignifTarget / Luminosity[CurrentAnalysis]) * math.sqrt(B + (aB*B)**2 + 2*deltaB)
        #y2m_i = (SignifTarget / Luminosity[CurrentAnalysis]) * math.sqrt(B + (aB*B)**2 - 2*deltaB)

        #print key, S, B, S/B, S/math.sqrt(B+(aB*B)**2), sigma
    
        dsigma = SignifTarget / Luminosity[CurrentAnalysis] * (aB**2 * B + 0.5)

        #error = math.sqrt((1/math.sqrt(S+B) - 0.5 * S/(S+B)**(1.5))**2 * S + (0.5 * sigma * math.sqrt(B)/ (S+B))**2)
        y1p.append( sigma+dsigma )
        y1m.append( sigma-dsigma )
        y2p.append( sigma+2*dsigma )
        y2m.append( sigma-2*dsigma )
        #y1p.append( xsec_limit[key][0] + error )
        #y1m.append( xsec_limit[key][0] - error )
        #y2p.append( xsec_limit[key][0] + 2 * error )
        #y2m.append( xsec_limit[key][0] - 2 * error )
        #print key, sigma, dsigma, S, B, (sigma+dsigma)*Luminosity[CurrentAnalysis], (sigma-dsigma)*Luminosity[CurrentAnalysis]


    interpkind = 'linear'
    mass_array = np.arange(min(x), 1100, 100) 
    y_interp = []
    y1p_interp = []
    y1m_interp = []
    y2p_interp = []
    y2m_interp = []

    interpolator = scipy.interpolate.interp1d(x, y, kind=interpkind, bounds_error=False, fill_value="extrapolate")
    y_interp = interpolator(mass_array)
    interpolator = interp1d(x, y1p, kind=interpkind, bounds_error=False, fill_value="extrapolate")
    y1p_interp = interpolator(mass_array)
    interpolator = interp1d(x, y1m, kind=interpkind, bounds_error=False, fill_value="extrapolate")
    y1m_interp = interpolator(mass_array)
    interpolator = interp1d(x, y2m, kind=interpkind, bounds_error=False, fill_value="extrapolate")
    y2m_interp = interpolator(mass_array)
    interpolator = interp1d(x, y2p, kind=interpkind, bounds_error=False, fill_value="extrapolate")
    y2p_interp = interpolator(mass_array)
    
    # plot the interpolations instead:
    interp = False
    if interp == True:
        x_plot = mass_array
        y_plot = y_interp
        y1p_plot = y1p_interp
        y1m_plot = y1m_interp
        y2p_plot = y2p_interp
        y2m_plot = y2m_interp
    else:
        x_plot = x
        y_plot = y
        y1p_plot = y1p
        y1m_plot = y1m
        y2p_plot = y2p
        y2m_plot = y2m
    
    ########################################
    # mass exclusion plotting starts here: #
    ########################################
        
    # set the plot type
    plot_type = 'massexclusion_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$\\sigma(h_2) \\times \\mathrm{BR}($' + LatexName[CurrentAnalysis] + '$)$ [fb]'
    ymin = 0.005
    ymax = 0.05
    xmin = 250.
    xmax = 1250.
    ylog = True
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    pl.plot(x_plot, y_plot, color="blue", label='Expected', ms=0, ls='--', lw=1)
    ax.fill_between(x_plot, y2m_plot, y2p_plot, alpha=.8, linewidth=0, color='yellow')
    ax.fill_between(x_plot, y1m_plot, y1p_plot, alpha=1.0, linewidth=0, color='limegreen')
    p1 = ax.plot(np.NaN, np.NaN, color='limegreen', alpha=1, label='$\\pm 1\\sigma$', lw=10)
    p2 = ax.plot(np.NaN, np.NaN, color='yellow', alpha=0.8, label='$\\pm 2\\sigma$', lw=10)
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center')
    insettext = "$e, \\mu$,\n$\\Gamma(h_2) = 1$ GeV,\n5% syst." + smear_text
    pl.text(0.7, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=12)
    

    # set the ticks, labels and limits 
    ax.yaxis.set_major_locator(MultipleLocator(0.01))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(200.))
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':12})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)

    
    #############################################################################
    # mass exclusion plotting starts here, including the BR to the final state: #
    #############################################################################

    ###############################################################################
    # ALSO WRITE OUT THE DATA FILE FOR THE EXCLUSION of SIGMA * BR(PARENT PROCESS)#
    ###############################################################################


    
    # set the plot type
    plot_type = 'massexclusion_parent_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$\\sigma(h_2) \\times \\mathrm{BR}($' + LatexNameParent[CurrentAnalysis] + '$)$ [fb]'
    ymin = 0.01
    ymax = 10.0
    xmin = 200.
    xmax = 1000.
    ylog = True
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    # divide by the BR to the final state:
    y_plot = np.array(y_plot) / FacFinalState[CurrentAnalysis]
    y1p_plot = np.array(y1p_plot) / FacFinalState[CurrentAnalysis]
    y1m_plot = np.array(y1m_plot) / FacFinalState[CurrentAnalysis]
    y2p_plot = np.array(y2p_plot) / FacFinalState[CurrentAnalysis]
    y2m_plot = np.array(y2m_plot) / FacFinalState[CurrentAnalysis]

    # write out the data files
    datafile_central = "data_" + CurrentAnalysis + "_L" + str(Luminosity[CurrentAnalysis]) + "_signif" + str(SignifTarget) + "_central" + smear_tag + ".txt"
    datafile_1sigma = "data_" + CurrentAnalysis + "_L" + str(Luminosity[CurrentAnalysis]) + "_signif" + str(SignifTarget) + "_1sigma" + smear_tag + ".txt"
    datafile_2sigma = "data_" + CurrentAnalysis + "_L" + str(Luminosity[CurrentAnalysis]) + "_signif" + str(SignifTarget) + "_2sigma" + smear_tag + ".txt"
    print('writing out data files', datafile_central, datafile_1sigma, datafile_2sigma)
    dc_stream = open(digit_plot_location + datafile_central,'w')
    d1_stream = open(digit_plot_location + datafile_1sigma,'w')
    d2_stream = open(digit_plot_location + datafile_2sigma,'w')
    for xx in range(len(x_plot)):
        dc_stream.write(str(x_plot[xx]) + "\t" + str(y_plot[xx]/1000.) + "\n") # CONVERT TO pb!
        d1_stream.write(str(x_plot[xx]) + "\t" + str(y1m_plot[xx]/1000.) + "\n") # CONVERT TO pb!
        d2_stream.write(str(x_plot[xx]) + "\t" + str(y2m_plot[xx]/1000.) + "\n") # CONVERT TO pb!
    dc_stream.close()
    d1_stream.close()
    d2_stream.close()

    pl.plot(x_plot, y_plot, color="blue", label='Expected', ms=0, ls='--', lw=1)
    ax.fill_between(x_plot, y2m_plot, y2p_plot, alpha=.8, linewidth=0, color='yellow')
    ax.fill_between(x_plot, y1m_plot, y1p_plot, alpha=1.0, linewidth=0, color='limegreen')
    p1 = ax.plot(np.NaN, np.NaN, color='limegreen', alpha=1, label='$\\pm 1\\sigma$', lw=10)
    p2 = ax.plot(np.NaN, np.NaN, color='yellow', alpha=0.8, label='$\\pm 2\\sigma$', lw=10)
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center')
    insettext = "$e, \\mu$,\n$\\Gamma(h_2) = 1$ GeV,\n5% syst." + smear_text
    pl.text(0.2, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=12)
    
    # set the ticks, labels and limits 
    ax.yaxis.set_major_locator(MultipleLocator(0.01))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(200.))
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':12})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)

    ###############################################################################
    # WRITE OUT THE EXCLUSION AT DIFFERENT LUMINOSITIES 
    ###############################################################################

    #array of luminosities to write out (in inverse femtobarn):
    Lumis = [100., 200., 500., 1000., 2000., 5000., 10000., 15000., 20000., 25000., 30000.]
    facfs_current = FacFinalState[CurrentAnalysis]

    # define a function that gives the NSIGMA-sigma exclusion at a given lumi, given sigma_B and given parent process factor
    # THE ANSWER IS IN PB!
    def xsec_signif_at_Lumi(NSIGMA, sB, Lp, alpha, facfs):
        return (NSIGMA / Lp) * math.sqrt( Lp *  sB + alpha**2 * Lp**2 * sB**2 )/1000./facfs

    # loop over the lumi array and write out each 
    NSIGMA = 2.
    for Lumi in Lumis:
        lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_central" + smear_tag + ".txt"
        lumistream = open(digit_plot_location + lumifile,'w')
        for key in sorted(sigmaB_dictionary.keys()):
            lumistream.write(str(key) + '\t' + str(xsec_signif_at_Lumi(NSIGMA, sigmaB_dictionary[key], Lumi, aB, facfs_current)) + '\n')
        lumistream.close()

    ###############################################################################
    # WRITE OUT THE EXCLUSION AT 27 TeV
    ###############################################################################

    # define a function that gives the NSIGMA-sigma exclusion at a given lumi, given sigma_B and given parent process factor
    # FOR 27 TeV
    # THE ANSWER IS IN PB!
    def xsec_signif_at_Lumi_27(NSIGMA, sB, Lp, alpha, facfs, ratio_27to100):
        return (NSIGMA / Lp) * math.sqrt( Lp *  sB*ratio_27to100 + alpha**2 * Lp**2 * (sB*ratio_27to100)**2 )/1000./facfs

    # first assume backgrounds are purely gluon-fusion:
    NSIGMA = 2.
    # the gluon-fusion reduction factor 100 -> 27 TeV:
    for Lumi in Lumis:
        lumifile =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVGGF_central" + smear_tag + ".txt"
        lumistream = open(digit_plot_location + lumifile,'w')
        lumifile_qq =  "data_" + CurrentAnalysis + "_L" + str(Lumi) + "_signif" + str(NSIGMA) + "_lumivar_27TeVQQ_central" + smear_tag + ".txt"
        lumistream_qq = open(digit_plot_location + lumifile_qq,'w')
        for key in sorted(sigmaB_dictionary.keys()):
            red_factor = RATIO_interpolator_SM_27TeV_N3LON3LL(float(key))
            #print key, red_factor
            lumistream.write(str(key) + '\t' + str(xsec_signif_at_Lumi_27(NSIGMA, sigmaB_dictionary[key], Lumi, aB, facfs_current, red_factor)) + '\n')
            red_factor_qq = RATIO_QQ_interpolator_SM_27TeV_N3LON3LL(float(key))
            print('27 TeV', key, red_factor, red_factor_qq)
            lumistream_qq.write(str(key) + '\t' + str(xsec_signif_at_Lumi_27(NSIGMA, sigmaB_dictionary[key], Lumi, aB, facfs_current, red_factor_qq)) + '\n')


    #############################################################################
    # mass DISCOVERY plotting starts here, including the BR to the final state: #
    #############################################################################

    ###############################################################################
    # ALSO WRITE OUT THE DATA FILE FOR THE DISCOVERY of SIGMA * BR(PARENT PROCESS)#
    ###############################################################################
    
    # set the plot type
    plot_type = 'massdiscovery_parent_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$\\sigma(h_2) \\times \\mathrm{BR}($' + LatexNameParent[CurrentAnalysis] + '$)$ [fb]'
    ymin = 0.01
    ymax = 5.0
    xmin = 200.
    xmax = 1000.
    ylog = True
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    # divide by the BR to the final state (already done before)

    # write out the data files
    datafile_central = "data_dis_" + CurrentAnalysis + "_L" + str(Luminosity[CurrentAnalysis]) + "_signif" + str(5) + "_central" + smear_tag + ".txt"
    print('writing out data files (DISCOVERY)', datafile_central)
    dc_stream = open(digit_plot_location + datafile_central,'w')
    for xx in range(len(x_plot)):
        dc_stream.write(str(x_plot[xx]) + "\t" + str(y_plot[xx]*5./2./1000.) + "\n") # CONVERT TO pb! # MULTIPLY BY 5./2. FOR DISCOVERY
    dc_stream.close()

    pl.plot(x_plot, y_plot*5./2., color="blue", label='Expected discovery', ms=0, ls='--', lw=1)
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center')
    insettext = "$e, \\mu$,\n$\\Gamma(h_2) = 1$ GeV,\n5% syst." + smear_text
    pl.text(0.7, 0.7, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=12)
    
    # set the ticks, labels and limits 
    ax.yaxis.set_major_locator(MultipleLocator(0.01))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(200.))
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':12})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)

    ###############################################################################
    # ALSO WRITE OUT THE DATA FILE FOR THE DISCOVERY/EXCLUSION of SIGMA * BR(PARENT PROCESS)#
    ###############################################################################
    
    # set the plot type
    plot_type = 'massdiscexcl_parent_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$\\sigma(h_2) \\times \\mathrm{BR}($' + LatexNameParent[CurrentAnalysis] + '$)$ [fb]'
    ymin = 0.001
    ymax = 1000.0
    xmin = 200.
    xmax = 1000.
    ylog = True
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    # divide by the BR to the final state (already done before)
    pl.plot(x_plot, y_plot*5./2., color="red", label='Expected discovery', ms=0, ls='--', lw=1)
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center')
    insettext = "$e, \\mu$,\n$\\Gamma(h_2) = 1$ GeV,\n5% syst." + smear_text
    pl.text(0.2, 0.15, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=12)

    pl.plot(x_plot, y_plot, color="blue", label='Expected exclusion', ms=0, ls='--', lw=1)
    ax.fill_between(x_plot, y2m_plot, y2p_plot, alpha=.8, linewidth=0, color='yellow')
    ax.fill_between(x_plot, y1m_plot, y1p_plot, alpha=1.0, linewidth=0, color='limegreen')
    p1 = ax.plot(np.NaN, np.NaN, color='limegreen', alpha=1, label='$\\pm 1\\sigma$', lw=10)
    p2 = ax.plot(np.NaN, np.NaN, color='yellow', alpha=0.8, label='$\\pm 2\\sigma$', lw=10)

    print('yplot=', y_plot)
    print('yplot*5/2=', y_plot*5./2.)
    
    # set the ticks, labels and limits 
    ax.yaxis.set_major_locator(MultipleLocator(0.01))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(200.))
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':12})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)


    ###########################################################################################
    # the costheta exclusion                                                                 #
    ###########################################################################################

    # read in the total cross sections and calculate the total cross section per signal point #
    TotalCrossSectionOutputFile = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_totalxsec' + smear_tag + '.txt'
    TotalCrossSectionInputStream = open(TotalCrossSectionOutputFile,'r')
    TotalCrossSection = {}
    for line in TotalCrossSectionInputStream:
        TotalCrossSection[line.split()[0]] = float(line.split()[1])
    TotalCrossSectionInputStream.close()
    Total_sum_signals = {}
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            Total_sum_signals[Processes[CurrentAnalysis][Process]] = 0. # reset the signal sum for the given mass
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            print(Process, TotalCrossSection[Process])
            Total_sum_signals[Processes[CurrentAnalysis][Process]] = Total_sum_signals[Processes[CurrentAnalysis][Process]] + TotalCrossSection[Process]
    print("Total undecayed cross sections for signals:", Total_sum_signals, len(Total_sum_signals))
    # put the total undecayed cross sections for the signals in a sorted list and multiply by the appropriate K-factor
    xsec_m2_dict = {}
    for Process in sorted(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] != 0: # not a background
            if Processes[CurrentAnalysis][Process] not in list(xsec_m2_dict.keys()):
                xsec_m2_dict[Processes[CurrentAnalysis][Process]] = (TotalKFacsProcesses[CurrentAnalysis][Process]*Total_sum_signals[Processes[CurrentAnalysis][Process]]) 
            else: 
                xsec_m2_dict[Processes[CurrentAnalysis][Process]] = xsec_m2_dict[Processes[CurrentAnalysis][Process]] + (TotalKFacsProcesses[CurrentAnalysis][Process]*Total_sum_signals[Processes[CurrentAnalysis][Process]])
                
    xsec_m2 = []    
    for key in sorted(xsec_m2_dict.keys()):
        xsec_m2.append(xsec_m2_dict[key])
    # get the SM BRs for the masses
    # x contains the masses used for the mass exclusion plot: re-use here
    # BR_m2 is the "SM" Branching ratio corresponding to m2
    BR_m2 = interpolate_HiggsBR_plot(HiggsBRs, x)
    #print 'BR(h2) for M=', x, ':', BR_interpolation_array_for_sintheta
    print('BR for', CurrentAnalysis,'for M=', x, ':', BR_m2[BRElement[CurrentAnalysis]])
    # the SM-like Higgs width for mass of m2 is the 11th element:
    GammaSM_m2 = np.array(BR_m2[11])
    print('Gamma_SM(m2)=', GammaSM_m2)
    # create the quadratic for the (sintheta)^2 for each mass point:
    # use y as the limit on the cross section:  ***WARNING***: do not forget to divide by the BR to the actual final state!
    R_lim = np.array(y) / np.array(BR_m2[BRElement[CurrentAnalysis]]) / np.array(xsec_m2) / FacFinalState[CurrentAnalysis]
    print('R_lim=', R_lim)
    # calculate kappa a la Spannowsky/No:
    kappa_lim = np.array(y)/np.array(xsec_m2)
    
    # define the function funcF which will be used for solving for the sin:
    def FuncF(m2, GammaSM):
        FuncF_result = []
        for m in range(len(m2)):
            FuncF_result.append(v0**2 * cmath.sqrt(1 - 4 * MH**2 / m2[m]**2) / (8 * math.pi * GammaSM[m]))
        return np.array(FuncF_result)
    print('FuncF(np.array(x), GammaSM_m2)=', FuncF(np.array(x), GammaSM_m2))
    # vary the values of the h1-h1-h2 coupling and get the exclusion vs m2
    lambda112 = [ 0, 0.05*v0, 0.1*v0, 0.15*v0, 0.2*v0, 0.25*v0, 0.3*v0, 0.35*v0, 0.4*v0 ]
    mh1 = 125.
    costheta_sol = [] # an array of the costheta solutions
    for l112 in lambda112:
        a = 1.
        b = - R_lim
        c = - (l112**2/v0**2) * FuncF(np.array(x), GammaSM_m2) * R_lim
        d = (b**2) - (4*a*c)
        # find two solutions
        sol1 = (-b-np.sqrt(d))/(2*a)
        sol2 = (-b+np.sqrt(d))/(2*a)
        costheta_sol.append(np.sqrt(1-sol2.real)) # sol2 seems to be the "good" solution that we want
        # calculate the BR(hh) as well corresponding to l112 and the solution 
        sintheta = np.sqrt(1 - np.square(np.sqrt(1-sol2.real)))
        #BR_interpolation_array_for_plot_heavy = calc_HeavyHiggsBRs(BR_interpolators_SM, mh1, x, sintheta, l112)
        #print BR_interpolation_array_for_plot_heavy
        print(l112/v0, np.sqrt(1-sol2).real)

    # now get the data for fixed BR(h2->h1h1)
    costheta_sol_br = []
    BRh1h1 = [0.0, 0.2, 0.4, 0.6, 0.8]
    for br in BRh1h1:
        sol = 1./(1.-br) * R_lim # sintheta**2
        costheta_sol_br.append(np.sqrt(1.-sol))
        
        
    ############################################
    # costheta exclusion plotting starts here: #
    ############################################
    
    # set the plot type
    plot_type = 'costheta_exclusion_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag 

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$\\cos \\theta$'
    ymin = 0.85
    ymax = 1.0
    xmin = 250.
    xmax = 950.
    ylog = False
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    # interpolate for smoothness:
    interpkind = 'cubic'
    mass_array = np.arange(80, 1000, 1) 
    interpolated_costheta_sol = []
    for l in range(len(lambda112)):
        interpolator = interp1d(x, costheta_sol[l], kind=interpkind, bounds_error=False)
        interpolated_costheta_sol.append(interpolator(mass_array))
        

    #mymap = mpl.colors.LinearSegmentedColormap.from_list('mycolors',['red'])
    #mymap = mpl.cm.get_cmap('red', 6)
    c = np.arange(0, len(lambda112)+1)
    mymap = mpl.cm.get_cmap('viridis_r', len(lambda112))
    dummie_cax = ax.scatter(c, c, c=c, cmap=mymap)
    ax.cla()
    # colour bar:
    dummie_cax._A = []
    cbar = pl.colorbar(dummie_cax, ticks=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
    cbar.ax.set_yticklabels(['0', '', '0.1', '', '0.2', '', '0.3', '', '0.4'])
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel('$\\lambda_{112}/v_0$', rotation=270, fontsize=15)    
    ax.fill_between(mass_array, 0, interpolated_costheta_sol[-1], alpha=1, linewidth=0, color=mymap(len(lambda112)-1))
    for l in range(len(lambda112)-1):
        pl.plot(mass_array, interpolated_costheta_sol[l], label='', ms=0, ls='-', c=mymap(l))
        ax.fill_between(mass_array, interpolated_costheta_sol[l], interpolated_costheta_sol[l+1], linewidth=0, color=mymap(l))

        
    # title
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center', fontsize=12)
    
    # set the ticks, labels and limits 
    ax.yaxis.set_major_locator(MultipleLocator(0.1))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.02))
    ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(100.))
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':15})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)


    ###################################################################
    # costheta exclusion for fixed BR(h2->h1h1) plotting starts here: #
    ###################################################################
    
    # set the plot type
    plot_type = 'costheta_exclusion_fixedBRhh_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag 

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$\\cos \\theta$'
    ymin = 0.95
    ymax = 1.0
    xmin = 250.
    xmax = 950.
    ylog = False
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    # interpolate for smoothness:
    interpkind = 'cubic'
    mass_array = np.arange(80, 1000, 1) 
    interpolated_costheta_sol_br = []
    for l in range(len(BRh1h1)):
        interpolator = interp1d(x, costheta_sol_br[l], kind=interpkind, bounds_error=False)
        interpolated_costheta_sol_br.append(interpolator(mass_array))
        

    #mymap = mpl.colors.LinearSegmentedColormap.from_list('mycolors',['red'])
    #mymap = mpl.cm.get_cmap('red', 6)
    c = np.arange(0, len(BRh1h1)+1)
    mymap = mpl.cm.get_cmap('viridis_r', len(BRh1h1))
    dummie_cax = ax.scatter(c, c, c=c, cmap=mymap)
    ax.cla()
    # colour bar:
    dummie_cax._A = []
    cbar = pl.colorbar(dummie_cax, ticks=[0, 1, 2, 3, 4])
    cbar.ax.set_yticklabels(['0.0', '0.2', '0.4', '0.6', '0.8'])
    cbar.ax.get_yaxis().labelpad = 15
    cbar.ax.set_ylabel('BR$(h_2\\rightarrow h_1 h_1)$', rotation=270, fontsize=15)    
    ax.fill_between(mass_array, 0, interpolated_costheta_sol_br[-1], alpha=1, linewidth=0, color=mymap(len(lambda112)-1))
    for l in range(len(BRh1h1)-1):
        pl.plot(mass_array, interpolated_costheta_sol_br[l], label='', ms=0, ls='-', c=mymap(l))
        ax.fill_between(mass_array, interpolated_costheta_sol_br[l], interpolated_costheta_sol_br[l+1], linewidth=0, color=mymap(l))

        
    # title
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center', fontsize=12)
    
    # set the ticks, labels and limits 
    #ax.yaxis.set_major_locator(MultipleLocator(0.1))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.001))
    #ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(100.))
    #ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.yaxis.set_minor_locator(AutoMinorLocator())


    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    ax.yaxis.set_major_formatter(formatter)

    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':15})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)
    
    ###################################################################
    # R_lim exclusion #
    ###################################################################
    
    # set the plot type
    plot_type = 'R_lim_L' + str(Luminosity[CurrentAnalysis]) + '_signif' + str(SignifTarget) + smear_tag

    # plot settings
    xlab = '$m_2$ [GeV]'
    ylab = '$R_\\mathrm{lim} = \\frac{\\sigma_\\mathrm{lim}(pp\\rightarrow ' +  LatexNameParent[CurrentAnalysis].replace('$','') + ')}{\\sigma_\\mathrm{SM}(pp\\rightarrow h_2) \\times BR_\\mathrm{SM} (' + LatexNameParent[CurrentAnalysis].replace('$','') + ')} $'
    #ymin = 1E-3
    #ymax = 0.1
    xmin = 250.
    xmax = 950.
    ylog = True
    xlog = False
    
    # construct the axes for the plot
    gs = gridspec.GridSpec(4, 4)
    fig = pl.figure()
    ax = fig.add_subplot(111)
    ax.grid(False)

    # interpolate for smoothness:
    interpkind = 'cubic'
    mass_array = np.arange(80, 1000, 1) 
    interpolator = interp1d(x, R_lim, kind=interpkind, bounds_error=False)
    interpolated_R_lim = interpolator(mass_array)
        
    pl.plot(mass_array, interpolated_R_lim, label='', ms=0, ls='-')
        
    # title
    titletext = LatexName[CurrentAnalysis] + ", pp@" + str(Energy[CurrentAnalysis]) + "TeV, $\\mathcal{L}=" + str(Luminosity[CurrentAnalysis]/1000) + "$ ab$^{-1}$"
    pl.title(titletext, loc='center', fontsize=12)
    
    # set the ticks, labels and limits 
    #ax.yaxis.set_major_locator(MultipleLocator(0.1))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.001))
    #ax.yaxis.set_minor_locator(AutoMinorLocator())
    ax.xaxis.set_major_locator(MultipleLocator(100.))
    #ax.xaxis.set_minor_locator(AutoMinorLocator())
    #ax.yaxis.set_minor_locator(AutoMinorLocator())


    formatter = FuncFormatter(lambda y, _: '{:.16g}'.format(y))
    ax.yaxis.set_major_formatter(formatter)

    #ax.xaxis.set_minor_locator(MultipleLocator(10.))
    ax.set_ylabel(ylab, fontsize=15)
    ax.set_xlabel(xlab, fontsize=15)
    #pl.ylim([ymin,ymax])
    pl.xlim([xmin, xmax])
    # choose x and y log scales
    if ylog:
        ax.set_yscale('log')
    else:
        ax.set_yscale('linear')
    # create legend and plot/font size
    ax.legend()
    ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':15})
    #pl.rcParams.update({'font.size': 15})
    #pl.rcParams['figure.figsize'] = 12, 12

    # save the figure
    print('saving the figure')
    # save the figure in PDF format
    infile = plot_type + '.dat'
    outputdirectory = Location[CurrentAnalysis]
    print('---')
    print('output in', outputdirectory + infile.replace('.dat','.pdf'))
    pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight')
    pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
    pl.close(fig)
    
