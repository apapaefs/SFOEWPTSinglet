##########################
# H -> ZZ -> 4l ANALYSIS #
##########################

# list of analysed processes
Analyses.append('HZZ100_4l')

# the latex name of the parent process (without the decay into the final state):
LatexNameParent['HZZ100_4l'] = '$h_2 \\rightarrow ZZ$' 

# the latex name of the analysis
LatexName['HZZ100_4l'] = '$h_2 \\rightarrow ZZ \\rightarrow 4 \\ell$'

# the Branching ratio of the primary decay particles of the heavy particle into the observed final state:
FacFinalState['HZZ100_4l'] = 4*BR_z_ellell**2

# the proton-proton energy for the analysis in TeV
Energy['HZZ100_4l'] = 100

# location of analyses' info
Location['HZZ100_4l'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HZZ100_4l/'

# analyses' executables (with respect to Location directory)
Executable['HZZ100_4l'] = 'HwSimPostAnalysis_4l'

# analyses' executables (with respect to Location directory)
ExecutableCuts['HZZ100_4l'] = 'HwSimPostAnalysis_4l'

# analyses' executables for smearing (with respect to Location directory)
ExecutableSmear['HZZ100_4l'] = 'HwSimPostAnalysis_4l_smear'


# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HZZ100_4l'] = {'gg-eta0-ZZ-4l-M200':200.0,
                          'gg-eta0-ZZ-4l-M250':250.0,
                         'gg-eta0-ZZ-4l-M300':300.0,
                         'gg-eta0-ZZ-4l-M350':350.0,
                         'gg-eta0-ZZ-4l-M400':400.0,
                         'gg-eta0-ZZ-4l-M450':450.0,
                         'gg-eta0-ZZ-4l-M500':500.0,
                         'gg-eta0-ZZ-4l-M550':550.0,
                         'gg-eta0-ZZ-4l-M600':600.0,
                         'gg-eta0-ZZ-4l-M650':650.0,
                         'gg-eta0-ZZ-4l-M700':700.0,
                         'gg-eta0-ZZ-4l-M750':750.0,
                         'gg-eta0-ZZ-4l-M800':800.0,
                         'gg-eta0-ZZ-4l-M850':850.0,
                         'gg-eta0-ZZ-4l-M900':900.0,
                         'gg-eta0-ZZ-4l-M950':950.0,
                         'gg-eta0-ZZ-4l-M1000':1000.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M200':200.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M250':250.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M300':300.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M350':350.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M400':400.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M450':450.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M500':500.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M550':550.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M600':600.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M650':650.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M700':700.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M750':750.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M800':800.0,
                        'pp-eta0jj-ZZ-4l-evmuv-M850':850.0,
                        'pp-eta0jj-ZZ-4l-evmuv-M900':900.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M950':950.0,
                         'pp-eta0jj-ZZ-4l-evmuv-M1000':1000.0,
                         'pp-4l':0.,
                         'gg-zz-4l':0.
                         }
                         
# the order of the processes
OrderProcesses['HZZ100_4l'] = {'gg-eta0-ZZ-4l-M200':0,
                         'gg-eta0-ZZ-4l-M250':0,
                         'gg-eta0-ZZ-4l-M300':0,
                         'gg-eta0-ZZ-4l-M350':0,
                         'gg-eta0-ZZ-4l-M400':0,
                         'gg-eta0-ZZ-4l-M450':0,
                         'gg-eta0-ZZ-4l-M500':0,
                         'gg-eta0-ZZ-4l-M550':0,
                         'gg-eta0-ZZ-4l-M600':0,
                         'gg-eta0-ZZ-4l-M650':0,
                         'gg-eta0-ZZ-4l-M700':0,
                         'gg-eta0-ZZ-4l-M750':0,
                         'gg-eta0-ZZ-4l-M800':0,
                         'gg-eta0-ZZ-4l-M850':0,
                         'gg-eta0-ZZ-4l-M900':0,
                         'gg-eta0-ZZ-4l-M950':0,
                         'gg-eta0-ZZ-4l-M1000':0,
                         'gg-eta0-ZZ-4l-M1100':0,
                         'gg-eta0-ZZ-4l-M1200':0,
                         'gg-eta0-ZZ-4l-M1500':0,
                         'gg-eta0-ZZ-4l-M2000':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M200':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M250':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M300':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M350':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M400':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M450':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M500':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M550':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M600':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M650':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M700':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M750':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M800':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M850':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M900':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M950':0,
                         'pp-eta0jj-ZZ-4l-evmuv-M1000':0,
                         'pp-4l':1,
                         'gg-zz-4l':1
                         }

# K-factors for the processes: MUST INCLUDE SYMMETRY FACTORS (e.g. for the lepton flavours)
KFacsProcesses['HZZ100_4l'] = {'gg-eta0-ZZ-4l-M200':2.153378605,
                                   'gg-eta0-ZZ-4l-M250':2.18073096985,
                                    'gg-eta0-ZZ-4l-M300':2.29775496014,
                                    'gg-eta0-ZZ-4l-M350':2.46589800207,
                                    'gg-eta0-ZZ-4l-M400':1.9707292403,
                                    'gg-eta0-ZZ-4l-M450':1.72468921497,
                                    'gg-eta0-ZZ-4l-M500':1.66261118224,
                                    'gg-eta0-ZZ-4l-M550':1.60230559544,
                                    'gg-eta0-ZZ-4l-M600':1.56017321503,
                                    'gg-eta0-ZZ-4l-M650':1.52629944125,
                                    'gg-eta0-ZZ-4l-M700':1.49797704809,
                                    'gg-eta0-ZZ-4l-M750':1.4753696585,
                                    'gg-eta0-ZZ-4l-M800':1.45478852862,
                                    'gg-eta0-ZZ-4l-M850':1.43632233525,
                                    'gg-eta0-ZZ-4l-M900':1.42118100769,
                                    'gg-eta0-ZZ-4l-M950':1.40459637177,
                                    'gg-eta0-ZZ-4l-M1000':1.39037544923,
                                    'gg-eta0-ZZ-4l-M1100':1.36671510558,
                                    'gg-eta0-ZZ-4l-M1200':1.34539183607,
                                    'gg-eta0-ZZ-4l-M1500':1.28924838934,
                                    'gg-eta0-ZZ-4l-M2000':1.22343037638,
                                    'pp-eta0jj-ZZ-4l-evmuv-M200':1.34144694309,
                                    'pp-eta0jj-ZZ-4l-evmuv-M250':1.31763219163,
                                    'pp-eta0jj-ZZ-4l-evmuv-M300':1.33269322546,
                                    'pp-eta0jj-ZZ-4l-evmuv-M350':1.33413720446,
                                    'pp-eta0jj-ZZ-4l-evmuv-M400':1.35075506325,
                                    'pp-eta0jj-ZZ-4l-evmuv-M450':1.34429175954,
                                    'pp-eta0jj-ZZ-4l-evmuv-M500':1.36933619048,
                                    'pp-eta0jj-ZZ-4l-evmuv-M550':1.36225180698,
                                    'pp-eta0jj-ZZ-4l-evmuv-M600':1.3588164263,
                                    'pp-eta0jj-ZZ-4l-evmuv-M650':1.34668089912,
                                    'pp-eta0jj-ZZ-4l-evmuv-M700':1.33902198374,
                                    'pp-eta0jj-ZZ-4l-evmuv-M750':1.35037833258,
                                    'pp-eta0jj-ZZ-4l-evmuv-M800':1.35564696253,
                                    'pp-eta0jj-ZZ-4l-evmuv-M850':1.34048099174,
                                    'pp-eta0jj-ZZ-4l-evmuv-M900':1.33779585209,
                                    'pp-eta0jj-ZZ-4l-evmuv-M950':1.34768444622,
                                    'pp-eta0jj-ZZ-4l-evmuv-M1000':1.33078921944,
                                    'pp-4l':1.0, 
                                    'gg-zz-4l':2.0*4*BR_z_ellell**2
                         }


# K-factors to apply to processes on the TOTAL cross section -> NO symmetry factors from Decays.
TotalKFacsProcesses['HZZ100_4l'] = { 'gg-eta0-ZZ-4l-M200':2.153378605,
                                    'gg-eta0-ZZ-4l-M250':2.18073096985,
                                    'gg-eta0-ZZ-4l-M300':2.29775496014,
                                    'gg-eta0-ZZ-4l-M350':2.46589800207,
                                    'gg-eta0-ZZ-4l-M400':1.9707292403,
                                    'gg-eta0-ZZ-4l-M450':1.72468921497,
                                    'gg-eta0-ZZ-4l-M500':1.66261118224,
                                    'gg-eta0-ZZ-4l-M550':1.60230559544,
                                    'gg-eta0-ZZ-4l-M600':1.56017321503,
                                    'gg-eta0-ZZ-4l-M650':1.52629944125,
                                    'gg-eta0-ZZ-4l-M700':1.49797704809,
                                    'gg-eta0-ZZ-4l-M750':1.4753696585,
                                    'gg-eta0-ZZ-4l-M800':1.45478852862,
                                    'gg-eta0-ZZ-4l-M850':1.43632233525,
                                    'gg-eta0-ZZ-4l-M900':1.42118100769,
                                    'gg-eta0-ZZ-4l-M950':1.40459637177,
                                    'gg-eta0-ZZ-4l-M1000':1.39037544923,
                                    'gg-eta0-ZZ-4l-M1100':1.36671510558,
                                    'gg-eta0-ZZ-4l-M1200':1.34539183607,
                                    'gg-eta0-ZZ-4l-M1500':1.28924838934,
                                    'gg-eta0-ZZ-4l-M2000':1.22343037638,
                                    'pp-eta0jj-ZZ-4l-evmuv-M200':1.34144694309,
                                    'pp-eta0jj-ZZ-4l-evmuv-M250':1.31763219163,
                                    'pp-eta0jj-ZZ-4l-evmuv-M300':1.33269322546,
                                    'pp-eta0jj-ZZ-4l-evmuv-M350':1.33413720446,
                                    'pp-eta0jj-ZZ-4l-evmuv-M400':1.35075506325,
                                    'pp-eta0jj-ZZ-4l-evmuv-M450':1.34429175954,
                                    'pp-eta0jj-ZZ-4l-evmuv-M500':1.36933619048,
                                    'pp-eta0jj-ZZ-4l-evmuv-M550':1.36225180698,
                                    'pp-eta0jj-ZZ-4l-evmuv-M600':1.3588164263,
                                    'pp-eta0jj-ZZ-4l-evmuv-M650':1.34668089912,
                                    'pp-eta0jj-ZZ-4l-evmuv-M700':1.33902198374,
                                    'pp-eta0jj-ZZ-4l-evmuv-M750':1.35037833258,
                                    'pp-eta0jj-ZZ-4l-evmuv-M800':1.35564696253,
                                    'pp-eta0jj-ZZ-4l-evmuv-M850':1.34048099174,
                                    'pp-eta0jj-ZZ-4l-evmuv-M900':1.33779585209,
                                    'pp-eta0jj-ZZ-4l-evmuv-M950':1.34768444622,
                                    'pp-eta0jj-ZZ-4l-evmuv-M1000':1.33078921944,
                         'pp4-ll':1.0,
                         'gg-zz-4l':2.0
                         }

# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HZZ100_4l'] = {
'gg-eta0-ZZ-4l-M200':['gg_eta0_ZZ_4l/Events/run0_200_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M250':['gg_eta0_ZZ_4l/Events/run0_250_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M300':['gg_eta0_ZZ_4l/Events/run0_300_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M350':['gg_eta0_ZZ_4l/Events/run0_350_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M400':['gg_eta0_ZZ_4l/Events/run0_400_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M450':['gg_eta0_ZZ_4l/Events/run0_450_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M500':['gg_eta0_ZZ_4l/Events/run0_500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M550':['gg_eta0_ZZ_4l/Events/run0_550_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M600':['gg_eta0_ZZ_4l/Events/run0_600_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M650':['gg_eta0_ZZ_4l/Events/run0_650_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M700':['gg_eta0_ZZ_4l/Events/run0_700_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M750':['gg_eta0_ZZ_4l/Events/run0_750_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M800':['gg_eta0_ZZ_4l/Events/run0_800_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M850':['gg_eta0_ZZ_4l/Events/run0_850_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M900':['gg_eta0_ZZ_4l/Events/run0_900_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M950':['gg_eta0_ZZ_4l/Events/run0_950_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M1000':['gg_eta0_ZZ_4l/Events/run0_1000_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M1100':['gg_eta0_ZZ_4l/Events/run0_1100_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M1200':['gg_eta0_ZZ_4l/Events/run0_1200_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M1500':['gg_eta0_ZZ_4l/Events/run0_1500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-ZZ-4l-M2000':['gg_eta0_ZZ_4l/Events/run0_2000_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M200':['pp_eta0jj_4l/Events/run1_200_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M250':['pp_eta0jj_4l/Events/run1_250_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M300':['pp_eta0jj_4l/Events/run1_300_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M350':['pp_eta0jj_4l/Events/run1_350_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M400':['pp_eta0jj_4l/Events/run1_400_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M450':['pp_eta0jj_4l/Events/run1_450_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M500':['pp_eta0jj_4l/Events/run1_500_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M550':['pp_eta0jj_4l/Events/run1_550_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M600':['pp_eta0jj_4l/Events/run1_600_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M650':['pp_eta0jj_4l/Events/run1_650_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M700':['pp_eta0jj_4l/Events/run1_700_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M750':['pp_eta0jj_4l/Events/run1_750_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M800':['pp_eta0jj_4l/Events/run1_800_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M850':['pp_eta0jj_4l/Events/run1_850_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M900':['pp_eta0jj_4l/Events/run1_900_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M950':['pp_eta0jj_4l/Events/run1_950_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-ZZ-4l-evmuv-M1000':['pp_eta0jj_4l/Events/run1_1000_decayed_1/unweighted_events.lhe.gz'],
'pp-4l':['pp_4l/Events/run_01/events.lhe.gz'],
'gg-zz-4l':['gg_zz_4l/Events/run_01_decayed_1/unweighted_events.lhe.gz']
    }

# Where to look for the UNDECAYED total cross section: Note the difference between LO and NLO!
MGProcessLocations_Undecayed['HZZ100_4l'] = {
'gg-eta0-ZZ-4l-M200':['gg_eta0_ZZ_4l/Events/run0_200/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M250':['gg_eta0_ZZ_4l/Events/run0_250/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M250':['gg_eta0_ZZ_4l/Events/run0_250/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M300':['gg_eta0_ZZ_4l/Events/run0_300/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M350':['gg_eta0_ZZ_4l/Events/run0_350/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M400':['gg_eta0_ZZ_4l/Events/run0_400/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M450':['gg_eta0_ZZ_4l/Events/run0_450/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M500':['gg_eta0_ZZ_4l/Events/run0_500/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M550':['gg_eta0_ZZ_4l/Events/run0_550/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M600':['gg_eta0_ZZ_4l/Events/run0_600/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M650':['gg_eta0_ZZ_4l/Events/run0_650/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M700':['gg_eta0_ZZ_4l/Events/run0_700/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M750':['gg_eta0_ZZ_4l/Events/run0_750/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M800':['gg_eta0_ZZ_4l/Events/run0_800/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M850':['gg_eta0_ZZ_4l/Events/run0_850/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M900':['gg_eta0_ZZ_4l/Events/run0_900/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M950':['gg_eta0_ZZ_4l/Events/run0_950/unweighted_events.lhe'],
'gg-eta0-ZZ-4l-M1000':['gg_eta0_ZZ_4l/Events/run0_1000/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M200':['pp_eta0jj_4l/Events/run1_200/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M250':['pp_eta0jj_4l/Events/run1_250/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M300':['pp_eta0jj_4l/Events/run1_300/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M350':['pp_eta0jj_4l/Events/run1_350/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M400':['pp_eta0jj_4l/Events/run1_400/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M450':['pp_eta0jj_4l/Events/run1_450/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M500':['pp_eta0jj_4l/Events/run1_500/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M550':['pp_eta0jj_4l/Events/run1_550/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M600':['pp_eta0jj_4l/Events/run1_600/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M650':['pp_eta0jj_4l/Events/run1_650/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M700':['pp_eta0jj_4l/Events/run1_700/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M750':['pp_eta0jj_4l/Events/run1_750/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M800':['pp_eta0jj_4l/Events/run1_800/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M850':['pp_eta0jj_4l/Events/run1_850/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M900':['pp_eta0jj_4l/Events/run1_900/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M950':['pp_eta0jj_4l/Events/run1_950/unweighted_events.lhe'],
'pp-eta0jj-ZZ-4l-evmuv-M1000':['pp_eta0jj_4l/Events/run1_1000/unweighted_events.lhe'],
'pp-4l':['pp_4l/Events/run_01/summary.txt'],
'gg-zz-4l':['gg_zz_4l/Events/run_01/unweighted_events.lhe']
}

Luminosity['HZZ100_4l'] = 30000. # luminosity in inv fb.

NVariables['HZZ100_4l'] = 14 # the number of variables that go into the BDT

BRElement['HZZ100_4l'] = 10 # the element of the Higgs Branching Ratio array corresponding to this decay mode 

NBDTAnalysis['HZZ100_4l'] = 1 #3 # 10 # the number of BDT runs to search for the optimal solution

SignalFactorAnalysis['HZZ100_4l'] = 1.0E-4 # the initial signal factor guess for the BDT

HwSimLibrary['HZZ100_4l'] = 'HwSim' # the name of the HwSim-type library to be loaded by Herwig 

FatAnalysis['HZZ100_4l'] = '#' # add a "#" to remove the HwSimFat stuff in the Herwig file
