from singlet_constraint_functions import *
from os import listdir
from os.path import isfile, join

import glob

# function to generate html file. 
def write_html(outputdir, htmlfiletag, htmlfiles):
    print('Writing out html file:', htmlfiletag + '.html')
    htmlout = open(htmlfiletag + '.html', "w")
    htmlout.write('<!DOCTYPE html>\n')
    htmlout.write('<html>\n')
    htmlout.write('<body>\n')
    htmlout.write('<h1>Singlet constraint plots')
    htmlout.write('<br>\n')

    #htmlout.write('<p> reweighing &beta; = ' + str(angu) + ':</p>')
    for hf in htmlfiles:
        htmlout.write('<a href="./' + hf +'.pdf">\n')
        htmlout.write('<img src="' + hf + '.png" alt="' + hf + '" style="width:360px;height:240px;border:0;">\n')
        htmlout.write('</a>\n')
    htmlout.write('<br>\n')
    htmlout.write('</body>')
    htmlout.write('</html>')
    htmlout.close()
    

# generate html file for plots
outputdir = 'plots_150920_vev2/'
tag = 'categories2'

# get the files in the directory:
#print glob.glob(outputdir + "/*.png")

pngfiles = glob.glob(outputdir + "/*.png")

htmltags = [t.replace('.png','') for t in pngfiles]

write_html(outputdir,tag, htmltags)

print(htmltags)

#write_html()
