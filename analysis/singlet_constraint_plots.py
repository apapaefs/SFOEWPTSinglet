########################
# PLOTTING STARTS HERE #
########################



###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_122 and lambda_112 over benchmark categories (3D, Centrist)')
# plot settings ########
plot_type = 'l122_l112_M2_3D_Centr'
# plot:
# plot settings
ylab = '$\\lambda_{122}$ [GeV]'
xlab = '$\\lambda_{112}$ [GeV]'
ymin = -1000
ymax = 2000
xmin = -250
xmax = 250
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    if key == 'GWAPCentr':
        scatter = mscatter(Tree_l112[key], Tree_l122[key], z=OneLoop_mh2[key], plot_kind='3D', colmaplab='$M_2$ [GeV]', m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colb=True, label='', s=4)
        pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_122 and lambda_112 over benchmark categories (3D, No Trans)')
# plot settings ########
plot_type = 'l122_l112_M2_3D_NoTrans'
# plot:
# plot settings
ylab = '$\\lambda_{122}$ [GeV]'
xlab = '$\\lambda_{112}$ [GeV]'
ymin = -1000
ymax = 2000
xmin = -250
xmax = 250
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    if key == 'NoTrans':
        scatter = mscatter(Tree_l112[key], Tree_l122[key], z=OneLoop_mh2[key], plot_kind='3D', colmaplab='$M_2$ [GeV]', m=np.array(Constraints[key]), colb=True, c=np.array(corrcolors_array[key]), label='', s=4)
        pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_122 and lambda_112 over benchmark categories (3D, Centrist)')
# plot settings ########
plot_type = 'l122_l112_sintheta_3D_Centr'
# plot:
# plot settings
ylab = '$\\lambda_{122}$ [GeV]'
xlab = '$\\lambda_{112}$ [GeV]'
ymin = -1000
ymax = 2000
xmin = -250
xmax = 250
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    if key == 'GWAPCentr':
        scatter = mscatter(Tree_l112[key], Tree_l122[key], z=OneLoop_sintheta[key], plot_kind='3D', colmaplab='$\\sin \\theta$', m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colb=True, label='', s=4)
        pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_122 and lambda_112 over benchmark categories (3D, No Trans)')
# plot settings ########
plot_type = 'l122_l112_3D_sintheta_NoTrans'
# plot:
# plot settings
ylab = '$\\lambda_{122}$ [GeV]'
xlab = '$\\lambda_{112}$ [GeV]'
ymin = -1000
ymax = 2000
xmin = -250
xmax = 250
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    if key == 'NoTrans':
        scatter = mscatter(Tree_l112[key], Tree_l122[key], z=OneLoop_sintheta[key], plot_kind='3D', colmaplab='$\\sin \\theta$', m=np.array(Constraints[key]), colb=True, c=np.array(corrcolors_array[key]), label='', s=4)
        pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()


###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_222 and lambda_122 over benchmark categories')
# plot settings ########
plot_type = 'l222_l122'
# plot:
# plot settings
ylab = '$\\lambda_{222}$ [GeV]'
xlab = '$\\lambda_{122}$ [GeV]'
ymin = -750
ymax = 750
xmin = -1000
xmax = 1000.
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    scatter = mscatter(Tree_l122[key], Tree_l222[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()


###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_122 and lambda_112 over benchmark categories')
# plot settings ########
plot_type = 'l122_l112'
# plot:
# plot settings
ylab = '$\\lambda_{122}$ [GeV]'
xlab = '$\\lambda_{112}$ [GeV]'
ymin = -1000
ymax = 2000
xmin = -250
xmax = 250
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    scatter = mscatter(Tree_l112[key], Tree_l122[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for lambda_122 vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of lambda_222 and lambda_112 over benchmark categories')
# plot settings ########
plot_type = 'l222_l112'
# plot:
# plot settings
ylab = '$\\lambda_{222}$ [GeV]'
xlab = '$\\lambda_{112}$ [GeV]'
ymin = -750
ymax = 750
xmin = -250.
xmax = 250.
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


for key in list(OneLoop_sintheta.keys()):
    scatter = mscatter(Tree_l112[key], Tree_l222[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()


    
########################
# HIGGS BRs            #
########################
print('---')
print('plotting SM Higgs BRs')
# plot settings ########
plot_type = 'BR_SM_Higgs'
# plot:
# plot settings
xlab = '$m_1 \\mathrm{[GeV]}$'
ylab = 'BR'
ymin = 1E-4
ymax = 1.
xmin = mass_plot_array[0]
xmax = mass_plot_array[-1]
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(100))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')

for pp in range(len(BR_interpolation_array_for_plot)):
    if BR_plot_or_not[pp] is True: # choose which ones to plot
        pl.plot(mass_plot_array, BR_interpolation_array_for_plot[pp], color=next_color(), label='', ms=0, ls='-')
        p1 = ax.plot(np.NaN, np.NaN, color=same_color(), alpha=1, label=BR_text_array[pp], lw=7)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

###########################################
# HIGGS BRs with varying lambda           #
###########################################
print('---')
print('plotting Heavy Higgs BRs')
# plot settings ########
plot_type = 'BR_Heavy_Higgs'
# plot:
# plot settings
xlab = '$m_2\\; \\mathrm{[GeV]}$'
ylab = 'BR ($h_2 \\rightarrow xx$)'
ymin = 1E-3
ymax = 1.1
xmin = 200. #mass_plot_array[0]
xmax = mass_plot_array[-1]
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(100))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')


# fix the SM Higgs mass = 125.
mh1 = 125.
# fix sintheta
sintheta = 0.001
plot_type = plot_type + '_stheta' + str(sintheta)

insettext = "$\\sin \\theta =" + str(sintheta) + '$'
pl.text(0.4, 0.2, insettext, horizontalalignment='center', verticalalignment='center', transform = ax.transAxes, fontsize=14)
    


# fix lambda112 to a low value:
l112 = 0.05 * v0
# construct the plot array of BRs:
BR_interpolation_array_for_plot_heavy_lowlam = calc_HeavyHiggsBRs(BR_interpolators_SM, mh1, mass_plot_array, sintheta, l112)
# fix lambda112 to a high value:
l112 = 0.4 * v0
BR_interpolation_array_for_plot_heavy_hilam = calc_HeavyHiggsBRs(BR_interpolators_SM, mh1, mass_plot_array, sintheta, l112)

# reset the colours:
reset_color()
for pp in range(len(BR_interpolation_array_for_plot_heavy_lowlam)):
    if BR_plot_or_not_heavy[pp] is True: # choose which ones to plot
        pl.plot(mass_plot_array, BR_interpolation_array_for_plot_heavy_lowlam[pp], color=next_color(), label='', ms=0, ls='-')
        pl.plot(mass_plot_array, BR_interpolation_array_for_plot_heavy_hilam[pp], color=same_color(), label='', ms=0, ls=(0, (3, 1, 1, 1, 1, 1)))
        ax.fill_between(mass_plot_array, BR_interpolation_array_for_plot_heavy_lowlam[pp], BR_interpolation_array_for_plot_heavy_hilam[pp], alpha=.1, linewidth=0, color=same_color())

        p1 = ax.plot(np.NaN, np.NaN, color=same_color(), alpha=1, label='$h_2 \\rightarrow$' + BR_text_array_heavy[pp], lw=2)

p1 = ax.plot(np.NaN, np.NaN, color='black', alpha=1, label=' ', lw=0.)
p1 = ax.plot(np.NaN, np.NaN, color='black', alpha=1, label=' ', lw=0.)        

p1 = ax.plot(np.NaN, np.NaN, color='black', alpha=1, label='$\\lambda_{112} = 0.4 \\times v_0$', lw=2,ls=(0, (3, 1, 1, 1, 1, 1)))        
p1 = ax.plot(np.NaN, np.NaN, color='black', alpha=1, label='$\\lambda_{112} = 0.05 \\times v_0$', lw=2,ls='-')
    

# create legend and plot/font size
ax.legend()
ax.legend(loc='center', bbox_to_anchor=(0., 1.015, 1., .1015), ncol=4, numpoints=2, frameon=True, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)


#################################################################
# mh1 masses at one loop from different groups of benchmarks    #
#################################################################
print('---')
print('plotting mh1 at one loop over benchmark categories')
# plot settings ########
plot_type = 'oneloop_mh1'
# plot:
# plot settings
xlab = '$M_{h_1} \\mathrm{[GeV]}$'
ylab = '$P(M_{h_1})$'
#ymin = 1E-4
#ymax = 1.5
xmin = 124.
xmax = 126.
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(0.2))
ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')

reset_color()
# digitize the mh1 masses:
for key in list(OneLoop_mh1.keys()):
    bins, edges = np.histogram(np.array(OneLoop_mh1[key]), bins=50)
    #print key, bins, np.sqrt(bins)
    #b = np.sqrt(bins)
    #a = [1 for x in range(len(b))]
    #print b
    errors = np.divide(np.sqrt(bins), bins, out=np.zeros_like(np.sqrt(bins)), where=bins!=0.)
    #print errors
    bins = bins/float(len(OneLoop_mh1[key]))
    errors = bins*errors
    left,right = edges[:-1],edges[1:]
    X = np.array([left,right]).T.flatten()
    Y = np.array([bins,bins]).T.flatten()
    plt.plot(X,Y, label=key, color=next_color(), lw=1)
    center = (edges[:-1] + edges[1:]) / 2
    plt.errorbar(center, bins, yerr=errors, color=same_color(), lw=0, elinewidth=1, capsize=1)
    #plt.show()
    #plt.plot(center, hist, label=key)

    
# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)


###################################################################################
# scatter plot for sintheta vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of sintheta at one loop and lambda_112 over benchmark categories')
# plot settings ########
plot_type = 'sintheta_l112'
# plot:
# plot settings
ylab = '$\\lambda_{112}$'
xlab = '$|\\sin \\theta|$'
ymin = -300
ymax = 300
xmin = 0.001
xmax = 0.160
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

sintheta_l112_convexhull_positive = {}
sintheta_l112_convexhull_negative = {}


# REMOVE POINTS WITH M2 ABOVE CERTAIN MASS AND EXCLUDED POINTS
corrcolors_array_m2cut = {}
m2_cut = 850.
corrcolors_m2cut = {}
corralphas_m2cut = {}
corrcolors_array_m2cut = {}
for key in list(Constraints.keys()):
    TypeTag = ''.join([i for i in key if not i.isdigit()])
    corrcolors_m2cut[TypeTag] = []
    corralphas_m2cut[TypeTag] = []
    corrcolors_array_m2cut[TypeTag] = []
for key in list(Constraints.keys()):
    for i in range(len(Constraints[key])):
        if Constraints[key][i] == 'x' or OneLoop_mh2[key][i] > m2_cut:# remove excluded points and points with m2 > 850 GeV
            corralphas_m2cut[key].append([corrcolors_key['excl'], 0.0])
        else:
            corralphas_m2cut[key].append([corrcolors_key[key], 1.0])
for key in list(Constraints.keys()):
    #for alphacol in corralphas[key]:
    #    print alphacol[0], alphacol[1], to_rgb(alphacol[0])
    corrcolors_array_m2cut[key] = [(to_rgb(alphacol[0])[0], to_rgb(alphacol[0])[1], to_rgb(alphacol[0])[2], alphacol[1]) for alphacol in corralphas_m2cut[key]]
    #print len(corrcolors_array[key]), np.array(corrcolors_array[key]).shape, corrcolors_array[key][0], corrcolors_array[key][0], corrcolors_array[key][1], corrcolors_array[key][1]
    


for key in list(OneLoop_sintheta.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #ax.scatter(OneLoop_sintheta[key], np.array(Tree_l112[key])/v0, c=np.array(corrcolors_array[key]))
    scatter = mscatter(np.absolute(OneLoop_sintheta[key]), np.array(Tree_l112[key]), m=np.array(Constraints[key]), c=np.array(corrcolors_array_m2cut[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)
    sintheta_l112_convexhull_positive[key] = []
    sintheta_l112_convexhull_negative[key] = []
    # add the errors 
    for ss in range(0,len(OneLoop_sintheta[key])):
        # add the points to the dictionary for the convex hull boundary:
        if Constraints[key][ss] != 'x' and OneLoop_mh2[key][ss] < m2_cut: # remove excluded points and points with m2 > 850 GeV 
            if Tree_l112[key][ss] > 0:
                sintheta_l112_convexhull_positive[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
            else:
                sintheta_l112_convexhull_negative[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
            # plot measurement bands around the selected benchmark points 
            if sintheta_fit_prec_scatter[key][ss] > 0 and l112_fit_prec_scatter[key][ss] > 0:
                #print(l112_fit_prec_scatter[key][ss],sintheta_fit_prec_scatter[key][ss])
                xcenter = np.absolute(OneLoop_sintheta[key])[ss]
                ycenter = np.array(Tree_l112[key])[ss]
                width = 4*sintheta_fit_prec_scatter[key][ss]*xcenter
                height = 4*l112_fit_prec_scatter[key][ss]*ycenter
                e1 = patches.Ellipse((xcenter, ycenter), width, height,linewidth=2, alpha=0.7, color=corrcolors_key[key])
                ax.add_patch(e1)

pl.title("Only currently-allowed points, $m_2 < " + str(int(m2_cut)) + "$ GeV")

#for key in OneLoop_sintheta.keys():
#    hull = ConvexHull(sintheta_l112_convexhull_positive[key])
#    for simplex in hull.simplices:
#        pl.plot(np.array(sintheta_l112_convexhull_positive[key])[simplex, 0], np.array(sintheta_l112_convexhull_positive[key])[simplex, 1], '-', color=corrcolors_key[key])
#    hull = ConvexHull(sintheta_l112_convexhull_negative[key])
#    for simplex in hull.simplices:
#        pl.plot(np.array(sintheta_l112_convexhull_negative[key])[simplex, 0], np.array(sintheta_l112_convexhull_negative[key])[simplex, 1], '-', color=corrcolors_key[key])
            
pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(100))
ax.yaxis.set_minor_locator(MultipleLocator(20))
ax.xaxis.set_major_locator(MultipleLocator(0.02))
ax.xaxis.set_minor_locator(MultipleLocator(0.004))


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for sintheta vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of sintheta at one loop and lambda_112 over benchmark categories')
# plot settings ########
plot_type = 'sintheta_l112_hull'
# plot:
# plot settings
ylab = '$\\lambda_{112}$'
xlab = '$|\\sin \\theta|$'
ymin = -300
ymax = 300
xmin = 0.001
xmax = 0.160
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

sintheta_l112_convexhull_positive = {}
sintheta_l112_convexhull_negative = {}
sintheta_l112_convexhull = {}
xp = {}
yp = {}
xn = {}
yn = {}
for key in list(OneLoop_sintheta.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #ax.scatter(OneLoop_sintheta[key], np.array(Tree_l112[key])/v0, c=np.array(corrcolors_array[key]))
    #scatter = mscatter(np.absolute(OneLoop_sintheta[key]), np.array(Tree_l112[key]), m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='', lw=1, color=corrcolors_key[key], label=key)
    sintheta_l112_convexhull_positive[key] = []
    sintheta_l112_convexhull_negative[key] = []
    sintheta_l112_convexhull[key] = []
    xp[key] = []
    yp[key] = []
    xn[key] = []
    yn[key] = []
    # add the errors 
    for ss in range(0,len(OneLoop_sintheta[key])):
        # add the points to the dictionary for the convex hull boundary:
        if Constraints[key][ss] != 'x' and OneLoop_mh2[key][ss] < m2_cut : # remove excluded points and points with m2 > 800 GeV
            if Tree_l112[key][ss] > 0:
                sintheta_l112_convexhull_positive[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xp[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yp[key].append(Tree_l112[key][ss])
            else:
                sintheta_l112_convexhull_negative[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xn[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yn[key].append(Tree_l112[key][ss])
            sintheta_l112_convexhull[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])

            # plot measurement bands around the selected benchmark points 
            if sintheta_fit_prec_scatter[key][ss] > 0 and l112_fit_prec_scatter[key][ss] > 0:
                #print l112_fit_prec_scatter[key][ss],sintheta_fit_prec_scatter[key][ss]
                xcenter = np.absolute(OneLoop_sintheta[key])[ss]
                ycenter = np.array(Tree_l112[key])[ss]
                #width = sintheta_fit_prec_scatter[key][ss]*xcenter
                width = 0.5*(sintheta_max_fit_errormass_scatter[key][ss] - sintheta_min_fit_errormass_scatter[key][ss])
                #height_plus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                #height_minus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                height_plus = l112_max_fit_errormass_scatter[key][ss] - abs(ycenter)
                height_minus = abs(ycenter) - l112_min_fit_errormass_scatter[key][ss]
                #print('xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss]=', xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss])
                #print('ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss]=', ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss])
                #print(height_plus, height_minus)

                yasymmerror = np.array([(height_minus, height_plus)]).T
                yasymmerror_minus = np.array([(height_plus, height_minus)]).T
                pl.errorbar(xcenter, ycenter, yerr=yasymmerror, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                pl.errorbar(xcenter, -ycenter, yerr=yasymmerror_minus, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                #e1 = patches.Ellipse((xcenter, -ycenter), 2*width, yasymmerror,linewidth=2, alpha=0.5, fc=corrcolors_key[key])
                #ax.add_patch(e1)
                print("PointName, height_plus, height_minus, width=", PointName[key][ss], height_plus, height_minus, width)
                #pl.plot(xcenter, ycenter, marker='1', markersize=0.8, lw=0, color=corrcolors_key[key], label='')


for key in list(OneLoop_sintheta.keys()):
    hull = ConvexHull(sintheta_l112_convexhull_positive[key])
    for simplex in hull.simplices:
        pl.plot(np.array(sintheta_l112_convexhull_positive[key])[simplex, 0], np.array(sintheta_l112_convexhull_positive[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    hull = ConvexHull(sintheta_l112_convexhull_negative[key])
    for simplex in hull.simplices:
        pl.plot(np.array(sintheta_l112_convexhull_negative[key])[simplex, 0], np.array(sintheta_l112_convexhull_negative[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    # smooth hull
    #points = np.vstack((xp[key],yp[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xpp, ypp = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xpp,ypp, color=corrcolors_key[key], ls='-', lw=1)
    
    #points = np.vstack((xn[key],yn[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xnn, ynn = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xnn,ynn, color=corrcolors_key[key], ls='-', lw=1)
    
#    hull = ConvexHull(sintheta_l112_convexhull[key])
#    for simplex in hull.simplices:
#        pl.plot(np.array(sintheta_l112_convexhull[key])[simplex, 0], np.array(sintheta_l112_convexhull[key])[simplex, 1], '-', color=corrcolors_key[key])
   
            
#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')
pl.title("Only currently-allowed points, $m_2 < " + str(int(m2_cut)) + "$ GeV")

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
    
# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(100))
ax.yaxis.set_minor_locator(MultipleLocator(20))
ax.xaxis.set_major_locator(MultipleLocator(0.02))
ax.xaxis.set_minor_locator(MultipleLocator(0.004))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for sintheta vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of sintheta at one loop and lambda_112 over benchmark categories (bin1)')
# plot settings ########
plot_type = 'sintheta_l112_hull_bin1'
# plot:
# plot settings
ylab = '$\\lambda_{112}$'
xlab = '$|\\sin \\theta|$'
ymin = -300
ymax = 300
xmin = 0.001
xmax = 0.160
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

m2_min_bin1 = 260.
m2_max_bin1 = 400.

sintheta_l112_convexhull_positive = {}
sintheta_l112_convexhull_negative = {}
sintheta_l112_convexhull = {}
xp = {}
yp = {}
xn = {}
yn = {}
for key in list(OneLoop_sintheta.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #ax.scatter(OneLoop_sintheta[key], np.array(Tree_l112[key])/v0, c=np.array(corrcolors_array[key]))
    #scatter = mscatter(np.absolute(OneLoop_sintheta[key]), np.array(Tree_l112[key]), m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='', lw=1, color=corrcolors_key[key], label=key)
    sintheta_l112_convexhull_positive[key] = []
    sintheta_l112_convexhull_negative[key] = []
    sintheta_l112_convexhull[key] = []
    xp[key] = []
    yp[key] = []
    xn[key] = []
    yn[key] = []
    # add the errors 
    for ss in range(0,len(OneLoop_sintheta[key])):
        # add the points to the dictionary for the convex hull boundary:
        if Constraints[key][ss] != 'x' and OneLoop_mh2[key][ss] < m2_max_bin1 and OneLoop_mh2[key][ss] > m2_min_bin1: # remove excluded points and points with m2 > 800 GeV
            if Tree_l112[key][ss] > 0:
                sintheta_l112_convexhull_positive[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xp[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yp[key].append(Tree_l112[key][ss])
            else:
                sintheta_l112_convexhull_negative[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xn[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yn[key].append(Tree_l112[key][ss])
            sintheta_l112_convexhull[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])

            # plot measurement bands around the selected benchmark points 
            if sintheta_fit_prec_scatter[key][ss] > 0 and l112_fit_prec_scatter[key][ss] > 0:
                #print l112_fit_prec_scatter[key][ss],sintheta_fit_prec_scatter[key][ss]
                xcenter = np.absolute(OneLoop_sintheta[key])[ss]
                ycenter = np.array(Tree_l112[key])[ss]
                #width = sintheta_fit_prec_scatter[key][ss]*xcenter
                width = 0.5*(sintheta_max_fit_errormass_scatter[key][ss] - sintheta_min_fit_errormass_scatter[key][ss])
                #height_plus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                #height_minus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                height_plus = l112_max_fit_errormass_scatter[key][ss] - abs(ycenter)
                height_minus = abs(ycenter) - l112_min_fit_errormass_scatter[key][ss]
                #print('xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss]=', xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss])
                #print('ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss]=', ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss])
                #print(height_plus, height_minus)

                yasymmerror = np.array([(height_minus, height_plus)]).T
                yasymmerror_minus = np.array([(height_plus, height_minus)]).T
                pl.errorbar(xcenter, ycenter, yerr=yasymmerror, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                pl.errorbar(xcenter, -ycenter, yerr=yasymmerror_minus, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                #e1 = patches.Ellipse((xcenter, -ycenter), 2*width, yasymmerror,linewidth=2, alpha=0.5, fc=corrcolors_key[key])
                #ax.add_patch(e1)
                print("PointName, height_plus, height_minus, width, mh2=", PointName[key][ss], height_plus, height_minus, width, OneLoop_mh2[key][ss])
                #pl.plot(xcenter, ycenter, marker='1', markersize=0.8, lw=0, color=corrcolors_key[key], label='')


for key in list(OneLoop_sintheta.keys()):
    if len(sintheta_l112_convexhull_positive[key]) > 2:
        hull = ConvexHull(sintheta_l112_convexhull_positive[key])
        for simplex in hull.simplices:
            pl.plot(np.array(sintheta_l112_convexhull_positive[key])[simplex, 0], np.array(sintheta_l112_convexhull_positive[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    if len(sintheta_l112_convexhull_negative[key]) > 2:
        hull = ConvexHull(sintheta_l112_convexhull_negative[key])
        for simplex in hull.simplices:
            pl.plot(np.array(sintheta_l112_convexhull_negative[key])[simplex, 0], np.array(sintheta_l112_convexhull_negative[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    # smooth hull
    #points = np.vstack((xp[key],yp[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xpp, ypp = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xpp,ypp, color=corrcolors_key[key], ls='-', lw=1)
    
    #points = np.vstack((xn[key],yn[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xnn, ynn = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xnn,ynn, color=corrcolors_key[key], ls='-', lw=1)
    
#    hull = ConvexHull(sintheta_l112_convexhull[key])
#    for simplex in hull.simplices:
#        pl.plot(np.array(sintheta_l112_convexhull[key])[simplex, 0], np.array(sintheta_l112_convexhull[key])[simplex, 1], '-', color=corrcolors_key[key])
   
            
#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')
pl.title("Only currently-allowed points, $" + str(int(m2_min_bin1)) + "< m_2 < " + str(int(m2_max_bin1)) + "$ GeV")

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
    
# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(100))
ax.yaxis.set_minor_locator(MultipleLocator(20))
ax.xaxis.set_major_locator(MultipleLocator(0.02))
ax.xaxis.set_minor_locator(MultipleLocator(0.004))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
###################################################################################
# scatter plot for sintheta vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of sintheta at one loop and lambda_112 over benchmark categories (bin2)')
# plot settings ########
plot_type = 'sintheta_l112_hull_bin2'
# plot:
# plot settings
ylab = '$\\lambda_{112}$'
xlab = '$|\\sin \\theta|$'
ymin = -300
ymax = 300
xmin = 0.001
xmax = 0.160
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

m2_min_bin2 = 400.
m2_max_bin2 = 600.

sintheta_l112_convexhull_positive = {}
sintheta_l112_convexhull_negative = {}
sintheta_l112_convexhull = {}
xp = {}
yp = {}
xn = {}
yn = {}
for key in list(OneLoop_sintheta.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #ax.scatter(OneLoop_sintheta[key], np.array(Tree_l112[key])/v0, c=np.array(corrcolors_array[key]))
    #scatter = mscatter(np.absolute(OneLoop_sintheta[key]), np.array(Tree_l112[key]), m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='', lw=1, color=corrcolors_key[key], label=key)
    sintheta_l112_convexhull_positive[key] = []
    sintheta_l112_convexhull_negative[key] = []
    sintheta_l112_convexhull[key] = []
    xp[key] = []
    yp[key] = []
    xn[key] = []
    yn[key] = []
    # add the errors 
    for ss in range(0,len(OneLoop_sintheta[key])):
        # add the points to the dictionary for the convex hull boundary:
        if Constraints[key][ss] != 'x' and OneLoop_mh2[key][ss] < m2_max_bin2 and OneLoop_mh2[key][ss] > m2_min_bin2: # remove excluded points and points with m2 > 800 GeV
            if Tree_l112[key][ss] > 0:
                sintheta_l112_convexhull_positive[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xp[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yp[key].append(Tree_l112[key][ss])
            else:
                sintheta_l112_convexhull_negative[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xn[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yn[key].append(Tree_l112[key][ss])
            sintheta_l112_convexhull[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])

            # plot measurement bands around the selected benchmark points 
            if sintheta_fit_prec_scatter[key][ss] > 0 and l112_fit_prec_scatter[key][ss] > 0:
                #print l112_fit_prec_scatter[key][ss],sintheta_fit_prec_scatter[key][ss]
                xcenter = np.absolute(OneLoop_sintheta[key])[ss]
                ycenter = np.array(Tree_l112[key])[ss]
                #width = sintheta_fit_prec_scatter[key][ss]*xcenter
                width = 0.5*(sintheta_max_fit_errormass_scatter[key][ss] - sintheta_min_fit_errormass_scatter[key][ss])
                #height_plus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                #height_minus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                height_plus = l112_max_fit_errormass_scatter[key][ss] - abs(ycenter)
                height_minus = abs(ycenter) - l112_min_fit_errormass_scatter[key][ss]
                #print('xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss]=', xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss])
                #print('ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss]=', ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss])
                #print(height_plus, height_minus)

                yasymmerror = np.array([(height_minus, height_plus)]).T
                yasymmerror_minus = np.array([(height_plus, height_minus)]).T
                pl.errorbar(xcenter, ycenter, yerr=yasymmerror, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                pl.errorbar(xcenter, -ycenter, yerr=yasymmerror_minus, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                #e1 = patches.Ellipse((xcenter, -ycenter), 2*width, yasymmerror,linewidth=2, alpha=0.5, fc=corrcolors_key[key])
                #ax.add_patch(e1)
                print("PointName, height_plus, height_minus, width=", PointName[key][ss], height_plus, height_minus, width)
                #pl.plot(xcenter, ycenter, marker='1', markersize=0.8, lw=0, color=corrcolors_key[key], label='')


for key in list(OneLoop_sintheta.keys()):
    if len(sintheta_l112_convexhull_positive[key]) > 2:
        hull = ConvexHull(sintheta_l112_convexhull_positive[key])
        for simplex in hull.simplices:
            pl.plot(np.array(sintheta_l112_convexhull_positive[key])[simplex, 0], np.array(sintheta_l112_convexhull_positive[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    if len(sintheta_l112_convexhull_negative[key]) > 2:
        hull = ConvexHull(sintheta_l112_convexhull_negative[key])
        for simplex in hull.simplices:
            pl.plot(np.array(sintheta_l112_convexhull_negative[key])[simplex, 0], np.array(sintheta_l112_convexhull_negative[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    # smooth hull
    #points = np.vstack((xp[key],yp[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xpp, ypp = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xpp,ypp, color=corrcolors_key[key], ls='-', lw=1)
    
    #points = np.vstack((xn[key],yn[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xnn, ynn = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xnn,ynn, color=corrcolors_key[key], ls='-', lw=1)
    
#    hull = ConvexHull(sintheta_l112_convexhull[key])
#    for simplex in hull.simplices:
#        pl.plot(np.array(sintheta_l112_convexhull[key])[simplex, 0], np.array(sintheta_l112_convexhull[key])[simplex, 1], '-', color=corrcolors_key[key])
   
            
#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')
pl.title("Only currently-allowed points, $" + str(int(m2_min_bin2)) + "< m_2 < " + str(int(m2_max_bin2)) + "$ GeV")

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
    
# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(100))
ax.yaxis.set_minor_locator(MultipleLocator(20))
ax.xaxis.set_major_locator(MultipleLocator(0.02))
ax.xaxis.set_minor_locator(MultipleLocator(0.004))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
# scatter plot for sintheta vs. lambda_112 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot of sintheta at one loop and lambda_112 over benchmark categories (bin2)')
# plot settings ########
plot_type = 'sintheta_l112_hull_bin3'
# plot:
# plot settings
ylab = '$\\lambda_{112}$'
xlab = '$|\\sin \\theta|$'
ymin = -300
ymax = 300
xmin = 0.001
xmax = 0.160
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

m2_min_bin3 = 600.
m2_max_bin3 = 850.

sintheta_l112_convexhull_positive = {}
sintheta_l112_convexhull_negative = {}
sintheta_l112_convexhull = {}
xp = {}
yp = {}
xn = {}
yn = {}
for key in list(OneLoop_sintheta.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #ax.scatter(OneLoop_sintheta[key], np.array(Tree_l112[key])/v0, c=np.array(corrcolors_array[key]))
    #scatter = mscatter(np.absolute(OneLoop_sintheta[key]), np.array(Tree_l112[key]), m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='', lw=1, color=corrcolors_key[key], label=key)
    sintheta_l112_convexhull_positive[key] = []
    sintheta_l112_convexhull_negative[key] = []
    sintheta_l112_convexhull[key] = []
    xp[key] = []
    yp[key] = []
    xn[key] = []
    yn[key] = []
    # add the errors 
    for ss in range(0,len(OneLoop_sintheta[key])):
        # add the points to the dictionary for the convex hull boundary:
        if Constraints[key][ss] != 'x' and OneLoop_mh2[key][ss] < m2_max_bin3 and OneLoop_mh2[key][ss] > m2_min_bin3: # remove excluded points and points with m2 > 800 GeV
            if Tree_l112[key][ss] > 0:
                sintheta_l112_convexhull_positive[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xp[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yp[key].append(Tree_l112[key][ss])
            else:
                sintheta_l112_convexhull_negative[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])
                xn[key].append(np.absolute(OneLoop_sintheta[key][ss]))
                yn[key].append(Tree_l112[key][ss])
            sintheta_l112_convexhull[key].append([np.absolute(OneLoop_sintheta[key][ss]), Tree_l112[key][ss]])

            # plot measurement bands around the selected benchmark points 
            if sintheta_fit_prec_scatter[key][ss] > 0 and l112_fit_prec_scatter[key][ss] > 0:
                #print l112_fit_prec_scatter[key][ss],sintheta_fit_prec_scatter[key][ss]
                xcenter = np.absolute(OneLoop_sintheta[key])[ss]
                ycenter = np.array(Tree_l112[key])[ss]
                #width = sintheta_fit_prec_scatter[key][ss]*xcenter
                width = 0.5*(sintheta_max_fit_errormass_scatter[key][ss] - sintheta_min_fit_errormass_scatter[key][ss])
                #height_plus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                #height_minus = l112_fit_prec_scatter[key][ss]*abs(ycenter)
                height_plus = l112_max_fit_errormass_scatter[key][ss] - abs(ycenter)
                height_minus = abs(ycenter) - l112_min_fit_errormass_scatter[key][ss]
                #print('xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss]=', xcenter, sintheta_min_fit_errormass_scatter[key][ss], sintheta_max_fit_errormass_scatter[key][ss])
                #print('ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss]=', ycenter, l112_min_fit_errormass_scatter[key][ss], l112_max_fit_errormass_scatter[key][ss])
                #print(height_plus, height_minus)

                yasymmerror = np.array([(height_minus, height_plus)]).T
                yasymmerror_minus = np.array([(height_plus, height_minus)]).T
                pl.errorbar(xcenter, ycenter, yerr=yasymmerror, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                pl.errorbar(xcenter, -ycenter, yerr=yasymmerror_minus, xerr=width, color=corrcolors_key[key], elinewidth=0.5, label='')
                #e1 = patches.Ellipse((xcenter, -ycenter), 2*width, yasymmerror,linewidth=2, alpha=0.5, fc=corrcolors_key[key])
                #ax.add_patch(e1)
                print("PointName, height_plus, height_minus, width=", PointName[key][ss], height_plus, height_minus, width)
                #pl.plot(xcenter, ycenter, marker='1', markersize=0.8, lw=0, color=corrcolors_key[key], label='')


for key in list(OneLoop_sintheta.keys()):
    if len(sintheta_l112_convexhull_positive[key]) > 2:
        hull = ConvexHull(sintheta_l112_convexhull_positive[key])
        for simplex in hull.simplices:
            pl.plot(np.array(sintheta_l112_convexhull_positive[key])[simplex, 0], np.array(sintheta_l112_convexhull_positive[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    if len(sintheta_l112_convexhull_negative[key]) > 2:
        hull = ConvexHull(sintheta_l112_convexhull_negative[key])
        for simplex in hull.simplices:
            pl.plot(np.array(sintheta_l112_convexhull_negative[key])[simplex, 0], np.array(sintheta_l112_convexhull_negative[key])[simplex, 1], '-', color=corrcolors_key[key], lw=0.5)
    # smooth hull
    #points = np.vstack((xp[key],yp[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xpp, ypp = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xpp,ypp, color=corrcolors_key[key], ls='-', lw=1)
    
    #points = np.vstack((xn[key],yn[key])).T
    #hull = ConvexHull(points)
    #xhull = points[hull.vertices,0]
    #yhull = points[hull.vertices,1]
    #xhull = np.r_[xhull, xhull[0]]
    #yhull = np.r_[yhull, yhull[0]]
    #t, u = splprep([xhull, yhull], s=0, k=1, per=True)
    #xnn, ynn = splev(np.linspace(0, 1, 1000), t)
    #pl.plot(xnn,ynn, color=corrcolors_key[key], ls='-', lw=1)
    
#    hull = ConvexHull(sintheta_l112_convexhull[key])
#    for simplex in hull.simplices:
#        pl.plot(np.array(sintheta_l112_convexhull[key])[simplex, 0], np.array(sintheta_l112_convexhull[key])[simplex, 1], '-', color=corrcolors_key[key])
   
            
#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')
pl.title("Only currently-allowed points, $" + str(int(m2_min_bin3)) + "< m_2 < " + str(int(m2_max_bin3)) + "$ GeV")

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
    
# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(100))
ax.yaxis.set_minor_locator(MultipleLocator(20))
ax.xaxis.set_major_locator(MultipleLocator(0.02))
ax.xaxis.set_minor_locator(MultipleLocator(0.004))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()




###################################################################################
# scatter plot for BR_HH vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for BR_HH vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'BRhh_M2'
# plot:
# plot settings
ylab = '$BR(h_2 \\rightarrow h_1 h_1)$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 1.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], BR_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')



# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
# scatter plot for AbsTree112 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for AbsTree112 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'AbsTree112_M2'
# plot:
# plot settings
ylab = 'tree-level $|\\lambda_{112}| [GeV]$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 200.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(50))
ax.yaxis.set_minor_locator(MultipleLocator(10))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################


###################################################################################
# scatter plot for BR_ZZ vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for BR_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'BRZZ_M2'
# plot:
# plot settings
ylab = '$BR(h_2 \\rightarrow ZZ)$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 1.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], BR_ZZ_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################


###################################################################################
# scatter plot for BR_HH vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for BR_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'BRWW_M2'
# plot:
# plot settings
ylab = '$BR(h_2 \\rightarrow W^+W^-)$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 1.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], BR_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

    
# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################


###################################################################################
# scatter plot for BR_HH vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for BR_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'BRhhoverZZ_M2'
# plot:
# plot settings
ylab = '$BR(h_2 \\rightarrow h_1 h_1)/BR(h_2 \\rightarrow ZZ)$'
xlab = '$m_2$ [GeV]'
ymin = 0.0001
ymax = 100
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], np.divide(np.array(BR_HH_scatter[key]),np.array(BR_ZZ_scatter[key])), m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
# scatter plot for BR_hh vs BR_ZZ  from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for BR_hh vs BR_ZZ from different groups of benchmarks ')
# plot settings ########
plot_type = 'BRhh_ZZ'
# plot:
# plot settings
ylab = '$BR(h_2 \\rightarrow h_1 h_1)$'
xlab = '$BR(h_2 \\rightarrow ZZ)$'
ymin = 0.001
ymax = 1.1
xmin = 0.01
xmax = 0.4
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(BR_ZZ_scatter[key], BR_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(200))
#ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################



###################################################################################
print('---')
print('plotting scatter plot for XS100_HH vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS100_HH_M2'
# plot:
# plot settings
ylab = '$\\sigma(h_2 \\rightarrow h_1 h_1)$ at 100 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 100.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

limit_FCC_HH_central_array, limit_FCC_HH_1sigma_array, limit_FCC_HH_2sigma_array, FCC_HH_process_tag = get_xsec_limit_future(mass_plot_array, FCC_HH_central, FCC_HH_1sigma, FCC_HH_2sigma, FCC_HH_tag)
# and at different luminosities:
Lumis = [1000., 5000., 10000., 20000.]
limit_FCC_HH_lumi_array = {}
limit_FCC_HH_lumi_tag = {}
for Lumi in Lumis:
    limit_FCC_HH_lumi_array[Lumi], limit_FCC_HH_lumi_tag[Lumi] = get_xsec_limit_current(mass_plot_array, FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi])
discovery_FCC_HH_30, discovery_FCC_HH_30_tag = get_xsec_limit_current(mass_plot_array, FCC_HH_DISC, FCC_HH_DISC_tag)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS100_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker='', color='black', lw=0, label='FCC $h_2 \\rightarrow h_1 h_1$ limits:')

# plot the limit
#for Lumi in Lumis:
#    pl.plot(mass_plot_array, limit_FCC_HH_lumi_array[Lumi], lw=1, label='' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, limit_FCC_HH_central_array, lw=1, label='30 ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, discovery_FCC_HH_30, lw=1, linestyle='--', label='Discovery 30 ab$^{-1}$', color=next_color())

    

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
print('---')
print('plotting scatter plot for XS100_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS100_WW_M2'
# plot:
# plot settings
ylab = '$\\sigma(h_2 \\rightarrow W^+ W^-)$ at 100 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 100.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# get the FCC constraint curve on the plot:
limit_FCC_WW_central_array, limit_FCC_WW_1sigma_array, limit_FCC_WW_2sigma_array, FCC_WW_process_tag = get_xsec_limit_future(mass_plot_array, FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag)
# and at different luminosities:
Lumis = [1000., 5000., 10000., 20000.]
limit_FCC_WW_lumi_array = {}
limit_FCC_WW_lumi_tag = {}
for Lumi in Lumis:
    limit_FCC_WW_lumi_array[Lumi], limit_FCC_WW_lumi_tag[Lumi] = get_xsec_limit_current(mass_plot_array, FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi])
discovery_FCC_WW_30, discovery_FCC_WW_30_tag = get_xsec_limit_current(mass_plot_array, FCC_WW_DISC, FCC_WW_DISC_tag)

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS100_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


pl.plot(np.nan, np.nan, marker='', color='black', lw=0, label='FCC $h_2 \\rightarrow W^+ W^-$ limits:')

# plot the limit
#for Lumi in Lumis:
#    pl.plot(mass_plot_array, limit_FCC_WW_lumi_array[Lumi], lw=1, label='' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, limit_FCC_WW_central_array, lw=1, label='30 ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, discovery_FCC_WW_30, lw=1, linestyle='--', label='Discovery 30 ab$^{-1}$', color=next_color())

    
# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
print('---')
print('plotting scatter plot for XS100_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS100_WW_M2'
# plot:
# plot settings
ylab = '$\\sigma(h_2 \\rightarrow W^+ W^-)$ at 100 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 100.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# get the FCC constraint curve on the plot:
limit_FCC_WW_central_array, limit_FCC_WW_1sigma_array, limit_FCC_WW_2sigma_array, FCC_WW_process_tag = get_xsec_limit_future(mass_plot_array, FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag)
# and at different luminosities:
Lumis = [1000., 5000., 10000., 20000.]
limit_FCC_WW_lumi_array = {}
limit_FCC_WW_lumi_tag = {}
for Lumi in Lumis:
    limit_FCC_WW_lumi_array[Lumi], limit_FCC_WW_lumi_tag[Lumi] = get_xsec_limit_current(mass_plot_array, FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi])
discovery_FCC_WW_30, discovery_FCC_WW_30_tag = get_xsec_limit_current(mass_plot_array, FCC_WW_DISC, FCC_WW_DISC_tag)

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS100_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


pl.plot(np.nan, np.nan, marker='', color='black', lw=0, label='FCC $h_2 \\rightarrow W^+ W^-$ limits:')

# plot the limit
#for Lumi in Lumis:
#    pl.plot(mass_plot_array, limit_FCC_WW_lumi_array[Lumi], lw=1, label='' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, limit_FCC_WW_central_array, lw=1, label='30 ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, discovery_FCC_WW_30, lw=1, linestyle='--', label='Discovery 30 ab$^{-1}$', color=next_color())

    
# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
print('---')
print('plotting scatter plot for XS100_MAX vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS100_MAX_M2'
# plot:
# plot settings
ylab = '$\\sigma(h_2 \\rightarrow XX)_\\mathrm{max}$ at 100 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 100.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# get the FCC constraint curve on the plot:
limit_FCC_ZZ_central_array, limit_FCC_ZZ_1sigma_array, limit_FCC_ZZ_2sigma_array, FCC_ZZ_process_tag = get_xsec_limit_future(mass_plot_array, FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS100_MAX_scatter[key], m=np.array(Constraints[key]), color=corrcolors_key[key], label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


# plot the limit


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
print('---')
print('plotting scatter plot for XS100_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS100_ZZ_M2'
# plot:
# plot settings
ylab = '$\\sigma(h_2 \\rightarrow ZZ)$ at 100 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-4
ymax = 100.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

# get the FCC constraint curve on the plot:
limit_FCC_ZZ_central_array, limit_FCC_ZZ_1sigma_array, limit_FCC_ZZ_2sigma_array, FCC_ZZ_process_tag = get_xsec_limit_future(mass_plot_array, FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag)

limit_FCC_ZZ_lumi_array = {}
limit_FCC_ZZ_lumi_tag = {}
for Lumi in Lumis:
    limit_FCC_ZZ_lumi_array[Lumi], limit_FCC_ZZ_lumi_tag[Lumi] = get_xsec_limit_current(mass_plot_array, FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi])
discovery_FCC_ZZ_30, discovery_FCC_ZZ_30_tag = get_xsec_limit_current(mass_plot_array, FCC_ZZ_DISC, FCC_ZZ_DISC_tag)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS100_ZZ_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


# plot the limit


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


#pl.plot(np.nan, np.nan, marker='', color='black', lw=0, label='FCC $h_2 \\rightarrow ZZ$ limits:')

# plot the limit
#for Lumi in Lumis:
#    pl.plot(mass_plot_array, limit_FCC_ZZ_lumi_array[Lumi], lw=1, label='' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, limit_FCC_ZZ_central_array, lw=1, label='30 ab$^{-1}$', color=next_color())
#pl.plot(mass_plot_array, discovery_FCC_ZZ_30, lw=1, linestyle='--', label='Discovery 30 ab$^{-1}$', color=next_color())

    

# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))
# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################
###################################################################################
# scatter plot for RHO_MAX vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for RHO_MAX vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'rhomax_M2'
# plot:
# plot settings
ylab = '$\\mathrm{max}(\\rho_\\mathrm{max})$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 10.
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(6, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    if 'NoTrans' in key:
        continue
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    colmaplast = ''
    if first_cmap == True:
        colmaplast = r'$|\lambda_{112}|$ [GeV]' 
        first_cmap = False
    scatter = mscatter(M2_scatter[key], RHO_MAX_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(1.0))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
####################
###################################################################################
# scatter plot for delta mh2 (tree vs loop) vs m2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for delta mh1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'deltaMFITfrac_M2'
# plot:
# plot settings
ylab = '$\\Delta m_2/m_2$ from fit'
xlab = '$m_2$ [GeV]'
ymin = 1E-5
ymax = 1
xmin = 200.
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
DMASSFITfrac_scatter = {}
for key in list(BR_HH_scatter.keys()):
    DMASSFITfrac_scatter[key] = np.divide(np.array(DMASSFIT_scatter[key]),np.array(M2_scatter[key]))
    #print DMASSFITfrac_scatter[key]
    #print M2_scatter[key]
    scatter = mscatter(M2_scatter[key], DMASSFITfrac_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
plt.title("Fractional (statistical) error on $m_2$ from measurement through $m_{4 \\ell}$")


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(20))
#yaxis.set_minor_locator(MultipleLocator(5))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
###################################################################################
# scatter plot for delta mh2 (tree vs loop) vs m2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for delta lambda_112/lambda_112 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'deltal112FITfrac_M2'
# plot:
# plot settings
ylab = '$\\Delta \\lambda_{112}/\\lambda_{112}$ from fit'
xlab = '$m_2$ [GeV]'
ymin = 1E-5
ymax = 1
xmin = 200.
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
DMASSFITfrac_scatter = {}
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], l112_fit_prec_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
plt.title("Fractional (statistical) error on $\\lambda_{112}$")


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(20))
#yaxis.set_minor_locator(MultipleLocator(5))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for delta mh2 (tree vs loop) vs m2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for delta sintheta/sintheta vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'deltasinthetaFITfrac_M2'
# plot:
# plot settings
ylab = '$\\Delta \\sin \\theta/\\sin \\theta$ from fit'
xlab = '$m_2$ [GeV]'
ymin = 1E-5
ymax = 1
xmin = 200.
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
DMASSFITfrac_scatter = {}
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], sintheta_fit_prec_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
plt.title("Fractional (statistical) error on $\\sin \\theta$")


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(20))
#yaxis.set_minor_locator(MultipleLocator(5))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for delta mh2 (tree vs loop) vs m2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for gg > h eta0 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS_HETA0_M2'
# plot:
# plot settings
ylab = '$\\sigma ( gg \\rightarrow h_1 h_2 )$ [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-4
ymax = 1E-1
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
DMASSFITfrac_scatter = {}
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], XS_HETA0_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
plt.title("cross section for gg \\rightarrow h_1 h_2 production versus $m_2$")


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(20))
#yaxis.set_minor_locator(MultipleLocator(5))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
###################################################################################
# scatter plot for RHO_AV vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for RHO_MAX vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'rhoav_M2'
# plot:
# plot settings
ylab = '$\\left<\\rho_\\mathrm{max}\\right>$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 1.
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(6, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    if 'NoTrans' in key:
        continue
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    colmaplast = ''
    if first_cmap == True:
        colmaplast = r'$|\lambda_{112}|$ [GeV]' 
        first_cmap = False
    scatter = mscatter(M2_scatter[key], RHO_AV_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(1.0))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
####################

###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for SINTHETA vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'ABSsintheta_M2'
# plot:
# plot settings
ylab = '$\\left|\\sin \\theta\\right|$'
xlab = '$m_H$ [GeV]'
ymin = 0
ymax = 0.25
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

sintheta_m2_convexhull = []
first_cmap = True
for key in list(BR_HH_scatter.keys()):
    abssintheta_list = [abs(value) for value in OneLoop_sintheta[key]]

    scatter = mscatter(M2_scatter[key], abssintheta_list, z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    # get the convex hull
    for ss in range(0,len(M2_scatter[key])):    
        sintheta_m2_convexhull.append([M2_scatter[key][ss], np.absolute(OneLoop_sintheta[key][ss])])

hull = ConvexHull(sintheta_m2_convexhull)
for simplex in hull.simplices:
    pl.plot(np.array(sintheta_m2_convexhull)[simplex, 0], np.array(sintheta_m2_convexhull)[simplex, 1], '-', color='red', lw=0.5)
    
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')
pl.plot(np.nan, np.nan, marker='o', lw=0, color='red', label='SFO-EWPT')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(0.1))
yaxis.set_minor_locator(MultipleLocator(0.02))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for SINTHETA vs M2 for cons ')
# plot settings ########
plot_type = 'ABSsintheta_M2_CONS'
# plot:
# plot settings
ylab = '$\\left|\\sin \\theta\\right|$'
xlab = '$m_H$ [GeV]'
ymin = 0
ymax = 0.25
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

sintheta_m2_convexhull = []
first_cmap = True
for key in list(BR_HH_scatter.keys()):
    if key != "GWAPCons":
        continue
    abssintheta_list = [abs(value) for value in OneLoop_sintheta[key]]

    scatter = mscatter(M2_scatter[key], abssintheta_list, z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    # get the convex hull
    for ss in range(0,len(M2_scatter[key])):    
        sintheta_m2_convexhull.append([M2_scatter[key][ss], np.absolute(OneLoop_sintheta[key][ss])])

hull = ConvexHull(sintheta_m2_convexhull)
for simplex in hull.simplices:
    pl.plot(np.array(sintheta_m2_convexhull)[simplex, 0], np.array(sintheta_m2_convexhull)[simplex, 1], '-', color='red', lw=0.5)
    
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')
pl.plot(np.nan, np.nan, marker='o', lw=0, color='red', label='SFO-EWPT, Conservative uncertainties')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(0.1))
yaxis.set_minor_locator(MultipleLocator(0.02))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()


###################################################################################
# scatter plot for RHO_STDOVAV vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for RHO_STDOVAV vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'rhostdovav_M2'
# plot:
# plot settings
ylab = '$\\sigma(\\rho_\\mathrm{max}) / \\left<\\rho_\\mathrm{max}\\right>$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 1.
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(6, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    if 'NoTrans' in key:
        continue
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    colmaplast = ''
    if first_cmap == True:
        colmaplast = r'$|\lambda_{112}|$ [GeV]' 
        first_cmap = False
    scatter = mscatter(M2_scatter[key], RHO_STDOVAV_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)

# set the ticks, labels and limits 
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(1.0))
ax.yaxis.set_minor_locator(MultipleLocator(0.5))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))



# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
####################
###################################################################################
# scatter plot for RHO_MAX vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for RHO_STDOVAV vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'rhostd_M2'
# plot:
# plot settings
ylab = '$\\sigma(\\rho_\\mathrm{max})$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 1.
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(6, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    if 'NoTrans' in key:
        continue
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    colmaplast = ''
    if first_cmap == True:
        colmaplast = r'$|\lambda_{112}|$ [GeV]' 
        first_cmap = False
    scatter = mscatter(M2_scatter[key], RHO_STD_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)

# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
####################
####################
###################################################################################
print('---')
print('plotting scatter plot for XS27_HH vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS27_HH_M2'
# plot:
# plot settings
ylab = '$\\sigma(gg \\rightarrow h_2 \\rightarrow h_1 h_1)$ at 27 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-4
ymax = 100.
xmin = 250
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS27_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
    
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
print('---')
print('plotting scatter plot for XS27_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS27_WW_M2'
# plot:
# plot settings
ylab = '$\\sigma(gg \\rightarrow h_2 \\rightarrow W^+ W^-)$ at 27 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-4
ymax = 100.
xmin = 200.
xmax = 1000
ylog = True
xlog = False

# get the HE-LHC limit at at different luminosities:
Lumis = [1000., 5000., 15000.]
limit_HE_WW_lumi_array = {}
limit_HE_WW_lumi_tag = {}
limit_HE_WW_lumi_QQ_array = {}
limit_HE_WW_lumi_QQ_tag = {}
for Lumi in Lumis:
    limit_HE_WW_lumi_array[Lumi], limit_HE_WW_lumi_tag[Lumi] = get_xsec_limit_current(mass_plot_array, HE_WW_lumi_GGF[Lumi], HE_WW_lumi_GGF_tag[Lumi])
    limit_HE_WW_lumi_QQ_array[Lumi], limit_HE_WW_lumi_QQ_tag[Lumi] = get_xsec_limit_current(mass_plot_array, HE_WW_lumi_QQ[Lumi], HE_WW_lumi_QQ_tag[Lumi])

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS27_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


pl.plot(np.nan, np.nan, marker='', color='black', lw=0, label='HE-LHC $h_2 \\rightarrow W^+ W^-$ limits:')

# plot the limit
#for Lumi in Lumis:
    #pl.plot(mass_plot_array, limit_HE_WW_lumi_array[Lumi], lw=1, label='$gg$' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
    #pl.plot(mass_plot_array, limit_HE_WW_lumi_QQ_array[Lumi], lw=1, label='$q\\bar{q}$' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
    #pl.fill_between(mass_plot_array, limit_HE_WW_lumi_array[Lumi], limit_HE_WW_lumi_QQ_array[Lumi], label=str(Lumi/1000.) + ' ab$^{-1}$', color=next_color(), alpha=0.4)


# set the ticks, labels and limits 
ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
print('---')
print('plotting scatter plot for XS27_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'XS27_ZZ_M2'
# plot:
# plot settings
ylab = '$\\sigma(gg \\rightarrow h_2 \\rightarrow ZZ)$ at 27 TeV [pb]'
xlab = '$m_2$ [GeV]'
ymin = 1E-7
ymax = 100.
xmin = 250.
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)


limit_HE_ZZ_lumi_array = {}
limit_HE_ZZ_lumi_tag = {}
limit_HE_ZZ_lumi_QQ_array = {}
limit_HE_ZZ_lumi_QQ_tag = {}
for Lumi in Lumis:
    limit_HE_ZZ_lumi_array[Lumi], limit_HE_ZZ_lumi_tag[Lumi] = get_xsec_limit_current(mass_plot_array, HE_ZZ_lumi_GGF[Lumi], HE_ZZ_lumi_GGF_tag[Lumi])
    limit_HE_ZZ_lumi_QQ_array[Lumi], limit_HE_ZZ_lumi_QQ_tag[Lumi] = get_xsec_limit_current(mass_plot_array, HE_ZZ_lumi_QQ[Lumi], HE_ZZ_lumi_QQ_tag[Lumi])

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], XS27_ZZ_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4, plot_kind="scatter")
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


# plot the limit
pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


pl.plot(np.nan, np.nan, marker='', color='black', lw=0, label='HE-LHC $h_2 \\rightarrow ZZ$ limits:')

# plot the limit
#for Lumi in Lumis:
    #pl.plot(mass_plot_array, limit_HE_ZZ_lumi_array[Lumi], lw=1, label='$gg$' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
    #pl.plot(mass_plot_array, limit_HE_ZZ_lumi_QQ_array[Lumi], lw=1, label='$gg$' + str(Lumi/1000.) + ' ab$^{-1}$', color=next_color())
    #pl.fill_between(mass_plot_array, limit_HE_ZZ_lumi_array[Lumi], limit_HE_ZZ_lumi_QQ_array[Lumi], label=str(Lumi/1000.) + ' ab$^{-1}$', color=next_color(), alpha=0.4)

# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

#########################
# EWPO results          #
#########################

# EW parameters:
# G_F * m_z**2 / (2 * math.sqrt(2) * math.pi**2 * alpha) = 0.4761 from hep-ph/9409380
#expansion_parameter = 0.4761 [compare to hep-ph/9409380]

    
# the GFITTER CURRENT central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 11
# central values:
Delta_S_central = 0.06
Delta_T_central = 0.10
# errors:
errS = 0.09
errT = 0.07
# correlation (rho_12):
covST=0.91

# the GFITTER FUTURE central values and errors, see https://arxiv.org/pdf/1407.3792.pdf, page 13, Table 3
# central values:
Delta_S_central_F = 0.00
Delta_T_central_F = 0.00
# errors:
errS_F = math.sqrt(0.017**2 + 0.006**2) # experimental + theoretical errors in quadrature
errT_F = math.sqrt(0.022**2 + 0.005**2) 
# correlation (rho_12):
covST_F=0.91

# scan and create exclusion plot:
# Table 39.2 of http://pdg.lbl.gov/2019/reviews/rpp2019-rev-statistics.pdf:
# require Delta Chi^2 = 2.30, 6.18 (5.99 for 95% exactly), 11.83 for 1sigma, 2sigma, 3sigma confidence-level.

# fix m1 to = 125 GeV
m1 = 125.
m2_array = np.arange(150, 2000, 5) 
costheta_array = np.arange(0.8, 1.0, 0.001)
# arrays for the plot
m2ar = []
ctar = []
star = []
chisqar = []
chisqar_F = []
# loop over and calculate chisq
for m2i in range(len(m2_array)):
    for cthi in range(len(costheta_array)):
        sthi = math.sqrt(1-costheta_array[cthi]**2)
        chisqi = get_chisq_EWPO(m1, m2_array[m2i], sthi, mz, mw, Delta_S_central, Delta_T_central, errS, errT, covST) # present
        chisqi_F  = get_chisq_EWPO(m1, m2_array[m2i], sthi, mz, mw, Delta_S_central_F, Delta_T_central_F, errS_F, errT_F, covST_F) # future
        chisqar.append(chisqi)
        chisqar_F.append(chisqi_F)
        m2ar.append(m2_array[m2i])
        ctar.append(costheta_array[cthi])
        star.append(sthi)
        #print m2_array[m2i], costheta_array[cthi], sthi, chisqi
            
############        
# plot     #
############
gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

#xi = np.arange(150, 2000, 1)
#yi = np.arange(0.8, 1.0, 0.0005)
xi = np.linspace(150, 2000, 1000)
yi = np.linspace(0.8, 1.0, 1000)
#zi = matplotlib.mlab.griddata(m2ar, ctar, chisqar, xi, yi, interp='linear')
zi = scipy.interpolate.griddata((m2ar, ctar), chisqar, (xi[None,:], yi[:,None]), method='linear')
#cs = plt.contourf(xi, yi, zi, levels=[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4.5, 5, 5.5, 6, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11, 11.5, 12])
cs = plt.contour(xi, yi, zi, levels=[2.30, 6.18 , 11.83], extend='both', colors='k')
#cbar = fig.colorbar(cs)
#cbar.ax.get_yaxis().labelpad = 15
#cbar.ax.set_ylabel('$\\chi^2$', rotation=270)
#plt.clabel(cs, inline=1, fontsize=10)
manual_locations = [(300, 0.96), (1200, 0.94)]

strs = ['$2\\sigma$', '$3\\sigma$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s

for key in list(OneLoop_costheta.keys()):
    scatter = mscatter(M2_scatter[key], OneLoop_costheta[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4, plot_kind="scatter")
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

ax.axhline(math.sqrt(1-0.25**2), color='black', lw=2, ls="--", alpha=0.5)

ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$\\cos \\theta$', fontsize=20)
ax.set_xlabel('$m_2$ [GeV]', fontsize=20)
titletext = r'EWPO exclusion (current)'
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_ylim(0.9,1.0)
ax.set_xlim(200,1000.)


# plot the limit
pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

ax.yaxis.set_major_locator(MultipleLocator(0.02))
ax.yaxis.set_minor_locator(MultipleLocator(0.005))

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower left", numpoints=1, frameon=False, prop={'size':8})

plot_type = 'EWPO_current'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

##################      
# plot  FUTURE   #
##################
gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

xi = np.linspace(150, 2000, 1000)
yi = np.linspace(0.8, 1.0, 1000)
#zi = matplotlib.mlab.griddata(m2ar, ctar, chisqar, xi, yi, interp='linear')
zi = scipy.interpolate.griddata((m2ar, ctar), chisqar_F, (xi[None,:], yi[:,None]), method='linear')
#cs = plt.contourf(xi, yi, zi, levels=[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4.5, 5, 5.5, 6, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11, 11.5, 12])
cs = plt.contour(xi, yi, zi, levels=[2.30, 6.18 , 11.83], extend='both', colors='k')
#cbar = fig.colorbar(cs)
#cbar.ax.get_yaxis().labelpad = 15
#cbar.ax.set_ylabel('$\\chi^2$', rotation=270)
manual_locations = [(300, 0.96), (400, 0.95), (600, 0.94)]
strs = ['$1\\sigma$', '$2\\sigma$', '$3\\sigma$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s
plt.clabel(cs, inline=1, fontsize=12, fmt=fmt,manual=manual_locations)


for key in list(OneLoop_costheta.keys()):
    scatter = mscatter(M2_scatter[key], OneLoop_costheta[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

ax.axhline(math.sqrt(1-0.25**2), color='black', lw=2, ls="--", alpha=0.5)

ax.set_ylabel('$\\cos \\theta$', fontsize=20)
ax.set_xlabel('$m_2$ [GeV]', fontsize=20)
ax.set_title('EWPO exclusion (ILC/GigaZ)')
ax.set_ylim(0.9,1.0)
ax.set_xlim(200,1000.)

# plot the limit
pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

ax.yaxis.set_major_locator(MultipleLocator(0.02))
ax.yaxis.set_minor_locator(MultipleLocator(0.005))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

plot_type = 'EWPO_future'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()
reset_cmap()

##################################        
# plot EWPO current sintheta     #
##################################
gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(OneLoop_costheta.keys()):
    scatter = mscatter(M2_scatter[key], OneLoop_sintheta[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4, plot_kind="scatter")
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

xi = np.linspace(150, 2000, 1000)
yi = np.linspace(0.0, 0.5, 1000)
#zi = matplotlib.mlab.griddata(m2ar, ctar, chisqar, xi, yi, interp='linear')
zi = scipy.interpolate.griddata((m2ar, star), chisqar, (xi[None,:], yi[:,None]), method='linear')
print(zi)
#cs = plt.contourf(xi, yi, zi, levels=[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4.5, 5, 5.5, 6, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11, 11.5, 12])
cs = plt.contour(xi, yi, zi, levels=[6.0, 6.18 , 11.83], linewidths=[0, 2, 2], extend='both', colors='k')
#cbar = fig.colorbar(cs)
#cbar.ax.get_yaxis().labelpad = 15
#cbar.ax.set_ylabel('$\\chi^2$', rotation=270)
#plt.clabel(cs, inline=1, fontsize=10)
manual_locations = [(800, 0.16), (800, 0.18), (600, 0.44)]

strs = ['test', r'$2 \sigma$', r'$3 \sigma$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s
    print(l, s)


hline1=pl.axhline(0.25, color='black', lw=2, ls="--", alpha=0.5)
hline2=pl.axhline(0.1, color='black', lw=2, ls="--", alpha=0.5)
#labelLine(hline1,1200,label=r'$\sin \theta = 0.25$',align=False, fontsize=8)
#labelLine(hline2,1200,label=r'$\sin \theta = 0.1$',align=False, fontsize=8)


ax.clabel(cs, cs.levels, inline=True, fmt=fmt, fontsize=12, manual=manual_locations)
ax.set_ylabel('$|\\sin \\theta|$', fontsize=20)
ax.set_xlabel('$m_2$ [GeV]', fontsize=20)
titletext = r'EWPO exclusion (current)'
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
ax.set_title(titletext)
ax.set_ylim(0.0,0.5)
ax.set_xlim(200,1000.)


# plot the limit
pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

ax.yaxis.set_major_locator(MultipleLocator(0.04))
ax.yaxis.set_minor_locator(MultipleLocator(0.01))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':6})

plot_type = 'EWPO_current_sintheta'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

################################
# plot EWPO future sintheta   #
################################
gs = gridspec.GridSpec(6,6)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(OneLoop_costheta.keys()):
    scatter = mscatter(M2_scatter[key], OneLoop_sintheta[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


xi = np.linspace(150, 2000, 1000)
yi = np.linspace(0.0, 0.5, 1000)
#zi = matplotlib.mlab.griddata(m2ar, ctar, chisqar, xi, yi, interp='linear')
zi = scipy.interpolate.griddata((m2ar, star), chisqar_F, (xi[None,:], yi[:,None]), method='linear')
#cs = plt.contourf(xi, yi, zi, levels=[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4.5, 5, 5.5, 6, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5, 10.0, 10.5, 11, 11.5, 12])
cs = plt.contour(xi, yi, zi, levels=[1.0, 2.30, 6.18 , 11.83], linewidths=[0, 2, 2, 2], extend='both', colors='k')
#cbar = fig.colorbar(cs)
#cbar.ax.get_yaxis().labelpad = 15
#cbar.ax.set_ylabel('$\\chi^2$', rotation=270)
manual_locations = [(800, 0.16), (800, 0.2), (800, 0.24), (800, 0.34)]
strs = ['test', r'$1 \sigma$', r'$2 \sigma$', r'$3 \sigma$']
fmt = {}
for l, s in zip(cs.levels, strs):
    fmt[l] = s
    print(l, s)
plt.clabel(cs, inline=1, fontsize=12, fmt=fmt,manual=manual_locations)



hline1=pl.axhline(0.25, color='black', lw=2, ls="--", alpha=0.5)
hline2=pl.axhline(0.1, color='black', lw=2, ls="--", alpha=0.5)
#labelLine(hline1,1200,label=r'$\sin \theta = 0.25$',align=False, fontsize=8)
#labelLine(hline2,1200,label=r'$\sin \theta = 0.1$',align=False, fontsize=8)


ax.set_ylabel('$|\\sin \\theta|$', fontsize=20)
ax.set_xlabel('$m_2$ [GeV]', fontsize=20)
ax.set_title('EWPO exclusion (ILC/GigaZ)')
ax.set_ylim(0.0,0.5)
ax.set_xlim(200,1000.)

# plot the limit
pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC'], color='black', lw=0, label='HL-LHC $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

ax.yaxis.set_major_locator(MultipleLocator(0.04))
ax.yaxis.set_minor_locator(MultipleLocator(0.01))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':6})

plot_type = 'EWPO_future_sintheta'
# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()
reset_cmap()


###################################################################################
print('---')
print('plotting scatter plot for SIGNIF100_HH vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF100_HH_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow h_1 h_1$'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 200000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF100_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('100 TeV/30 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()


###################################################################################
print('---')
print('plotting scatter plot for SIGNIF100_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF100_ZZ_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow ZZ$'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 200000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF100_ZZ_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))


ax.set_title('100 TeV/30 ab$^{-1}$')

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF100_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF100_WW_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow W^+W^-$'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 200000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF100_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('100 TeV/30 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()




###################################################################################
print('---')
print('plotting scatter plot for SIGNIF100_MAX vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF100_MAX_M2'
# plot:
# plot settings
ylab = 'Significance maximum'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 200000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF100_MAX_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('100 TeV/30 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
###################################################################################
print('---')
print('plotting scatter plot for SIGNIF27_HH vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF27_HH_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow h_1 h_1$'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 100000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF27_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('27 TeV/15 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF27_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF27_ZZ_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow ZZ$'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 100000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF27_ZZ_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('27 TeV/15 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF27_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF27_WW_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow W^+W^-$'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 100000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF27_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('27 TeV/15 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF27_MAX vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF27_MAX_M2'
# plot:
# plot settings
ylab = 'Significance maximum'
xlab = '$m_2$ [GeV]'
ymin = 1E-2
ymax = 100000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF27_MAX_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('27 TeV/15 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
##########################################################
# the number of points above a certain significance.
##########################################################



###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for FINE TUNING for m1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'finetuningM1_M2'
# plot:
# plot settings
ylab = 'Fine-tuning measure for $m_1$'
xlab = '$m_2$ [GeV]'
ymin = 0.01
ymax = 10000.
xmin = 200.
xmax = 1000
ylog = True
xlog = False


# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], FINETUNING_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(1000., color='grey', lw=2, ls="--", label='')
labelLine(hline1,1600,label='$10^3$',align=False, fontsize=8)
hline2 = pl.axhline(100., color='grey', lw=2, ls="--", label='')
labelLine(hline2,1600,label='$10^2$',align=False, fontsize=8)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12



# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
#xaxis.set_major_locator(MultipleLocator(500))
#xaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for FINE TUNING for m1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'finetuningM2_M2'
# plot:
# plot settings
ylab = 'Fine-tuning measure for $m_2$'
xlab = '$m_2$ [GeV]'
ymin = 0.01
ymax = 10000.
xmin = 200.
xmax = 1000
ylog = True
xlog = False


# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], FINETUNING2_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(1000., color='grey', lw=2, ls="--", label='')
labelLine(hline1,1600,label='$10^3$',align=False, fontsize=8)
hline2 = pl.axhline(100., color='grey', lw=2, ls="--", label='')
labelLine(hline2,1600,label='$10^2$',align=False, fontsize=8)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12



# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
#xaxis.set_major_locator(MultipleLocator(500))
#xaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for DEKENS FINE TUNING vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'finetuningDEK_M2'
# plot:
# plot settings
ylab = 'Dekens fine-tuning measure'
xlab = '$m_2$ [GeV]'
ymin = 0.01
ymax = 10000.
xmin = 200.
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], FINETUNING3_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(1000., color='grey', lw=2, ls="--", label='')
labelLine(hline1,1600,label='$10^3$',align=False, fontsize=8)
hline2 = pl.axhline(100., color='grey', lw=2, ls="--", label='')
labelLine(hline2,1600,label='$10^2$',align=False, fontsize=8)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12



# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
#xaxis.set_major_locator(MultipleLocator(500))
#xaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()


###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for max FINE TUNING vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'finetuningMAX_M2'
# plot:
# plot settings
ylab = 'Maximum of fine-tuning measures'
xlab = '$m_2$ [GeV]'
ymin = 0.01
ymax = 10000.
xmin = 200.
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], FINETUNINGMAX_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(1000., color='grey', lw=2, ls="--", label='')
labelLine(hline1,1600,label='$10^3$',align=False, fontsize=8)
hline2 = pl.axhline(100., color='grey', lw=2, ls="--", label='')
labelLine(hline2,1600,label='$10^2$',align=False, fontsize=8)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12



# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
#xaxis.set_major_locator(MultipleLocator(500))
#xaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()


###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for FINE TUNING for m1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'finetuningM1_M2_oneloop'
# plot:
# plot settings
ylab = 'One-loop fine-tuning measure for $m_1$'
xlab = '$m_2$ [GeV]'
ymin = 0.01
ymax = 10000.
xmin = 200.
xmax = 1000
ylog = True
xlog = False


# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], FINETUNING_1loop_m1_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

#hline1 = pl.axhline(1000., color='grey', lw=2, ls="--", label='')
#labelLine(hline1,1600,label='$10^3$',align=False, fontsize=8)
#hline2 = pl.axhline(100., color='grey', lw=2, ls="--", label='')
#labelLine(hline2,1600,label='$10^2$',align=False, fontsize=8)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12



# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
#xaxis.set_major_locator(MultipleLocator(500))
#xaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for FINE TUNING for m2 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'finetuningM2_M2_oneloop'
# plot:
# plot settings
ylab = 'One-loop fine-tuning measure for $m_2$'
xlab = '$m_2$ [GeV]'
ymin = 0.01
ymax = 10000.
xmin = 200.
xmax = 1000
ylog = True
xlog = False


# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], FINETUNING_1loop_m2_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(1000., color='grey', lw=2, ls="--", label='')
labelLine(hline1,1600,label='$10^3$',align=False, fontsize=8)
hline2 = pl.axhline(100., color='grey', lw=2, ls="--", label='')
labelLine(hline2,1600,label='$10^2$',align=False, fontsize=8)

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12



# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
#xaxis.set_major_locator(MultipleLocator(500))
#xaxis.set_minor_locator(MultipleLocator(100))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()


#############################################################
# plot the sign of sintheta (from the tree-level extraction)#
#############################################################
###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for SIGN of SINTHETA vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'sign_M2'
# plot:
# plot settings
ylab = 'Sign of $\\sin 2\\theta$'
xlab = '$m_2$ [GeV]'
ymin = -2
ymax = 2
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], SIGN_SINTHETA_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0,color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
s#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()
###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for SINTHETA vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'sintheta_M2'
# plot:
# plot settings
ylab = '$\\sin \\theta$'
xlab = '$m_2$ [GeV]'
ymin = -2
ymax = 2
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], OneLoop_sintheta[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
s#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(0.1))
yaxis.set_minor_locator(MultipleLocator(0.02))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()



###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for l111 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'l111_M2'
# plot:
# plot settings
ylab = '$\\lambda_{111}/\\lambda_{111,SM}$'
xlab = '$m_2$ [GeV]'
ymin = -2
ymax = 2
xmin = 200.
xmax = 1000
ylog = False
xlog = False

l111_SM = 31.808963414634142

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], np.array(Tree_l111[key])/l111_SM, z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
s#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(0.2))
yaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for fine tuning in M1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for l111 vs rho_max from different groups of benchmarks ')
# plot settings ########
plot_type = 'l111_rhomax'
# plot:
# plot settings
ylab = '$\\lambda_{111}/\\lambda_{111,SM}$'
xlab = '$\\mathrm{max}(\\rho_\\mathrm{max})$'
ymin = -2
ymax = 2
xmin = 0.
xmax = 10.
ylog = False
xlog = False

l111_SM = 31.808963414634142

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    if 'NoTrans' in key:
        continue
    scatter = mscatter(RHO_MAX_scatter[key], np.array(Tree_l111[key])/l111_SM, z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
#plt.title(titletext)


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
#yaxis = plt.gca().yaxis
s#yaxis.set_major_locator(MultipleLocator(0.02))
#yaxis.set_minor_locator(MultipleLocator(0.005))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(1))
ax.xaxis.set_minor_locator(MultipleLocator(0.2))
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(0.2))
yaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()


###################################################################################
# scatter plot for delta mh1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for delta mh1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'deltaM1_M2'
# plot:
# plot settings
ylab = '$\\Delta m_1[0.5\\times m_Z, 2 \\times m_Z] $'
xlab = '$m_2 (m_Z)$ [GeV]'
ymin = 0
ymax = 220
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], DELTAMH1_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
plt.title("Variation of one-loop $m_1$ in $[0.5\\times m_Z, 2 \\times m_Z]$")


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(20))
yaxis.set_minor_locator(MultipleLocator(5))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()

###################################################################################
# scatter plot for delta mh2 (tree vs loop) vs m2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for delta mh1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'deltaM2tree_M2'
# plot:
# plot settings
ylab = '$\\Delta m_2$[Tree,Loop@$m_Z$]'
xlab = '$m_2 (m_Z)$ [GeV]'
ymin = 0
ymax = 220
xmin = 200.
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(16, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

first_cmap = True
for key in list(BR_HH_scatter.keys()):
    scatter = mscatter(M2_scatter[key], DELTAMH2tree_scatter[key], z=AbsTree_l112[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), colmap=next_cmap(), label='', s=4, plot_kind="scatter", colmaplab=colmaplast)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

#titletext = r''
#for key in OneLoop_costheta.keys():
#    titletext = titletext + str(key) + ' '
plt.title("Variation of $m_2$ between tree-level and one-loop at $m_Z$")


#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper left", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set the ticks, labels and limits
yaxis = plt.gca().yaxis
yaxis.set_major_locator(MultipleLocator(20))
yaxis.set_minor_locator(MultipleLocator(5))
xaxis = plt.gca().xaxis
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))


ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=15)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
reset_cmap()



###################################################################################
# scatter plot for K1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for K1 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'K1_M2'
# plot:
# plot settings
ylab = '$K_1$ [GeV]'
xlab = '$m_2$ [GeV]'
ymin = -400
ymax = 400.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], K1_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(50))
ax.yaxis.set_minor_locator(MultipleLocator(10))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
# scatter plot for K1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for K2 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'K2_M2'
# plot:
# plot settings
ylab = '$K_2$'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 5.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], K2_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.2))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################


###################################################################################
# scatter plot for Kappa vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for Kappa vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'Kappa_M2'
# plot:
# plot settings
ylab = '$\\kappa$ [GeV]'
xlab = '$m_2$ [GeV]'
ymin = -3500
ymax = 0
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], Kappa_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color='black', lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)

# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
ax.yaxis.set_major_locator(MultipleLocator(1000))
ax.yaxis.set_minor_locator(MultipleLocator(500))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################


###################################################################################
# scatter plot for K1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for Kappa vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'Lambda_M2'
# plot:
# plot settings
ylab = '$\\lambda$'
xlab = '$m_2$ [GeV]'
ymin = -2
ymax = 2
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], Lambda_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################
###################################################################################
# scatter plot for MS vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for MS vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'MS_M2'
# plot:
# plot settings
ylab = '$M_S^2$ [GeV$^2$]'
xlab = '$m_2$ [GeV]'
ymin = -500000
ymax = 500000
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], MS_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(400000))
ax.yaxis.set_minor_locator(MultipleLocator(50000))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################
###################################################################################
# scatter plot for mu2 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for MS vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'mu2_M2'
# plot:
# plot settings
ylab = '$\\mu^2$ [GeV$^2$]'
xlab = '$m_2$ [GeV]'
ymin = -100000
ymax = 100000
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], mu2_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(5000))
ax.yaxis.set_minor_locator(MultipleLocator(1000))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
# scatter plot for mu2 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for MS vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'x0_M2'
# plot:
# plot settings
ylab = '$x_0$ [GeV]'
xlab = '$m_2$ [GeV]'
ymin = 0
ymax = 400.
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], x0_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(200))
ax.yaxis.set_minor_locator(MultipleLocator(20))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################


###################################################################################
# scatter plot for K1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for K2 vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'K1_K2'
# plot:
# plot settings
ylab = '$K_2$'
xlab = '$K_1$ [GeV]'
ymin = 0
ymax = 5.
xmin = -400
xmax = 400.
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(K1_scatter[key], K2_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
#pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(1))
ax.yaxis.set_minor_locator(MultipleLocator(0.2))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

###################################################################################
# scatter plot for K1 vs M2 from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting scatter plot for Kappa vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'LambdaS_M2'
# plot:
# plot settings
ylab = '$\\lambda_S$'
xlab = '$m_2$ [GeV]'
ymin = -2
ymax = 2
xmin = 200
xmax = 1000
ylog = False
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], LambdaS_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)


pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')


# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

ax.yaxis.set_major_locator(MultipleLocator(0.1))
ax.yaxis.set_minor_locator(MultipleLocator(0.02))
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
####################

#CLIC STUDIES 

CLIC14_HH_frac = {}
CLIC3_HH_frac = {}
CLIC_VV_frac = {}

# calculate CLIC fractions:
for TypeTag in list(BR_HH_scatter.keys()):
    print('TypeTag=', TypeTag)
    #print 'CLIC_VV_scatter[TypeTag]=', CLIC_VV_scatter[TypeTag]
    #print 'CLIC14_HH_scatter[TypeTag]=', CLIC14_HH_scatter[TypeTag]
    #print 'CLIC3_HH_scatter[TypeTag]=', CLIC3_HH_scatter[TypeTag]
    CLIC_VV_frac[TypeTag] = float((np.asarray(CLIC_VV_scatter[TypeTag]) > 0.5).sum()) /float(len(CLIC_VV_scatter[TypeTag]))
    CLIC14_HH_frac[TypeTag] = float((np.asarray(CLIC14_HH_scatter[TypeTag]) > 0.5).sum()) /float(len(CLIC14_HH_scatter[TypeTag]))
    CLIC3_HH_frac[TypeTag] = float((np.asarray(CLIC3_HH_scatter[TypeTag]) > 0.5).sum()) /float(len(CLIC3_HH_scatter[TypeTag]))
    print("\t CLIC HH@1.4 TeV", float((np.asarray(CLIC14_HH_scatter[TypeTag]) > 0.5).sum()), '/', float(len(CLIC14_HH_scatter[TypeTag])), 1.-CLIC14_HH_frac[TypeTag])
    print("\t CLIC VV@3 TeV", float((np.asarray(CLIC_VV_scatter[TypeTag]) > 0.5).sum()), '/', float(len(CLIC_VV_scatter[TypeTag])), 1.-CLIC_VV_frac[TypeTag])
    print("\t CLIC HH@3 TeV", float((np.asarray(CLIC3_HH_scatter[TypeTag]) > 0.5).sum()), '/', float(len(CLIC3_HH_scatter[TypeTag])), 1.-CLIC3_HH_frac[TypeTag])


###################################################################################
print('---')
print('plotting scatter plot for SIGNIF14_HH vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF14_HH_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow h_1 h_1$'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 10000000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF14_HH_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)


# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))


ax.set_title('14 TeV/3 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF14_ZZ vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF14_ZZ_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow ZZ$'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 10000000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF14_ZZ_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 
#ax.yaxis.set_major_locator(MultipleLocator(0.1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.05))
#ax.xaxis.set_major_locator(MultipleLocator(0.2))
#ax.xaxis.set_minor_locator(MultipleLocator(0.05))
ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))


ax.set_title('14 TeV/3 ab$^{-1}$')

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF14_WW vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF14_WW_M2'
# plot:
# plot settings
ylab = 'Significance via $h_2 \\rightarrow W^+W^-$'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 10000000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF14_WW_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('14 TeV/3 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()

###################################################################################
print('---')
print('plotting scatter plot for SIGNIF14_MAX vs M2 from different groups of benchmarks ')
# plot settings ########
plot_type = 'SIGNIF14_MAX_M2'
# plot:
# plot settings
ylab = 'Significance maximum'
xlab = '$m_2$ [GeV]'
ymin = 1E-3
ymax = 10000000.
xmin = 200
xmax = 1000
ylog = True
xlog = False

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for key in list(BR_HH_scatter.keys()):
    #print OneLoop_sintheta[key], Tree_l112[key]
    #print BR_HH_scatter[key],  M2_scatter[key]
    scatter = mscatter(M2_scatter[key], SIGNIF14_MAX_scatter[key], m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=4)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color=ColorExcluded, lw=0, label='Excluded')

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)

# set the ticks, labels and limits 

ax.set_ylabel(ylab, fontsize=15)
ax.set_xlabel(xlab, fontsize=20)
pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('log')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')

hline1 = pl.axhline(5.0, color='red', lw=2, ls="--", label='')
#hline2 = pl.axhline(2.0, color='black', lw=2, ls="--", label='')
labelLine(hline1,600,label=r'$5\sigma$',align=False, fontsize=8)
#labelLine(hline2,600,label=r'$2\sigma$',align=False, fontsize=8)



# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

ax.xaxis.set_major_locator(MultipleLocator(200))
ax.xaxis.set_minor_locator(MultipleLocator(50))
ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

ax.set_title('14 TeV/3 ab$^{-1}$')


# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)

reset_color()
###################################################################################



exit()



##########################################################
# EXCLUSION (2 sigma)
##########################################################

Lumis = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
Lumis27 = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
# arrays for plotting:

EXCLFRAC100_HH_lumi = {}
EXCLFRAC100_WW_lumi = {}
EXCLFRAC100_ZZ_lumi = {}
EXCLFRAC100_MAX_lumi = {}
EXCLFRAC27_HH_lumi = {}
EXCLFRAC27_WW_lumi = {}
EXCLFRAC27_ZZ_lumi = {}
EXCLFRAC27_MAX_lumi = {}

for TypeTag in list(BR_HH_scatter.keys()):
    EXCLFRAC100_HH_lumi[TypeTag] = []
    EXCLFRAC100_WW_lumi[TypeTag] = []
    EXCLFRAC100_ZZ_lumi[TypeTag] = []
    EXCLFRAC100_MAX_lumi[TypeTag] = []
    EXCLFRAC27_HH_lumi[TypeTag] = []
    EXCLFRAC27_WW_lumi[TypeTag] = []
    EXCLFRAC27_ZZ_lumi[TypeTag] = []
    EXCLFRAC27_MAX_lumi[TypeTag] = []

# count the number of parameter space points that are above this significance
count_signif = 2.0
print('100 TeV:')
for Lumi in Lumis:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])
        #print '\t\t\tHH:', (np.asarray(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])  > count_signif).sum(), '/', len(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])
        print('\t\t\tMAX:', (np.asarray(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif ).sum(), '/', len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag])) != 0.:
            exclfrac = float((np.asarray(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if exclfrac == 1.:
            #    exclfrac = 1.-1./float(len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            exclfrac = 1.
        EXCLFRAC100_MAX_lumi[TypeTag].append(1-exclfrac)
        

print('27 TeV:')
for Lumi in Lumis27:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag]),
        #print '\t\t\tHH:', (np.asarray(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])
        #print SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]
        print('\t\t\tMAX:', (np.asarray(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag])) != 0.:
            exclfrac = float((np.asarray(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if exclfrac == 1.:
            #    exclfrac = 1.-1./float(len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            exclfrac = 1.
        EXCLFRAC27_MAX_lumi[TypeTag].append(1-exclfrac)



# plot: using the maximum significance:
print('---')
print('plotting fraction of points above certain significance (2) as a function of lumi - Current constr./FineTuning < 1000.')
# plot settings ########
plot_type = 'SURVIVAL_MAX_LUMI_CURRENTFT'
# plot:
# plot settings
ylab = '1-[excluded fraction]'
xlab = 'Luminosity [fb$^{-1}$]'
ymin = 0.0
ymax = 0.08
xmin = 100
xmax = 30000
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for TypeTag in list(BR_HH_scatter.keys()):
    pl.plot(Lumis, EXCLFRAC100_MAX_lumi[TypeTag], marker='', lw=3, ls='-', color=next_color(), label=TypeTag)
    pl.plot(Lumis27, EXCLFRAC27_MAX_lumi[TypeTag], marker='', lw=3, ls=':', color=same_color(), label='')
pl.plot(np.nan, np.nan, marker='', lw=3, ls='-', color='black', label='100 TeV')
pl.plot(np.nan, np.nan, marker='', lw=3, ls=':', color='black', label='27 TeV')

# set the ticks, labels and limits
#ax.yaxis.set_major_locator(LogLocator())

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('symlog')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.set_title('[Points currently viable with fine tuning < $10^3$]')

ax.yaxis.set_major_locator(LogLocator(base=100))
ax.yaxis.set_minor_locator(LogLocator(subs=np.arange(2, 10)))
ax.xaxis.set_major_locator(LogLocator())
ax.xaxis.set_minor_locator(LogLocator(subs=np.arange(2, 10)))
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
yticks = [0, 0.01]
ticklabels = ['$0$', '$10^{-2}$']
plt.yticks(yticks, ticklabels)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()


##########################################################
# DISCOVERY (5 sigma)
##########################################################
# arrays for plotting:

DISCFRAC100_HH_lumi = {}
DISCFRAC100_WW_lumi = {}
DISCFRAC100_ZZ_lumi = {}
DISCFRAC100_MAX_lumi = {}
DISCFRAC27_HH_lumi = {}
DISCFRAC27_WW_lumi = {}
DISCFRAC27_ZZ_lumi = {}
DISCFRAC27_MAX_lumi = {}

for TypeTag in list(BR_HH_scatter.keys()):
    DISCFRAC100_HH_lumi[TypeTag] = []
    DISCFRAC100_WW_lumi[TypeTag] = []
    DISCFRAC100_ZZ_lumi[TypeTag] = []
    DISCFRAC100_MAX_lumi[TypeTag] = []
    DISCFRAC27_HH_lumi[TypeTag] = []
    DISCFRAC27_WW_lumi[TypeTag] = []
    DISCFRAC27_ZZ_lumi[TypeTag] = []
    DISCFRAC27_MAX_lumi[TypeTag] = []

# count the number of parameter space points that are above this significance
count_signif = 5.0
print('100 TeV:')
for Lumi in Lumis:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print Lumi, SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]
        #print '\t\t\tZZ:', (np.asarray(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])
        #print '\t\t\tHH:', (np.asarray(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])  > count_signif).sum(), '/', len(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])
        print('\t\t\tMAX:', (np.asarray(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif ).sum(), '/', len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag])) != 0:
            discfrac = float((np.asarray(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if discfrac == 1.:
            #    discfrac = 1.-1./float(len(SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            discfrac = 1.
        DISCFRAC100_MAX_lumi[TypeTag].append(1-discfrac)

print('27 TeV:')
for Lumi in Lumis27:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag]),
        #print '\t\t\tHH:', (np.asarray(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])
        #print SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]
        print('\t\t\tMAX:', (np.asarray(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag])) != 0:
            discfrac = float((np.asarray(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if discfrac == 1.:
            #    discfrac = 1.-1./float(len(SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            discfrac = 1.
        DISCFRAC27_MAX_lumi[TypeTag].append(1-discfrac)

# plot: using the maximum significance:
print('---')
print('plotting fraction of points above certain significance (5) as a function of lumi - Current constr./FineTuning < 1000.')
# plot settings ########
plot_type = 'DISCOVERY_MAX_LUMI_CURRENTFT'
# plot:
# plot settings
ylab = '1-[discovery fraction]'
xlab = 'Luminosity [fb$^{-1}$]'
ymin = 0.0
ymax = 0.08
xmin = 100
xmax = 30000
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for TypeTag in list(BR_HH_scatter.keys()):
    pl.plot(Lumis, DISCFRAC100_MAX_lumi[TypeTag], marker='', lw=3, ls='-', color=next_color(), label=TypeTag)
    pl.plot(Lumis27, DISCFRAC27_MAX_lumi[TypeTag], marker='', lw=3, ls=':', color=same_color(), label='')
pl.plot(np.nan, np.nan, marker='', lw=3, ls='-', color='black', label='100 TeV')
pl.plot(np.nan, np.nan, marker='', lw=3, ls=':', color='black', label='27 TeV')

# set the ticks, labels and limits
#ax.yaxis.set_major_locator(MultipleLocator(0.02))
#ax.yaxis.set_minor_locator(MultipleLocator(0.005))
ax.xaxis.set_major_locator(MultipleLocator(2000))
ax.xaxis.set_minor_locator(MultipleLocator(500))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('symlog')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.set_title('[Points currently viable with fine tuning < $10^3$]')


ax.yaxis.set_major_locator(LogLocator(base=100))
ax.yaxis.set_minor_locator(LogLocator(subs=np.arange(2, 5)))
ax.xaxis.set_major_locator(LogLocator())
ax.xaxis.set_minor_locator(LogLocator(subs=np.arange(2, 5)))
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
yticks = [0, 0.01]
ticklabels = ['$0$', '$10^{-2}$']
plt.yticks(yticks, ticklabels)

#xaxis.set_major_locator(MultipleLocator(5000))
#xaxis.set_minor_locator(MultipleLocator(1000))
#locs, labels = pl.xticks()
#pl.xticks([0, 1, 2], ['January', 'February', 'March'])
#pl.xticks([])  # Disable xticks.

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()



##########################################################

# write out data files:

print('writing out data files for discovery/exclusion (current, with fine tuning)')

exclfile100 = 'Exclusion100_Current_wFineTuning_'
exclfile27 = 'Exclusion27_Current_wFineTuning_'
discfile100 = 'Discovery100_Current_wFineTuning_'
discfile27 = 'Discovery27_Current_wFineTuning_'
for TypeTag in list(BR_HH_scatter.keys()):
    exclstream100 = open(exclfile100 + TypeTag + '.txt', 'w')
    exclstream27 = open(exclfile27 + TypeTag + '.txt', 'w')
    discstream100 = open(discfile100 + TypeTag + '.txt', 'w')
    discstream27 = open(discfile27 + TypeTag + '.txt', 'w')
    ii = 0
    for Lumi in Lumis:
        exclstream100.write(str(Lumi)  + '\t' + str(EXCLFRAC100_MAX_lumi[TypeTag][ii]) + '\n')
        exclstream27.write(str(Lumi)  + '\t' + str(EXCLFRAC27_MAX_lumi[TypeTag][ii]) + '\n')
        discstream100.write(str(Lumi)  + '\t' + str(DISCFRAC100_MAX_lumi[TypeTag][ii]) + '\n')
        discstream27.write(str(Lumi)  + '\t' + str(DISCFRAC27_MAX_lumi[TypeTag][ii]) + '\n')
        ii = ii + 1



##########################################################
# EXCLUSION (2 sigma)
##########################################################

Lumis = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
Lumis27 = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
# arrays for plotting:

EXCLFRAC100_HH_lumi = {}
EXCLFRAC100_WW_lumi = {}
EXCLFRAC100_ZZ_lumi = {}
EXCLFRAC100_MAX_lumi = {}
EXCLFRAC27_HH_lumi = {}
EXCLFRAC27_WW_lumi = {}
EXCLFRAC27_ZZ_lumi = {}
EXCLFRAC27_MAX_lumi = {}

EXCLFRAC27_MAX_CentrLib_lumi = []
EXCLFRAC100_MAX_CentrLib_lumi = []
for TypeTag in list(BR_HH_scatter.keys()):
    EXCLFRAC100_HH_lumi[TypeTag] = []
    EXCLFRAC100_WW_lumi[TypeTag] = []
    EXCLFRAC100_ZZ_lumi[TypeTag] = []
    EXCLFRAC100_MAX_lumi[TypeTag] = []
    EXCLFRAC27_HH_lumi[TypeTag] = []
    EXCLFRAC27_WW_lumi[TypeTag] = []
    EXCLFRAC27_ZZ_lumi[TypeTag] = []
    EXCLFRAC27_MAX_lumi[TypeTag] = []

# count the number of parameter space points that are above this significance
count_signif = 2.0
print('100 TeV:')
for Lumi in Lumis:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])
        #print '\t\t\tHH:', (np.asarray(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])  > count_signif).sum(), '/', len(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])
        print('\t\t\tMAX:', (np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif ).sum(), '/', len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag])) != 0.:
            exclfrac = float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if exclfrac == 1.:
            #   exclfrac = 1.-1./float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            exclfrac = 1.
        EXCLFRAC100_MAX_lumi[TypeTag].append(1-exclfrac)
    # Lib + Centr together
    if category == 1:
        print('\t\t\tMAX:', ( float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())),'/', (float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib']))))
        exclfrac = ( float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())) / (float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib'])))
        EXCLFRAC100_MAX_CentrLib_lumi.append(1-exclfrac)


print('27 TeV:')
for Lumi in Lumis27:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag]),
        #print '\t\t\tHH:', (np.asarray(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])
        #print SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]
        print('\t\t\tMAX:', (np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag])) != 0.:
            exclfrac = float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if exclfrac == 1.:
            #    exclfrac = 1.-1./float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            exclfrac = 1.
        EXCLFRAC27_MAX_lumi[TypeTag].append(1-exclfrac)
    # Lib + Centr together
    if category == 1:
        print('\t\t\tMAX:',  ( float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())), '/', (float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib']))))
        exclfrac = ( float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())) / (float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib'])))
        EXCLFRAC27_MAX_CentrLib_lumi.append(1-exclfrac)

# plot: using the maximum significance:
print('---')
print('plotting fraction of points above certain significance (2) as a function of lumi - Current constraints < 1000.')
# plot settings ########
plot_type = 'SURVIVAL_MAX_LUMI_CURRENT'
# plot:
# plot settings
ylab = '1-[excluded fraction]'
xlab = 'Luminosity [fb$^{-1}$]'
ymin = 0.0
ymax = 0.08
xmin = 100
xmax = 30000
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for TypeTag in list(BR_HH_scatter.keys()):
    pl.plot(Lumis, EXCLFRAC100_MAX_lumi[TypeTag], marker='', lw=3, ls='-', color=next_color(), label=TypeTag)
    pl.plot(Lumis27, EXCLFRAC27_MAX_lumi[TypeTag], marker='', lw=3, ls=':', color=same_color(), label='')
pl.plot(np.nan, np.nan, marker='', lw=3, ls='-', color='black', label='100 TeV')
pl.plot(np.nan, np.nan, marker='', lw=3, ls=':', color='black', label='27 TeV')

# set the ticks, labels and limits
#ax.yaxis.set_major_locator(LogLocator())

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('symlog')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.set_title('[Points currently viable]')

ax.yaxis.set_major_locator(LogLocator(base=100))
ax.yaxis.set_minor_locator(LogLocator(subs=np.arange(2, 10)))
ax.xaxis.set_major_locator(LogLocator())
ax.xaxis.set_minor_locator(LogLocator(subs=np.arange(2, 10)))
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
yticks = [0, 0.01]
ticklabels = ['$0$', '$10^{-2}$']
plt.yticks(yticks, ticklabels)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()


##########################################################
# DISCOVERY (5 sigma)
##########################################################
# arrays for plotting:

DISCFRAC100_HH_lumi = {}
DISCFRAC100_WW_lumi = {}
DISCFRAC100_ZZ_lumi = {}
DISCFRAC100_MAX_lumi = {}
DISCFRAC27_HH_lumi = {}
DISCFRAC27_WW_lumi = {}
DISCFRAC27_ZZ_lumi = {}
DISCFRAC27_MAX_lumi = {}

DISCFRAC100_MAX_CentrLib_lumi = []
DISCFRAC27_MAX_CentrLib_lumi = []
for TypeTag in list(BR_HH_scatter.keys()):
    DISCFRAC100_HH_lumi[TypeTag] = []
    DISCFRAC100_WW_lumi[TypeTag] = []
    DISCFRAC100_ZZ_lumi[TypeTag] = []
    DISCFRAC100_MAX_lumi[TypeTag] = []
    DISCFRAC27_HH_lumi[TypeTag] = []
    DISCFRAC27_WW_lumi[TypeTag] = []
    DISCFRAC27_ZZ_lumi[TypeTag] = []
    DISCFRAC27_MAX_lumi[TypeTag] = []

# count the number of parameter space points that are above this significance
count_signif = 5.0
print('100 TeV:')
for Lumi in Lumis:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print Lumi, SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]
        #print '\t\t\tZZ:', (np.asarray(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])
        #print '\t\t\tHH:', (np.asarray(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])  > count_signif).sum(), '/', len(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])
        print('\t\t\tMAX:', (np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif ).sum(), '/', len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag])) != 0:
            discfrac = float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if discfrac == 1.:
            #    discfrac = 1.-1./float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            discfrac = 1.
        DISCFRAC100_MAX_lumi[TypeTag].append(1-discfrac)
        # Lib + Centr together
    if category == 1:
        print('\t\t\tMAX:', ( float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())), '/', (float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib']))))
        discfrac = ( float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())) / (float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF100_Current_MAX_lumi_scatter[Lumi]['GWAPLib'])))
        DISCFRAC100_MAX_CentrLib_lumi.append(1-discfrac)

print('27 TeV:')
for Lumi in Lumis27:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag]),
        #print '\t\t\tHH:', (np.asarray(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])
        #print SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]
        print('\t\t\tMAX:', (np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag])) != 0:
            discfrac = float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]))
            # reset to a "minimum" 
            #if discfrac == 1.:
            #    discfrac = 1.-1./float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            discfrac = 1.
        DISCFRAC27_MAX_lumi[TypeTag].append(1-discfrac)
    if category == 1:
        print('\t\t\tMAX:', ( float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())), '/', (float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib']))))
        discfrac = ( float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr']) > count_signif).sum()) + float((np.asarray(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib']) > count_signif).sum())) / (float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPCentr'])) + float(len(SIGNIF27_Current_MAX_lumi_scatter[Lumi]['GWAPLib'])))
        DISCFRAC27_MAX_CentrLib_lumi.append(1-discfrac)
            
# plot: using the maximum significance:
print('---')
print('plotting fraction of points above certain significance (5) as a function of lumi - Current constraints')
# plot settings ########
plot_type = 'DISCOVERY_MAX_LUMI_CURRENT'
# plot:
# plot settings
ylab = '1-[discovery fraction]'
xlab = 'Luminosity [fb$^{-1}$]'
ymin = 0.0
ymax = 0.08
xmin = 100
xmax = 30000
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for TypeTag in list(BR_HH_scatter.keys()):
    pl.plot(Lumis, DISCFRAC100_MAX_lumi[TypeTag], marker='', lw=3, ls='-', color=next_color(), label=TypeTag)
    pl.plot(Lumis27, DISCFRAC27_MAX_lumi[TypeTag], marker='', lw=3, ls=':', color=same_color(), label='')
pl.plot(np.nan, np.nan, marker='', lw=3, ls='-', color='black', label='100 TeV')
pl.plot(np.nan, np.nan, marker='', lw=3, ls=':', color='black', label='27 TeV')

# set the ticks, labels and limits
#ax.yaxis.set_major_locator(MultipleLocator(0.02))
#ax.yaxis.set_minor_locator(MultipleLocator(0.005))
ax.xaxis.set_major_locator(MultipleLocator(2000))
ax.xaxis.set_minor_locator(MultipleLocator(500))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('symlog')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.set_title('[Points currently viable]')


ax.yaxis.set_major_locator(LogLocator(base=100))
ax.yaxis.set_minor_locator(LogLocator(subs=np.arange(2, 5)))
ax.xaxis.set_major_locator(LogLocator())
ax.xaxis.set_minor_locator(LogLocator(subs=np.arange(2, 5)))
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
yticks = [0, 0.01]
ticklabels = ['$0$', '$10^{-2}$']
plt.yticks(yticks, ticklabels)

#xaxis.set_major_locator(MultipleLocator(5000))
#xaxis.set_minor_locator(MultipleLocator(1000))
#locs, labels = pl.xticks()
#pl.xticks([0, 1, 2], ['January', 'February', 'March'])
#pl.xticks([])  # Disable xticks.

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()


##########################################################

# write out data files:

print('writing out data files for discovery/exclusion (current, with fine tuning)')

exclfile100 = 'Exclusion100_Current_'
exclfile27 = 'Exclusion27_Current_'
discfile100 = 'Discovery100_Current_'
discfile27 = 'Discovery27_Current_'
for TypeTag in list(BR_HH_scatter.keys()):
    exclstream100 = open(exclfile100 + TypeTag + '.txt', 'w')
    exclstream27 = open(exclfile27 + TypeTag + '.txt', 'w')
    discstream100 = open(discfile100 + TypeTag + '.txt', 'w')
    discstream27 = open(discfile27 + TypeTag + '.txt', 'w')
    ii = 0
    for Lumi in Lumis:
        exclstream100.write(str(Lumi)  + '\t' + str(EXCLFRAC100_MAX_lumi[TypeTag][ii]) + '\n')
        exclstream27.write(str(Lumi)  + '\t' + str(EXCLFRAC27_MAX_lumi[TypeTag][ii]) + '\n')
        discstream100.write(str(Lumi)  + '\t' + str(DISCFRAC100_MAX_lumi[TypeTag][ii]) + '\n')
        discstream27.write(str(Lumi)  + '\t' + str(DISCFRAC27_MAX_lumi[TypeTag][ii]) + '\n')
        ii = ii + 1


if category == 1:
    exclfile100 = 'Exclusion100_Current_GWAPCentrLib.txt'
    exclfile27 = 'Exclusion27_Current_GWAPCentrLib.txt'
    discfile100 = 'Discovery100_Current_GWAPCentrLib.txt'
    discfile27 = 'Discovery27_Current_GWAPCentrLib.txt'
    exclstream100 = open(exclfile100, 'w')
    exclstream27 = open(exclfile27, 'w')
    discstream100 = open(discfile100, 'w')
    discstream27 = open(discfile27, 'w')
    ii = 0
    for Lumi in Lumis:
        exclstream100.write(str(Lumi)  + '\t' + str(EXCLFRAC100_MAX_CentrLib_lumi[ii]) + '\n')
        exclstream27.write(str(Lumi)  + '\t' + str(EXCLFRAC27_MAX_CentrLib_lumi[ii]) + '\n')
        discstream100.write(str(Lumi)  + '\t' + str(DISCFRAC100_MAX_CentrLib_lumi[ii]) + '\n')
        discstream27.write(str(Lumi)  + '\t' + str(DISCFRAC27_MAX_CentrLib_lumi[ii]) + '\n')
        ii = ii + 1



##########################################################
# EXCLUSION (2 sigma)
##########################################################

Lumis = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
Lumis27 = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
# arrays for plotting:

EXCLFRAC100_HH_lumi = {}
EXCLFRAC100_WW_lumi = {}
EXCLFRAC100_ZZ_lumi = {}
EXCLFRAC100_MAX_lumi = {}
EXCLFRAC27_HH_lumi = {}
EXCLFRAC27_WW_lumi = {}
EXCLFRAC27_ZZ_lumi = {}
EXCLFRAC27_MAX_lumi = {}

for TypeTag in list(BR_HH_scatter.keys()):
    EXCLFRAC100_HH_lumi[TypeTag] = []
    EXCLFRAC100_WW_lumi[TypeTag] = []
    EXCLFRAC100_ZZ_lumi[TypeTag] = []
    EXCLFRAC100_MAX_lumi[TypeTag] = []
    EXCLFRAC27_HH_lumi[TypeTag] = []
    EXCLFRAC27_WW_lumi[TypeTag] = []
    EXCLFRAC27_ZZ_lumi[TypeTag] = []
    EXCLFRAC27_MAX_lumi[TypeTag] = []

# count the number of parameter space points that are above this significance
count_signif = 2.0
print('100 TeV:')
for Lumi in Lumis:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])
        #print '\t\t\tHH:', (np.asarray(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])  > count_signif).sum(), '/', len(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])
        print('\t\t\tMAX:', (np.asarray(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif ).sum(), '/', len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag])) != 0.:
            exclfrac = float((np.asarray(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            exclfrac = 1.
        EXCLFRAC100_MAX_lumi[TypeTag].append(1-exclfrac)

print('27 TeV:')
for Lumi in Lumis27:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag]),
        #print '\t\t\tHH:', (np.asarray(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])
        #print SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]
        print('\t\t\tMAX:', (np.asarray(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag])) != 0.:
            exclfrac = float((np.asarray(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            exclfrac = 1.
        EXCLFRAC27_MAX_lumi[TypeTag].append(1-exclfrac)

# plot: using the maximum significance:
print('---')
print('plotting fraction of points above certain significance (2) as a function of lumi')
# plot settings ########
plot_type = 'SURVIVAL_MAX_LUMI'
# plot:
# plot settings
ylab = '1-[excluded fraction]'
xlab = 'Luminosity [fb$^{-1}$]'
ymin = 0.0
ymax = 0.08
xmin = 100
xmax = 30000
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for TypeTag in list(BR_HH_scatter.keys()):
    pl.plot(Lumis, EXCLFRAC100_MAX_lumi[TypeTag], marker='', lw=3, ls='-', color=next_color(), label=TypeTag)
    pl.plot(Lumis27, EXCLFRAC27_MAX_lumi[TypeTag], marker='', lw=3, ls=':', color=same_color(), label='')
pl.plot(np.nan, np.nan, marker='', lw=3, ls='-', color='black', label='100 TeV')
pl.plot(np.nan, np.nan, marker='', lw=3, ls=':', color='black', label='27 TeV')

# set the ticks, labels and limits
#ax.yaxis.set_major_locator(LogLocator())

ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('symlog')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.set_title('[Points viable at HL-LHC with $\\sin \\theta < 0.1$]')

ax.yaxis.set_major_locator(LogLocator(base=100))
ax.yaxis.set_minor_locator(LogLocator(subs=np.arange(2, 10)))
ax.xaxis.set_major_locator(LogLocator())
ax.xaxis.set_minor_locator(LogLocator(subs=np.arange(2, 10)))
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
yticks = [0, 0.01]
ticklabels = ['$0$', '$10^{-2}$']
plt.yticks(yticks, ticklabels)

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()


##########################################################
# DISCOVERY (5 sigma)
##########################################################
# arrays for plotting:

DISCFRAC100_HH_lumi = {}
DISCFRAC100_WW_lumi = {}
DISCFRAC100_ZZ_lumi = {}
DISCFRAC100_MAX_lumi = {}
DISCFRAC27_HH_lumi = {}
DISCFRAC27_WW_lumi = {}
DISCFRAC27_ZZ_lumi = {}
DISCFRAC27_MAX_lumi = {}

for TypeTag in list(BR_HH_scatter.keys()):
    DISCFRAC100_HH_lumi[TypeTag] = []
    DISCFRAC100_WW_lumi[TypeTag] = []
    DISCFRAC100_ZZ_lumi[TypeTag] = []
    DISCFRAC100_MAX_lumi[TypeTag] = []
    DISCFRAC27_HH_lumi[TypeTag] = []
    DISCFRAC27_WW_lumi[TypeTag] = []
    DISCFRAC27_ZZ_lumi[TypeTag] = []
    DISCFRAC27_MAX_lumi[TypeTag] = []

# count the number of parameter space points that are above this significance
count_signif = 5.0
print('100 TeV:')
for Lumi in Lumis:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print Lumi, SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]
        #print '\t\t\tZZ:', (np.asarray(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag])
        #print '\t\t\tHH:', (np.asarray(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])  > count_signif).sum(), '/', len(SIGNIF100_HH_lumi_scatter[Lumi][TypeTag])
        print('\t\t\tMAX:', (np.asarray(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif ).sum(), '/', len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag])) != 0:
            discfrac = float((np.asarray(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            discfrac = 1.
        DISCFRAC100_MAX_lumi[TypeTag].append(1-discfrac)

print('27 TeV:')
for Lumi in Lumis27:
    print('\t', Lumi,'/fb')
    for TypeTag in list(BR_HH_scatter.keys()):
        print('\t\tType=', TypeTag)
        #print '\t\t\tZZ:', (np.asarray(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag]),
        #print '\t\t\tHH:', (np.asarray(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_HH_lumi_scatter[Lumi][TypeTag])
        #print SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]
        print('\t\t\tMAX:', (np.asarray(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag])  > count_signif ).sum(), '/', len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]))
        if float(len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag])) != 0:
            discfrac = float((np.asarray(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]) > count_signif).sum()) /float(len(SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag]))
        else:
            discfrac = 1.
        DISCFRAC27_MAX_lumi[TypeTag].append(1-discfrac)

# plot: using the maximum significance:
print('---')
print('plotting fraction of points above certain significance (5) as a function of lumi')
# plot settings ########
plot_type = 'DISCOVERY_MAX_LUMI'
# plot:
# plot settings
ylab = '1-[discovery fraction]'
xlab = 'Luminosity [fb$^{-1}$]'
ymin = 0.0
ymax = 0.08
xmin = 100
xmax = 30000
ylog = True
xlog = True

# construct the axes for the plot
gs = gridspec.GridSpec(4, 4)
fig = pl.figure()
ax = fig.add_subplot(111)
ax.grid(False)

for TypeTag in list(BR_HH_scatter.keys()):
    pl.plot(Lumis, DISCFRAC100_MAX_lumi[TypeTag], marker='', lw=3, ls='-', color=next_color(), label=TypeTag)
    pl.plot(Lumis27, DISCFRAC27_MAX_lumi[TypeTag], marker='', lw=3, ls=':', color=same_color(), label='')
pl.plot(np.nan, np.nan, marker='', lw=3, ls='-', color='black', label='100 TeV')
pl.plot(np.nan, np.nan, marker='', lw=3, ls=':', color='black', label='27 TeV')

# set the ticks, labels and limits
#ax.yaxis.set_major_locator(MultipleLocator(0.02))
#ax.yaxis.set_minor_locator(MultipleLocator(0.005))
ax.xaxis.set_major_locator(MultipleLocator(2000))
ax.xaxis.set_minor_locator(MultipleLocator(500))
ax.set_ylabel(ylab, fontsize=20)
ax.set_xlabel(xlab, fontsize=20)
#pl.ylim([ymin,ymax])
pl.xlim([xmin, xmax])
# choose x and y log scales
if ylog:
    ax.set_yscale('symlog')
else:
    ax.set_yscale('linear')
if xlog:
    ax.set_xscale('log')
else:
    ax.set_xscale('linear')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12
ax.set_title('[Points viable at HL-LHC with $\\sin \\theta < 0.1$]')


ax.yaxis.set_major_locator(LogLocator(base=100))
ax.yaxis.set_minor_locator(LogLocator(subs=np.arange(2, 5)))
ax.xaxis.set_major_locator(LogLocator())
ax.xaxis.set_minor_locator(LogLocator(subs=np.arange(2, 5)))
plt.gca().xaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
plt.gca().yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
yticks = [0, 0.01]
ticklabels = ['$0$', '$10^{-2}$']
plt.yticks(yticks, ticklabels)

#xaxis.set_major_locator(MultipleLocator(5000))
#xaxis.set_minor_locator(MultipleLocator(1000))
#locs, labels = pl.xticks()
#pl.xticks([0, 1, 2], ['January', 'February', 'March'])
#pl.xticks([])  # Disable xticks.

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig)
reset_color()

