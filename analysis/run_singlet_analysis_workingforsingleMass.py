#! /usr/bin/env python

import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
from matplotlib.mlab import griddata
import matplotlib.font_manager as fm
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
from scipy.optimize import fsolve
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
import pickle
import re


####################
####################
# USEFUL FUNCTIONS #                 
####################
####################

# function to get template
def getTemplate(basename):
    with open('%s.template' % basename, 'r') as f:
        templateText = f.read()
    return string.Template( templateText )

# write a filename
def writeFile(filename, text):
    with open(filename,'w') as f:
        f.write(text)



#####################################
#####################################
# GENERAL SETTINGS FOR THIS MACHINE #
#####################################
#####################################

parser = OptionParser(usage="%prog [mode]")

opts, args = parser.parse_args()

if len(args) < 2:
    print('usage: run_singlet_analysis.py [mode] [analysis]')
    exit()

print("\nDriver script to run and collect results for the SM+Singlet analyses")
print("---")

# Herwig installation location on system:
HerwigInstallation = '/Users/apapaefs/Documents/Projects/Herwig-stable/'
HerwigSourceCmd = 'source ' + HerwigInstallation + 'bin/activate;'

# Herwig input file sub-dir and output for the events
HerwigLocation = 'Herwig/'
HerwigOutputLocation = HerwigLocation + 'events/'

# Input file templates for LO and MC@NLO:
# the real files have a .in.template extension
HW_template = ['','']
HW_template[0] = 'Templates/HW-LO.in' # 0th element is LO
HW_template[1] = 'Templates/HW-MCatNLO.in' # element 1 is NLO

# MG5/aMC sub-dir
MGLocation = 'MG5_aMC_v2_7_2/'
# the run cards for LO and NLO MG5/aMC runs:
MG_runcard = ['','']
MG_runcard[0] = 'run_card-LO.dat' # 0th element is LO
MG_runcard[1] = 'run_card-NLO.dat' # element 1 is NLO

# ROOT TMVA location and file
TMVALocation = '/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/TMVA/'
TMVAFile = 'TMVAClassification_singlet_multi.C' # the BDT file, run with: root -q -b -l 'TMVAClassification_singlet_multi.C(30000, "/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/TMVA/HWW100_signals.txt", "/Users/apapaefs/cernbox/SFOEWPT_Singlet/analysis/TMVA/HWW100_backgrounds.txt", 7)'

# available modes:
genherwig = False # generate the Herwig input files
runherwig = False # run Herwig via the generated input files
runanalysis = False # run the analysis on the generated HwSim root files
collectanalysis = False # collect the analysis results
runbdt = False # run the BDT 
runbdtsolve = False # run the BDT and find the minimum cross section for given values of the significance

# re-run?
ReRun = False
if len(args) > 2:
    if args[2] == "rerun":
        ReRun = True
        print('WARNING: Re-running everything forced! Even existing files')

############################
############################
# PROCESS SPECIFIC SECTION #
############################
############################

# dictionaries and lists for all processes
Analyses = []
Location = {}
Executable = {}
Processes = {} 
OrderProcesses = {} 
KFacsProcesses = {}
MGProcessLocations = {}
Luminosity = {}
NVariables = {}

####################
# H -> WW ANALYSIS #
####################

# list of analysed processes
Analyses.append('HWW100')

# location of analyses' info
Location['HWW100'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HWW100/'

# analyses' executables (with respect to Location directory)
Executable['HWW100'] = 'HwSimPostAnalysis'

# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HWW100'] = { 'gg-eta0-WW-evmuv-M400':1, 'pp-wpwm-evmuv':0, 'pp-tt-wbwb-evbmuvb':0 } # 1 stands for signal, 0 for backgrounds
# the order of the processes
OrderProcesses['HWW100'] = { 'gg-eta0-WW-evmuv-M400':0, 'pp-wpwm-evmuv':1, 'pp-tt-wbwb-evbmuvb':1 } # stands for LO and 1 for NLO (i.e. MC@NLO)
# K-factors for the processes
KFacsProcesses['HWW100'] = { 'gg-eta0-WW-evmuv-M400':1.0, 'pp-wpwm-evmuv':1.0, 'pp-tt-wbwb-evbmuvb':1.0 } # any K-factors to apply to processes

# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HWW100'] = { 'gg-eta0-WW-evmuv-M400':['gg_eta0_WW/Events/run_05_decayed_1/unweighted_events.lhe.gz'] , 'pp-wpwm-evmuv':['pp_wpwm_evmuv/Events/run_01_decayed_2/events.lhe.gz'], 'pp-tt-wbwb-evbmuvb':['pp_tt_wbwb_evbmuvb/Events/run_02_decayed_1/events.lhe.gz'] }

Luminosity['HWW100'] = 30. # luminosity in pb

NVariables['HWW100'] = 7 # the number of variables that go into the BDT

########################################
########################################
# SET PARAMETERS AND EXECUTE MODE HERE #
########################################
########################################

# choose the mode from the command line
if 'genherwig' == args[0]:
    genherwig = True

if 'runherwig' == args[0]:
    runherwig = True

if 'runanalysis' == args[0]:
    runanalysis = True

if 'collectanalysis' == args[0]:
    collectanalysis = True

if 'runbdt' == args[0]:
    runbdt = True

if 'runbdtsolve' == args[0]:
    runbdtsolve = True

# the current analysis (e.g. 'HW100')
CurrentAnalysis = args[1]

# generate the Herwig input files from the templates:
if genherwig == True:
    print('Generating Herwig input files for the analysis:', CurrentAnalysis)
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        print('\tfor Process:', Process, 'order:', OrderProcesses[CurrentAnalysis][Process], 'S or B:', Processes[CurrentAnalysis][Process])
        # choose template according to LO or MC@NLO:
        HerwigInputTemplate = getTemplate(HW_template[OrderProcesses[CurrentAnalysis][Process]])
        # loop over the LHE files and write the HW inputs for each, increment a counter
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = Process + '-' + str(counter)
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            hwinputfile = Location[CurrentAnalysis] + HerwigLocation + processname + '.in'
            parmtextsubs = {
                'PROCESSNAME' : processname, 
                'LHEFILE' : lhefile,
                'OUTPUTLOCATION' : outputlocation,
            }
            counter = counter + 1
            print('\t\twriting', hwinputfile)
            writeFile(hwinputfile, HerwigInputTemplate.substitute(parmtextsubs) )

if runherwig == True:
    print('Running Herwig from the input files previously generated, for:', CurrentAnalysis)
    # LOOP over the processes:
    for Process in list(Processes[CurrentAnalysis].keys()):
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            lhefile = Location[CurrentAnalysis] + MGLocation + LHEinputs
            processname = 'HW-' + Process + '-' + str(counter)
            hwinputfile = processname + '.in'
            hwrunfile = processname + '.run'
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            if os.path.exists(rootfile) is True:
                 print('File', rootfile, 'exists')
            if os.path.exists(rootfile) is False or (os.path.exists(rootfile) is True and ReRun is True): # if the root file exists, do not proceed except if ReRun is true
                if os.path.exists(rootfile) is True and ReRun is True:
                    print('File', rootfile, 'exists, but have chosen to re-run!')
                # get the number of events in the corresponding lhe file:
                zgrepcommand = 'zgrep "= nevents" ' + lhefile
                #print zgrepcommand
                p = subprocess.Popen(zgrepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    nevents = float(line.split()[0])
                print('\t\tHerwig reading:', hwinputfile)
                readcommand = HerwigSourceCmd + 'Herwig read ' + hwinputfile
                p = subprocess.Popen(readcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                #print out, err
                print('\t\tHerwig running:', hwrunfile, 'for', nevents, 'events')
                runcommand = HerwigSourceCmd + 'Herwig run ' + hwrunfile + ' -N' + str(int(nevents*0.995))
                p = subprocess.Popen(runcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                #print out, err
            counter = counter + 1

if runanalysis == True:
    print('Running analysis from the HwSim .root files generated, for:', CurrentAnalysis)
    # LOOP over the processes and write an .input file:
    for Process in list(Processes[CurrentAnalysis].keys()):
        # open the input file for all the root files in a given process:
        analysisInputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.input'
        analysisInputstream = open(analysisInputfile,'w') 
        counter = 0
        print('\twriting the .input file for the analysis')
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
            rootfile = outputlocation + processname + '.root'
            analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
            if os.path.exists(analysisOutputfile) is True:
                    print(analysisOutputfile, 'exists')
            if os.path.exists(analysisOutputfile) is False or (os.path.exists(analysisOutputfile) is True and ReRun is True):
                if os.path.exists(analysisOutputfile) is True and ReRun is True:
                    print(analysisOutputfile, 'exists but re-running anyway')
                # check if .root file exists:
                if os.path.exists(rootfile) is False:
                    print('Error: ROOT file:', rootfile, 'does not exist!')
                    exit()
                elif os.path.exists(rootfile) is True:
                    analysisInputstream.write(rootfile + '\n')
                analysisInputstream.close()
                print('running the analysis', Executable[CurrentAnalysis], 'on the input file', analysisInputfile)
                analysiscommand = HerwigSourceCmd + Location[CurrentAnalysis] + Executable[CurrentAnalysis] + ' ' + analysisInputfile
                p = subprocess.Popen(analysiscommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
                for line in iter(p.stdout.readline, b''):
                    print('\t\t', line, end=' ')
                out, err = p.communicate()
                print('\n')
            counter = counter + 1
 
        

if collectanalysis == True:
    print('Collecting analysis results for', CurrentAnalysis)
    # Cross section dictionary:
    InitialCrossSection = {}
    # LOOP over the processes and calculate the cross section before the analysis:
    print('\tcalculating the cross sections for each process from Herwig .out files')
    for Process in list(Processes[CurrentAnalysis].keys()):
        AverageCrossSection = 0. # calculate the average cross section
        counter = 0
        for LHEinputs in MGProcessLocations[CurrentAnalysis][Process]:
            processname = 'HW-' + Process + '-' + str(counter)
            outfilelocation = Location[CurrentAnalysis] + HerwigLocation
            outfile = outfilelocation + processname + '.out'
            # check if .out file exists:
            if os.path.exists(outfile) is False:
                print('Error: .out file:', outfile, 'does not exist!')
                exit()
            elif os.path.exists(outfile) is True:
                # read the cross section provided it exists:
                grepcommand = 'grep "theLHReader " ' + outfile
                p = subprocess.Popen(grepcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigLocation)
                for line in iter(p.stdout.readline, b''):
                    xsec = str(line.split()[-1])
                # remove the error
                firstDelPos=xsec.find("(")
                secondDelPos=xsec.find(")")
                xsec = xsec.replace(xsec[firstDelPos:secondDelPos+1],"")
                xsec = float(xsec)*1000. # convert to pb
                print('\t\tCross section in', outfile, '=', xsec, 'pb')
                AverageCrossSection = AverageCrossSection + xsec
            counter = counter + 1
        # average over all files and put into the initial cross section file:
        AverageCrossSection = AverageCrossSection / float(counter)
        InitialCrossSection[Process] = AverageCrossSection
    # now get the efficiency from the analysis .dat file for each process
    Efficiencies = {}
    print('\tGetting the efficiencies of the analysis for each process')
    for Process in list(Processes[CurrentAnalysis].keys()):
        analysisOutputfile = Location[CurrentAnalysis] + HerwigOutputLocation + Process + '.dat'
        if os.path.exists(outfile) is False:
            print('Error: .dat file:', analysisOutputfile, 'does not exist!')
            exit()
        elif os.path.exists(outfile) is True:
            analysisOutputstream = open(analysisOutputfile, 'r')
            for line in analysisOutputstream:
                efficiency = float(line.split()[0])
        print('\t\tAnalysis efficiency for:', Process, '=', efficiency)
        Efficiencies[Process] = efficiency
    # now get the cross section * efficiency * K-factor:
    OutputCrossSection = {}
    print('\tResulting cross sections after analysis * K-factors:')
    for Process in list(Processes[CurrentAnalysis].keys()):
        OutputCrossSection[Process] = Efficiencies[Process] * InitialCrossSection[Process] * KFacsProcesses[CurrentAnalysis][Process]
        print('\t\tProcess:', Process,'output cross section (inc. Factors)=', OutputCrossSection[Process], 'pb')
    # get an estimate of the significance given the above output cross sections at Luminosity
    sum_signals = 0.
    sum_backgrounds = 0.
    for Process in list(Processes[CurrentAnalysis].keys()):
        if Processes[CurrentAnalysis][Process] == 1: # signal
            sum_signals = sum_signals + OutputCrossSection[Process]
        elif Processes[CurrentAnalysis][Process] == 0:
            sum_backgrounds = sum_backgrounds + OutputCrossSection[Process]
            
    significance = sum_signals * math.sqrt(Luminosity[CurrentAnalysis]) / math.sqrt(sum_backgrounds) 
    print('\tCurrent significance estimate=', significance)
    # write output files for the BDT
    print('\tWriting the BDT txt files:')
    BDTOutputfileSignals = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    print('\t\twriting signal results in:', BDTOutputfileSignals)
    print('\t\twriting background results in:', BDTOutputfileBackgrounds)
    BDTSignals_Stream = open(BDTOutputfileSignals,'w')
    BDTBackgrounds_Stream = open(BDTOutputfileBackgrounds, 'w')
    for Process in list(Processes[CurrentAnalysis].keys()):
        processname = Process
        outputlocation = Location[CurrentAnalysis] + HerwigOutputLocation
        varrootfile = outputlocation + processname + '_var.root'
        if Processes[CurrentAnalysis][Process] == 1: # signal
            #print Process, OutputCrossSection[Process]*1000. # convert to fb
            BDTSignals_Stream.write(varrootfile + "\t" + str(OutputCrossSection[Process]*1000.) + "\n")
        elif Processes[CurrentAnalysis][Process] == 0: # background to
            #print Process, OutputCrossSection[Process] # convert to fb
            BDTBackgrounds_Stream.write(varrootfile + "\t" + str(OutputCrossSection[Process]*1000.) + "\n")

# run the BDT:
if runbdt == True:
    print('Running the BDT for:', CurrentAnalysis)
    # the BDT files:
    BDTOutputfileSignals = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]*1000
    SignalFactor = 1.0
    rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(SignalFactor) + ", " + str(NVariables[CurrentAnalysis]) + ")'"
    print(rootcommand)
    p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
    for line in iter(p.stdout.readline, b''):
        if "Maximum" in line:
            signif = line.split()[3] # Note that the the significance is given by the 4th word in the line
    out, err = p.communicate()

# run the BDT and find solution:
if runbdtsolve == True:
    print('Running the BDT for:', CurrentAnalysis, 'finding the cross section for a given significance')
    # the BDT files:
    BDTOutputfileSignals = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_signals.txt'
    BDTOutputfileBackgrounds = Location[CurrentAnalysis] + HerwigOutputLocation + CurrentAnalysis + '_backgrounds.txt'
    #TMVALocation TMVAFile NVariables
    LumiFB = Luminosity[CurrentAnalysis]*1000
    # function to run the BDT -- to assist solution:
    def FuncBDT(SignalFactor):
        #print SignalFactor
        rootcommand = "root -q -b -l '" + TMVALocation + TMVAFile + "(" + str(LumiFB) + "," + "\"" + BDTOutputfileSignals + "\", \"" + BDTOutputfileBackgrounds + "\"," + str(abs(SignalFactor[0])) + ", " + str(NVariables[CurrentAnalysis]) + ")'"
        #print rootcommand
        p = subprocess.Popen(rootcommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=Location[CurrentAnalysis] + HerwigOutputLocation)
        signif = 0.
        for line in iter(p.stdout.readline, b''):
            if "Maximum" in line:
                signif = line.split()[3] # Note that the the significance is give by the 4th word in the line
                print(SignalFactor, signif)
        #out, err = p.communicate()
        return float(signif)
    SignifTarget = 2.0
    if len(args) > 2:
        SignifTarget = float(args[2])
        print("Significance target is ", SignifTarget, " standard deviations")
    popt = []
    func_arg = lambda f : FuncBDT(f, *popt) - SignifTarget
    f_initial_guess = 1.0E-5
    f_solution = fsolve(func_arg, f_initial_guess)
    print(f_solution)
