##############################################
# H ETA -> (bb) ZZ -> (bb) (2l) (2v) ANALYSIS #
##############################################

# list of analysed processes
Analyses.append('HETA_BBZZ_BB2l2v')

# the latex name of the parent process (without the decay into the final state):
LatexNameParent['HETA_BBZZ_BB2l2v'] = '$h_1 h_2 \\rightarrow (b\\bar{b}) (ZZ)$' 

# the latex name of the analysis
LatexName['HETA_BBZZ_BB2l2v'] = '$h_1 h_2 \\rightarrow (b\\bar{b}) (ZZ) \\rightarrow (b\\bar{b}) (2 \\ell 2 \\nu)$'

# the Branching ratio of the primary decay particles of the heavy particle into the observed final state:
FacFinalState['HETA_BBZZ_BB2l2v'] = BR_h_bb*(2*BR_z_ellell + BR_z_vv)**2 

# the proton-proton energy for the analysis in TeV
Energy['HETA_BBZZ_BB2l2v'] = 100

# location of analyses' info
Location['HETA_BBZZ_BB2l2v'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HETA_BBZZ_BB2l2v/'

# analyses' executables (with respect to Location directory)
Executable['HETA_BBZZ_BB2l2v'] = 'HwSimPostAnalysis_bb2l2v_smear'

# analyses' executables (with respect to Location directory)
ExecutableCuts['HETA_BBZZ_BB2l2v'] = 'HwSimPostAnalysis_bb2l2v_smear'

# analyses' executables (with respect to Location directory)
ExecutableSmear['HETA_BBZZ_BB2l2v'] = 'HwSimPostAnalysis_bb2l2v_smear'

# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HETA_BBZZ_BB2l2v'] = {
                         'gg-eta0-ZZ-2l2v-GWAPCentr10055':'GWAPCentr10055',
                         'pp-ttbar-lvlvbb':0.0
                         }
                         
# the order of the processes
OrderProcesses['HETA_BBZZ_BB2l2v'] = {    'gg-eta0-ZZ-2l2v-GWAPCentr10055':0,
                         'pp-ttbar-lvlvbb':1
                         }

# K-factors for the processes: MUST INCLUDE SYMMETRY FACTORS (e.g. for the lepton flavours)
KFacsProcesses['HETA_BBZZ_BB2l2v'] = { 'gg-eta0-ZZ-2l2v-GWAPCentr10055':2.0*0.673,
                         'pp-ttbar-lvlvbb':1.0
         }


# K-factors to apply to processes on the TOTAL cross section -> NO symmetry factors from Decays.
TotalKFacsProcesses['HETA_BBZZ_BB2l2v'] = { 'gg-eta0-ZZ-2l2v-GWAPCentr10055':2.,                              
                                    'pp-ttbar-lvlvbb':1.0      
                         }

# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HETA_BBZZ_BB2l2v'] = {
'gg-eta0-ZZ-2l2v-GWAPCentr10055':['gg_heta0/Events/run_01_decayed_1/unweighted_events.lhe.gz'],
'pp-ttbar-lvlvbb':['pp_ttbar/Events/run_04_decayed_1/events.lhe.gz']
    }
 
# Where to look for the UNDECAYED total cross section: Note the difference between LO and NLO!
MGProcessLocations_Undecayed['HETA_BBZZ_BB2l2v'] = {
'gg-eta0-ZZ-2l2v-GWAPCentr10055':['gg_heta0/Events/run_01/unweighted_events.lhe'],
'pp-ttbar-lvlvbb':['pp_ttbar/Events/run_04/summary.txt']
}

Luminosity['HETA_BBZZ_BB2l2v'] = 30000. # luminosity in inv fb.

NVariables['HETA_BBZZ_BB2l2v'] = 9 # the number of variables that go into the BDT

BRElement['HETA_BBZZ_BB2l2v'] = 10 # the element of the Higgs Branching Ratio array corresponding to this decay mode 

NBDTAnalysis['HETA_BBZZ_BB2l2v'] = 9#10# the number of BDT runs to search for the optimal solution

SignalFactorAnalysis['HETA_BBZZ_BB2l2v'] = 1.E-2 # the initial signal factor guess for the BDT

HwSimLibrary['HETA_BBZZ_BB2l2v'] = 'HwSim' # the name of the HwSim-type library to be loaded by Herwig 

FatAnalysis['HETA_BBZZ_BB2l2v'] = '#' # add a "#" to remove the HwSimFat stuff in the Herwig file

