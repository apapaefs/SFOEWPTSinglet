from singlet_constraint_functions import *
# point parameters:
mh1 = 125.1
mh2 = 377.5
streal = -0.03445632783
ctreal = math.sqrt(1-streal**2)
k112real = -30.183 # true value of k112
signif_ZZ = 53.02147212128927
signif_HH = 123.37915977816529
Tag = 'GWAPUCons49'
get_sintheta_l112_fit_fixedmass(mh1, mh2, streal, k112real, signif_ZZ, signif_HH, Tag)
