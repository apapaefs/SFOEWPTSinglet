from singlet_constraint_functions import *
from singlet_sigmahs_massfit import *

# the dictionary of benchmark points
BenchmarkPoints = {}
# element                = [ 0    , 1    , 2    , 3   , 4   , 5   , 6    , 7    , 8     , 9  ] 
#BenchmarkPoints["NAME"] = [ costh, sinth, mass2, gam2, x0  , lamb, a_1, , a_2  , b_3   , b_4]
#BenchmarkPoints["B1max"] = [ 0.976, 0.220, 341.0, 2.42, 257., 0.92, -377., 0.392, -403., 0.77 ]
#BenchmarkPoints["B2max"]= [ 0.982, 0.188, 353.0, 2.17, 265., 0.99, -400., 0.446, -378., 0.69 ]
#BenchmarkPoints["B3max"] = [ 0.983, 0.181, 415.0, 1.59, 54.6, 0.17, -642., 3.80, -214., 0.16 ]
#BenchmarkPoints["B4max"] = [ 0.984, 0.176, 455.0, 2.08, 47.4, 0.18, -707., 4.63, -607., 0.85 ]
#BenchmarkPoints["B5max"] = [ 0.986, 0.164, 511.0, 2.44, 40.7, 0.18, -744., 5.17, -618., 0.82 ]
#BenchmarkPoints["B6max"] = [ 0.988, 0.153, 563.0, 2.92, 40.5, 0.19, -844., 5., -151., 0.083 ]
#BenchmarkPoints["B7max"] = [ 0.992, 0.129, 604.0, 2.82, 36.4, 0.18, -898., 7.36, -424., 0.28 ]
#BenchmarkPoints["B8max"] = [ 0.994, 0.113, 662.0, 2.97, 32.9, 0.17, -976., 8.98, -542., 0.53 ]
#BenchmarkPoints["B9max"] = [ 0.993, 0.115, 714.0, 3.27, 29.2, 0.18, -941., 8.28, 497., 0.38 ]
#BenchmarkPoints["B10max"] = [ 0.996, 0.094, 767.0, 2.83, 24.5, 0.17, -920., 9.87, 575., 0.41 ]
#BenchmarkPoints["B11max"] = [ 0.994, 0.105, 840.0, 4.03, 21.7, 0.19, -988., 9.22, 356., 0.83 ]
#BenchmarkPoints["NAME"] = [ costh, sinth, mass2, gam2,  x0  , lamb, a_1, , a_2  , b_3   , b_4]
#BenchmarkPoints["B1min"] = [ 0.999, 0.029, 343.0, 0.041, 105., 0.13, -850., 3.91, -106., 0.29 ]
#BenchmarkPoints["B2min"] = [ 0.973, 0.231, 350.0, 0.777, 225., 0.18, -639., 0.986,-111., 0.97 ]
#BenchmarkPoints["B3min"] = [ 0.980, 0.197, 419.0, 1.32,  234., 0.18, -981., 1.56, 0.42, 0.96 ]
#BenchmarkPoints["B4min"] = [ 0.999, 0.026, 463.0, 0.086, 56.8, 0.13, -763., 6.35, 113., 0.73 ]
#BenchmarkPoints["B5min"] = [ 0.999, 0.035, 545.0, 0.278, 50.2, 0.13, -949., 8.64, 151., 0.57 ]
#BenchmarkPoints["B6min"] = [ 0.999, 0.043, 563.0, 0.459, 33.0, 0.13, -716., 9.25, -448., 0.96 ]
#BenchmarkPoints["B7min"] = [ 0.984, 0.180, 609.0, 4.03,  34.2, 0.22, -822., 4.53, -183., 0.57 ]
#BenchmarkPoints["B8min"] = [ 0.987, 0.161, 676.0, 4.47,  30.3, 0.22, -931., 5.96, -680., 0.43 ]
#BenchmarkPoints["B9min"] = [ 0.990, 0.138, 729.0, 4.22,  27.3, 0.21, -909., 6.15, 603., 0.93 ]
#BenchmarkPoints["B10min"] = [ 0.995, 0.104, 792.0, 3.36, 22.2, 0.18, -936., 9.47, -848., 0.66 ]
#BenchmarkPoints["B11min"] = [ 0.994, 0.105, 841.0, 3.95, 21.2, 0.19, -955., 8.69, 684., 0.53 ]

#BenchmarkPoints["1701.04442_B6max"] = [0.986, math.sqrt(1-0.986**2), 511, 2.44, 40.7, 0.18, -744, 5.17, -618, 0.82]
#BenchmarkPoints["1701.04442_B8max"] = [0.992, math.sqrt(1-0.992**2), 604, 2.82, 36.4, 0.18, -898, 7.36, -424, 0.28]
#BenchmarkPoints["1701.04442_B12max"] = [0.994, math.sqrt(1-0.994**2), 840, 4.03, 21.7, 0.19, -988, 9.22, 356, 0.83]


# Our own benchmark points:
OurBenchmarkPoints = {}
#OurBenchmarkPoints["Test1"] = [246., 28.73104102624717, -8502.853839569823, 82218.30712566273,  -90.01898849641022, 0.9630232407603962, -256.63996730948486, 0.8036145723919945, 0.3496081158172681, 0.19551182514516174]
#OurBenchmarkPoints["Test2"] = [246., 38.33684785322958, -6587.397065540476, -3691.5394814469805,-87.080774337433, 2.4012672347502297, -48.86493693457646,  0.9858633109040484, 0.26570885900263347, 0.005376582674682184]
#OurBenchmarkPoints["Test3"] = [246., 5.65890561098029, -12106.838691305065, 329482.4124451493, -135.29550565863224, 0.8298442357440221, 347.28060190349277, 0.9422293921652389, 0.3388917549894227, 0.09094003321761433]
#OurBenchmarkPoints["Test4"] = [246., 27.23549843333949, -10223.662333809232, 172406.00093719576, -222.71088105447677, 1.5357924759301493, 248.84527364483927, 0.41163663024272745, 0.4236174932730022, 0.20078798963249367]
#OurBenchmarkPoints["Test5"] = [246., 13.93050742349821, -19442.79727868895, 460920.08832155436, -126.24641642233114, 2.5649942281785147, -314.8808356947973, 0.08440290415257423, 0.2482519295016067, 0.05680079520654735]
#OurBenchmarkPoints["Test6"] = [246., 14.968096932303679, -16062.496157302381, 743164.2490181373, -178.20737042777145, 0.9781188521256698, -346.94200265031577, 0.8090942816781423, 0.3314247301956522, 0.059555193128321283]

MaxPoints = 1000 # the maximum number of points to read in

# add points to analyze in detail randomly
NRandomSelection = 400 # how many points to randomly select
ProbRandomSelection = 1.2*float(NRandomSelection)/float(MaxPoints) #  the probability for a point to be randomly selected
random.seed(1263) # set the seed for the random numbers
NRandomCounter = {} # the counters for the randomly-selected points
EnableRandomSelection = False # whether to enable the random selection or not

# read the benchmarks from GW's files:
#BenchmarkDir = "GWpoints/"
#BenchmarkFile_CorrectMass = "parameters_with_correct_mass.m"
#OurBenchmarkPoints_CorrectMass = ReadGWBenchmarks(BenchmarkDir + BenchmarkFile_CorrectMass, "GWAP", MaxPoints)
#OurBenchmarkPoints.update(OurBenchmarkPoints_CorrectMass)


# CHOOSE CATEGORY
category = 1

# read the benchmarks from the processed files:
# NOTE THAT THE ORDER IS DIFFERENT THAN THE .m FILES ABOVE BUT THIS IS TAKEN CARE OF BY THE
# READING FUNCTION!
ProcessedBenchmarkDir = "../pointgenerator"

if category == 0:
    MRMBenchmarkPoints = {}
    #MRMBenchmarkPoints["NAME"] = [ costh, sinth, mass2, gam2, x0  , lamb, a_1, , a_2  , b_3   , b_4]
    MRMBenchmarkPoints["MRMB1max"] = [ 0.976, 0.220, 341.0, 2.42, 257., 0.92, -377., 0.392, -403., 0.77 ]
    MRMBenchmarkPoints["MRMB2max"]= [ 0.982, 0.188, 353.0, 2.17, 265., 0.99, -400., 0.446, -378., 0.69 ]
    MRMBenchmarkPoints["MRMB3max"] = [ 0.983, 0.181, 415.0, 1.59, 54.6, 0.17, -642., 3.80, -214., 0.16 ]
    MRMBenchmarkPoints["MRMB4max"] = [ 0.984, 0.176, 455.0, 2.08, 47.4, 0.18, -707., 4.63, -607., 0.85 ]
    MRMBenchmarkPoints["MRMB5max"] = [ 0.986, 0.164, 511.0, 2.44, 40.7, 0.18, -744., 5.17, -618., 0.82 ]
    MRMBenchmarkPoints["MRMB6max"] = [ 0.988, 0.153, 563.0, 2.92, 40.5, 0.19, -844., 5., -151., 0.083 ]
    MRMBenchmarkPoints["MRMB7max"] = [ 0.992, 0.129, 604.0, 2.82, 36.4, 0.18, -898., 7.36, -424., 0.28 ]
    MRMBenchmarkPoints["MRMB8max"] = [ 0.994, 0.113, 662.0, 2.97, 32.9, 0.17, -976., 8.98, -542., 0.53 ]
    MRMBenchmarkPoints["MRMB9max"] = [ 0.993, 0.115, 714.0, 3.27, 29.2, 0.18, -941., 8.28, 497., 0.38 ]
    MRMBenchmarkPoints["MRMB10max"] = [ 0.996, 0.094, 767.0, 2.83, 24.5, 0.17, -920., 9.87, 575., 0.41 ]
    MRMBenchmarkPoints["MRMB11max"] = [ 0.994, 0.105, 840.0, 4.03, 21.7, 0.19, -988., 9.22, 356., 0.83 ]
    MRMBenchmarkPoints["MRMB1min"] = [ 0.999, 0.029, 343.0, 0.041, 105., 0.13, -850., 3.91, -106., 0.29 ]
    MRMBenchmarkPoints["MRMB2min"] = [ 0.973, 0.231, 350.0, 0.777, 225., 0.18, -639., 0.986,-111., 0.97 ]
    MRMBenchmarkPoints["MRMB3min"] = [ 0.980, 0.197, 419.0, 1.32,  234., 0.18, -981., 1.56, 0.42, 0.96 ]
    MRMBenchmarkPoints["MRMB4min"] = [ 0.999, 0.026, 463.0, 0.086, 56.8, 0.13, -763., 6.35, 113., 0.73 ]
    MRMBenchmarkPoints["MRMB5min"] = [ 0.999, 0.035, 545.0, 0.278, 50.2, 0.13, -949., 8.64, 151., 0.57 ]
    MRMBenchmarkPoints["MRMB6min"] = [ 0.999, 0.043, 563.0, 0.459, 33.0, 0.13, -716., 9.25, -448., 0.96 ]
    MRMBenchmarkPoints["MRMB7min"] = [ 0.984, 0.180, 609.0, 4.03,  34.2, 0.22, -822., 4.53, -183., 0.57 ]
    MRMBenchmarkPoints["MRMB8min"] = [ 0.987, 0.161, 676.0, 4.47,  30.3, 0.22, -931., 5.96, -680., 0.43 ]
    MRMBenchmarkPoints["MRMB9min"] = [ 0.990, 0.138, 729.0, 4.22,  27.3, 0.21, -909., 6.15, 603., 0.93 ]
    MRMBenchmarkPoints["MRMB10min"] = [ 0.995, 0.104, 792.0, 3.36, 22.2, 0.18, -936., 9.47, -848., 0.66 ]
    MRMBenchmarkPoints["MRMB11min"] = [ 0.994, 0.105, 841.0, 3.95, 21.2, 0.19, -955., 8.69, 684., 0.53 ]
    for key in list(MRMBenchmarkPoints.keys()):
        OurBenchmarkPoints[key] = ConvertMRMBenchmarkToGWAPBenchmark(MRMBenchmarkPoints[key])


INCLUDE_CENTR = True
INCLUDE_LIB = True
INCLUDE_CONS = True
INCLUDE_UCONS = True
INCLUDE_NOTRANS = False
        
if category == 1:

    if INCLUDE_CENTR is True:
        OurBenchmarkPoints_PROCESSED_CENTR = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints_PROCESSED_CENTR = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CENTR, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_centrist.txt', 'GWAPCentr', MaxPoints)
        OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_CENTR)

    if INCLUDE_LIB is True:
        OurBenchmarkPoints_PROCESSED_LIB = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints_PROCESSED_LIB = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LIB, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_liberal.txt', "GWAPLib", MaxPoints)
        OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_LIB)

    if INCLUDE_CONS is True:
        OurBenchmarkPoints_PROCESSED_CONS = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_CONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_CONS, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_conservative.txt', "GWAPCons", MaxPoints)
        OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_CONS)

    if INCLUDE_UCONS is True:
        OurBenchmarkPoints_PROCESSED_UCONS = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints_PROCESSED_UCONS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_UCONS, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_fascist.txt', "GWAPUCons", MaxPoints)
        OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_UCONS)

    if INCLUDE_NOTRANS is True:
        OurBenchmarkPoints_PROCESSED_NOTRANS = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/results_SET1_run9/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET2_run9/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET3_run9/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET4_run9/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET5_run9/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET6_run9/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET7_run10/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/results_SET8_run10/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints_PROCESSED_NOTRANS = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_NOTRANS, ProcessedBenchmarkDir + '/SET9_run10/pointoutput_notrans.txt', "NoTrans", MaxPoints)
        OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_NOTRANS)

    
elif category == 2:
    OurBenchmarkPoints_PROCESSED_TIGHT = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/SET1_vev10/pointoutput_conservative.txt', 'GWAPTight', MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET2_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET3_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET4_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET5_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET6_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET7_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET8_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)
    OurBenchmarkPoints_PROCESSED_TIGHT = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_TIGHT, ProcessedBenchmarkDir + '/SET9_vev10/pointoutput_conservative.txt', "GWAPTight", MaxPoints)

    OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_TIGHT)
    
    OurBenchmarkPoints_PROCESSED_LOOSE = ReadProcessedBenchmarks(ProcessedBenchmarkDir + '/SET1_vev10/pointoutput_liberal.txt', 'GWAPLoose', MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET2_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET3_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET4_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET5_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET6_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET7_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET8_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints_PROCESSED_LOOSE = AddProcessedBenchmarks(OurBenchmarkPoints_PROCESSED_LOOSE, ProcessedBenchmarkDir + '/SET9_vev10/pointoutput_liberal.txt', "GWAPLoose", MaxPoints)
    OurBenchmarkPoints.update(OurBenchmarkPoints_PROCESSED_LOOSE)


# PLOT OUTPUT DIRECTORY
addtag = ''
outputdirectory = 'plots_' + str(todaysdate) + '_cat' + str(category) + addtag + '/'
pickledir = 'data_' + str(todaysdate) + '_cat' + str(category) + addtag + '/'
# check if the directories exist and create them if not
if not os.path.exists(outputdirectory):
    os.makedirs(outputdirectory)
if not os.path.exists(pickledir):
    os.makedirs(pickledir)

OneLoop_mh1 = {}
OneLoop_mh2 = {}
OneLoop_sintheta = {}
OneLoop_costheta = {}
Tree_l112 = {}
Tree_l122 = {}
Tree_l222 = {}
Tree_l111 = {}
AbsTree_l112 = {}
BR_hh_scatter = {} 
M2_scatter = {}
Constraints = {}
RHO_MAX_scatter = {}
RHO_STD_scatter = {}
RHO_AV_scatter = {}
RHO_STDOVAV_scatter = {}

K1_scatter = {}
K2_scatter = {}
Kappa_scatter = {}
Lambda_scatter = {}
LambdaS_scatter = {}
x0_scatter = {}
MS_scatter = {}
mu2_scatter = {}


BR_HH_scatter = {}
BR_ZZ_scatter = {}
BR_WW_scatter = {}

XS100_HH_scatter = {}
XS100_WW_scatter = {}
XS100_ZZ_scatter = {}
XS100_MAX_scatter = {}
SIGNIF100_HH_scatter = {}
SIGNIF100_WW_scatter = {}
SIGNIF100_ZZ_scatter = {}
SIGNIF100_MAX_scatter = {}
SIGNIF14_HH_scatter = {}
SIGNIF14_WW_scatter = {}
SIGNIF14_ZZ_scatter = {}
SIGNIF14_MAX_scatter = {}
XS27_HH_scatter = {}
XS27_WW_scatter = {}
XS27_ZZ_scatter = {}
SIGNIF27_HH_scatter = {}
SIGNIF27_WW_scatter = {}
SIGNIF27_ZZ_scatter = {}
SIGNIF27_MAX_scatter = {}

FINETUNING_scatter = {}
FINETUNING2_scatter = {}
FINETUNING3_scatter = {}
FINETUNINGMAX_scatter = {}
FINETUNING_1loop_m1_scatter = {}
FINETUNING_1loop_m2_scatter = {}

SIGN_SINTHETA_scatter = {}
DELTAMH1_scatter = {}

DELTAMH2tree_scatter = {}


SIGNIF100_HH_lumi_scatter = {}
SIGNIF100_WW_lumi_scatter = {}
SIGNIF100_ZZ_lumi_scatter = {}
SIGNIF100_MAX_lumi_scatter = {}
SIGNIF27_HH_lumi_scatter = {}
SIGNIF27_WW_lumi_scatter = {}
SIGNIF27_ZZ_lumi_scatter = {}
SIGNIF27_MAX_lumi_scatter = {}
# starting from current constraints only
SIGNIF100_Current_HH_lumi_scatter = {}
SIGNIF100_Current_WW_lumi_scatter = {}
SIGNIF100_Current_ZZ_lumi_scatter = {}
SIGNIF100_Current_MAX_lumi_scatter = {}
SIGNIF27_Current_HH_lumi_scatter = {}
SIGNIF27_Current_WW_lumi_scatter = {}
SIGNIF27_Current_ZZ_lumi_scatter = {}
SIGNIF27_Current_MAX_lumi_scatter = {}
# fine-tuning and starting from current constraints
SIGNIF100_CurrentFT_HH_lumi_scatter = {}
SIGNIF100_CurrentFT_WW_lumi_scatter = {}
SIGNIF100_CurrentFT_ZZ_lumi_scatter = {}
SIGNIF100_CurrentFT_MAX_lumi_scatter = {}
SIGNIF27_CurrentFT_HH_lumi_scatter = {}
SIGNIF27_CurrentFT_WW_lumi_scatter = {}
SIGNIF27_CurrentFT_ZZ_lumi_scatter = {}
SIGNIF27_CurrentFT_MAX_lumi_scatter = {}
# CLIC scatter arrays
CLIC14_HH_scatter = {}
CLIC3_HH_scatter = {}
CLIC_VV_scatter = {}
# MASS FIT AND ERROR
MASSFIT_scatter = {}
DMASSFIT_scatter = {}
# l112 and sintheta fit precision
l112_fit_prec_scatter = {}
sintheta_fit_prec_scatter = {}
# l112 and sintheta fit limits

l112_max_fit_errormass_scatter = {}
l112_min_fit_errormass_scatter = {}
sintheta_max_fit_errormass_scatter = {}
sintheta_min_fit_errormass_scatter = {}

# p p > h eta0 cross section from fit
XS_HETA0_scatter = {}

# the names of the points:
PointName = {}

Lumis = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
Lumis27 = [100., 200., 500., 1000., 5000., 10000., 20000., 30000.]
for Lumi in Lumis:
    SIGNIF100_HH_lumi_scatter[Lumi] = {}
    SIGNIF100_WW_lumi_scatter[Lumi] = {}
    SIGNIF100_ZZ_lumi_scatter[Lumi] = {}
    SIGNIF100_MAX_lumi_scatter[Lumi] = {}
    SIGNIF100_CurrentFT_HH_lumi_scatter[Lumi] = {}
    SIGNIF100_CurrentFT_WW_lumi_scatter[Lumi] = {}
    SIGNIF100_CurrentFT_ZZ_lumi_scatter[Lumi] = {}
    SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi] = {}
    SIGNIF100_Current_HH_lumi_scatter[Lumi] = {}
    SIGNIF100_Current_WW_lumi_scatter[Lumi] = {}
    SIGNIF100_Current_ZZ_lumi_scatter[Lumi] = {}
    SIGNIF100_Current_MAX_lumi_scatter[Lumi] = {}
for Lumi in Lumis27:
    SIGNIF27_HH_lumi_scatter[Lumi] = {}
    SIGNIF27_WW_lumi_scatter[Lumi] = {}
    SIGNIF27_ZZ_lumi_scatter[Lumi] = {}
    SIGNIF27_MAX_lumi_scatter[Lumi] = {}
    SIGNIF27_CurrentFT_HH_lumi_scatter[Lumi] = {}
    SIGNIF27_CurrentFT_WW_lumi_scatter[Lumi] = {}
    SIGNIF27_CurrentFT_ZZ_lumi_scatter[Lumi] = {}
    SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi] = {}
    SIGNIF27_Current_HH_lumi_scatter[Lumi] = {}
    SIGNIF27_Current_WW_lumi_scatter[Lumi] = {}
    SIGNIF27_Current_ZZ_lumi_scatter[Lumi] = {}
    SIGNIF27_Current_MAX_lumi_scatter[Lumi] = {}

for key in list(OurBenchmarkPoints.keys()):
    BenchmarkPoints[key] = ConvertBenchmarks(OurBenchmarkPoints[key])
    #print key, BenchmarkPoints[key]
    # for plotting the mh1:
    TypeTag = ''.join([i for i in key if not i.isdigit()])
    if TypeTag not in list(OneLoop_mh1.keys()):
        SIGN_SINTHETA_scatter[TypeTag] = []
        OneLoop_mh1[TypeTag] = []
        OneLoop_mh2[TypeTag] = []
        OneLoop_sintheta[TypeTag] = []
        OneLoop_costheta[TypeTag] = []
        Tree_l112[TypeTag] = []
        Tree_l111[TypeTag] = []
        Tree_l122[TypeTag] = []
        Tree_l222[TypeTag] = []
        AbsTree_l112[TypeTag] = []
        BR_HH_scatter[TypeTag] = []
        BR_ZZ_scatter[TypeTag] = []
        BR_WW_scatter[TypeTag] = []
        M2_scatter[TypeTag] = []
        Constraints[TypeTag] = []
        RHO_MAX_scatter[TypeTag] = []
        RHO_STD_scatter[TypeTag] = []
        RHO_AV_scatter[TypeTag] = []
        RHO_STDOVAV_scatter[TypeTag] = []
        FINETUNING2_scatter[TypeTag] = []
        FINETUNING_scatter[TypeTag] = []
        FINETUNING3_scatter[TypeTag] = []
        FINETUNINGMAX_scatter[TypeTag] = []
        FINETUNING_1loop_m1_scatter[TypeTag] = []
        FINETUNING_1loop_m2_scatter[TypeTag] = []
        XS100_HH_scatter[TypeTag] = []
        XS100_WW_scatter[TypeTag] = []
        XS100_ZZ_scatter[TypeTag] = []
        SIGNIF100_HH_scatter[TypeTag] = []
        SIGNIF100_WW_scatter[TypeTag] = []
        SIGNIF100_ZZ_scatter[TypeTag] = []
        XS27_HH_scatter[TypeTag] = []
        XS27_WW_scatter[TypeTag] = []
        XS27_ZZ_scatter[TypeTag] = []
        XS100_MAX_scatter[TypeTag] = []
        SIGNIF100_MAX_scatter[TypeTag] = []
        SIGNIF27_MAX_scatter[TypeTag] = []
        SIGNIF27_HH_scatter[TypeTag] = []
        SIGNIF27_WW_scatter[TypeTag] = []
        SIGNIF27_ZZ_scatter[TypeTag] = []
        SIGNIF14_MAX_scatter[TypeTag] = []
        SIGNIF14_HH_scatter[TypeTag] = []
        SIGNIF14_WW_scatter[TypeTag] = []
        SIGNIF14_ZZ_scatter[TypeTag] = []
        DELTAMH1_scatter[TypeTag] = []
        DELTAMH2tree_scatter[TypeTag] = []
        CLIC14_HH_scatter[TypeTag] = []
        CLIC3_HH_scatter[TypeTag] = []
        CLIC_VV_scatter[TypeTag] = []
        K1_scatter[TypeTag] = []
        K2_scatter[TypeTag] = []
        Kappa_scatter[TypeTag] = []
        Lambda_scatter[TypeTag] = []
        LambdaS_scatter[TypeTag] = []
        x0_scatter[TypeTag] = []
        MS_scatter[TypeTag] = []
        mu2_scatter[TypeTag] = []
        MASSFIT_scatter[TypeTag] = []
        DMASSFIT_scatter[TypeTag] = []
        XS_HETA0_scatter[TypeTag] = []
        sintheta_fit_prec_scatter[TypeTag] = []
        l112_fit_prec_scatter[TypeTag] = []
        l112_max_fit_errormass_scatter[TypeTag] = []
        l112_min_fit_errormass_scatter[TypeTag] = []
        sintheta_max_fit_errormass_scatter[TypeTag] = []
        sintheta_min_fit_errormass_scatter[TypeTag] = []
        PointName[TypeTag] = []
        NRandomCounter[TypeTag] = 0
        for Lumi in Lumis:
            SIGNIF100_CurrentFT_HH_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_CurrentFT_WW_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_CurrentFT_ZZ_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_Current_HH_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_Current_WW_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_Current_ZZ_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_HH_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_WW_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag] = []
        for Lumi in Lumis27:
            SIGNIF27_CurrentFT_HH_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_CurrentFT_WW_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_CurrentFT_ZZ_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_Current_HH_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_Current_WW_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_Current_ZZ_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_HH_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_WW_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag] = []
            SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag] = []


# get template for MG5 param card:                
param_card_template = getTemplate("param_card.dat")


# Select only a few points to print detailed info/write param card
WriteBenchmarkSelection = 'GWAPCentr10055'

#BenchmarkSelection = ['GWAPCentr3087', 'GWAPCentr11797', 'GWAPCentr17208', 'GWAPCentr1793', 'GWAPCentr16025', ]#['GWAPUCons219']#['GWAPCons1239']#['GWAPUCons49', 'GWAPCons1009', 'GWAPCons805', 'GWAPCons1204', 'GWAPCons1487', 'GWAPCons1549']

#PAPER BENCHMARKS:
#BenchmarkSelection = ['GWAPUCons49', 'GWAPUCons30', 'GWAPCons805', 'GWAPCons1487', 'GWAPCons1392', 'GWAPCons590', 'GWAPCentr3509', 'GWAPCentr7311', 'GWAPCentr18730', 'GWAPCentr16684', 'GWAPCentr19133', 'GWAPCentr15078']

#Other benchmarks for Pablo:
#BenchmarkSelection = ['GWAPCons1524', 'GWAPCons1148', 'GWAPCons1070']
#BenchmarkSelection = ['GWAPCons1731', 'GWAPCons1743', 'GWAPCentr1408', 'GWAPCentr422', 'GWAPCentr1972', 'GWAPCons194']
#BenchmarkSelection = ['GWAPCentr422', 'GWAPCons85', 'GWAPCentr10978', 'GWAPCentr1295', 'GWAPCentr8698', 'GWAPCentr617', 'GWAPCons145']
#BenchmarkSelection = ['GWAPCentr19047', 'GWAPCentr19493', 'GWAPCons1847', 'GWAPCentr15062']


# maximum h heta0 * BR(ZZ) benchmark:
BenchmarkSelection = ['GWAPCentr10055']

# a bigger selection of benchmarks:
#BenchmarkSelection = ['GWAPUCons49', 'GWAPUCons30', 'GWAPCons805', 'GWAPCons1487', 'GWAPCons1392', 'GWAPCons590', 'GWAPCentr3509', 'GWAPCentr7311', 'GWAPCentr18730', 'GWAPCentr16684', 'GWAPCentr19133', 'GWAPCentr15078', 'GWAPCons1524', 'GWAPCons1148', 'GWAPCons1070', 'GWAPCons1731', 'GWAPCons1743', 'GWAPCentr1408', 'GWAPCentr422', 'GWAPCentr1972', 'GWAPCons194', 'GWAPCentr422', 'GWAPCons85', 'GWAPCentr10978', 'GWAPCentr1295', 'GWAPCentr8698', 'GWAPCentr617', 'GWAPCons145', 'GWAPCentr19047', 'GWAPCentr19493', 'GWAPCons1847', 'GWAPCentr15062', 'GWAPCentr14227', 'GWAPCentr10055', 'NoTrans1358', 'NoTrans5858', 'NoTrans5859', 'NoTrans8570', 'NoTrans1132', 'NoTrans5857', 'NoTrans2440', 'NoTrans7731', 'NoTrans258', 'NoTrans326', 'NoTrans983', 'NoTrans1599', 'NoTrans1727', 'NoTrans16', 'NoTrans45', 'NoTrans430', 'NoTrans5196', 'NoTrans5686', 'NoTrans2583', 'NoTrans3538', 'NoTrans3806', 'NoTrans5746', 'NoTrans4509', 'NoTrans5009', 'NoTrans1651', 'NoTrans2214', 'NoTrans156', 'NoTrans836', 'NoTrans899', 'NoTrans1041', 'NoTrans1404', 'NoTrans573', 'NoTrans1277', 'NoTrans8644', 'NoTrans6833', 'NoTrans5565', 'NoTrans1122', 'NoTrans416', 'NoTrans0', 'NoTrans1', 'NoTrans12', 'NoTrans25', 'NoTrans38', 'NoTrans46', 'NoTrans47', 'NoTrans64', 'NoTrans80', 'GWAPCons74', 'GWAPCons248', 'GWAPCons620', 'GWAPCons689', 'GWAPCons871']

#BenchmarkSelection = ['NoTrans1358', 'NoTrans5858', 'NoTrans5859', 'NoTrans8570', 'NoTrans1132', 'NoTrans5857', 'NoTrans2440', 'NoTrans7731', 'NoTrans258', 'NoTrans326', 'NoTrans983', 'NoTrans1599', 'NoTrans1727', 'NoTrans16', 'NoTrans45', 'NoTrans430', 'NoTrans5196', 'NoTrans5686']

#BenchmarkSelection = ['NoTrans0', 'NoTrans12', 'NoTrans46', 'NoTrans64', 'NoTrans1132']

#BenchmarkSelection = ['GWAPCons1743']

Benchmark_NoMassFit = []

# File to write benchmark point info
benchmark_outfile = 'benchmark_info_widerselection6.txt'
bprint = open(benchmark_outfile, "w")

# Write an mg5 card to run a specific process (e.g. g g > h eta0 [QCD])
writemg5gen = True
writemg5process = 'gg_heta0'
writemg5file = 'run_gg_heta0_widerselection6.script'
writemg5nevents = '1000'
mg5print = open(writemg5file, 'w')
if writemg5gen is True:
    print('Writing the mg5 runcard:', writemg5file ,'for the process:', writemg5process, end=' ') 
    mg5print.write('launch ' + writemg5process + ' -i\n\n')

###################################################
# DO THE FIT FOR THE g g > h eta0 [QCD] process:  #
###################################################
Ai, Bi, Ci, Di, Ei, Fi = do_Fit_heta0()
# the cross section can then be calculate by: func_t((k112real,k122real), Ai(m2real), Bi(m2real), Ci(m2real), Di(m2real), Ei(m2real), Fi(m2real), streal, ctreal)
# where "real" are the point values

# open a file to write the cross sections for the h eta0 process:
hetafile = 'xsec_gg_heta0.txt'
xsecheta = open(hetafile, 'w')

# find the maximum g g > h eta0 [QCD] > h Z Z cross section:
sigma_heta0_hZZ_fit_max = -1.
sigma_heta0_hZZ_fit_max_key = ''

#######################################################
# CALCULATION FOR heavy scalar parameters starts here #
#######################################################


print('Constraint calculator for SM+real singlet scalar')
print('---')




# remove points outside the region of interest for mh2
removed_point = 0
for key in list(BenchmarkPoints.keys()):
    #print "benchmark point:", key
    rho_max = OurBenchmarkPoints[key][-3]
    rho_av = OurBenchmarkPoints[key][-2]
    rho_std = OurBenchmarkPoints[key][-1]
    # Set the parameters via a function
    # get the parameters from our benchmark in the SARAH format:
    v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta = GetSARAHParams(OurBenchmarkPoints[key])
    
    # get the cosine:
    costheta = math.sqrt(1-sintheta**2)
    
    # convert the SARAH parameters to MRM: 
    lam, v0, x0, a1, a2, b2, b3, b4 = param_SARAH_to_MRM(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH)

    # TESTING THE ONE-LOOP MASSES:
    mh1_1loop, mh2_1loop = OneLoop_masses_diag_scale_SARAH(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    # scale variation of the one-loop massses:
    mh1_1loop_at2mz, mh2_1loop_at2mz = OneLoop_masses_diag_scale_SARAH(2*91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    mh1_1loop_athalfmz, mh2_1loop_athalfmz = OneLoop_masses_diag_scale_SARAH(0.5*91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    # calculate the difference beteween mh1(2*mz) and mh1(0.5*mz)
    delta_mh1_mzvar = abs(mh1_1loop_athalfmz-mh1_1loop_at2mz)

    
    
    # check the sign of sintheta
    tree_sign = tree_sign_sintheta(v0, x0, a1, a2, mh1_1loop, mh2_1loop)
    #print 'tree_sign_sintheta=', tree_sign

    # change the sign of sintheta:
    sintheta = sintheta*tree_sign

    # calculate the fine tuning for the mass m1, m2:
    ft = finetuning_m1(lam, v0, x0, a1, a2, b3, b4)
    ft2 = finetuning_m2(lam, v0, x0, a1, a2, b3, b4)
    ft3 = finetuning_mu2b2(lam, v0, x0, a1, a2, b3, b4)

    # one-loop fine tuning:
    ft_mh1_oneloop, ft_mh2_oneloop = finetuning_oneloop(91., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)


    # get the tree-level 1-1-2 coupling:
    l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l222 = lambda222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
    l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)

    # print the parameters
    #print_xsm_params(v0, x0, a1, a2, b3, b4)
    # convert the SM BRs and width to Heavy Higgs BRs and width for the model parameters:
    # BR positions: = [ '$b\\bar{b}$', '$\\tau \\tau$', '$\\mu \\mu$', '$c\\bar{c}$', '$s\\bar{s}$', '$t\\bar{t}$', '$gg$', '$\\gamma\\gamma$', '$Z \\gamma$', '$WW$', '$ZZ$', '$h_1 h_1$', '$h_1 h_1 h_1$', '$\\Gamma$' ]
    
    calculate_tripe_Higgs_width = False # whether to calculate the triple Higgs h2 -> h1 h1 h1 width (SLOW!)
    HeavyHiggsBRs, mh1, mh2, Gam1, Gam2, BR_hh, name = convert_to_heavy_withtripleHiggs_OneLoop(BR_interpolators_SM, key, v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, costheta, calculate_tripe_Higgs_width)
    
   
    mh1_1loop_atMH, mh2_1loop_atMH = OneLoop_masses_diag_scale_SARAH(125., v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)
    mh1_1loop_atMt, mh2_1loop_atMt = OneLoop_masses_diag_scale_SARAH(mt, v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt) 
    mh1_1loop_atv0, mh2_1loop_atv0 = OneLoop_masses_diag_scale_SARAH(v0, v0, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Lambda_SARAH, LambdaS_SARAH, Kappa_SARAH, g1, g2, yt)

    # remove points outside certain range of interest. 
    if mh2 >= 1000. or mh2 < 200.:
        removed_point = removed_point+1
        continue

    # get the tree-level masses for comparison
    mh1_tree, mh2_tree, sintheta_tree, costheta_tree = mass_and_mixing(lam, v0, x0, a1, a2, b3, b4)
    mh1_tree_MRM, mh2_tree_MRM = mass_and_mixing_MRM(lam, v0, x0, a1, a2, b3, b4)
    delta_mh2_treevsloop = abs(mh2_1loop-mh2_tree)
        
    # print the parameters and masses
    #print_xsm_params_and_masses(lam, v0, x0, a1, a2, b3, b4, round_sig(mh1_tree,5), round_sig(mh2_tree,5), round_sig(mh1_1loop,5), round_sig(mh2_1loop,5))
    # WARNING! WARNING!
    # RESET THE HIGGS MASS AND WIDTH TO THE SM!
    # WARNING! WARNING!
    mh1 = 125.1
    #Gam1 = 0.00411

    # get the light Higgs BRs as well -- rescaled by costheta:
    LightHiggsBRs = []
    for hh in range(len(BR_interpolators_SM)):
        LightHiggsBRs.append(BR_interpolators_SM[hh](mh1) * costheta**2)
        
    # get the 13 TeV N3LO cross section for h2:
    #xs13_n3lo = round_sig(sintheta**2 * XS_interpolator_SM_13TeV_N3LO(mh2),5)
    xs13_n3lo = round_sig(sintheta**2 * XS_interpolator_SM_13TeV_NNLONNLL(mh2),5)
    
    # get the 14, 8, 7 TeV NNLO+NNLL cross sections for h2:
    xs14_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_14TeV_NNLONNLL(mh2),5)
    xs8_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_8TeV_NNLONNLL(mh2),5)
    xs7_nnlonnll = round_sig(sintheta**2 * XS_interpolator_SM_7TeV_NNLONNLL(mh2),5)
    # get the 100 TeV cross sections:
    xs100_n3lon3ll = round_sig(sintheta**2 * XS_interpolator_SM_100TeV_N3LON3LL(mh2),5)
    xs100_vbf_nlo = round_sig(sintheta**2 * XS_interpolator_SM_100TeV_VBF_NLO(mh2),5)
    # get the 27 TeV cross sections:
    xs27_n3lon3ll = round_sig(sintheta**2 * XS_interpolator_SM_27TeV_N3LON3LL(mh2),5)

    # get the EWPO constraints:
    chisq_EWPO_cur = get_chisq_EWPO(mh1, mh2, sintheta, mz, mw, Delta_S_central, Delta_T_central, errS, errT, covST)
    chisq_EWPO_fut = get_chisq_EWPO(mh1, mh2, sintheta, mz, mw, Delta_S_central_F, Delta_T_central_F, errS_F, errT_F, covST_F)
    # check the "current" ATLAS+CMS constraints (1909.02845 and CMS HIG-19-005-PAS):
    # the central value is above mu = 1 so the current constraint is stronger than expected.
    # for HL-LHC studies see: ATL-PHYS-PUB-2014-016, CMS-FTR-011-PAS
    if sintheta**2 > 0.05:
        couplstr_cur = 0
    else:
        couplstr_cur = 1
    # FCC-hh and FCC-ee give similar constraints (see FCC Physics p. 53, table 4.4)    
    if sintheta**2 > 0.01:
        couplstr_fut = 0
    else:
        couplstr_fut = 1
    
    # get the sigma(h2) * BR for the processes to check:
    BR_ZZ = HeavyHiggsBRs[10]
    BR_WW = HeavyHiggsBRs[9]
    BR_hh = HeavyHiggsBRs[11]
    BR_hhh = HeavyHiggsBRs[12]
    xs13_ZZ = xs13_n3lo * BR_ZZ
    xs13_WW = xs13_n3lo * BR_WW
    xs14_WW = xs14_nnlonnll * BR_WW
    xs13_HH = xs13_n3lo * BR_hh
    xs100_WW = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_WW
    xs100_ZZ = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_ZZ
    xs100_HH = (xs100_n3lon3ll+xs100_vbf_nlo) * BR_hh
        
    # get the sigma(h2) * BR (GGF only) for 27 TeV:
    xs27_WW = (xs27_n3lon3ll) * BR_WW
    xs27_ZZ = (xs27_n3lon3ll) * BR_ZZ
    xs27_HH = (xs27_n3lon3ll) * BR_hh

    # get the HL-LHC projections:
    limit_HLLHC_ZZ_central, limit_HLLHC_ZZ_1sigma, limit_HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag = get_xsec_limit_future(mh2, HLLHC_ZZ_central, HLLHC_ZZ_1sigma, HLLHC_ZZ_2sigma, HLLHC_ZZ_process_tag)
    limit_HLLHC_WW_central, limit_HLLHC_WW_1sigma, limit_HLLHC_WW_2sigma, HLLHC_WW_process_tag = get_xsec_limit_future(mh2, HLLHC_WW_central, HLLHC_WW_1sigma, HLLHC_WW_2sigma, HLLHC_WW_process_tag)
    
    # current ATLAS/CMS HH limits
    limit_ATLAS_HH_central, ATLAS_HH_process_tag = get_xsec_limit_current(mh2, ATLAS_HH_central, ATLAS_HH_process_tag) # 27.5-36.1/fb
    limit_CMS_HH_central, CMS_HH_process_tag = get_xsec_limit_current(mh2, CMS_HH_central, CMS_HH_process_tag) # 35.9/fb

    # current ATLAS WW/CMS ZZ limits
    limit_ATLAS_WW_central, ATLAS_WW_process_tag = get_xsec_limit_current(mh2, ATLAS_WW_central, ATLAS_WW_process_tag) # 36.1/fb
    limit_CMS_ZZ_central, CMS_ZZ_process_tag = get_xsec_limit_current(mh2, CMS_ZZ_central, CMS_ZZ_process_tag) # 35.9/fb

    # extrapolate naively to HL-LHC 3000/fb
    limit_ATLAS_HH_extrap = limit_ATLAS_HH_central * math.sqrt(36.1/3000.)
    limit_CMS_HH_extrap = limit_CMS_HH_central * math.sqrt(35.9/3000.)
    limit_ATLAS_WW_extrap = limit_ATLAS_WW_central * math.sqrt(36.1/3000.)
    limit_CMS_ZZ_extrap = limit_CMS_ZZ_central * math.sqrt(35.9/3000.)
    limit_LHC_HH_extrap = min(limit_ATLAS_HH_extrap, limit_CMS_HH_extrap)

    SIGNIF14_HH = xs13_HH/limit_LHC_HH_extrap
    SIGNIF14_WW = xs13_WW/limit_ATLAS_WW_extrap
    SIGNIF14_ZZ = xs13_ZZ/limit_CMS_ZZ_extrap
    SIGNIF14_MAX = max(SIGNIF14_WW, SIGNIF14_HH, SIGNIF14_ZZ)

    # get the FCC 100 TeV limits (30/ab):
    limit_FCC_WW_central, limit_FCC_WW_1sigma, limit_FCC_WW_2sigma, FCC_WW_process_tag = get_xsec_limit_future(mh2, FCC_WW_central, FCC_WW_1sigma, FCC_WW_2sigma, FCC_WW_tag)
    limit_FCC_ZZ_central, limit_FCC_ZZ_1sigma, limit_FCC_ZZ_2sigma, FCC_ZZ_process_tag = get_xsec_limit_future(mh2, FCC_ZZ_central, FCC_ZZ_1sigma, FCC_ZZ_2sigma, FCC_ZZ_tag)
    limit_FCC_HH_central, limit_FCC_HH_1sigma, limit_FCC_HH_2sigma, FCC_HH_process_tag = get_xsec_limit_future(mh2, FCC_HH_central, FCC_HH_1sigma, FCC_HH_2sigma, FCC_HH_tag)
    # and at different luminosities:
    signif100_ZZ_lumi = {}
    signif100_HH_lumi = {}
    signif100_WW_lumi = {}
    signif100_MAX_lumi = {}
    limit_FCC_HH_lumi_array = {}
    limit_FCC_HH_lumi_tag = {}
    limit_FCC_WW_lumi_array = {}
    limit_FCC_WW_lumi_tag = {}
    limit_FCC_ZZ_lumi_array = {}
    limit_FCC_ZZ_lumi_tag = {}
    for Lumi in Lumis:
        limit_FCC_HH_lumi_array[Lumi], limit_FCC_HH_lumi_tag[Lumi] = get_xsec_limit_current(mh2, FCC_HH_lumi[Lumi], FCC_HH_lumi_tag[Lumi])
        signif100_HH_lumi[Lumi] = xs100_HH * 2 / limit_FCC_HH_lumi_array[Lumi]
        limit_FCC_WW_lumi_array[Lumi], limit_FCC_WW_lumi_tag[Lumi] = get_xsec_limit_current(mh2, FCC_WW_lumi[Lumi], FCC_WW_lumi_tag[Lumi])
        signif100_WW_lumi[Lumi] = xs100_WW * 2 / limit_FCC_WW_lumi_array[Lumi]
        limit_FCC_ZZ_lumi_array[Lumi], limit_FCC_ZZ_lumi_tag[Lumi] = get_xsec_limit_current(mh2, FCC_ZZ_lumi[Lumi], FCC_ZZ_lumi_tag[Lumi])
        signif100_ZZ_lumi[Lumi] = xs100_ZZ * 2 / limit_FCC_ZZ_lumi_array[Lumi]
        # get the maximum significance for each point at each lumi
        signif100_MAX_lumi[Lumi] = max([signif100_ZZ_lumi[Lumi], signif100_HH_lumi[Lumi], signif100_WW_lumi[Lumi]])
    # 27 TeV limits (15/ab):
    Lumi27 = 15000.
    limit_HELHC_HH_GGF_central, HELHC_HH_GGF_process_tag = get_xsec_limit_current(mh2, HE_HH_lumi_GGF[Lumi27], HE_HH_lumi_GGF_tag[Lumi27])
    limit_HELHC_HH_QQ_central, HELHC_HH_QQ_process_tag = get_xsec_limit_current(mh2, HE_HH_lumi_QQ[Lumi27], HE_HH_lumi_QQ_tag[Lumi27])
    limit_HELHC_ZZ_GGF_central, HELHC_ZZ_GGF_process_tag = get_xsec_limit_current(mh2, HE_ZZ_lumi_GGF[Lumi27], HE_ZZ_lumi_GGF_tag[Lumi27])
    limit_HELHC_ZZ_QQ_central, HELHC_ZZ_QQ_process_tag = get_xsec_limit_current(mh2, HE_ZZ_lumi_QQ[Lumi27], HE_ZZ_lumi_QQ_tag[Lumi27])
    limit_HELHC_WW_GGF_central, HELHC_WW_GGF_process_tag = get_xsec_limit_current(mh2, HE_WW_lumi_GGF[Lumi27], HE_WW_lumi_GGF_tag[Lumi27])
    limit_HELHC_WW_QQ_central, HELHC_WW_QQ_process_tag = get_xsec_limit_current(mh2, HE_WW_lumi_QQ[Lumi27], HE_WW_lumi_QQ_tag[Lumi27])
    
    # and at different luminosities:
    limit_HELHC_HH_GGF_lumi = {}
    HELHC_HH_GGF_process_tag_lumi = {}
    limit_HELHC_HH_QQ_lumi = {}
    HELHC_HH_QQ_process_tag_lumi = {}
    limit_HELHC_ZZ_GGF_lumi = {}
    HELHC_ZZ_GGF_process_tag_lumi = {}
    limit_HELHC_ZZ_QQ_lumi = {}
    HELHC_ZZ_QQ_process_tag_lumi = {}
    limit_HELHC_WW_GGF_lumi = {}
    HELHC_WW_GGF_process_tag_lumi = {}
    limit_HELHC_WW_QQ_lumi = {}
    HELHC_WW_QQ_process_tag_lumi = {}
    signif27_ZZ_lumi = {}
    signif27_WW_lumi = {}
    signif27_HH_lumi = {}
    signif27_MAX_lumi = {}
    for Lumi27 in Lumis27:
        limit_HELHC_HH_GGF_lumi[Lumi27], HELHC_HH_GGF_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_HH_lumi_GGF[Lumi27], HE_HH_lumi_GGF_tag[Lumi27])
        limit_HELHC_HH_QQ_lumi[Lumi27], HELHC_HH_QQ_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_HH_lumi_QQ[Lumi27], HE_HH_lumi_QQ_tag[Lumi27])
        limit_HELHC_ZZ_GGF_lumi[Lumi27], HELHC_ZZ_GGF_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_ZZ_lumi_GGF[Lumi27], HE_ZZ_lumi_GGF_tag[Lumi27])
        limit_HELHC_ZZ_QQ_lumi[Lumi27], HELHC_ZZ_QQ_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_ZZ_lumi_QQ[Lumi27], HE_ZZ_lumi_QQ_tag[Lumi27])
        limit_HELHC_WW_GGF_lumi[Lumi27], HELHC_WW_GGF_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_WW_lumi_GGF[Lumi27], HE_WW_lumi_GGF_tag[Lumi27])
        limit_HELHC_WW_QQ_lumi[Lumi27], HELHC_WW_QQ_process_tag_lumi[Lumi27] = get_xsec_limit_current(mh2, HE_WW_lumi_QQ[Lumi27], HE_WW_lumi_QQ_tag[Lumi27])
        signif27_ZZ_lumi[Lumi27] = min([xs27_ZZ * 2 / limit_HELHC_ZZ_GGF_lumi[Lumi27], xs27_ZZ * 2 / limit_HELHC_ZZ_QQ_lumi[Lumi27]])
        signif27_WW_lumi[Lumi27] = min([xs27_WW * 2 / limit_HELHC_WW_GGF_lumi[Lumi27], xs27_WW * 2 / limit_HELHC_WW_QQ_lumi[Lumi27]])
        signif27_HH_lumi[Lumi27] = min([xs27_HH * 2 / limit_HELHC_HH_GGF_lumi[Lumi27], xs27_HH * 2 / limit_HELHC_HH_QQ_lumi[Lumi27]])
        signif27_MAX_lumi[Lumi27] = max([signif27_ZZ_lumi[Lumi27], signif27_HH_lumi[Lumi27], signif27_WW_lumi[Lumi27]])


    #################################################
    ############ SINGLET INVERT HERE ################
    #################################################

    # do the mass fit at L=30000?
    do_invert_fit = True
    Mfit = -1.
    DMfit = -1.
    l112_fit_prec = -1.
    sintheta_fit_prec = -1.
    l112_min_fit_errormass = -1.
    l112_max_fit_errormass = -1.
    sintheta_min_fit_errormass = -1.
    sintheta_max_fit_errormass = -1.
 
    # get the h+eta0 cross section from the fit:
    sigma_heta0_fit = func_t((l112,l122), Ai(mh2), Bi(mh2), Ci(mh2), Di(mh2), Ei(mh2), Fi(mh2), sintheta, costheta)

    #################################################
    ############ SINGLET INVERT HERE ################
    #################################################

    # print info:
    # Select points randomly up to a pre-defined number
    RandomSelection = False
    if EnableRandomSelection is True:
        if random.random() < ProbRandomSelection and NRandomCounter[TypeTag]< NRandomSelection:
            TypeTag = ''.join([i for i in key if not i.isdigit()])
            RandomSelection = True
            print("Selected point randomly")
            NRandomCounter[TypeTag] = NRandomCounter[TypeTag] + 1
            if NRandomCounter[TypeTag] == NRandomSelection:
                print("ALL THE RANDOMLY SELECTED POINTS HAVE BEEN CHOSEN FOR:", TypeTag)
    
    #if key in BenchmarkSelection:
    #if abs(sintheta) < 0.15 and mh2_1loop > 600. and mh2_1loop < 800.:
    printall = False
    #if (printall is True or key in BenchmarkSelection) and abs(l112) < 40. and abs(sintheta) < 0.16 and abs(sintheta) > 0.01 and mh2_1loop < 850.:# and signif100_HH_lumi[Lumi] < 5.0:
    if printall is True or key in BenchmarkSelection or RandomSelection is True:
        print("benchmark point:", key)
        print_SARAH_params_and_masses(v0_SARAH, x0_SARAH, mu2_SARAH, MS_SARAH, K1_SARAH, K2_SARAH, Kappa_SARAH, LambdaS_SARAH, Lambda_SARAH, sintheta, round_sig(mh1_1loop,5), round_sig(mh2_1loop,5))
        print_XSECS_AND_BRS(xs13_n3lo, xs27_n3lon3ll, xs100_n3lon3ll, BR_ZZ, BR_WW, BR_hh)
        print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy_withtripleHiggs, 'Heavy Higgs BRs & width')
        
        #l112 = lambda112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        #l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l2222 = lambda2222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1222 = lambda1222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1122 = lambda1122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1111 = lambda1111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l222 = lambda222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        
        print('signif100_HH_lumi[Lumi]=', signif100_HH_lumi[Lumi])
        print('signif100_ZZ_lumi[Lumi]=', signif100_ZZ_lumi[Lumi])
        print('signif100_WW_lumi[Lumi]=', signif100_WW_lumi[Lumi])
        print('signif100_MAX_lumi[Lumi]=', signif100_MAX_lumi[Lumi])

        # do the mass fit at L=LumiFit
        LumiFit = 30000.
        signif_cutoff_ZZ4l = 3.0
        signif_cutoff_HH = 3.0
        signif_cutoff_ZZ2l2v = 3.0

        if do_invert_fit is True:
            Mfit_ZZ4l, DMfit_ZZ4l = get_m4l_fit(mh2, LumiFit, sintheta, l112, signif_cutoff_ZZ4l, BR_interpolators_SM)
            Mfit_HH, DMfit_HH = get_mbbaa_fit(mh2, LumiFit, sintheta, l112, signif_cutoff_HH, BR_interpolators_SM)
            print('mh1, mh2, sintheta, l112, signif100_ZZ_lumi[30000.], signif100_HH_lumi[30000.], key=', mh1, mh2, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key)
            print('Mfit, DMfit from ZZ->4l=', Mfit_ZZ4l, DMfit_ZZ4l)
            print('Mfit, DMfit from HH->bbaa=', Mfit_HH, DMfit_HH)

            Mfit = -1
            DMfit = -1
            # check if the mass fits are done correctly and pick the one with the lowest error
            # the fits are acceptable if both masses around the desired mass yield significances of 3 sigma at least. 
            if Mfit_ZZ4l != -2 and Mfit_HH != -2:
                if DMfit_ZZ4l < DMfit_HH:
                    DMfit = DMfit_ZZ4l
                    Mfit = Mfit_ZZ4l
                else:
                    DMfit = DMfit_HH
                    Mfit = Mfit_HH
            elif Mfit_ZZ4l == -2 and Mfit_HH != -2:
                Mfit = Mfit_HH
                DMfit = DMfit_HH
            elif Mfit_HH == -2 and Mfit_ZZ4l != -2:
                Mfit = Mfit_ZZ4l
                DMfit = DMfit_ZZ4l
            elif Mfit_ZZ4l == -2 and Mfit_HH == -2:
                Mfit = -2
                DMfit = -2
            if Mfit == -2 and DMfit == -2:
                print('HH and ZZ(4l) mass fit failed, trying ZZ2l2v')
                Mfit_ZZ2l2v, DMfit_ZZ2l2v = get_mT_fit_ZZ2l2v(mh2, LumiFit, sintheta, l112, signif_cutoff_ZZ2l2v, BR_interpolators_SM)
                if Mfit_ZZ2l2v != -2 and DMfit_ZZ2l2v != -2:
                    Mfit = Mfit_ZZ2l2v
                    DMfit = DMfit_ZZ2l2v

            print('Mfit, DMfit resulting=', Mfit, DMfit)

            if mh2 > 260. and Mfit > 0.:# and signif100_ZZ_lumi[LumiFit] > 5. and signif100_HH_lumi[LumiFit] > 5.:
                #l112_min_fit, l112_max_fit, sintheta_min_fit, sintheta_max_fit = get_sintheta_l112_fit_fixedmass(mh1, mh2, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key)
                l112_min_fit_errormass, l112_max_fit_errormass, sintheta_min_fit_errormass, sintheta_max_fit_errormass = get_sintheta_l112_fit_errormass_curve(mh1, mh2, DMfit, sintheta, l112, signif100_ZZ_lumi[LumiFit], signif100_HH_lumi[LumiFit], key)
                print('l112_min_fit_errormass, l112_max_fit_errormass, sintheta_min_fit_errormass, sintheta_max_fit_errormass=', l112_min_fit_errormass, l112_max_fit_errormass, sintheta_min_fit_errormass, sintheta_max_fit_errormass)

            else:
                l112_min_fit_errormass = -1.
                l112_max_fit_errormass = -1.
                sintheta_min_fit_errormass = -1.
                sintheta_max_fit_errormass = -1.
            l112_fit_prec = (l112_max_fit_errormass - l112_min_fit_errormass)/(l112_max_fit_errormass + l112_min_fit_errormass)
            sintheta_fit_prec = (sintheta_max_fit_errormass - sintheta_min_fit_errormass) / (sintheta_min_fit_errormass + sintheta_max_fit_errormass)
            if Mfit == -2:
                print('MASS FIT HAS FAILED!')
                Benchmark_NoMassFit.append(key)
        # read the h+eta0 cross section:
        sigma_heta0_real = read_file_single_heta0_norun(key, ProcLocation_heta0)
        print('mh1_tree_MRM, mh2_tree_MRM=', mh1_tree_MRM, mh2_tree_MRM)
        print('tree-level fine tuning (m1)=', ft, '(m2)=', ft2, '(dekens,mu2,b2)=', ft3, 'delta_mh1_mzvar=',delta_mh1_mzvar)
        print('one-loop fine tuning', ft_mh1_oneloop, ft_mh2_oneloop)
        print("rho_max/av/std=", rho_max, rho_av, rho_std)
        print("At One Loop (Sc=MZ): mh1, mh2=", mh1_1loop, mh2_1loop)
        print("At One Loop (Sc=MH): mh1, mh2=", mh1_1loop_atMH, mh2_1loop_atMH)
        print("At One Loop (Sc=MT): mh1, mh2=", mh1_1loop_atMt, mh2_1loop_atMt)
        print("At One Loop (Sc=v0): mh1, mh2=", mh1_1loop_atv0, mh2_1loop_atv0)
        # print xs and xs constraints
        print('xs100_n3lon3ll, xs100_vbf_nlo', xs100_n3lon3ll, xs100_vbf_nlo)
        print('xs13_HH, limit_ATLAS_HH_central=',xs13_HH, limit_ATLAS_HH_central)
        print('xs13_HH, limit_CMS_HH_central=',xs13_HH, limit_CMS_HH_central)
        print('xs13_ZZ, limit_HLLHC_ZZ_central=',xs13_ZZ, limit_HLLHC_ZZ_central)
        print('xs13_WW, limit_HLLHC_WW_central=',xs13_WW, limit_HLLHC_WW_central)
        print('xs13_ZZ, limit_CMS_ZZ_extrap=',xs13_ZZ, limit_CMS_ZZ_extrap)
        print('xs13_WW, limit_ATLAS_WW_extrap=',xs13_WW, limit_ATLAS_WW_extrap)
        print('xs100_WW, limit_FCC_WW_central, ratio=', xs100_WW, limit_FCC_WW_central, xs100_WW/limit_FCC_WW_central)
        print('xs100_ZZ, limit_FCC_ZZ_central, ratio=', xs100_ZZ, limit_FCC_ZZ_central, xs100_ZZ/limit_FCC_ZZ_central)
        print('xs100_HH, limit_FCC_HH_central, ratio=', xs100_HH, limit_FCC_HH_central, xs100_HH/limit_FCC_HH_central)
        print('xs27_WW, limit_HELHC_WW_GGF_central, ratio=', xs27_WW, limit_HELHC_WW_GGF_central, xs27_WW/limit_HELHC_WW_GGF_central)
        print('xs27_ZZ, limit_HELHC_ZZ_GGF_central, ratio=', xs27_ZZ, limit_HELHC_ZZ_GGF_central, xs27_ZZ/limit_HELHC_ZZ_GGF_central)
        print('xs27_HH, limit_HELHC_HH_GGF_central, ratio=', xs27_HH, limit_HELHC_HH_GGF_central,  xs27_HH/limit_HELHC_HH_GGF_central)
        print('xs27_WW, limit_HELHC_WW_QQ_central, ratio=', xs27_WW, limit_HELHC_WW_QQ_central, xs27_WW/limit_HELHC_WW_QQ_central)
        print('xs27_ZZ, limit_HELHC_ZZ_QQ_central, ratio=', xs27_ZZ, limit_HELHC_ZZ_QQ_central, xs27_ZZ/limit_HELHC_ZZ_QQ_central)
        print('xs27_HH, limit_HELHC_HH_QQ_central, ratio=', xs27_HH, limit_HELHC_HH_QQ_central,  xs27_HH/limit_HELHC_HH_QQ_central)
        print('xs27/xs100=', xs27_HH/xs100_HH)

        print('l111, l112, l122, l1112=', l111, l112, l122, l1112)
        print('Mass fit=', Mfit, DMfit)
        print('l112_prec, sintheta_prec=', l112_fit_prec, sintheta_fit_prec)
        #print 'l112_min_fit, l112_max_fit, sintheta_min_fit, sintheta_max_fit=', l112_min_fit_errormass, l112_max_fit_errormass, sintheta_min_fit_errormass, sintheta_max_fit_errormass
        print('g g > h eta0 [QCD] from mg5 =', sigma_heta0_real)
        print('g g > h eta0 [QCD] from fit =', sigma_heta0_fit)
        sigma_heta0_hZZ_fit = sigma_heta0_fit * BR_ZZ 
        print('g g > h (eta0 > ZZ) from fit =', sigma_heta0_hZZ_fit)
        print('\n')
        if sigma_heta0_hZZ_fit_max < sigma_heta0_hZZ_fit:
            sigma_heta0_hZZ_fit_max = sigma_heta0_hZZ_fit
            sigma_heta0_hZZ_fit_max_key = key
        bprint.write(str(key) + '\n')
        bprint.write('mh1_tree_MRM, mh2_tree_MRM= ' + str(mh1_tree_MRM) + " " + str(mh2_tree_MRM) +'\n')
        bprint.write('tree-level fine tuning (m1)= ' + str(ft) + ' (m2)= ' + str(ft2) + ' (dekens,mu2,b2)= ' + str(ft3) + ' delta_mh1_mzvar= ' + str(delta_mh1_mzvar)+'\n')
        bprint.write('one-loop fine tuning ' + str(ft_mh1_oneloop) + '\t' +str(ft_mh2_oneloop)+'\n')
        bprint.write("rho_max/av/std= " + str(rho_max) + " " + str(rho_av) + " " + str(rho_std)+'\n')
        bprint.write("At One Loop (Sc=MZ): mh1, mh2= " + str(mh1_1loop) + " " + str(mh2_1loop)  +'\n')
        bprint.write("At One Loop (Sc=MH): mh1, mh2= " + str(mh1_1loop_atMH) + " " + str(mh2_1loop_atMH) +'\n')
        bprint.write("At One Loop (Sc=MT): mh1, mh2= " + str(mh1_1loop_atMt) + " " + str(mh2_1loop_atMt) +'\n')
        bprint.write("At One Loop (Sc=v0): mh1, mh2= " + str(mh1_1loop_atv0) + " " + str(mh2_1loop_atv0) +'\n')
        # print xs and xs constraints
        bprint.write('xs13_HH, limit_ATLAS_HH_central= ' + str(xs13_HH) + " " + str(limit_ATLAS_HH_central) +'\n')
        bprint.write('xs13_HH, limit_CMS_HH_central= ' + str(xs13_HH) + " " + str(limit_CMS_HH_central) +'\n')
        bprint.write('xs13_ZZ, limit_HLLHC_ZZ_central= ' + str(xs13_ZZ) + " " + str(limit_HLLHC_ZZ_central) +'\n')
        bprint.write('xs13_WW, limit_HLLHC_WW_central= ' + str(xs13_WW) + " " + str(limit_HLLHC_WW_central) +'\n')
        bprint.write('xs13_ZZ, limit_CMS_ZZ_extrap= ' + str(xs13_ZZ)  + " " + str(limit_CMS_ZZ_extrap)  +'\n')
        bprint.write('xs13_WW, limit_ATLAS_WW_extrap= ' + str(xs13_WW)  + " " + str(limit_ATLAS_WW_extrap) +'\n')
        bprint.write('xs100_WW, limit_FCC_WW_central= ' + str(xs100_WW) + " " + str(limit_FCC_WW_central) +'\n')
        bprint.write('xs100_ZZ, limit_FCC_ZZ_central= ' + str(xs100_ZZ) + " " + str(limit_FCC_ZZ_central) +'\n')
        bprint.write('xs100_HH, limit_FCC_HH_central=' + str(xs100_HH) + " " + str(limit_FCC_HH_central) +'\n')
        bprint.write('signif100_HH_lumi[Lumi]= ' + str(signif100_HH_lumi[Lumi]) +'\n')
        bprint.write('signif100_ZZ_lumi[Lumi]= ' + str(signif100_ZZ_lumi[Lumi]) +'\n')
        bprint.write('signif100_WW_lumi[Lumi]=' + str(signif100_WW_lumi[Lumi]) +'\n')
        bprint.write('signif100_MAX_lumi[Lumi]= ' + str(signif100_MAX_lumi[Lumi]) +'\n')
        bprint.write('l111, l112, l122, l1112= ' + str(l111) + " " + str(l112)  + " " + str(l122) + " " + str(l1112) +'\n')
        bprint.write('g g > h eta0 [QCD] from mg5= ' + str(sigma_heta0_real) +'\n')
        bprint.write('g g > h eta0 [QCD] from fit = ' + str(sigma_heta0_fit) +'\n')
        bprint.write('\n\n\n')
        # write out the xsec for g g > h eta0 [QCD] coming from fit, the BR(h2->ZZ), the real h eta0 cross section and the % error from the fit
        xsecheta.write(key + '\t' + str(sigma_heta0_fit) + '\t' + str(BR_ZZ) + '\t' + str(sigma_heta0_real) + '\t' + str(100.*abs(sigma_heta0_fit-sigma_heta0_real)/sigma_heta0_real) + '\n')

        if key in BenchmarkSelection and writemg5gen is True:
            mg5print.write('launch run_' + key + '\n0\n')
            mg5print.write('set Meta ' + str(mh2) + '\n')
            mg5print.write('set Weta ' + str(Gam2) + '\n')
            mg5print.write('set MH ' + str(mh1) + '\n')
            mg5print.write('set WH ' + str(Gam1) + '\n')
            mg5print.write('set ctheta ' + str(costheta) + '\n')
            mg5print.write('set stheta ' + str(sintheta) + '\n')
            mg5print.write('set kap111 ' + str(l111) + '\n')
            mg5print.write('set kap112 ' + str(l112) + '\n')
            mg5print.write('set kap122 ' + str(l122) + '\n')
            mg5print.write('set kap222 ' + str(l222) + '\n')
            mg5print.write('set kap1111 ' + str(l1111) + '\n')
            mg5print.write('set kap1112 ' + str(l1112) + '\n')
            mg5print.write('set kap1122 ' + str(l1122) + '\n')
            mg5print.write('set kap1222 ' + str(l1222) + '\n')
            mg5print.write('set kap2222 ' + str(l2222) + '\n')
            mg5print.write('set nevents ' + str(writemg5nevents) + '\n\n')

        #print 'l112, sintheta fits, fixed mass=', l112_min_fit, l112_max_fit, sintheta_min_fit, sintheta_max_fit
        print('l112, sintheta fit precisions=', l112_fit_prec, sintheta_fit_prec)

        
    # calculate the significance for this point at 100 TeV/30 inv.ab
    signif100_ZZ = xs100_ZZ * 2 / limit_FCC_ZZ_central
    signif100_WW = xs100_WW * 2 / limit_FCC_WW_central
    signif100_HH = xs100_HH * 2 / limit_FCC_HH_central
    signif100_MAX = max([signif100_ZZ, signif100_HH, signif100_WW])

    # calculate the maximum significance for this point at 27 TeV/15 inv.ab
    signif27_ZZ = max([xs27_ZZ * 2 / limit_HELHC_ZZ_GGF_central, xs27_ZZ * 2 / limit_HELHC_ZZ_QQ_central])
    signif27_WW = max([xs27_WW * 2 / limit_HELHC_WW_GGF_central, xs27_WW * 2 / limit_HELHC_WW_QQ_central])
    signif27_HH = max([xs27_HH * 2 / limit_HELHC_HH_GGF_central, xs27_HH * 2 / limit_HELHC_HH_QQ_central])
    signif27_MAX = max([signif27_ZZ, signif27_HH, signif27_WW])
    
    # check cross sections against these limits
    
    # CHECK CURRENT: 
    # ATLAS CURRENT HH:
    if xs13_HH > limit_ATLAS_HH_central:
        ATLAS_HH = 0
    else:
        ATLAS_HH = 1
    # CMS CURRENT HH:
    if xs13_HH > limit_CMS_HH_central:
        CMS_HH = 0
    else:
        CMS_HH = 1

    # CHECK HL-LHC:
    # HL-LHC ZZ:
    if xs13_ZZ > limit_HLLHC_ZZ_central:
        HLLHC_ZZ = 0
    else:
        HLLHC_ZZ = 1
    # HL-LHC WW:
    if xs14_WW > limit_HLLHC_WW_central:
        HLLHC_WW = 0
    else:
        HLLHC_WW = 1
    # ATLAS HH HL-LHC extrap.:
    if xs13_HH > limit_ATLAS_HH_extrap:
        ATLAS_HH_extrap = 0
    else:
        ATLAS_HH_extrap = 1
    # CMS HH HL-LHC extrap.:
    if xs13_HH > limit_CMS_HH_extrap:
        CMS_HH_extrap = 0
    else:
        CMS_HH_extrap = 1
    # ATLAS WW HL-LHC extrap., if mh2 < 550 GeV:
    if mh2 < 550.:
        if xs13_WW > limit_ATLAS_WW_extrap:
            ATLAS_WW_extrap = 0
        else:
            ATLAS_WW_extrap = 1
        # CMS ZZ HL-LHC extrap.:
        if xs13_ZZ > limit_CMS_ZZ_extrap:
            CMS_ZZ_extrap = 0
        else:
            CMS_ZZ_extrap = 1
        HLLHC_ZZ = CMS_ZZ_extrap
        HLLHC_WW = ATLAS_WW_extrap

    # CHECK THE FCC CONSTRAINTS
    if xs100_WW > limit_FCC_WW_central:
        FCC_WW_pass = 0
    else:
        FCC_WW_pass = 1
    if xs100_HH > limit_FCC_HH_central:
        FCC_HH_pass = 0
    else:
        FCC_HH_pass = 1
    if xs100_ZZ > limit_FCC_ZZ_central:
        FCC_ZZ_pass = 0
    else:
        FCC_ZZ_pass = 1
    

    # check the CLIC constraints:
    GammaSM = BR_interpolators_SM[-1](mh2) 
    #check_sthetam2_limit(stheta, m1, m2, l112, GammaSM, limtype, finterp, process_tag)
    CLIC_VV_pass = check_sthetam2_limit(v0, abs(sintheta), mh1, mh2, l112, GammaSM, "VV", CLIC_VV, CLIC_VV_tag) # this is also 3 TeV
    CLIC3_HH_pass = check_sthetam2_limit(v0, abs(sintheta), mh1, mh2, l112, GammaSM, "hh", CLIC3_HH, CLIC3_HH_tag)
    CLIC14_HH_pass = check_sthetam2_limit(v0, abs(sintheta), mh1, mh2, l112, GammaSM, "hh", CLIC14_HH, CLIC3_HH_tag)


    # the point info
    xsm_point = [name, mh1, mh2, Gam1, Gam2, sintheta, costheta, BR_hh, xs13_n3lo, xs14_nnlonnll, xs8_nnlonnll, xs7_nnlonnll, LightHiggsBRs, HeavyHiggsBRs]
    xsm_constraints = [couplstr_cur, couplstr_fut, chisq_EWPO_cur, chisq_EWPO_fut, ATLAS_HH, CMS_HH, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap, CLIC14_HH_pass, CLIC_VV_pass, CLIC3_HH_pass, FCC_WW_pass, FCC_ZZ_pass, FCC_HH_pass, signif100_HH_lumi, signif100_WW_lumi, signif100_ZZ_lumi, signif100_MAX_lumi,  signif27_HH_lumi, signif27_WW_lumi, signif27_ZZ_lumi, signif27_MAX_lumi, signif100_HH, signif100_WW, signif100_ZZ, signif100_MAX, ft]
    
    # add the info to an array:
    xsm_point_info.append(xsm_point)
    xsm_point_constraints.append(xsm_constraints)

    # save the one loop h1 mass/ h2 mass/sintheta and tree-level l112 in a dictionary:
    TypeTag = ''.join([i for i in key if not i.isdigit()])
    PointName[TypeTag].append(key)
    OneLoop_mh1[TypeTag].append(mh1_1loop)
    OneLoop_mh2[TypeTag].append(mh2_1loop)
    OneLoop_sintheta[TypeTag].append(sintheta)
    OneLoop_costheta[TypeTag].append(costheta)
    Tree_l112[TypeTag].append(l112)
    Tree_l122[TypeTag].append(l122)
    Tree_l222[TypeTag].append(l222)
    Tree_l111[TypeTag].append(l111)
    AbsTree_l112[TypeTag].append(abs(l112))
    BR_HH_scatter[TypeTag].append(BR_hh)
    BR_ZZ_scatter[TypeTag].append(BR_ZZ)
    BR_WW_scatter[TypeTag].append(BR_WW)
    XS100_HH_scatter[TypeTag].append(xs100_HH)
    XS100_ZZ_scatter[TypeTag].append(xs100_ZZ)
    XS100_WW_scatter[TypeTag].append(xs100_WW)
    XS100_MAX_scatter[TypeTag].append(max([xs100_HH, xs100_ZZ, xs100_WW]))
    SIGNIF100_HH_scatter[TypeTag].append(signif100_HH)
    SIGNIF100_ZZ_scatter[TypeTag].append(signif100_ZZ)
    SIGNIF100_WW_scatter[TypeTag].append(signif100_WW)
    SIGNIF100_MAX_scatter[TypeTag].append(signif100_MAX)
    SIGNIF27_HH_scatter[TypeTag].append(signif27_HH)
    SIGNIF27_ZZ_scatter[TypeTag].append(signif27_ZZ)
    SIGNIF27_WW_scatter[TypeTag].append(signif27_WW)
    SIGNIF27_MAX_scatter[TypeTag].append(signif27_MAX)
    XS27_HH_scatter[TypeTag].append(xs27_HH)
    XS27_ZZ_scatter[TypeTag].append(xs27_ZZ)
    XS27_WW_scatter[TypeTag].append(xs27_WW)
    M2_scatter[TypeTag].append(mh2_1loop)
    RHO_MAX_scatter[TypeTag].append(rho_max)
    RHO_AV_scatter[TypeTag].append(rho_av)
    RHO_STD_scatter[TypeTag].append(rho_std)
    RHO_STDOVAV_scatter[TypeTag].append(rho_std/rho_av)
    FINETUNING_scatter[TypeTag].append(ft)
    FINETUNING2_scatter[TypeTag].append(ft2)
    FINETUNING3_scatter[TypeTag].append(ft3)
    FINETUNINGMAX_scatter[TypeTag].append(max([ft, ft2, ft3]))

    SIGNIF14_HH_scatter[TypeTag].append(SIGNIF14_HH)
    SIGNIF14_WW_scatter[TypeTag].append(SIGNIF14_WW)
    SIGNIF14_ZZ_scatter[TypeTag].append(SIGNIF14_ZZ)
    SIGNIF14_MAX_scatter[TypeTag].append(SIGNIF14_MAX)

    FINETUNING_1loop_m1_scatter[TypeTag].append(ft_mh1_oneloop)
    FINETUNING_1loop_m2_scatter[TypeTag].append(ft_mh2_oneloop)

    SIGN_SINTHETA_scatter[TypeTag].append(tree_sign)
    DELTAMH1_scatter[TypeTag].append(delta_mh1_mzvar)
    DELTAMH2tree_scatter[TypeTag].append(delta_mh2_treevsloop)
    K1_scatter[TypeTag].append(K1_SARAH)
    K2_scatter[TypeTag].append(K2_SARAH)
    Kappa_scatter[TypeTag].append(Kappa_SARAH)
    Lambda_scatter[TypeTag].append(Lambda_SARAH)
    LambdaS_scatter[TypeTag].append(LambdaS_SARAH)
    x0_scatter[TypeTag].append(x0_SARAH)
    MS_scatter[TypeTag].append(MS_SARAH)
    mu2_scatter[TypeTag].append(mu2_SARAH)

    XS_HETA0_scatter[TypeTag].append(sigma_heta0_fit)

    MASSFIT_scatter[TypeTag].append(Mfit)
    DMASSFIT_scatter[TypeTag].append(DMfit)
    l112_fit_prec_scatter[TypeTag].append(l112_fit_prec)
    sintheta_fit_prec_scatter[TypeTag].append(sintheta_fit_prec)

    l112_max_fit_errormass_scatter[TypeTag].append(l112_max_fit_errormass)
    l112_min_fit_errormass_scatter[TypeTag].append(l112_min_fit_errormass)
    sintheta_max_fit_errormass_scatter[TypeTag].append(sintheta_max_fit_errormass)
    sintheta_min_fit_errormass_scatter[TypeTag].append(sintheta_min_fit_errormass)
    
    # debug signif100_MAX
    if signif100_MAX == 0.0:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! signif100_MAX is ZERO')
    
    #HLLHC_Constraints[TypeTag].append()
    # use the information to write out the param_card.dat
    if key == WriteBenchmarkSelection:
        # write out the parameter card:
        def write_param_card(paramfile, paramsubs):
            writeFile('param_card.dat', param_card_template.substitute(paramsubs) )
        l2222 = lambda2222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1222 = lambda1222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1122 = lambda1122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1112 = lambda1112(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l1111 = lambda1111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l111 = lambda111(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l222 = lambda222(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        l122 = lambda122(lam, a1, a2, x0, v0, costheta, sintheta, b3, b4)
        paramsubs = {
            'CT' : costheta, 
            'ST' : sintheta,
            'M1' : mh1,
            'M2' : mh2,
            'width1': Gam1,
            'width2': Gam2,
            'K111': l111,
            'K112': l112,
            'K122': l122,
            'K222': l222,
            'K1111' : l1111,
            'K1112' : l1112,
            'K1122' : l1122,
            'K1222' : l1222, 
            'K2222' : l2222,
            'lam' : lam,
            'v0' : v0,
            'a1' : a1,
            'a2' : a2,
            'b2' : b2,
            'b3' : b3,
            'b4' : b4,
            'bb' : HeavyHiggsBRs[0],
            'tautau' : HeavyHiggsBRs[1],
            'mumu' : HeavyHiggsBRs[2],
            'cc' : HeavyHiggsBRs[3],
            'ss' : HeavyHiggsBRs[4],
            'tt' : HeavyHiggsBRs[5],
            'gg' : HeavyHiggsBRs[6],
            'gammagamma' : HeavyHiggsBRs[7],
            'Zgamma' : HeavyHiggsBRs[8],
            'WW' : HeavyHiggsBRs[9],
            'ZZ' : HeavyHiggsBRs[10],
            'h1h1' : HeavyHiggsBRs[11],
            'POINTNAME' : key
            }
        write_param_card('param_card.dat', paramsubs)


# print the maximum h eta0 > h Z Z cross section:
print('maximum g g > h eta0 [QCD] > h Z Z cross section=', sigma_heta0_hZZ_fit_max, 'for key', sigma_heta0_hZZ_fit_max_key)

mg5print.close()
bprint.close()
xsecheta.close()

# run and retrieve the HiggsBounds/HiggsSignals:
HB_results, HS_results = get_HiggsBoundsSignals_results('.', 'XSM_1605.06123', xsm_point_info)


# add the info to an array for points that pass the FCC constraints:
xsm_point_info_FCCpass = []
xsm_point_constraints_FCCpass = []


# DEFINE THE MARKER MAPS FOR VARIOUS COLLIDER SCENARIOS
marker_map = {}
marker_map['HL-LHC'] = 'o'
marker_map['HL-LHC-CS'] = '^'
marker_map['CURRENT'] = 'o' #'o'
marker_map['EXCLUDED'] = 'x' #'x'

# now check all constraints together
for p in range(len(xsm_point_info)):
        # xsm point info array
        name =  str(xsm_point_info[p][0])
        mh1 = str(xsm_point_info[p][1])
        mh2 = str(xsm_point_info[p][2])
        G1 = str(xsm_point_info[p][3])
        G2 = str(xsm_point_info[p][4])
        st = str(xsm_point_info[p][5])
        ct = str(xsm_point_info[p][6])
        BR_hh = str(xsm_point_info[p][7])
        xs13 = str(xsm_point_info[p][8])
        xs14 = str(xsm_point_info[p][9])
        xs8 = str(xsm_point_info[p][10])
        xs7 = str(xsm_point_info[p][11])

        # HiggsBounds/HiggsSignals results
        HB = str(HB_results[p])
        HS = str(HS_results[p])

        couplstr_cur = str(xsm_point_constraints[p][0])
        couplstr_fut = str(xsm_point_constraints[p][1])
        EWPOcurr = str( round_sig(xsm_point_constraints[p][2], 4))
        EWPOfut = str(round_sig(xsm_point_constraints[p][3], 5))
        ATLAS_HH = str(xsm_point_constraints[p][4])
        CMS_HH = str(xsm_point_constraints[p][5])
        HLLHC_ZZ = str(xsm_point_constraints[p][6])
        HLLHC_WW = str(xsm_point_constraints[p][7])
        ATLAS_HH_extrap = str(xsm_point_constraints[p][8])
        CMS_HH_extrap = str(xsm_point_constraints[p][9])
        CLIC14_HH_pass = str(xsm_point_constraints[p][10])
        CLIC_VV_pass = str(xsm_point_constraints[p][11])
        CLIC3_HH_pass = str(xsm_point_constraints[p][12])
        FCC_WW = str(xsm_point_constraints[p][13])
        FCC_ZZ = str(xsm_point_constraints[p][14])
        FCC_HH = str(xsm_point_constraints[p][15])
        signif100_HH_L = xsm_point_constraints[p][16]
        signif100_WW_L = xsm_point_constraints[p][17]
        signif100_ZZ_L = xsm_point_constraints[p][18]
        signif100_MAX_L = xsm_point_constraints[p][19]
        signif27_HH_L = xsm_point_constraints[p][20]
        signif27_WW_L = xsm_point_constraints[p][21]
        signif27_ZZ_L = xsm_point_constraints[p][22]
        signif27_MAX_L = xsm_point_constraints[p][23]

        signif100_HH_30 = xsm_point_constraints[p][24]
        signif100_WW_30 = xsm_point_constraints[p][25]
        signif100_ZZ_30 = xsm_point_constraints[p][26]
        signif100_MAX_30 = xsm_point_constraints[p][27]

        finetuning = xsm_point_constraints[p][28]


        TypeTag = ''.join([i for i in name if not i.isdigit()])
        Current_Constraint_pass = 0
        if float(EWPOcurr) < 6.18 and float(ATLAS_HH) == 1 and float(CMS_HH) == 1 and float(HB) == 1 and float(HS) > 0.95 and float(couplstr_cur) ==1:
            Constraint_pass = marker_map['CURRENT']
            print(name, 'passed current constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, couplstr_cur, mh1, mh2)
            if float(mh2) < 1000. and float(mh2) > 200.:
                CLIC_VV_scatter[TypeTag].append(float(CLIC_VV_pass))
                CLIC14_HH_scatter[TypeTag].append(float(CLIC14_HH_pass))
                CLIC3_HH_scatter[TypeTag].append(float(CLIC3_HH_pass))
            for Lumi in Lumis:
                SIGNIF100_Current_HH_lumi_scatter[Lumi][TypeTag].append(signif100_HH_L[Lumi])
                SIGNIF100_Current_WW_lumi_scatter[Lumi][TypeTag].append(signif100_WW_L[Lumi])
                SIGNIF100_Current_ZZ_lumi_scatter[Lumi][TypeTag].append(signif100_ZZ_L[Lumi])
                SIGNIF100_Current_MAX_lumi_scatter[Lumi][TypeTag].append(signif100_MAX_L[Lumi])
            for Lumi in Lumis27:
                SIGNIF27_Current_HH_lumi_scatter[Lumi][TypeTag].append(signif27_HH_L[Lumi])
                SIGNIF27_Current_WW_lumi_scatter[Lumi][TypeTag].append(signif27_WW_L[Lumi])
                SIGNIF27_Current_ZZ_lumi_scatter[Lumi][TypeTag].append(signif27_ZZ_L[Lumi])
                SIGNIF27_Current_MAX_lumi_scatter[Lumi][TypeTag].append(signif27_MAX_L[Lumi])

            if finetuning < 1000.: 
                for Lumi in Lumis:
                    SIGNIF100_CurrentFT_HH_lumi_scatter[Lumi][TypeTag].append(signif100_HH_L[Lumi])
                    SIGNIF100_CurrentFT_WW_lumi_scatter[Lumi][TypeTag].append(signif100_WW_L[Lumi])
                    SIGNIF100_CurrentFT_ZZ_lumi_scatter[Lumi][TypeTag].append(signif100_ZZ_L[Lumi])
                    SIGNIF100_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag].append(signif100_MAX_L[Lumi])
                for Lumi in Lumis27:
                    SIGNIF27_CurrentFT_HH_lumi_scatter[Lumi][TypeTag].append(signif27_HH_L[Lumi])
                    SIGNIF27_CurrentFT_WW_lumi_scatter[Lumi][TypeTag].append(signif27_WW_L[Lumi])
                    SIGNIF27_CurrentFT_ZZ_lumi_scatter[Lumi][TypeTag].append(signif27_ZZ_L[Lumi])
                    SIGNIF27_CurrentFT_MAX_lumi_scatter[Lumi][TypeTag].append(signif27_MAX_L[Lumi])
            if float(HLLHC_ZZ) ==1 and float(HLLHC_WW)==1 and float(ATLAS_HH_extrap)==1 and float(CMS_HH_extrap)==1:
                Constraint_pass = marker_map['HL-LHC']
                if float(couplstr_fut) == 1:
                    Constraint_pass = marker_map['HL-LHC-CS']
                    for Lumi in Lumis:
                        SIGNIF100_HH_lumi_scatter[Lumi][TypeTag].append(signif100_HH_L[Lumi])
                        SIGNIF100_WW_lumi_scatter[Lumi][TypeTag].append(signif100_WW_L[Lumi])
                        SIGNIF100_ZZ_lumi_scatter[Lumi][TypeTag].append(signif100_ZZ_L[Lumi])
                        SIGNIF100_MAX_lumi_scatter[Lumi][TypeTag].append(signif100_MAX_L[Lumi])
                    for Lumi in Lumis27:
                        SIGNIF27_HH_lumi_scatter[Lumi][TypeTag].append(signif27_HH_L[Lumi])
                        SIGNIF27_WW_lumi_scatter[Lumi][TypeTag].append(signif27_WW_L[Lumi])
                        SIGNIF27_ZZ_lumi_scatter[Lumi][TypeTag].append(signif27_ZZ_L[Lumi])
                        SIGNIF27_MAX_lumi_scatter[Lumi][TypeTag].append(signif27_MAX_L[Lumi])
                print(name, '\talso passed HL-LHC constraints', HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap)
                if float(FCC_WW) == 1 and float(FCC_ZZ) == 1 and float(FCC_HH) == 1 and float(couplstr_fut) == 1:
                    print(name, '\t\talso passed FCC (ZZ+WW+HH) constraints', FCC_WW, FCC_ZZ, FCC_HH, couplstr_fut)
                    xsm_point_info_FCCpass.append(xsm_point_info[p])
                    xsm_point_constraints_FCCpass.append(xsm_point_constraints[p])
        else:
            Constraint_pass = marker_map['EXCLUDED']
            print(name, 'did not pass HL-LHC constraints', EWPOcurr, ATLAS_HH, CMS_HH, HB, HS, HLLHC_ZZ, HLLHC_WW, ATLAS_HH_extrap, CMS_HH_extrap)
        
        Constraints[TypeTag].append(Constraint_pass)

        

# print all the info:
print_point_results(HB_results, HS_results, xsm_point_info, xsm_point_constraints)

# convert the SM BRs and width to Heavy Higgs BRs and width for the model parameters:
#HeavyHiggsBRs = convert_to_heavy(BR_interpolators_SM, lam, v0, x0, a1, a2, b3, b4)
#print_heavy_Higgs_info(HeavyHiggsBRs, BR_text_array_heavy, 'Heavy Higgs BRs & width')
#print '\n'

print('Removed points because they were outside the region of interest for mh2=',removed_point)

# print info for points that pass FCC constraints:
print('\nPoints that passed the FCC constraints:')
print_point_results(HB_results, HS_results, xsm_point_info_FCCpass, xsm_point_constraints_FCCpass)
print('the following benchmark points have failed the Mass fit:', Benchmark_NoMassFit)


print('FINISHED PROCESSING')

########################
# PICKLING STARTS HERE #
########################

pickledata = True

if pickledata is True:
    write_datfile(pickledir + 'SIGN_SINTHETA_scatter', SIGN_SINTHETA_scatter)
    write_datfile(pickledir + 'OneLoop_mh1',	OneLoop_mh1)
    write_datfile(pickledir + 'OneLoop_mh2',	OneLoop_mh2)
    write_datfile(pickledir + 'OneLoop_sintheta',	OneLoop_sintheta)
    write_datfile(pickledir + 'OneLoop_costheta',	OneLoop_costheta)
    write_datfile(pickledir + 'Tree_l112',	Tree_l112)
    write_datfile(pickledir + 'Tree_l122',	Tree_l122)
    write_datfile(pickledir + 'Tree_l222',	Tree_l222)
    write_datfile(pickledir + 'Tree_l111',	Tree_l111)
    write_datfile(pickledir + 'AbsTree_l112',	AbsTree_l112)
    write_datfile(pickledir + 'BR_HH_scatter',	BR_HH_scatter)
    write_datfile(pickledir + 'BR_ZZ_scatter',	BR_ZZ_scatter)
    write_datfile(pickledir + 'BR_WW_scatter',	BR_WW_scatter)
    write_datfile(pickledir + 'M2_scatter',	M2_scatter)
    write_datfile(pickledir + 'Constraints',	Constraints)
    write_datfile(pickledir + 'RHO_MAX_scatter',	RHO_MAX_scatter)
    write_datfile(pickledir + 'RHO_STD_scatter',	RHO_STD_scatter)
    write_datfile(pickledir + 'RHO_AV_scatter',	RHO_AV_scatter)
    write_datfile(pickledir + 'RHO_STDOVAV_scatter',	RHO_STDOVAV_scatter)
    write_datfile(pickledir + 'FINETUNING_scatter',	FINETUNING_scatter)
    write_datfile(pickledir + 'FINETUNING2_scatter',	FINETUNING2_scatter)
    write_datfile(pickledir + 'FINETUNING3_scatter',	FINETUNING3_scatter)
    write_datfile(pickledir + 'FINETUNINGMAX_scatter',	FINETUNINGMAX_scatter)
    write_datfile(pickledir + 'FINETUNING_1loop_m1_scatter',	FINETUNING_1loop_m1_scatter)
    write_datfile(pickledir + 'FINETUNING_1loop_m2_scatter',	FINETUNING_1loop_m2_scatter)
    write_datfile(pickledir + 'XS100_HH_scatter',	XS100_HH_scatter)
    write_datfile(pickledir + 'XS100_WW_scatter',	XS100_WW_scatter)
    write_datfile(pickledir + 'XS100_ZZ_scatter',	XS100_ZZ_scatter)
    write_datfile(pickledir + 'SIGNIF100_HH_scatter',	SIGNIF100_HH_scatter)
    write_datfile(pickledir + 'SIGNIF100_WW_scatter',	SIGNIF100_WW_scatter)
    write_datfile(pickledir + 'SIGNIF100_ZZ_scatter',	SIGNIF100_ZZ_scatter)
    write_datfile(pickledir + 'XS27_HH_scatter',	XS27_HH_scatter)
    write_datfile(pickledir + 'XS27_WW_scatter',	XS27_WW_scatter)
    write_datfile(pickledir + 'XS27_ZZ_scatter',	XS27_ZZ_scatter)
    write_datfile(pickledir + 'XS100_MAX_scatter',	XS100_MAX_scatter)
    write_datfile(pickledir + 'SIGNIF100_MAX_scatter',	SIGNIF100_MAX_scatter)
    write_datfile(pickledir + 'SIGNIF27_MAX_scatter',	SIGNIF27_MAX_scatter)
    write_datfile(pickledir + 'SIGNIF27_HH_scatter',	SIGNIF27_HH_scatter)
    write_datfile(pickledir + 'SIGNIF27_WW_scatter',	SIGNIF27_WW_scatter)
    write_datfile(pickledir + 'SIGNIF27_ZZ_scatter',	SIGNIF27_ZZ_scatter)
    write_datfile(pickledir + 'SIGNIF14_MAX_scatter',	SIGNIF14_MAX_scatter)
    write_datfile(pickledir + 'SIGNIF14_HH_scatter',	SIGNIF14_HH_scatter)
    write_datfile(pickledir + 'SIGNIF14_WW_scatter',	SIGNIF14_WW_scatter)
    write_datfile(pickledir + 'SIGNIF14_ZZ_scatter',	SIGNIF14_ZZ_scatter)
    write_datfile(pickledir + 'DELTAMH1_scatter',	DELTAMH1_scatter)
    write_datfile(pickledir + 'DELTAMH2tree_scatter',	DELTAMH2tree_scatter)
    write_datfile(pickledir + 'SIGNIF100_CurrentFT_HH_lumi_scatter',	SIGNIF100_CurrentFT_HH_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_CurrentFT_WW_lumi_scatter',	SIGNIF100_CurrentFT_WW_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_CurrentFT_ZZ_lumi_scatter',	SIGNIF100_CurrentFT_ZZ_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_CurrentFT_MAX_lumi_scatter', SIGNIF100_CurrentFT_MAX_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_HH_lumi_scatter',	SIGNIF100_HH_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_WW_lumi_scatter',	SIGNIF100_WW_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_ZZ_lumi_scatter',	SIGNIF100_ZZ_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_MAX_lumi_scatter',	SIGNIF100_MAX_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_CurrentFT_HH_lumi_scatter',	SIGNIF27_CurrentFT_HH_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_CurrentFT_WW_lumi_scatter',	SIGNIF27_CurrentFT_WW_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_CurrentFT_ZZ_lumi_scatter',	SIGNIF27_CurrentFT_ZZ_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_CurrentFT_MAX_lumi_scatter',	SIGNIF27_CurrentFT_MAX_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_HH_lumi_scatter',	SIGNIF27_HH_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_WW_lumi_scatter',	SIGNIF27_WW_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_ZZ_lumi_scatter',	SIGNIF27_ZZ_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_MAX_lumi_scatter',	SIGNIF27_MAX_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_Current_HH_lumi_scatter',	SIGNIF27_Current_HH_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_Current_WW_lumi_scatter',	SIGNIF27_Current_WW_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_Current_ZZ_lumi_scatter',	SIGNIF27_Current_ZZ_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF27_Current_MAX_lumi_scatter',	SIGNIF27_Current_MAX_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_Current_HH_lumi_scatter',	SIGNIF100_Current_HH_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_Current_WW_lumi_scatter',	SIGNIF100_Current_WW_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_Current_ZZ_lumi_scatter',	SIGNIF100_Current_ZZ_lumi_scatter)
    write_datfile(pickledir + 'SIGNIF100_Current_MAX_lumi_scatter', SIGNIF100_Current_MAX_lumi_scatter)
    write_datfile(pickledir + 'CLIC_VV_scatter', CLIC_VV_scatter)
    write_datfile(pickledir + 'CLIC14_HH_scatter', CLIC14_HH_scatter) 
    write_datfile(pickledir + 'CLIC3_HH_scatter', CLIC3_HH_scatter) 
    write_datfile(pickledir + 'K1_scatter', K1_scatter)
    write_datfile(pickledir + 'K2_scatter', K2_scatter)
    write_datfile(pickledir + 'Kappa_scatter', Kappa_scatter)
    write_datfile(pickledir + 'Lambda_scatter', Lambda_scatter)
    write_datfile(pickledir + 'LambdaS_scatter', LambdaS_scatter)
    write_datfile(pickledir + 'x0_scatter', x0_scatter)
    write_datfile(pickledir + 'MS_scatter', MS_scatter)
    write_datfile(pickledir + 'mu2_scatter', mu2_scatter)
    write_datfile(pickledir + 'MASSFIT_scatter', MASSFIT_scatter)
    write_datfile(pickledir + 'DMASSFIT_scatter', DMASSFIT_scatter)
    write_datfile(pickledir + 'XS_HETA0_scatter', XS_HETA0_scatter)
    write_datfile(pickledir + 'sintheta_fit_prec_scatter', sintheta_fit_prec_scatter)
    write_datfile(pickledir + 'l112_fit_prec_scatter', l112_fit_prec_scatter)
    write_datfile(pickledir + 'l112_max_fit_errormass_scatter', l112_max_fit_errormass_scatter)
    write_datfile(pickledir + 'l112_min_fit_errormass_scatter', l112_min_fit_errormass_scatter)
    write_datfile(pickledir + 'sintheta_max_fit_errormass_scatter', sintheta_max_fit_errormass_scatter)
    write_datfile(pickledir + 'sintheta_min_fit_errormass_scatter', sintheta_min_fit_errormass_scatter)    
    write_datfile(pickledir + 'PointName', PointName)

    
########################
# PLOTTING STARTS HERE #
########################


# disable/enable plotting
plotting = False

if plotting is False:
    exit()


exec(compile(open('singlet_constraint_plots.py', "rb").read(), 'singlet_constraint_plots.py', 'exec'))

