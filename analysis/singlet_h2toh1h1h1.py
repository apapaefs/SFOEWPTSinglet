#! /usr/bin/env python

import math
from function_h2toh1h1h1 import DIntDxDy
from scipy.integrate import dblquad


def testfunc(x, y):
    return x*y**2


def Width_h2h1h1h1(lam1112, lam111, lam112, lam122, m1, m2, Gam1, Gam2):
    if m2 < 3 * m1:
        return 0.0000
    # limits of y integral, fixed:
    ymin = 4 * m1**2
    ymax = (m2 - m1)**2
    width = 0.
    Int = dblquad(DIntDxDy, ymin, ymax, lambda y: (3*m1**2 + m2**2 - y - math.sqrt(-4*m1**2 + y)*math.sqrt(-2*(m1**2 + m2**2) + (m1**2 - m2**2)**2/y + y))/2., lambda y: (3*m1**2 + m2**2 - y + math.sqrt(-4*m1**2 + y)*math.sqrt(-2*(m1**2 + m2**2) + (m1**2 - m2**2)**2/y + y))/2., args=(lam1112, lam111, lam112, lam122, m1, m2, Gam1, Gam2))
    width = (1./(2. * math.pi)**3)/32. * Int[0]
    return width


# test the width:
# parameters of test point:
#lam111 = 40.3
#lam112 = 73.8
#lam122 = 1132.5
#lam1112 = 0.42
#m2 = 841
#m1 = 125
#Gam1 = 0.006382339
#Gam2 = 3.95

#print Width_h2h1h1h1(lam1112, lam111, lam112, lam122, m1, m2, Gam1, Gam2)
# should be 0.00271 for these params
