########################
# PLOTTING STARTS HERE #
########################

###################################################################################
# scatter plots  from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting correlation scatter plots from different groups of benchmarks ')
# plot settings ########
plot_type = 'correlation'
# plot:
# plot settings
ylab = '$K_2$'
xlab = '$K_1$ [GeV]'
ylog = False
xlog = False

fig2 = plt.figure(constrained_layout=True)
spec2 = gridspec.GridSpec(ncols=4, nrows=4,wspace=0, hspace=0, figure=fig2)
f2_ax00 = fig2.add_subplot(spec2[0, 0])
f2_ax10 = fig2.add_subplot(spec2[1, 0])
f2_ax11 = fig2.add_subplot(spec2[1, 1])
f2_ax20 = fig2.add_subplot(spec2[2, 0])
f2_ax21 = fig2.add_subplot(spec2[2, 1])
f2_ax22 = fig2.add_subplot(spec2[2, 2])
f2_ax30 = fig2.add_subplot(spec2[3, 0])
f2_ax31 = fig2.add_subplot(spec2[3, 1])
f2_ax32 = fig2.add_subplot(spec2[3, 2])
f2_ax33 = fig2.add_subplot(spec2[3, 3])

f2_axUR = fig2.add_subplot(spec2[:2, 2:])
f2_axUR.axis('off')
f2_axUR.set(xticks=[], yticks=[])

#ax.grid(False)
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax00, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)

reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], K2_scatter[key], ax=f2_ax10, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], LambdaS_scatter[key], ax=f2_ax20, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], Kappa_scatter[key], ax=f2_ax30, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], K2_scatter[key], ax=f2_ax11, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], LambdaS_scatter[key], ax=f2_ax21, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], Kappa_scatter[key], ax=f2_ax31, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(K2_scatter[key], LambdaS_scatter[key], ax=f2_ax22, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K2_scatter[key], Kappa_scatter[key], ax=f2_ax32, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(LambdaS_scatter[key], Kappa_scatter[key], ax=f2_ax33, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
reset_color()


    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax6, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax7, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax8, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax9, m=np.array(Constraints[key]), c=next_color(), label='', s=2)

#    pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)

# set the ticks, labels and limits 
#ax.set_ylabel(ylab, fontsize=20)
#ax.set_xlabel(xlab, fontsize=20)
f2_ax00.set(xticks=[])
f2_ax10.set(xticks=[])
f2_ax20.set(xticks=[])
f2_ax11.set(xticks=[])
f2_ax21.set(xticks=[])
f2_ax22.set(xticks=[])

f2_ax11.set(yticks=[])
f2_ax21.set(yticks=[])
f2_ax31.set(yticks=[])
f2_ax32.set(yticks=[])
f2_ax33.set(yticks=[])
f2_ax22.set(yticks=[])


f2_ax30.set_xlabel('$M_S^2$ [GeV$^2$]')
f2_ax31.set_xlabel('$K_1$ [GeV]')
f2_ax32.set_xlabel('$K_2$')
f2_ax33.set_xlabel('$\\lambda_S$')

f2_ax00.set_ylabel('$K_1$ [GeV]')
f2_ax10.set_ylabel('$K_2$')
f2_ax20.set_ylabel('$\\lambda_S$')
f2_ax30.set_ylabel('$\\kappa$ [GeV]')

f2_ax00.yaxis.set_major_locator(MultipleLocator(200))
f2_ax00.yaxis.set_minor_locator(MultipleLocator(100))

f2_ax10.yaxis.set_major_locator(MultipleLocator(1))
f2_ax10.yaxis.set_minor_locator(MultipleLocator(0.5))

f2_ax20.yaxis.set_major_locator(MultipleLocator(0.5))
f2_ax20.yaxis.set_minor_locator(MultipleLocator(0.25))

f2_ax30.yaxis.set_major_locator(MultipleLocator(1000))
f2_ax30.yaxis.set_minor_locator(MultipleLocator(500))

f2_ax31.xaxis.set_major_locator(MultipleLocator(200))
f2_ax31.xaxis.set_minor_locator(MultipleLocator(100))

f2_ax32.xaxis.set_major_locator(MultipleLocator(1))
f2_ax32.xaxis.set_minor_locator(MultipleLocator(0.5))

f2_ax33.xaxis.set_major_locator(MultipleLocator(0.5))
f2_ax33.xaxis.set_minor_locator(MultipleLocator(0.25))

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
# create legend and plot/font size
f2_axUR.legend()
f2_axUR.legend(loc="center", numpoints=1, frameon=False, prop={'size':12})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig2)

reset_color()
####################


###################################################################################
# scatter plots  from different groups of benchmarks    #
###################################################################################
print('---')
print('plotting correlation scatter plots from different groups of benchmarks ')
# plot settings ########
plot_type = 'correlation_singlecol'
# plot:
# plot settings
ylab = '$K_2$'
xlab = '$K_1$ [GeV]'
ylog = False
xlog = False

fig2 = plt.figure(constrained_layout=True)
spec2 = gridspec.GridSpec(ncols=4, nrows=4,wspace=0, hspace=0, figure=fig2)
f2_ax00 = fig2.add_subplot(spec2[0, 0])
f2_ax10 = fig2.add_subplot(spec2[1, 0])
f2_ax11 = fig2.add_subplot(spec2[1, 1])
f2_ax20 = fig2.add_subplot(spec2[2, 0])
f2_ax21 = fig2.add_subplot(spec2[2, 1])
f2_ax22 = fig2.add_subplot(spec2[2, 2])
f2_ax30 = fig2.add_subplot(spec2[3, 0])
f2_ax31 = fig2.add_subplot(spec2[3, 1])
f2_ax32 = fig2.add_subplot(spec2[3, 2])
f2_ax33 = fig2.add_subplot(spec2[3, 3])

f2_axUR = fig2.add_subplot(spec2[:2, 2:])
f2_axUR.axis('off')
f2_axUR.set(xticks=[], yticks=[])

#ax.grid(False)
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax00, m=np.array(Constraints[key]), c='blue', label='', s=2)
    #pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)

reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], K2_scatter[key], ax=f2_ax10, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], LambdaS_scatter[key], ax=f2_ax20, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], Kappa_scatter[key], ax=f2_ax30, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], K2_scatter[key], ax=f2_ax11, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], LambdaS_scatter[key], ax=f2_ax21, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], Kappa_scatter[key], ax=f2_ax31, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(K2_scatter[key], LambdaS_scatter[key], ax=f2_ax22, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K2_scatter[key], Kappa_scatter[key], ax=f2_ax32, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(LambdaS_scatter[key], Kappa_scatter[key], ax=f2_ax33, m=np.array(Constraints[key]), c='blue', label='', s=2)
reset_color()


    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax6, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax7, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax8, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax9, m=np.array(Constraints[key]), c=next_color(), label='', s=2)

#    pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)

# set the ticks, labels and limits 
#ax.set_ylabel(ylab, fontsize=20)
#ax.set_xlabel(xlab, fontsize=20)
f2_ax00.set(xticks=[])
f2_ax10.set(xticks=[])
f2_ax20.set(xticks=[])
f2_ax11.set(xticks=[])
f2_ax21.set(xticks=[])
f2_ax22.set(xticks=[])

f2_ax11.set(yticks=[])
f2_ax21.set(yticks=[])
f2_ax31.set(yticks=[])
f2_ax32.set(yticks=[])
f2_ax33.set(yticks=[])
f2_ax22.set(yticks=[])


f2_ax30.set_xlabel('$M_S^2$ [GeV$^2$]')
f2_ax31.set_xlabel('$K_1$ [GeV]')
f2_ax32.set_xlabel('$K_2$')
f2_ax33.set_xlabel('$\\lambda_S$')

f2_ax00.set_ylabel('$K_1$ [GeV]')
f2_ax10.set_ylabel('$K_2$')
f2_ax20.set_ylabel('$\\lambda_S$')
f2_ax30.set_ylabel('$\\kappa$ [GeV]')

f2_ax00.yaxis.set_major_locator(MultipleLocator(200))
f2_ax00.yaxis.set_minor_locator(MultipleLocator(100))

f2_ax10.yaxis.set_major_locator(MultipleLocator(1))
f2_ax10.yaxis.set_minor_locator(MultipleLocator(0.5))

f2_ax20.yaxis.set_major_locator(MultipleLocator(0.5))
f2_ax20.yaxis.set_minor_locator(MultipleLocator(0.25))

f2_ax30.yaxis.set_major_locator(MultipleLocator(1000))
f2_ax30.yaxis.set_minor_locator(MultipleLocator(500))

f2_ax31.xaxis.set_major_locator(MultipleLocator(200))
f2_ax31.xaxis.set_minor_locator(MultipleLocator(100))

f2_ax32.xaxis.set_major_locator(MultipleLocator(1))
f2_ax32.xaxis.set_minor_locator(MultipleLocator(0.5))

f2_ax33.xaxis.set_major_locator(MultipleLocator(0.5))
f2_ax33.xaxis.set_minor_locator(MultipleLocator(0.25))

#pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
#pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
# create legend and plot/font size
f2_axUR.legend()
f2_axUR.legend(loc="center", numpoints=1, frameon=False, prop={'size':12})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig2)

reset_color()
####################


###################################################################################
# scatter plots  from different groups of benchmarks    #
###################################################################################


print('---')
print('plotting correlation scatter plots from different groups of benchmarks (excluded with yellow)')
# plot settings ########
plot_type = 'correlation_withexcluded_colours'
# plot:
# plot settings
ylab = '$K_2$'
xlab = '$K_1$ [GeV]'
ylog = False
xlog = False

fig2 = plt.figure(constrained_layout=True)
spec2 = gridspec.GridSpec(ncols=4, nrows=4,wspace=0, hspace=0, figure=fig2)
f2_ax00 = fig2.add_subplot(spec2[0, 0])
f2_ax10 = fig2.add_subplot(spec2[1, 0])
f2_ax11 = fig2.add_subplot(spec2[1, 1])
f2_ax20 = fig2.add_subplot(spec2[2, 0])
f2_ax21 = fig2.add_subplot(spec2[2, 1])
f2_ax22 = fig2.add_subplot(spec2[2, 2])
f2_ax30 = fig2.add_subplot(spec2[3, 0])
f2_ax31 = fig2.add_subplot(spec2[3, 1])
f2_ax32 = fig2.add_subplot(spec2[3, 2])
f2_ax33 = fig2.add_subplot(spec2[3, 3])

f2_axUR = fig2.add_subplot(spec2[:2, 2:])
f2_axUR.axis('off')
f2_axUR.set(xticks=[], yticks=[])

#ax.grid(False)
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax00, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
    pl.plot(np.nan, np.nan, marker='o', lw=0, color=corrcolors_key[key], label=key)

reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], K2_scatter[key], ax=f2_ax10, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], LambdaS_scatter[key], ax=f2_ax20, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(MS_scatter[key], Kappa_scatter[key], ax=f2_ax30, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], K2_scatter[key], ax=f2_ax11, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], LambdaS_scatter[key], ax=f2_ax21, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K1_scatter[key], Kappa_scatter[key], ax=f2_ax31, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(K2_scatter[key], LambdaS_scatter[key], ax=f2_ax22, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()
for key in list(K1_scatter.keys()):
    scatter = mscatter(K2_scatter[key], Kappa_scatter[key], ax=f2_ax32, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()

for key in list(K1_scatter.keys()):
    scatter = mscatter(LambdaS_scatter[key], Kappa_scatter[key], ax=f2_ax33, m=np.array(Constraints[key]), c=np.array(corrcolors_array[key]), label='', s=2)
reset_color()


    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax6, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax7, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax8, m=np.array(Constraints[key]), c=next_color(), label='', s=2)
    #scatter = mscatter(MS_scatter[key], K1_scatter[key], ax=f2_ax9, m=np.array(Constraints[key]), c=next_color(), label='', s=2)

#    pl.plot(np.nan, np.nan, marker='o', lw=0, color=same_color(), label=key)

# set the ticks, labels and limits 
#ax.set_ylabel(ylab, fontsize=20)
#ax.set_xlabel(xlab, fontsize=20)
f2_ax00.set(xticks=[])
f2_ax10.set(xticks=[])
f2_ax20.set(xticks=[])
f2_ax11.set(xticks=[])
f2_ax21.set(xticks=[])
f2_ax22.set(xticks=[])

f2_ax11.set(yticks=[])
f2_ax21.set(yticks=[])
f2_ax31.set(yticks=[])
f2_ax32.set(yticks=[])
f2_ax33.set(yticks=[])
f2_ax22.set(yticks=[])


f2_ax30.set_xlabel('$M_S^2$ [GeV$^2$]')
f2_ax31.set_xlabel('$K_1$ [GeV]')
f2_ax32.set_xlabel('$K_2$')
f2_ax33.set_xlabel('$\\lambda_S$')

f2_ax00.set_ylabel('$K_1$ [GeV]')
f2_ax10.set_ylabel('$K_2$')
f2_ax20.set_ylabel('$\\lambda_S$')
f2_ax30.set_ylabel('$\\kappa$ [GeV]')

f2_ax00.yaxis.set_major_locator(MultipleLocator(200))
f2_ax00.yaxis.set_minor_locator(MultipleLocator(100))

f2_ax10.yaxis.set_major_locator(MultipleLocator(1))
f2_ax10.yaxis.set_minor_locator(MultipleLocator(0.5))

f2_ax20.yaxis.set_major_locator(MultipleLocator(0.5))
f2_ax20.yaxis.set_minor_locator(MultipleLocator(0.25))

f2_ax30.yaxis.set_major_locator(MultipleLocator(1000))
f2_ax30.yaxis.set_minor_locator(MultipleLocator(500))

f2_ax31.xaxis.set_major_locator(MultipleLocator(200))
f2_ax31.xaxis.set_minor_locator(MultipleLocator(100))

f2_ax32.xaxis.set_major_locator(MultipleLocator(1))
f2_ax32.xaxis.set_minor_locator(MultipleLocator(0.5))

f2_ax33.xaxis.set_major_locator(MultipleLocator(0.5))
f2_ax33.xaxis.set_minor_locator(MultipleLocator(0.25))

pl.plot(np.nan, np.nan, marker=marker_map['HL-LHC-CS'], color='black', lw=0, label='HL-LHC+coupl. str. $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['CURRENT'], color='black', lw=0, label='Current $\\checkmark$')
pl.plot(np.nan, np.nan, marker=marker_map['EXCLUDED'], color='yellow', lw=0, label='Excluded')

# create legend and plot/font size
f2_axUR.legend()
f2_axUR.legend(loc="center", numpoints=1, frameon=False, prop={'size':12})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# save the figure
print('saving the figure')
# save the figure in PDF format
infile = plot_type + '.dat'
print('---')
print('output in', outputdirectory + infile.replace('.dat','.pdf'))
pl.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
pl.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight', scale=0.1)
pl.close(fig2)

reset_color()
####################
