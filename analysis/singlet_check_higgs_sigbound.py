#! /usr/bin/env python

import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
#from matplotlib.mlab import griddata
import matplotlib.font_manager as fm
from matplotlib.ticker import MultipleLocator
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
from numpy import linalg as LA
from prettytable import PrettyTable


##################################################
#### COMMAND LINE PARAMETERS
##################################################

parser = OptionParser(usage="%prog inputfile")

print('generating HiggsBounds/HiggsSignals input for real scalar singlet model\n')

# executables for HiggsBounds and HiggSignals:
HiggsBounds_bin = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/higgsbounds/build/HiggsBounds'
HiggsSignals_bin = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/higgssignals/build/HiggsSignals'


def generate_model_dir(output_dir, workingdirectory):
    # create the output directory if it does not exist
    if os.path.isdir(output_dir) is False:
        print('Directory', output_dir, 'not found, creating.')
        mkoutput = 'mkdir ' + output_dir
        p = subprocess.Popen(mkoutput, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd=workingdirectory)
    else:
        print('WARNING: directory', output_dir, 'already exists, will overwrite files within.')

#################################################
#### Functions to write out the necessary files #
#################################################
# write the HiggsBounds files
# params is a list of lists that contains the information
def write_HB(output_dir, model, params, filetype):
    filename = output_dir + model + filetype
    output_stream = open(filename,"w")
    for pp in range(len(params)):
        output_stream.write('  ' + str(pp+1) + '  ')
        for ppi in range(len(params[pp])):
            output_stream.write(str(params[pp][ppi]) + '  ')
        output_stream.write('\n')
    output_stream.close()

#####################################

# write the HiggsBounds files: APPEND to existing file 
# params is a list of lists that contains the information
def write_HB_append(point_number, output_dir, model, params, filetype):
    filename = output_dir + model + filetype
    output_stream = open(filename,"a")
    for pp in range(len(params)):
        output_stream.write('  ' + str(point_number) + '  ')
        for ppi in range(len(params[pp])):
            output_stream.write(str(params[pp][ppi]) + '  ')
        output_stream.write('\n')
    output_stream.close()

# generate the necessary arrays for a single point
def gen_HB_arrays(mh1, mh2, G1, G2, st, ct, BR_hh, h1_BRs, h2_BRs):
    params_MH_GammaTot = [[mh1, mh2, G1, G2]]
    params_effC = [
    [ct, st, # g2hjss_s
                0., 0.,           # g2hjss_p
                ct, st, # g2hjcc_s
                0., 0.,           # g2hjcc_p
                ct, st, # g2hjbb_s
                0., 0,            # g2hjbb_p
                ct, st, # g2hjtoptop_s
                0., 0.,           # g2hjtoptop_p
                ct, st, # g2hjmumu_s
                0., 0.,           # g2hjmumu_p
                ct, st, # g2hjtautau_s
                0., 0.,           # g2hjtautau_p
                ct, st, # g2hjWW
                ct, st, # g2hjZZ
                ct, st, # g2hjZga
                ct, st, # g2hjgaga
                ct, st, # g2hjgg
                0., 0., 0.], #some elements of g2hjhiZ
        ]

    params_CP_values = [[1, 1]]
    params_BR_H_NP = [
    [0., 0.,
    0., BR_hh,
    0., 0.,
    0., 0.,
    0., 0.,
    0., 0.]]
    params_BR_H_OP = [
        [h1_BRs[4],h2_BRs[4], # BR_hjss,
        h1_BRs[3], h2_BRs[3], # BR_hjcc,
        h1_BRs[0], h2_BRs[0], # BR_hjbb,
        h1_BRs[5], h2_BRs[5], # BR_hjtt
        h1_BRs[2], h2_BRs[2], # BR_hjmumu,
        h1_BRs[1], h2_BRs[1], # BR_hjtautau,
        h1_BRs[9], h2_BRs[9],  # BR_hjWW, 
        h1_BRs[10],h2_BRs[10], # BR_hjZZ,
        h1_BRs[8], h2_BRs[8], # BR_hjZga,
        h1_BRs[7], h2_BRs[7], # BR_hjgaga,
        h1_BRs[6], h2_BRs[6] # BR_hjgg
        ]]      
    params_BR_t = [[1, 0]]
    return params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t, params_BR_H_OP

# append the info to the files
def write_HB_append_all(point_number, output, model_name, params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t, params_BR_H_OP):
    if point_number == 1:
        write_HB(output, model_name, params_CP_values, '_CP_values.dat')
        write_HB(output, model_name, params_MH_GammaTot, '_MH_GammaTot.dat')
        write_HB(output, model_name, params_effC, '_effC.dat')
        write_HB(output, model_name, params_BR_H_NP, '_BR_H_NP.dat')
        write_HB(output, model_name, params_BR_t, '_BR_t.dat')
        write_HB(output, model_name, params_BR_H_OP, '_BR_H_OP.dat')
    else:
        write_HB_append(point_number, output, model_name, params_CP_values, '_CP_values.dat')
        write_HB_append(point_number, output, model_name, params_MH_GammaTot, '_MH_GammaTot.dat')
        write_HB_append(point_number, output, model_name, params_effC, '_effC.dat')
        write_HB_append(point_number, output, model_name, params_BR_H_NP, '_BR_H_NP.dat')
        write_HB_append(point_number, output, model_name, params_BR_t, '_BR_t.dat')
        write_HB_append(point_number, output, model_name, params_BR_H_OP, '_BR_H_OP.dat')
    return 1
    
def run_HB(HB_binary, output_dir, model_name):
    HB_command = HB_binary + ' LandH effC 2 0 ' + output_dir + model_name + '_'
    print(HB_command)
    p = subprocess.Popen(HB_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    out, err = p.communicate()
    print(out, err)


def run_HS(HS_binary, output_dir, model_name):
    HS_command = HS_binary + ' latestresults 2 effC 2 0 ' + output_dir + model_name + '_'
    print(HS_command)
    p = subprocess.Popen(HS_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd='.')
    out, err = p.communicate()
    print(out, err)

def check_HB_results(output_dir, model_name):
    HB_results_file = output_dir + model_name + '_HiggsBounds_results.dat'
    input_stream = open(HB_results_file,"r")
    HB_results = []
    for line in input_stream:
        if '#' not in line:
            #print line.split()[0], line.split()[3]
            HB_results.append(int(line.split()[3]))
    #print HB_results
    return HB_results

def check_HS_results(output_dir, model_name):
    HS_results_file = output_dir + model_name + '_HiggsSignals_results.dat'
    input_stream = open(HS_results_file,"r")
    HS_results = []
    for line in input_stream:
        if '#' not in line:
            #print line.split()[0], line.split()[9]
            HS_results.append(float(line.split()[9]))
    #print HS_results
    return HS_results

def print_HB_results(HB_result_array, xsm_point_info):
    print('point\t HiggsBounds bool')
    for i in range(len(HB_result_array)):
        print(i+1, '\t', HB_result_array[i])

def print_HS_results(HS_result_array, xsm_point_info):
    print('point\t HiggsSignals p-val')
    for i in range(len(HS_result_array)):
        print(i+1, '\t', HS_result_array[i])

def print_HBHS_results(HB_result_array, HS_result_array, xsm_point_info):
    column_text = [ '#', 'name', 'mh1', 'mh2', 'Gh1', 'Gh2', 'stheta', 'ctheta', 'BR(h2->h1h1)', 'HB res.', 'HS res.', 'XS13(mh2)[pb]', 'XS14(mh2)[pb]', 'XS8(mh2)[pb]', 'XS7(mh2)[pb]' ]
    tbl = PrettyTable(column_text)
    for p in range(len(xsm_point_info)):
        name =  str(xsm_point_info[p][0])
        mh1 = str(xsm_point_info[p][1])
        mh2 = str(xsm_point_info[p][2])
        G1 = str(xsm_point_info[p][3])
        G2 = str(xsm_point_info[p][4])
        st = str(xsm_point_info[p][5])
        ct = str(xsm_point_info[p][6])
        BR_hh = str(xsm_point_info[p][7])
        HB = str(HB_result_array[p])
        HS = str(HS_result_array[p])
        xs13 = str(xsm_point_info[p][8])
        xs14 = str(xsm_point_info[p][9])
        xs8 = str(xsm_point_info[p][10])
        xs7 = str(xsm_point_info[p][11])

        tbl.add_row([name, p+1, mh1, mh2, G1, G2, st, ct, BR_hh, HB, HS, xs13, xs14, xs8, xs7])
    print(tbl)

                           

#############################
#### TEST THE PIPELINE HERE #
#############################
# test:
# the model name and the output directory
#workingdirectory = '.'
#model_name = 'HB_singlet_test'
#output = './' + model_name + '/'

# first generate the model directory
#generate_model_dir(output, '.')

# add two points: 
#point_number = 1
#mh1 = 125.0
#mh2 = 343.0
#G1 = 0.00407
#G2 = 0.041
#st = 0.029
#ct = 0.999
#BR_hh = 0.72
#params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t = gen_HB_arrays(mh1, mh2, G1, G2, st, ct, BR_hh)
#write_HB_append_all(point_number, output, model_name, params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t)

#point_number = 2
#mh1 = 125.0
#mh2 = 841.0
#G1 = 0.00407
#G2 = 3.95
#st = 0.105
#ct = 0.994
#BR_hh = 0.062
#params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t = gen_HB_arrays(mh1, mh2, G1, G2, st, ct, BR_hh)
#write_HB_append_all(point_number, output, model_name, params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t)

# once you are done adding points, run HiggsSignals and HiggsBounds:
#run_HB(HiggsBounds_bin, output, model_name)
#run_HS(HiggsSignals_bin, output, model_name)

# check and print HiggsSignals and HiggsBounds results
#HB_results = check_HB_results(output, model_name)
#HS_results = check_HS_results(output, model_name)

#print_HB_results(HB_results)
#print_HS_results(HS_results)

###### FUNCTION TO RUN THE WHOLE THING ON A SET OF GIVEN POINTS #######

def get_HiggsBoundsSignals_results(workingdirectory, model_name, xsm_point_info):
    #workingdirectory = '.'
    #model_name = 'HB_singlet_test'
    output = './' + model_name + '/'

    # first generate the model directory
    generate_model_dir(output, workingdirectory)

    # loop through the points and add them to the arrays
    for p in range(len(xsm_point_info)):
        mh1 = xsm_point_info[p][1]
        mh2 = xsm_point_info[p][2]
        G1 = xsm_point_info[p][3]
        G2 = xsm_point_info[p][4]
        st = xsm_point_info[p][5]
        ct = xsm_point_info[p][6]
        BR_hh = xsm_point_info[p][7]
        h1_BRs = xsm_point_info[p][12] # LightHiggsBRs
        h2_BRs = xsm_point_info[p][13] # HeavyHiggsBRs
        params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t, params_BR_H_OP = gen_HB_arrays(mh1, mh2, G1, G2, st, ct, BR_hh, h1_BRs, h2_BRs)
        write_HB_append_all(p+1, output, model_name, params_MH_GammaTot, params_effC, params_CP_values, params_BR_H_NP, params_BR_t, params_BR_H_OP)

    # once you are done adding points, run HiggsSignals and HiggsBounds:
    run_HB(HiggsBounds_bin, output, model_name)
    run_HS(HiggsSignals_bin, output, model_name)

    # check and print HiggsSignals and HiggsBounds results
    HB_results = check_HB_results(output, model_name)
    HS_results = check_HS_results(output, model_name)

    return HB_results, HS_results
    #print_HB_results(HB_results, xsm_point_info)
    #print_HS_results(HS_results, xsm_point_info)
    #print_HBHS_results(HB_results, HS_results, xsm_point_info)
