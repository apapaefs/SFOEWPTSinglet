/// \file
/// \ingroup tutorial_tmva
/// \notebook -nodraw
/// This macro provides examples for the training and testing of the
/// TMVA classifiers.
///
/// As input data is used a toy-MC sample consisting of four Gaussian-distributed
/// and linearly correlated input variables.
/// The methods to be used can be switched on and off by means of booleans, or
/// via the prompt command, for example:
///
///     root -l ./TMVAClassification.C\(\"Fisher,Likelihood\"\)
///
/// If no method given, a default set of classifiers is used.
/// The output file "TMVA.root" can be analysed with the use of dedicated
/// macros (simply say: root -l <macro.C>), which can be conveniently
/// invoked through a GUI that will appear at the end of the run of this macro.
/// Launch the GUI via the command:
///
///     root -l ./TMVAGui.C
///
/// You can also compile and run the example with the following commands
///
///     make
///     ./TMVAClassification <Methods>
///
/// where: `<Methods> = "method1 method2"` are the TMVA classifier names
/// example:
///
///     ./TMVAClassification Fisher LikelihoodPCA BDT
///
/// If no method given, a default set is of classifiers is used
///
/// - Project   : TMVA - a ROOT-integrated toolkit for multi_outputvariate data analysis
/// - Package   : TMVA
/// - Root Macro: TMVAClassification
///
/// \macro_output
/// \macro_code
/// \author Andreas Hoecker


#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <fstream>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TGraph.h"


#include "TMVA/Factory.h"
#include "TMVA/DataLoader.h"
#include "TMVA/Tools.h"
#include "TMVA/TMVAGui.h"


int TMVAClassification_singlet_multi_output(double Lumi, std::string signal_file, std::string background_file, double fac_signal, int nvar, int rseed, double mass, TString myMethodList = "" )
{
   // The explicit loading of the shared libTMVA is done in TMVAlogon.C, defined in .rootrc
   // if you use your private .rootrc, or run from a different directory, please copy the
   // corresponding lines from .rootrc

   // Methods to be processed can be given as an argument; use format:
   //
   //     mylinux~> root -l TMVAClassification.C\(\"myMethod1,myMethod2,myMethod3\"\)

   //---------------------------------------------------------------
   // This loads the library
   TMVA::Tools::Instance();
   // Default MVA methods to be trained + tested
   std::map<std::string,int> Use;
   
   // Boosted Decision Trees
   Use["BDT"]             = 1; // uses Adaptive Boost


   std::cout << std::endl;
   std::cout << "==> Start TMVAClassification" << std::endl;

   // --------------------------------------------------------------------------------------------------

   // Here the preparation phase begins

   std::vector<std::string> signal_strings;
   std::vector<std::string> bkg_strings;
   std::vector<Double_t> xsecSarray;
   std::vector<Double_t> xsecBarray;

   //read in the _var.root files and cross sections:
   double total_xsecS(0.); //the total signal cross section
   std::ifstream signalstream;
   std::string line;
   signalstream.open(signal_file.c_str());
   std::cout << "reading signal file: " << signal_file << std::endl;
   while(std::getline(signalstream, line)) {
     std::vector<std::string> tokens;
     std::istringstream iss(line);
     std::string token;
     while(std::getline(iss, token, '\t')) { 
       tokens.push_back(token);
     }
     std::cout << tokens[0] << " " << tokens[1] << std::endl;
     signal_strings.push_back(tokens[0]);
     xsecSarray.push_back(std::stod(tokens[1]));
     total_xsecS += std::stod(tokens[1]);
   }

   double total_xsecB(0.); //the total background cross section
   std::ifstream backgroundstream;
   backgroundstream.open(background_file.c_str());
   std::cout << "reading background file: " << background_file << std::endl;
   while(std::getline(backgroundstream, line)) {
     std::vector<std::string> tokens2;
     std::istringstream iss(line);
     std::string token;
     while(std::getline(iss, token, '\t')) { 
       tokens2.push_back(token);
     }
     bkg_strings.push_back(tokens2[0]);
     xsecBarray.push_back(std::stod(tokens2[1]));
     total_xsecB += std::stod(tokens2[1]);
   }
   
   
   std::vector<TFile*> inputSarray;
   for(int s = 0; s < signal_strings.size(); ++s) inputSarray.push_back(TFile::Open( signal_strings[s].c_str() ));
   std::vector<TFile*> inputBarray;   
   for(int b = 0; b < bkg_strings.size(); ++b) inputBarray.push_back(TFile::Open( bkg_strings[b].c_str() ));
   
   std::vector<TTree*> signals;
   std::vector<TTree*> backgrounds;

   for(int s = 0; s < inputSarray.size(); s++) signals.push_back((TTree*)inputSarray[s]->Get("Data2"));
   for(int b = 0; b < inputBarray.size(); b++) backgrounds.push_back((TTree*)inputBarray[b]->Get("Data2"));
   

   // Create a ROOT output file where TMVA will store ntuples, histograms, etc.
   TString outfileName( "TMVA_WW_" + std::to_string(int(mass)) + ".root" );
   TFile* outputFile = TFile::Open( outfileName, "RECREATE" );


   // Create the factory object.
   TMVA::Factory *factory = new TMVA::Factory( "TMVAClassification", outputFile,
                                               "!V:Silent:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" );
   std::string dir_dataloader = "TMVA_results_WW/dataset_" + std::to_string(int(mass));
   TMVA::DataLoader *dataloader=new TMVA::DataLoader(dir_dataloader.c_str());

   // Define the input variables that shall be used for the MVA training
   // note that you may also use variable expressions, such as: "3*var1/var2*abs(var3)"
   // [all types of expressions that can also be parsed by TTree::Draw( "expression" )]

   //arbitrary number of variables up to nvar
   for(int n=0; n < nvar; n++) {
     std::string varname;
     varname = "variables[" + std::to_string(n) + "]";
     //std::string varname_sp;
     dataloader->AddVariable(varname, 'D');
     //dataloader->AddSpectator(varname, 'D' );
   }


   std::vector<Double_t> signalWeights;   
   std::vector<Double_t> backgroundWeights;


   for(int s = 0; s < inputSarray.size(); s++) signalWeights.push_back(xsecSarray[s]);
   for(int b = 0; b < inputBarray.size(); b++) backgroundWeights.push_back(xsecBarray[b]);

   // You can add an arbitrary number of signal or background trees
   for(int s = 0; s < inputSarray.size(); ++s) {
     std::cout << "Adding: " << signal_strings[s].c_str() << " with xsec= " << xsecSarray[s]*fac_signal << std::endl;
     dataloader->AddSignalTree( signals[s], signalWeights[s] );
   }
   for(int b = 0; b < inputBarray.size(); ++b) {
     std::cout << "Adding: " << bkg_strings[b].c_str() << " with xsec= " << xsecBarray[b] << std::endl;
     dataloader->AddBackgroundTree( backgrounds[b], backgroundWeights[b] );
   }

   dataloader->SetSignalWeightExpression( "weight" );
   dataloader->SetBackgroundWeightExpression( "weight" );

   // Apply additional cuts on the signal and background samples (can be different)
   TCut mycuts = ""; // for example: TCut mycuts = "abs(var1)<0.5 && abs(var2-0.5)<1";
   TCut mycutb = ""; // for example: TCut mycutb = "abs(var1)<0.5";

   // Tell the dataloader how to use the training and testing events
   std::string dataloader_s1 = "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:SplitSeed=";
   std::string dataloader_s2 = std::to_string(rseed);
   //std::string dataloader_s3 = ":NormMode=EqualNumEvents:!V";
   std::string dataloader_s3 = ":NormMode=NumEvents:!V";
   std::string dataloader_string = dataloader_s1 + dataloader_s2 + dataloader_s3;
   
   dataloader->PrepareTrainingAndTestTree( mycuts, mycutb,
					   dataloader_string );
   // "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=EqualNumEvents:!V" );

   // ### Book MVA method 
   if (Use["BDT"])  // Adaptive Boost
      factory->BookMethod( dataloader, TMVA::Types::kBDT, "BDT",
			   //"!H:!V:NTrees=400:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20:NegWeightTreatment=IgnoreNegWeightsInTraining" );
			   //"!H:!V:NTrees=200:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=CrossEntropy:nCuts=20:NegWeightTreatment=IgnoreNegWeightsInTraining" );
			   //"!H:!V:NTrees=850:MinNodeSize=2.5%:MaxDepth=3:BoostType=AdaBoost:SeparationType=GiniIndex:nCuts=20:VarTransform=Decorrelate:NegWeightTreatment=IgnoreNegWeightsInTraining" );
			   "!H:!V:NTrees=200:MinNodeSize=45%:MaxDepth=1:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=200:NegWeightTreatment=InverseBoostNegWeights:VarTransform=Decorrelate" );
			   //"!H:!V:NTrees=400:MinNodeSize=20%:BoostType=Grad:Shrinkage=0.10:UseBaggedBoost:BaggedSampleFraction=0.5:nCuts=200:MaxDepth=1:NegWeightTreatment=IgnoreNegWeightsInTraining:UseRandomisedTrees" );

   // Now you can tell the factory to train, test, and evaluate the MVAs
   //
   // Train MVAs using the set of training events
   factory->TrainAllMethods();

   // Evaluate all MVAs using the set of test events
   factory->TestAllMethods();

   // Evaluate and compare performance of all configured MVAs
   factory->EvaluateAllMethods();

   std::cout << "mass = " << mass << std::endl;
   std::cout << "std::to_string(int(mass)) = "  << std::to_string(int(mass)) << std::endl;


   std::string dir_mva_bdt_s = "TMVA_results_WW/dataset_" + std::to_string(int(mass)) + "/Method_BDT/BDT/MVA_BDT_S";
   std::string dir_mva_bdt_b = "TMVA_results_WW/dataset_" + std::to_string(int(mass)) + "/Method_BDT/BDT/MVA_BDT_B";
   std::string dir_mva_bdt_train_s = "TMVA_results_WW/dataset_" + std::to_string(int(mass)) + "/Method_BDT/BDT/MVA_BDT_Train_S";
   std::string dir_mva_bdt_train_b = "TMVA_results_WW/dataset_" + std::to_string(int(mass)) + "/Method_BDT/BDT/MVA_BDT_Train_B";
   TH1 *TestHist_S = (TH1 *)outputFile->Get(dir_mva_bdt_s.c_str());
   TH1 *TestHist_B = (TH1 *)outputFile->Get(dir_mva_bdt_b.c_str());
   TH1 *TrainHist_S = (TH1 *)outputFile->Get(dir_mva_bdt_train_s.c_str());
   TH1 *TrainHist_B = (TH1 *)outputFile->Get(dir_mva_bdt_train_b.c_str());



   double Klmgv_S = TestHist_S->KolmogorovTest(TrainHist_S, "X");
   double Klmgv_B = TestHist_B->KolmogorovTest(TrainHist_B, "X");
   
   std::cout << "Kolmogorov Test Signal = " << Klmgv_S << std::endl;
   std::cout << "Kolmogorov Test Background = " << Klmgv_B << std::endl;
   double Klmgv_cut = 0.53;

   //check the Kolmogorov test  
   if(Klmgv_S < Klmgv_cut || Klmgv_B < Klmgv_cut) {
     std::cout << "Kolmogorov test results insufficient" << std::endl; 
     std::cout << "Maximum significance = -1 found at effS, effB, rejB= -1 -1 -1 with NS, NB= -1 -1 error on signif=-1 initial cross sections S, B= " << total_xsecS*fac_signal << " " << total_xsecB << std::endl;
     // Save the output
     outputFile->Close();
     
     delete factory;
     delete dataloader;
     // Launch the GUI for the root macros
     if (!gROOT->IsBatch()) TMVA::TMVAGui( outfileName );
     
     return 0;
   }


   TGraph* roc;
   // get the ROC curve from the BDT
   if (Use["BDT"]) roc = factory->GetROCCurve( dataloader, "BDT", false );
   
   Double_t *x = roc->GetX(); // Signal efficiency
   Double_t *y = roc->GetY(); // Background rejection
   Int_t     n = roc->GetN(); // Length of arrays*/
   //get the maximum significance for given S and B:
   double S(0.), B(0.);
   for(int s = 0; s < xsecSarray.size(); s++) S += xsecSarray[s] * Lumi * fac_signal;
   for(int b = 0; b < xsecBarray.size(); b++) B += xsecBarray[b] * Lumi;

   std::cout << "S=" << S << " B = " << B << std::endl;
   double Signif = 0.;
   double Signif_i = 0.;
   double DeltaSignif_i(0.), DeltaSignif(0.);
   
   double effS = 0;
   double effB = 0.;
   double effS_best = 0.;
   double rejB_best = 0.;
   double effB_best = 0.;
   double NS(0.), NB(0.);
   double NS_best(0.), NB_best(0.);

   // the minimum number of acceptable signal events
   double NS_min = 100.; 
  
   //systematic uncertainties
   double S_syst = 0.05;
   double B_syst = 0.05;
   std::cout << "Initial significance= " << S / sqrt ( B + pow( B_syst * B, 2 ) ) << std::endl;

   // the maximum acceptable error on the significance
   double DeltaSignif_max = 0.2;
   
   for(int i=0; i<n; i++) {
     effS = x[i];
     effB = 1-y[i];
     if(effB > 0 && effS > 0) { 

       //estimate the number of events:
       NS = effS * total_xsecS * fac_signal * Lumi;
       NB = effB * total_xsecB * Lumi;

       //discovery:
       //Signif_i = NS / sqrt ( NS + NB + pow( S_syst * NS, 2 ) + pow( B_syst * NB, 2 ) );
       //calculate the statistical error on the significance:
       // DeltaSignif_i = sqrt( pow( (1/sqrt(NS+NB) - 0.5 * NS/pow(NS+NB,1.5)), 2) * NS + pow( -0.5 * NS/pow(NS+NB,1.5), 2) * NB );

       //exclusion:
       Signif_i = NS / sqrt ( NB + pow( B_syst * NB, 2 ) );
       //calculate the statistical error on the significance:
       DeltaSignif_i = sqrt( NS/NB + 0.25 * pow(NS/NB,2) );
       
       //std::cout << "DeltaSignif_i= " << DeltaSignif_i << std::endl;			     
       
       if(Signif_i > Signif && NS > NS_min) { Signif = Signif_i; effS_best = effS; rejB_best = 1-effB; effB_best = effB; NS_best = NS; NB_best = NB; DeltaSignif = DeltaSignif_i; }
     }
     //std::cout << "SigEff=" << x[i] << ", BkgRej=" << y[i] << std::endl;
   }
   std::cout << "Maximum significance = " << Signif << " found at effS, effB, rejB= " << effS_best << " " << effB_best << " " << rejB_best << " with NS, NB= " << NS_best << " " << NB_best << " error on signif=" << DeltaSignif << " initial cross sections S, B= " << total_xsecS*fac_signal << " " << total_xsecB << std::endl;

   // get the BDT score corresponding to the efficiency of the signal that gives the max signif. 
   /* auto tbdt = (TTree*)outputFile->Get("dataset/TestTree");
   int nbdt =  tbdt->Draw("BDT","classID==0");
   int nbdt_b =  tbdt->Draw("BDT","classID==1", "SAME");
   auto scoreVector = tbdt->GetV1();
   double p[99];
   for (int i = 0; i < 99; ++i) p[i] = 0.01*double(i+1); 
   double q[99]; 
   TMath::Quantiles(n, 99, scoreVector, q, p, false);
   std::cout << "Finding BDT score for signal efficiency (per-cent level)" << std::endl;
   double delta_effS_min = 1.E99;
   int i_effS_min = -1;
   double BDT_effS_min = -1;
   for (int i = 0; i< 99; ++i) {
     if( fabs((1.-p[i]) - effS_best) < delta_effS_min ) {
       delta_effS_min = fabs((1.-p[i]) - effS_best);
       i_effS_min = i;
       BDT_effS_min = q[i];
     }
   } 
   std::cout << " signal efficiency " << 1.-p[i_effS_min] << " BDT score " << q[i_effS_min] << std::endl;*/
   std::string dir_mva_bdt_effs = "TMVA_results_WW/dataset_" + std::to_string(int(mass)) + "/Method_BDT/BDT/MVA_BDT_effS";
   TH1F *mva_bdt_effs = (TH1F*)outputFile->Get(dir_mva_bdt_effs.c_str());
   //mva_bdt_effs->Draw();
   /*std::vector<double> *v = new std::vector<double>(mva_bdt_effs->GetNbinsX());
   for (int i = 0; i < mva_bdt_effs->GetNbinsX(); i++) {
     v->at(i) = mva_bdt_effs->GetBinContent(i + 1);
     //std::cout << "mva_bdt_effs->GetBinContent(i + 1)= " << mva_bdt_effs->GetBinContent(i + 1) << endl;
     }*/
   double delta_effS_min2 = 1.E99;
   int i_effS_min2 = -1;
   double BDT_effS_min2 = -1;
   double effS_min2 = -1;
   for(int i=1;i<=mva_bdt_effs->GetNbinsX();i++) { 
     for(int j=1;j<=mva_bdt_effs->GetNbinsY();j++) {
       //std::cout <<mva_bdt_effs->GetXaxis()->GetBinCenter(i)<<"\t"<<mva_bdt_effs->GetBinContent(i,j)<<std::endl;
       if( fabs(mva_bdt_effs->GetBinContent(i,j) - effS_best) < delta_effS_min2 ) {
	 delta_effS_min2 = fabs(mva_bdt_effs->GetBinContent(i,j) - effS_best);
	 i_effS_min2 = i;
	 BDT_effS_min2 = mva_bdt_effs->GetXaxis()->GetBinCenter(i);
	 effS_min2 = mva_bdt_effs->GetBinContent(i,j);
       }
     }
   }
   std::cout << " signal efficiency " << effS_min2 << " BDT score " << BDT_effS_min2 << std::endl;

   
   /* tbdt->SetLineColor(kRed);
   std::string signalstr = "BDT>" + std::to_string(q[i_effS_min]) + " && classID==0";
   tbdt->Draw("variables_4_",signalstr.c_str(), "");
   tbdt->SetLineColor(kBlue);
   std::string backgroundstr = "BDT>" + std::to_string(q[i_effS_min]) + "&& classID==1";
   tbdt->Draw("variables_4_",backgroundstr.c_str(),"same");
   TH1 *myh = (TH1*)gPad->GetPrimitive("htemp");
   Int_t nb = myh->GetNbinsX();
  
   for (Int_t i=1; i<=nb; i++) {
      printf("%g %g\n",
             myh->GetBinLowEdge(i)+myh->GetBinWidth(i)/2,
             myh->GetBinContent(i));
	     }*/
   
   // Save the output
   outputFile->Close();

   delete factory;
   delete dataloader;
   
   /* Apply the BDT response to the whole sample
    */
   /* TMVA::Reader *reader = new TMVA::Reader( "V:Color:!Silent" );
   Float_t *var = new Float_t[nvar];
   for(int n=0; n < nvar; ++n) {
     std::string varname;
     varname = "variables[" + std::to_string(n) + "]";
     reader->AddVariable(varname, &var[n]);
   }
   
   double variables[14];
   reader->BookMVA("BDT method", "./dataset/weights/TMVAClassification_BDT.weights.xml");  
   Float_t BDT_response;

   //loop over the signals
   for(int s = 0; s < inputSarray.size(); s++) { 
     signals[s]->SetBranchAddress("variables",&variables);
     for (Long64_t ievt=0; ievt<signals[s]->GetEntries();ievt++) {
       //for (int ievt=0; ievt<100;ievt++) {
       if (ievt%1000 == 0) std::cout << "--- ... Processing event: " << ievt <<std::endl;
       signals[s]->GetEntry(ievt);
       for(int n=0; n < nvar; ++n) {
	 //std::cout << "variables[n] = " << variables[n] << std::endl;
	 var[n]=float(variables[n]);
       }
       BDT_response=reader->EvaluateMVA("BDT method");
       //std::cout << "BDT_response = " << BDT_response << std::endl;
     }
     std::cout << "Done signal " << s << std::endl;
   }

   //loop over the backgrounds
   for(int b = 0; b < inputBarray.size(); b++) { 
     backgrounds[b]->SetBranchAddress("variables",&variables);
     for (Long64_t ievt=0; ievt<backgrounds[b]->GetEntries();ievt++) {
       //for (int ievt=0; ievt<100;ievt++) {
       if (ievt%1000 == 0) std::cout << "--- ... Processing event: " << ievt <<std::endl;
       backgrounds[b]->GetEntry(ievt);
       for(int n=0; n < nvar; ++n) {
	 //std::cout << "variables[n] = " << variables[n] << std::endl;
	 var[n]=float(variables[n]);
       }
       BDT_response=reader->EvaluateMVA("BDT method");
       //std::cout << "BDT_response = " << BDT_response << std::endl;
     }
     std::cout << "Done background " << b << std::endl;

     }*/

   // Launch the GUI for the root macros
   if (!gROOT->IsBatch()) TMVA::TMVAGui( outfileName );

   return 0;
}

int main( int argc, char** argv )
{
   // Select methods (don't look at this code - not of interest)
   TString methodList;
   /* for (int i=1; i<argc; i++) {
      TString regMethod(argv[i]);
      if(regMethod=="-b" || regMethod=="--batch") continue;
      if (!methodList.IsNull()) methodList += TString(",");
      methodList += regMethod;
      }*/
   double Lumi;
   std::string signal_file;
   std::string background_file;
   double fac_signal;
   int nvar; //the number of variables in the variables[] array
   int rseed; //the random seed
   double mass; //the mass under investigation
   
   Lumi = std::stod(argv[1]);
   signal_file = argv[2];
   background_file = argv[3];
   fac_signal = std::stod(argv[4]);
   nvar = std::stoi(argv[5]);
   rseed = std::stoi(argv[6]);
   mass = std::stod(argv[7]);
   return TMVAClassification_singlet_multi_output(Lumi, signal_file, background_file, fac_signal, nvar, rseed, mass, methodList);
}
