import ROOT
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from scipy.optimize import curve_fit
from lmfit.models import GaussianModel, VoigtModel, LinearModel, ConstantModel
import scipy

# read in the root file containing the BDT output
f = ROOT.TFile.Open("BDT_output.root")
tree = f.Get("Data3")
nev = tree.GetEntries()
print(('number of entries = ', nev))

colors = [ 'green', 'orange', 'red', 'magenta', 'blue', 'cyan', 'black', 'brown', 'violet'] # 9 colours

#BDT_cut = 0.416142 # 350
#BDT_cut = 0.498212 # 900
mass = 600. # the known mass 
BDT_cut = 0.306361 # 600
lumi = 30000
alpha = 0.05 # systematic uncertainty
signal_amplification = 10. # bring the signal up to the desired cross section 

print(('BDT cut =', BDT_cut))

# loop over the events and apply the BDT cut 
m4l = []
weights = []
m4l_dict = {}
weights_dict = {}
event_id_array = []
pass_id = {}
total_id = {}
pass_weight_id = {}
total_weight_id = {}
for evt in tree:
    #print(evt.event_id, evt.BDT_response, evt.m4l, evt.wgt)
    if evt.event_id not in list(total_id.keys()):
        total_id[evt.event_id] =[ np.sign(evt.wgt)]
        total_weight_id[evt.event_id] =[ evt.wgt ]
    else:
        total_id[evt.event_id].append(np.sign(evt.wgt))
        total_weight_id[evt.event_id].append(evt.wgt)
    if evt.BDT_response > BDT_cut:
        if evt.event_id not in list(pass_id.keys()):
            pass_id[evt.event_id] =[ np.sign(evt.wgt)]
            pass_weight_id[evt.event_id] =[evt.wgt]
        else:
            pass_id[evt.event_id].append(np.sign(evt.wgt))
            pass_weight_id[evt.event_id].append(evt.wgt)
        if evt.event_id not in event_id_array:
            event_id_array.append(evt.event_id)
        m4l.append(evt.m4l)
        if evt.event_id not in list(m4l_dict.keys()):
            m4l_dict[evt.event_id] = [evt.m4l]
            weights_dict[evt.event_id] = [evt.wgt]
        else:
            m4l_dict[evt.event_id].append(evt.m4l)
            weights_dict[evt.event_id].append(evt.wgt)
        weights.append(evt.wgt)


# calculate the efficiency of the BDT cut
efficiency_id = {}
for key in sorted(m4l_dict.keys()):
    efficiency_id[key] = np.sum(pass_weight_id[key])/np.sum(total_weight_id[key])
print(("efficiencies of BDT cut:", efficiency_id))


# define a label map
label_map = {}
label_map[2] = '$ gg \\rightarrow h_2 \\rightarrow ZZ \\rightarrow 4 \\ell$'
label_map[1] = '$ pp \\rightarrow h_2 jj \\rightarrow ZZ jj \\rightarrow 4 \\ell jj $'
label_map[-1] = '$ gg \\rightarrow Z Z \\rightarrow 4 \\ell$'
label_map[-2] = '$ pp \\rightarrow 4 \\ell$'

# push the events that pass into arrays
# counters for signal and background events:
S = 0
B = 0
# arrays to store the events:
m4l_arrays = []
m4l_arrays_backgroundonly = []
weights_arrays = []
weights_arrays_backgroundonly = []
color_map = {}
colors_keys = []
colors_keys_backgroundonly = []
labels_keys = []
labels_keys_backgroundonly = []
Amplification = {} # contains the arrays of factors to modify the signal cross section
n = 0
print(('expected number of events at Lumi = ', lumi, 'fb^-1 after BDT cut for:'))
for key in sorted(m4l_dict.keys()):
    #print(key)
    if int(key) > 0: # multiply the signal by a factor 
        Amplification[key] = signal_amplification
    else:
        Amplification[key] = 1.
    m4l_arrays.append(m4l_dict[key])
    weights_arrays.append(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
    print(('\t', key, '=', np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))))
    color_map[key] = colors[n]
    colors_keys.append(colors[n])
    labels_keys.append(label_map[key])
    if int(key) < 0:
        m4l_arrays_backgroundonly.append(m4l_dict[key])
        weights_arrays_backgroundonly.append(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
        colors_keys_backgroundonly.append(colors[n])
        labels_keys_backgroundonly.append(label_map[key])
        B = B + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi/len(pass_id[key]))
    else:
        S = S + np.sum(np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]))
    n = n+1
    
Significance = S/math.sqrt(B + (alpha*B)**2)
print(("Significance =", Significance))
    
# create total signal and background histograms
nbins = 40
xmin_h = 495
xmax_h = 659
m4l_background, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
m4l_signal, edges = np.histogram(0., nbins, range=(xmin_h,xmax_h))
for key in sorted(m4l_dict.keys()):
    hist_key, edges = np.histogram(m4l_dict[key],nbins, weights=np.array(weights_dict[key])*efficiency_id[key]*lumi*Amplification[key]/len(pass_id[key]), range=(xmin_h,xmax_h))
    # background
    if int(key) < 0:
        m4l_background = np.add( m4l_background, hist_key)
    # signal
    else:
        m4l_signal = np.add( m4l_signal, hist_key)

m4l_total = np.add( m4l_signal, m4l_background )

#print(m4l_signal)
#print(m4l_background)


###################################
# START PLOTTING AND FITTING HERE #
###################################


#############################
# TOTAL AND BACKGROUND PLOT #
#############################

plot_type = 'BDT_m4l'
outputdirectory = './'
fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

xmin = xmin_h
xmax = xmax_h
ylab = "Number of Events per bin"
xlab = '$m_{4l}$ [GeV]'
# number of bins
n_bins = nbins

# calculate the bin mid-points from the edges
bins = (edges[1:] + edges[:-1])/2
print(('bins=', bins))
    
# plot the total number of events with error bars
ax.errorbar(bins, m4l_total, yerr=np.sqrt(m4l_signal), xerr=0., ms=5, capthick=20, fmt='bo', label='Signal+Background')
    
# the colour map
print(color_map)

# histogram the background
ax.hist(m4l_arrays_backgroundonly, n_bins, weights=weights_arrays_backgroundonly, density=False, histtype='bar', stacked=True, color=colors_keys_backgroundonly, range=(xmin_h,xmax_h), label=labels_keys_backgroundonly)
ax.set_title('stacked bar')
#plt.step(bins, m4l_background, ls='-', color='magenta', where='mid')

# create legend and plot/font size
ax.legend()
ax.legend(loc="lower right", numpoints=1, frameon=False, prop={'size':8})
#pl.rcParams.update({'font.size': 15})
#pl.rcParams['figure.figsize'] = 12, 12

# set axis ticks
ax.xaxis.set_major_locator(MultipleLocator(100))
ax.xaxis.set_minor_locator(MultipleLocator(20))
#ax.yaxis.set_major_locator(LogLocator(base=10, numticks=15))

# set labels and limits
ax.set_title('100 TeV/' + str(lumi) + ' fb$^{-1}$')
ax.set_xlabel(xlab, fontsize=20)
ax.set_ylabel(ylab, fontsize=20)
plt.xlim([xmin, xmax])

# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})

# save the figure
print('---')
print('saving the figure')
fig.tight_layout()
# save the figure in PDF format
infile = plot_type + '.dat'
print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
plt.close(fig)


#############################
# DO THE FIT                #
#############################

# define the function to fit
def BreitWigner(x, A, M, G):
    return A* M**2 * G**2/ ( (x**2 - M**2)**2 + M**2 * G**2 )

# calculate the poisson errors at 1 sigma
def poisson_errors(N):
    y = (1 - scipy.special.ndtr(1))
    a =N+np.array(1)
    up = (N-scipy.special.gammaincinv(a,y))
    down = (scipy.special.gammaincinv(a,1-y)-N)
    di = 0
    for d in down:
        if N[di] - d < 0:
            down[di] = N[di]
        di = di + 1
    return up, down

# calculate the poisson errors at 1 sigma and symmetrize
def poisson_errors_symmetrize(N):
    y = (1 - scipy.special.ndtr(1))
    a =N+np.array(1)
    up = (N-scipy.special.gammaincinv(a,y))
    down = (scipy.special.gammaincinv(a,1-y)-N)
    di = 0
    for d in down:
        if N[di] - d < 0:
            down[di] = N[di]
        di = di + 1
    return (up+down)/2.


def fit_poisson(Ntot, Nbkg, xbins, nfit):
    central_param_array = []
    # do the fit nfit times:
    for ii in range(0,nfit):
        Nii_tot = []
        for it in range(0, len(Ntot)):
            Nii_tot.append(np.random.poisson(Ntot[it], 1)[0])
        Nii_bkg = []
        for it in range(0, len(Nbkg)):
            Nii_bkg.append(np.random.poisson(Nbkg[it], 1)[0])
        Nii_tot = np.array(Nii_tot)
        Nii_bkg = np.array(Nii_bkg)
        Nii = np.subtract(Nii_tot, Nii_bkg)
        Nii[Nii < 0] = 0
        mod = VoigtModel()
        pars = mod.guess(Nii, x=bins)
        out = mod.fit(Nii, pars, x=bins)
        central_param_array.append(out.params['center'].value)

    n = len(central_param_array)
    index_med = n / 2 # median.
    index_84 = int(round(n * 0.84135)) # 84th percentile from median.
    index_16 = int(round(n * 0.15865))
    central_param_array = sorted(np.array(central_param_array))
    central_mean = np.mean(central_param_array)
    central_std = np.std(central_param_array)
    central_median = np.median(central_param_array)
    central_84 = central_param_array[index_84]
    central_16 = central_param_array[index_16]
    #return central_mean, central_std, central_median, central_84, central_16
    return central_median, central_84-central_median, central_median-central_16, central_mean, central_std, 

# calculate the POISSON errors
m4l_total_poisson_up, m4l_total_poisson_down = poisson_errors(np.add(m4l_total,m4l_background))
m4l_background_poisson_up, m4l_background_poisson_down = poisson_errors(m4l_background)
m4l_signal_poisson_up = np.sqrt( np.add(m4l_total_poisson_up**2, m4l_background_poisson_up**2))
m4l_signal_poisson_down = np.sqrt( np.add(m4l_total_poisson_down**2, m4l_background_poisson_down**2))
di = 0
for d in m4l_signal_poisson_down:
    if m4l_signal[di] - d < 0:
        m4l_signal_poisson_down[di] = m4l_signal[di]
    di = di + 1

m4l_signal_poisson_symmetric = 0.5 * np.add(m4l_signal_poisson_up, m4l_signal_poisson_down)

# test the poisson fit:
print(m4l_total)
print(m4l_background)
print((fit_poisson(m4l_total, m4l_background, bins, 100)))
#print('mass, error=', mass, error)
    
# do the fit 
mod = VoigtModel()
pars = mod.guess(m4l_signal, x=bins)
out = mod.fit(m4l_signal, pars, x=bins, scale_covar=True, weights=1/m4l_signal_poisson_symmetric)

print((out.fit_report(min_correl=0.25)))
#print(out.params)
fitparams = out.params
#popt, pcov = curve_fit(BreitWigner, bins, m4l_signal_norm, sigma=inverse_m4l_signal_error_squared)

#print(popt)

#############################
# SIGNAL ONLY PLOT FOR FIT  #
#############################

plot_type = 'BDT_m4l_signal'
outputdirectory = './'
fig, ((ax)) = plt.subplots(nrows=1, ncols=1)

# plot the signal with error bars
ax.errorbar(bins, m4l_signal, yerr=[m4l_signal_poisson_down, m4l_signal_poisson_up], xerr=0., ms=5, capthick=20, fmt='bo', label='Signal after background subtraction')
#ax.errorbar(bins, m4l_signal_norm, yerr=m4l_signal_poisson_symmetric_norm, xerr=0., ms=5, capthick=20, fmt='go', label='Signal after background subtraction')

# plot the fit
x = np.arange(xmin_h, xmax_h, 1)
plt.plot(x, out.eval(x=x), '--', color='red', label='best fit')
dely = out.eval_uncertainty(x=x)

plt.fill_between(x, out.eval(x=x)-dely,out.eval(x=x)+dely, color='red', alpha=0.2)

#plt.plot(bins, out.best_fit, '--', color='red', label='best fit')


# create legend and plot/font size
ax.legend()
ax.legend(loc="upper right", numpoints=1, frameon=False, prop={'size':8})


# save the figure
print('---')
print('saving the figure')
fig.tight_layout()
# save the figure in PDF format
infile = plot_type + '.dat'
print(('output in', outputdirectory + infile.replace('.dat','.pdf')))
plt.savefig(outputdirectory + infile.replace('.dat','.pdf'), bbox_inches='tight', dpi=400)
plt.savefig(outputdirectory + infile.replace('.dat','.png'), bbox_inches='tight')
plt.close(fig)



