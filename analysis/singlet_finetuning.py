#! /usr/bin/env python

import cmath, string, os, sys, fileinput, pprint, math
from optparse import OptionParser
import subprocess
import random
import sys
import time
import datetime
import os.path
import numpy as np
import matplotlib
matplotlib.use('PDF')
import matplotlib.mlab as ml
import mpmath as mp
import pylab as pl
from scipy import interpolate, signal
#from matplotlib.mlab import griddata
import matplotlib.font_manager as fm
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator, ScalarFormatter, FuncFormatter)
import matplotlib.patches as mpatches
import math
from scipy.interpolate import interp1d
from collections import defaultdict
from collections import OrderedDict
import matplotlib.gridspec as gridspec
from optparse import OptionParser
import matplotlib.ticker as ticker
from matplotlib import container
import random
import scipy
from scipy import stats
from scipy.optimize import fsolve
#from scipy.interpolate import griddata
import sys
from numpy.linalg import inv
from numpy.linalg import eig
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import norm
from scipy.optimize import root
from decimal import *
from matplotlib.ticker import Locator
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
import pickle
import re
from matplotlib.colors import ListedColormap
from past.builtins import execfile
from math import log10, floor

def round_sig(x, sig=2):
    if x == 0.:
        return 0.
    if math.isnan(x) is True:
        print('Warning, NaN!')
        return 0.
    return round(x, sig-int(floor(log10(abs(x))))-1)


#def dm1da1(v0, x0, a1, a2, b3, b4):
#    return (0.3535533905932738*(b3 + 2*v0**2 + (a1*v0**2)/(4.*x0**2) + 4*b4*x0 - ((((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2)**0.5*((-2*v0**2*(a1 + 2*a2*x0)**2*(-b3 + 2*v0**2 - (a1*v0**2)/(4.*x0**2) - 4*b4*x0))/((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**3 + (4*a2*v0**2*(a1 + 2*a2*x0))/((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2)) - (1.*(-b3 + 2*v0**2 - (a1*v0**2)/(4.*x0**2) - 4*b4*x0)*((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2))/(((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(-(a1*v0**2)/(4.*x0) + b3*x0 + 2*v0**2*x0 + 2*b4*x0**2 - (((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/((a1*v0**2)/(4.*x0) - b3*x0 + 2*v0**2*x0 - 2*b4*x0**2)**2))

#def dm1da2(v0, x0, a1, a2, b3, b4):
#   return (0.3535533905932738*(2*v0**2 - v0**2/(4.*x0) - (((2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*((-2*v0**2*(2*v0**2 + v0**2/(4.*x0))*(a1 + 2*a2*x0)**2)/(2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3 + (2*v0**2*(a1 + 2*a2*x0))/(2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (1.*(2*v0**2 + v0**2/(4.*x0))*(2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*a1*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*a1*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

#def dm1db3(v0, x0, a1, a2, b3, b4):
#    return (0.3535533905932738*(2*v0**2 - (((2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*((-4*v0**4*(a1 + 2*a2*x0)**2)/(2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3 + (4*v0**2*x0*(a1 + 2*a2*x0))/(2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (2.*v0**2*(2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*a2*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*a2*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

#def dm1db4(v0, x0, a1, a2, b3, b4):
#    return (0.3535533905932738*(2*v0**2 + 2*x0**2 + (v0**2*(a1 + 2*a2*x0)**2*(2*v0**2 - 2*x0**2)*((2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)/((2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (1.*(2*v0**2 - 2*x0**2)*(2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*b4*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*b4*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm1dx0(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(b3 + (a1*v0**2)/(4.*x0**2) + 4*b4*x0 - (((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*((-2*v0**2*(a1 + 2*a2*x0)**2*(-b3 - (a1*v0**2)/(4.*x0**2) - 4*b4*x0))/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3 + (4*a2*v0**2*(a1 + 2*a2*x0))/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (1.*(-b3 - (a1*v0**2)/(4.*x0**2) - 4*b4*x0)*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm1da1(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(-v0**2/(4.*x0) - (((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*(-(v0**4*(a1 + 2*a2*x0)**2)/(2.*x0*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3) + (2*v0**2*(a1 + 2*a2*x0))/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (0.25*v0**2*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(x0*((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm1da2(lam, v0, x0, a1, a2, b3, b4):
    return (-0.7071067811865476*v0**2*x0*(a1 + 2*a2*x0))/(((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)*cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)))

def dm1db3(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(x0 - (v0**2*x0*(a1 + 2*a2*x0)**2*((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) + (1.*x0*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm1db4(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(2*x0**2 - (2*v0**2*x0**2*(a1 + 2*a2*x0)**2*((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) + (2.*x0**2*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 - ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm2dx0(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(b3 + (a1*v0**2)/(4.*x0**2) + 4*b4*x0 + (((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*((-2*v0**2*(a1 + 2*a2*x0)**2*(-b3 - (a1*v0**2)/(4.*x0**2) - 4*b4*x0))/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3 + (4*a2*v0**2*(a1 + 2*a2*x0))/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) + (1.*(-b3 - (a1*v0**2)/(4.*x0**2) - 4*b4*x0)*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 + ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm2da1(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(-v0**2/(4.*x0) + (((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*(-(v0**4*(a1 + 2*a2*x0)**2)/(2.*x0*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3) + (2*v0**2*(a1 + 2*a2*x0))/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(2.*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) + (0.25*v0**2*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/(x0*((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 + ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm2da2(lam, v0, x0, a1, a2, b3, b4):
    return (0.7071067811865476*v0**2*x0*(a1 + 2*a2*x0))/(((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)*cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 + ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)))

def dm2db3(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(x0 + (v0**2*x0*(a1 + 2*a2*x0)**2*((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (1.*x0*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 + ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dm2db4(lam, v0, x0, a1, a2, b3, b4):
    return (0.3535533905932738*(2*x0**2 + (2*v0**2*x0**2*(a1 + 2*a2*x0)**2*((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5)/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**3*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)) - (2.*x0**2*(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))/((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5))/cmath.sqrt(2*lam*v0**2 - (a1*v0**2)/(4.*x0) + b3*x0 + 2*b4*x0**2 + ((2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2)**0.5*cmath.sqrt(1 + (v0**2*(a1 + 2*a2*x0)**2)/(2*lam*v0**2 + (a1*v0**2)/(4.*x0) - b3*x0 - 2*b4*x0**2)**2))

def dmu2dlam(lam, v0, x0, a1, a2, b3, b4):
    return v0**2

def dmu2da1(lam, v0, x0, a1, a2, b3, b4):
    return x0/2.

def dmu2da2(lam, v0, x0, a1, a2, b3, b4):
    return x0**2/2.

def dmu2dx0(lam, v0, x0, a1, a2, b3, b4):
    return (a2*x0)/2. + (a1 + a2*x0)/2.

def db2dx0(lam, v0, x0, a1, a2, b3, b4):
    return (2*x0*(2*b4*x0 + 2*(b3 + b4*x0)) + 2*(a2*v0**2 + 2*x0*(b3 + b4*x0)))/(4.*x0) - (a1*v0**2 + 2*x0*(a2*v0**2 + 2*x0*(b3 + b4*x0)))/(4.*x0**2)

def db2da1(lam, v0, x0, a1, a2, b3, b4):
    return x0

def db2da2(lam, v0, x0, a1, a2, b3, b4):
    return v0**2/2.

def db2db3(lam, v0, x0, a1, a2, b3, b4):
    return x0

def db2db4(lam, v0, x0, a1, a2, b3, b4):
    return x0**2

def mu2_func(lam, v0, x0, a1, a2, b3, b4):
    return lam*v0**2 + (x0*(a1 + a2*x0))/2.

def b2_func(lam, v0, x0, a1, a2, b3, b4):
    return (a1*v0**2 + 2*x0*(a2*v0**2 + 2*x0*(b3 + b4*x0)))/(4.*x0)

def mh_sq(lam, v0):
    return round_sig(2 * lam * v0**2,10)

def ms_sq(a1, b3, b4, x0, v0):
    return round_sig(b3 * x0 + 2 * b4 * x0**2 - a1 * v0**2 / 4. / x0,10)

def mhs_sq(a1, a2, x0, v0):
    return round_sig((a1 + 2 * a2 * x0 ) * v0/2.,10)


def mass_and_mixing_MRM(lam, v0, x0, a1, a2, b3, b4):
    mhsq = mh_sq(lam, v0)
    mssq =  ms_sq(a1, b3, b4, x0, v0)
    mhssq = mhs_sq(a1, a2, x0, v0)
    A = mhsq + mssq
    B = abs(mhsq - mssq)
    D = cmath.sqrt(1 + (mhssq/(2*mhsq-mssq))**2)
    mh1 = cmath.sqrt(0.5 * (A-B*D))
    mh2 = cmath.sqrt(0.5 * (A+B*D))
    return mh1.real, mh2.real
    

def finetuning_m1(lam, v0, x0, a1, a2, b3, b4):
    m1 = 125.1
    fm1x0 = (x0/m1) * dm1dx0(lam, v0, x0, a1, a2, b3, b4)
    fm1a1 = (a1/m1) * dm1da1(lam, v0, x0, a1, a2, b3, b4)
    fm1a2 = (a2/m1) * dm1da2(lam, v0, x0, a1, a2, b3, b4)
    fm1b3 = (b3/m1) * dm1db3(lam, v0, x0, a1, a2, b3, b4)
    fm1b4 = (b4/m1) * dm1db4(lam, v0, x0, a1, a2, b3, b4)
    #print [abs(fm1x0), abs(fm1a1), abs(fm1a2), abs(fm1b3), abs(fm1b4)]
    return max([abs(fm1x0), abs(fm1a1), abs(fm1a2), abs(fm1b3), abs(fm1b4)])

def finetuning_m2(lam, v0, x0, a1, a2, b3, b4):
    m1, m2 = mass_and_mixing_MRM(lam, v0, x0, a1, a2, b3, b4)
    fm2x0 = (x0/m2) * dm2dx0(lam, v0, x0, a1, a2, b3, b4)
    fm2a1 = (a1/m2) * dm2da1(lam, v0, x0, a1, a2, b3, b4)
    fm2a2 = (a2/m2) * dm2da2(lam, v0, x0, a1, a2, b3, b4)
    fm2b3 = (b3/m2) * dm2db3(lam, v0, x0, a1, a2, b3, b4)
    fm2b4 = (b4/m2) * dm2db4(lam, v0, x0, a1, a2, b3, b4)
    return max([abs(fm2x0), abs(fm2a1), abs(fm2a2), abs(fm2b3), abs(fm2b4)])

def finetuning_mu2b2(lam, v0, x0, a1, a2, b3, b4):
    mu2 = mu2_func(lam, v0, x0, a1, a2, b3, b4)
    b2 = b2_func(lam, v0, x0, a1, a2, b3, b4)
    fmu2lam = (lam/mu2) * dmu2dlam(lam, v0, x0, a1, a2, b3, b4)
    fmu2a1 = (a1/mu2) * dmu2da1(lam, v0, x0, a1, a2, b3, b4)
    fmu2a2 = (a2/mu2) * dmu2da2(lam, v0, x0, a1, a2, b3, b4)    
    fmu2x0 = (x0/mu2) * dmu2dx0(lam, v0, x0, a1, a2, b3, b4)
    fb2x0 = (x0/b2) * db2dx0(lam, v0, x0, a1, a2, b3, b4)
    fb2a1 = (a1/b2) * db2da1(lam, v0, x0, a1, a2, b3, b4)
    fb2a2 = (a2/b2) * db2da2(lam, v0, x0, a1, a2, b3, b4)
    fb2b3 = (b3/b2) * db2db3(lam, v0, x0, a1, a2, b3, b4)
    fb2b4 = (b4/b2) * db2db4(lam, v0, x0, a1, a2, b3, b4)
    return max([abs(fmu2lam), abs(fmu2a1), abs(fmu2a2), abs(fmu2x0), abs(fb2x0), abs(fb2a1), abs(fb2a2), abs(fb2b3), abs(fb2b4)])
    
# calculate the fine tuning for given point:
#v0 = 246.
#lam = 0.23159402525
#x0 = 53.1533389  
#a1 = -840.9572336 
#a2 = 3.83242672  
#b3 = -1121.906784 
#b4 = 0.02032513346
#print mass_and_mixing_MRM(lam, v0, x0, a1, a2, b3, b4)
#print finetuning_m1(lam, v0, x0, a1, a2, b3, b4), finetuning_m2(lam, v0, x0, a1, a2, b3, b4)
#sprint finetuning_mu2b2(lam, v0, x0, a1, a2, b3, b4)
