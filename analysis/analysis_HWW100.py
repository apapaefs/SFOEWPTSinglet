####################
# H -> WW ANALYSIS #
####################

# list of analysed processes
Analyses.append('HWW100')

# the latex name of the parent process (without the decay into the final state):
LatexNameParent['HWW100'] = '$h_2 \\rightarrow W^+ W^-$'

# the latex name of the analysis
LatexName['HWW100'] = '$h_2 \\rightarrow W^+ W^- \\rightarrow e \\nu \\mu \\nu$'

# the Branching ratio of the primary decay particles of the heavy particle into the observed final state:
FacFinalState['HWW100'] = 2*BR_w_ellnu**2

# the proton-proton energy for the analysis in TeV
Energy['HWW100'] = 100

# location of analyses' info
Location['HWW100'] = '/Users/apapaefs/Documents/Projects/SFOEWPT_Singlet/analysis/HWW100/'

# analyses' executables (with respect to Location directory)
Executable['HWW100'] = 'HwSimPostAnalysis_LeptonEff2'

# analyses' executables for cuts (with respect to Location directory)
ExecutableCuts['HWW100'] = 'HwSimPostAnalysis_LeptonEff2_cuts'

# analyses' executables with smearing (with respect to Location directory)
ExecutableSmear['HWW100'] = 'HwSimPostAnalysis_LeptonEff2_smear'

# Processes involved in the analysis:
# these should correspond to the names of the templates for the .in files (minus the prefix 'HW-')
Processes['HWW100'] = {'gg-eta0-WW-evmuv-M200':200.0,
                           'gg-eta0-WW-evmuv-M250':250.0,
                         'gg-eta0-WW-evmuv-M300':300.0,
#                         'gg-eta0-WW-evmuv-M350':350.0,
                         'gg-eta0-WW-evmuv-M400':400.0,
#                         'gg-eta0-WW-evmuv-M450':450.0,
                         'gg-eta0-WW-evmuv-M500':500.0,
#                         'gg-eta0-WW-evmuv-M550':550.0,
                         'gg-eta0-WW-evmuv-M600':600.0,
                        #'gg-eta0-WW-evmuv-M650':650.0,
                         'gg-eta0-WW-evmuv-M700':700.0,
                         #'gg-eta0-WW-evmuv-M750':750.0,
                         'gg-eta0-WW-evmuv-M800':800.0,
#                         'gg-eta0-WW-evmuv-M850':850.0,
                         #'gg-eta0-WW-evmuv-M900':900.0,
                         'gg-eta0-WW-evmuv-M950':950.0,
                         'gg-eta0-WW-evmuv-M1000':1000.0,
#                         'gg-eta0-WW-evmuv-M1100':1100.0,
#                         'gg-eta0-WW-evmuv-M1200':1200.0,
#                         'gg-eta0-WW-evmuv-M1500':1500.0,
#                         'gg-eta0-WW-evmuv-M2000':2000.0,
                         'pp-eta0jj-WW-evmuv-M200':200.0,
                         'pp-eta0jj-WW-evmuv-M250':250.0,
                         'pp-eta0jj-WW-evmuv-M300':300.0,
#                        'pp-eta0jj-WW-evmuv-M350':350.0,
                         'pp-eta0jj-WW-evmuv-M400':400.0,
#                         'pp-eta0jj-WW-evmuv-M450':450.0,
                         'pp-eta0jj-WW-evmuv-M500':500.0,
#                        'pp-eta0jj-WW-evmuv-M550':550.0,
                         'pp-eta0jj-WW-evmuv-M600':600.0,
                        #'pp-eta0jj-WW-evmuv-M650':650.0,
                         'pp-eta0jj-WW-evmuv-M700':700.0,
                         #'pp-eta0jj-WW-evmuv-M750':750.0,
                         'pp-eta0jj-WW-evmuv-M800':800.0,
#                         'pp-eta0jj-WW-evmuv-M850':850.0,
                         #'pp-eta0jj-WW-evmuv-M900':900.0,
                         'pp-eta0jj-WW-evmuv-M950':950.0,
                         'pp-eta0jj-WW-evmuv-M1000':1000.0,
#                         'pp-eta0jj-WW-evmuv-M1100':1100.0,
#                         'pp-eta0jj-WW-evmuv-M1200':1200.0,
#                         'pp-eta0jj-WW-evmuv-M1500':1500.0,
#                         'pp-eta0jj-WW-evmuv-M2000':2000.0,
                         'pp-wpwm-evmuv':0.,
                         'pp-tt-wbwb-evbmuvb':0.,
                         'pp-zz-eemumu':0.,
                         'pp-wz-lvll':0.,
                         #'pp-singletop':0.,
                         'pp-wjets':0.
                            } # M ! =0 stands for signal, 0 for backgrounds
# the order of the processes
OrderProcesses['HWW100'] = {'gg-eta0-WW-evmuv-M200':0,
                            'gg-eta0-WW-evmuv-M250':0,
                         'gg-eta0-WW-evmuv-M300':0,
                         'gg-eta0-WW-evmuv-M350':0,
                         'gg-eta0-WW-evmuv-M400':0,
                         'gg-eta0-WW-evmuv-M450':0,
                         'gg-eta0-WW-evmuv-M500':0,
                         'gg-eta0-WW-evmuv-M550':0,
                         'gg-eta0-WW-evmuv-M600':0,
                         'gg-eta0-WW-evmuv-M650':0,
                         'gg-eta0-WW-evmuv-M700':0,
                         'gg-eta0-WW-evmuv-M750':0,
                         'gg-eta0-WW-evmuv-M800':0,
                         'gg-eta0-WW-evmuv-M850':0,
                         'gg-eta0-WW-evmuv-M900':0,
                         'gg-eta0-WW-evmuv-M950':0,
                         'gg-eta0-WW-evmuv-M1000':0,
                         'gg-eta0-WW-evmuv-M1100':0,
                         'gg-eta0-WW-evmuv-M1200':0,
                         'gg-eta0-WW-evmuv-M1500':0,
                         'gg-eta0-WW-evmuv-M2000':0,
                         'pp-eta0jj-WW-evmuv-M200':0,
                         'pp-eta0jj-WW-evmuv-M250':0,
                         'pp-eta0jj-WW-evmuv-M300':0,
                         'pp-eta0jj-WW-evmuv-M350':0,
                         'pp-eta0jj-WW-evmuv-M400':0,
                         'pp-eta0jj-WW-evmuv-M450':0,
                         'pp-eta0jj-WW-evmuv-M500':0,
                         'pp-eta0jj-WW-evmuv-M550':0,
                         'pp-eta0jj-WW-evmuv-M600':0,
                         'pp-eta0jj-WW-evmuv-M650':0,
                         'pp-eta0jj-WW-evmuv-M700':0,
                         'pp-eta0jj-WW-evmuv-M750':0,
                         'pp-eta0jj-WW-evmuv-M800':0,
                         'pp-eta0jj-WW-evmuv-M850':0,
                         'pp-eta0jj-WW-evmuv-M900':0,
                         'pp-eta0jj-WW-evmuv-M950':0,
                         'pp-eta0jj-WW-evmuv-M1000':0,
                         'pp-eta0jj-WW-evmuv-M1100':0,
                         'pp-eta0jj-WW-evmuv-M1200':0,
                         'pp-eta0jj-WW-evmuv-M1500':0,
                         'pp-eta0jj-WW-evmuv-M2000':0,
                         'pp-wpwm-evmuv':1,
                         'pp-tt-wbwb-evbmuvb':1,
                         'pp-zz-eemumu':1,
                         'pp-wz-lvll':1,
                         'pp-singletop':0,
                         'pp-wjets':2
        } # 0 stands for LO and 1 for NLO (i.e. MC@NLO), 2 for FxFx
        
# K-factors for the processes: MUST INCLUDE SYMMETRY FACTORS (e.g. for the lepton flavours)
KFacsProcesses['HWW100'] = {  # 2 for FLAVOURS (e+mu-, mu+e-) on top of  K-fac
                        'gg-eta0-WW-evmuv-M200':2.153378605*2,
                         'gg-eta0-WW-evmuv-M250':2.18073096985*2,
                         'gg-eta0-WW-evmuv-M300':2.29775496014*2,
                         'gg-eta0-WW-evmuv-M350':2.46589800207*2,
                         'gg-eta0-WW-evmuv-M400':1.9707292403*2,
                         'gg-eta0-WW-evmuv-M450':1.72468921497*2,
                         'gg-eta0-WW-evmuv-M500':1.66261118224*2,
                         'gg-eta0-WW-evmuv-M550':1.60230559544*2,
                         'gg-eta0-WW-evmuv-M600':1.56017321503*2,
                         'gg-eta0-WW-evmuv-M650':1.52629944125*2,
                         'gg-eta0-WW-evmuv-M700':1.49797704809*2,
                         'gg-eta0-WW-evmuv-M750':1.4753696585*2,
                         'gg-eta0-WW-evmuv-M800':1.45478852862*2,
                         'gg-eta0-WW-evmuv-M850':1.43632233525*2,
                         'gg-eta0-WW-evmuv-M900':1.42118100769*2,
                         'gg-eta0-WW-evmuv-M950':1.40459637177*2,
                         'gg-eta0-WW-evmuv-M1000':1.39037544923*2,
                         'gg-eta0-WW-evmuv-M1100':1.36671510558*2,
                         'gg-eta0-WW-evmuv-M1200':1.34539183607*2,
                         'gg-eta0-WW-evmuv-M1500':1.28924838934*2,
                         'gg-eta0-WW-evmuv-M2000':1.22343037638*2,
                         'pp-eta0jj-WW-evmuv-M200': 1.34144694309*2,
                         'pp-eta0jj-WW-evmuv-M250': 1.31763219163*2,
                         'pp-eta0jj-WW-evmuv-M300': 1.33269322546*2,
                         'pp-eta0jj-WW-evmuv-M350': 1.33413720446*2,
                         'pp-eta0jj-WW-evmuv-M400': 1.35075506325*2,
                         'pp-eta0jj-WW-evmuv-M450': 1.34429175954*2,
                         'pp-eta0jj-WW-evmuv-M500': 1.36933619048*2,
                         'pp-eta0jj-WW-evmuv-M550': 1.36225180698*2,
                         'pp-eta0jj-WW-evmuv-M600': 1.3588164263*2,
                         'pp-eta0jj-WW-evmuv-M650': 1.34668089912*2,
                         'pp-eta0jj-WW-evmuv-M700': 1.33902198374*2,
                         'pp-eta0jj-WW-evmuv-M750': 1.35037833258*2,
                         'pp-eta0jj-WW-evmuv-M800': 1.35564696253*2,
                         'pp-eta0jj-WW-evmuv-M850': 1.34048099174*2,
                         'pp-eta0jj-WW-evmuv-M900': 1.33779585209*2,
                         'pp-eta0jj-WW-evmuv-M950': 1.34768444622*2,
                         'pp-eta0jj-WW-evmuv-M1000': 1.34140978335*2,
                         'pp-eta0jj-WW-evmuv-M1100': 1.3385351023*2,
                         'pp-eta0jj-WW-evmuv-M1200': 1.31389948565*2,
                         'pp-eta0jj-WW-evmuv-M1500': 1.31595998714*2,
                         'pp-eta0jj-WW-evmuv-M2000': 1.28087286701*2,
                         'pp-wpwm-evmuv':2.0,
                         'pp-tt-wbwb-evbmuvb':2.0,
                         'pp-zz-eemumu':1.0, # there is no symmetry factor since we are decaying to either mu or e
                         'pp-wz-lvll':1.0, # there is no symmetry factor since we are decaying to either mu or e
                         'pp-singletop':2.0, # this is LO, and no symmetry factor needed
                         'pp-wjets':1.0 # this is NLO (FxFx) and both flavours are included
                        } # any K-factors to apply to processes

# K-factors to apply to processes on the TOTAL cross section -> NO symmetry factors from Decays.
TotalKFacsProcesses['HWW100'] = { 'gg-eta0-WW-evmuv-M200':2.153378605,
                                      'gg-eta0-WW-evmuv-M250':2.18073096985,
                                      'gg-eta0-WW-evmuv-M300':2.29775496014,
                                      'gg-eta0-WW-evmuv-M350':2.46589800207,
                                      'gg-eta0-WW-evmuv-M400':1.9707292403,
                                      'gg-eta0-WW-evmuv-M450':1.72468921497,
                                      'gg-eta0-WW-evmuv-M500':1.66261118224,
                                      'gg-eta0-WW-evmuv-M550':1.60230559544,
                                      'gg-eta0-WW-evmuv-M600':1.56017321503,
                                      'gg-eta0-WW-evmuv-M650':1.52629944125,
                                      'gg-eta0-WW-evmuv-M700':1.49797704809,
                                      'gg-eta0-WW-evmuv-M750':1.4753696585,
                                      'gg-eta0-WW-evmuv-M800':1.45478852862,
                                      'gg-eta0-WW-evmuv-M850':1.43632233525,
                                      'gg-eta0-WW-evmuv-M900':1.42118100769,
                                      'gg-eta0-WW-evmuv-M950':1.40459637177,
                                      'gg-eta0-WW-evmuv-M1000':1.39037544923,
                                      'gg-eta0-WW-evmuv-M1100':1.36671510558,
                                      'gg-eta0-WW-evmuv-M1200':1.34539183607,
                                      'gg-eta0-WW-evmuv-M1500':1.28924838934,
                                      'gg-eta0-WW-evmuv-M2000':1.22343037638,
                                      'pp-eta0jj-WW-evmuv-M200': 1.34144694309,
                                      'pp-eta0jj-WW-evmuv-M250': 1.31763219163,
                                      'pp-eta0jj-WW-evmuv-M300': 1.33269322546,
                                      'pp-eta0jj-WW-evmuv-M350': 1.33413720446,
                                      'pp-eta0jj-WW-evmuv-M400': 1.35075506325,
                                      'pp-eta0jj-WW-evmuv-M450': 1.34429175954,
                                      'pp-eta0jj-WW-evmuv-M500': 1.36933619048,
                                      'pp-eta0jj-WW-evmuv-M550': 1.36225180698,
                                      'pp-eta0jj-WW-evmuv-M600': 1.3588164263,
                                      'pp-eta0jj-WW-evmuv-M650': 1.34668089912,
                                      'pp-eta0jj-WW-evmuv-M700': 1.33902198374,
                                      'pp-eta0jj-WW-evmuv-M750': 1.35037833258,
                                      'pp-eta0jj-WW-evmuv-M800': 1.35564696253,
                                      'pp-eta0jj-WW-evmuv-M850': 1.34048099174,
                                      'pp-eta0jj-WW-evmuv-M900': 1.33779585209,
                                      'pp-eta0jj-WW-evmuv-M950': 1.34768444622,
                                      'pp-eta0jj-WW-evmuv-M1000': 1.34140978335,
                                      'pp-eta0jj-WW-evmuv-M1100': 1.3385351023,
                                      'pp-eta0jj-WW-evmuv-M1200': 1.31389948565,
                                      'pp-eta0jj-WW-evmuv-M1500': 1.31595998714,
                                      'pp-eta0jj-WW-evmuv-M2000': 1.28087286701,

                         'pp-wpwm-evmuv':1.0,
                         'pp-tt-wbwb-evbmuvb':1.0,
                         'pp-zz-eemumu':1.0, 
                         'pp-wz-lvll':1.0,
                         'pp-singletop':2.0,
                         'pp-wjets':1.0
                        } # any K-factors to apply to processes on the TOTAL cross section



# the locations of the MG5/aMC event files (lists, so that we can have more than one LHE file)
# locations with respect to the process directory, which should contain a full MG5 installation
MGProcessLocations['HWW100'] = { #'gg-eta0-WW-evmuv-M400':['gg_eta0_WW/Events/run_05_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M200':['gg_eta0_WW2/Events/run0_200_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M250':['gg_eta0_WW/Events/run0_250_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M300':['gg_eta0_WW/Events/run0_300_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M350':['gg_eta0_WW/Events/run0_350_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M400':['gg_eta0_WW/Events/run0_400_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M450':['gg_eta0_WW/Events/run0_450_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M500':['gg_eta0_WW/Events/run0_500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M550':['gg_eta0_WW/Events/run0_550_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M600':['gg_eta0_WW/Events/run0_600_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M650':['gg_eta0_WW/Events/run0_650_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M700':['gg_eta0_WW/Events/run0_700_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M750':['gg_eta0_WW/Events/run0_750_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M800':['gg_eta0_WW/Events/run0_800_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M850':['gg_eta0_WW/Events/run0_850_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M900':['gg_eta0_WW/Events/run0_900_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M950':['gg_eta0_WW/Events/run0_950_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M1000':['gg_eta0_WW/Events/run0_1000_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M1100':['gg_eta0_WW2/Events/run0_1100_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M1200':['gg_eta0_WW2/Events/run0_1200_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M1500':['gg_eta0_WW2/Events/run0_1500_decayed_1/unweighted_events.lhe.gz'],
'gg-eta0-WW-evmuv-M2000':['gg_eta0_WW2/Events/run0_2000_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M200':['pp_eta0jj_ww2/Events/run0_200_decayed_3/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M250':['pp_eta0jj_ww/Events/run0_250_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M300':['pp_eta0jj_ww/Events/run0_300_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M350':['pp_eta0jj_ww/Events/run0_350_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M400':['pp_eta0jj_ww/Events/run0_400_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M450':['pp_eta0jj_ww/Events/run0_450_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M500':['pp_eta0jj_ww/Events/run0_500_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M550':['pp_eta0jj_ww/Events/run0_550_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M600':['pp_eta0jj_ww/Events/run0_600_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M650':['pp_eta0jj_ww/Events/run0_650_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M700':['pp_eta0jj_ww/Events/run0_700_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M750':['pp_eta0jj_ww/Events/run0_750_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M800':['pp_eta0jj_ww/Events/run0_800_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M850':['pp_eta0jj_ww/Events/run0_850_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M900':['pp_eta0jj_ww/Events/run0_900_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M950':['pp_eta0jj_ww/Events/run0_950_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M1000':['pp_eta0jj_ww/Events/run0_1000_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M1100':['pp_eta0jj_ww2/Events/run0_1100_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M1200':['pp_eta0jj_ww2/Events/run0_1200_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M1500':['pp_eta0jj_ww2/Events/run0_1500_decayed_1/unweighted_events.lhe.gz'],
'pp-eta0jj-WW-evmuv-M2000':['pp_eta0jj_ww2/Events/run0_2000_decayed_1/unweighted_events.lhe.gz'],
'pp-wpwm-evmuv':['pp_wpwm_evmuv/Events/run_02_decayed_1/events.lhe.gz'],
'pp-tt-wbwb-evbmuvb':['pp_tt_wbwb_evbmuvb/Events/run_02_decayed_1/events.lhe.gz', 'pp_tt_wbwb_evbmuvb/Events/run_03_decayed_1/events.lhe.gz'],
'pp-zz-eemumu':['pp_zz_eemumu/Events/run_02_decayed_1/events.lhe.gz'],
'pp-wz-lvll':['pp_wz_lvll/Events/run_01_decayed_1/events.lhe.gz'],
'pp-singletop':['pp_singletop/Events/run_02_decayed_1/unweighted_events.lhe.gz'],
'pp-wjets':['pp_wjets/Events/run_01_decayed_1/events.lhe.gz', 'pp_wjets/Events/run_02_decayed_1/events.lhe.gz', 'pp_wjets/Events/run_03_decayed_1/events.lhe.gz']
}

# Where to look for the UNDECAYED total cross section: Note the difference between LO and NLO!
MGProcessLocations_Undecayed['HWW100'] = {
'gg-eta0-WW-evmuv-M200':['gg_eta0_WW2/Events/run0_200/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M250':['gg_eta0_WW/Events/run0_250/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M300':['gg_eta0_WW/Events/run0_300/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M350':['gg_eta0_WW/Events/run0_350/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M400':['gg_eta0_WW/Events/run0_400/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M450':['gg_eta0_WW/Events/run0_450/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M500':['gg_eta0_WW/Events/run0_500/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M550':['gg_eta0_WW/Events/run0_550/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M600':['gg_eta0_WW/Events/run0_600/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M650':['gg_eta0_WW/Events/run0_650/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M700':['gg_eta0_WW/Events/run0_700/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M750':['gg_eta0_WW/Events/run0_750/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M800':['gg_eta0_WW/Events/run0_800/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M850':['gg_eta0_WW/Events/run0_850/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M900':['gg_eta0_WW/Events/run0_900/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M950':['gg_eta0_WW/Events/run0_950/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M1000':['gg_eta0_WW/Events/run0_1000/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M1100':['gg_eta0_WW2/Events/run0_1100/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M1200':['gg_eta0_WW2/Events/run0_1200/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M1500':['gg_eta0_WW2/Events/run0_1500/unweighted_events.lhe'],
'gg-eta0-WW-evmuv-M2000':['gg_eta0_WW2/Events/run0_2000/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M200':['pp_eta0jj_ww2/Events/run0_200/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M250':['pp_eta0jj_ww/Events/run0_250/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M300':['pp_eta0jj_ww/Events/run0_300/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M350':['pp_eta0jj_ww/Events/run0_350/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M400':['pp_eta0jj_ww/Events/run0_400/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M450':['pp_eta0jj_ww/Events/run0_450/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M500':['pp_eta0jj_ww/Events/run0_500/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M550':['pp_eta0jj_ww/Events/run0_550/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M600':['pp_eta0jj_ww/Events/run0_600/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M650':['pp_eta0jj_ww/Events/run0_650/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M700':['pp_eta0jj_ww/Events/run0_700/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M750':['pp_eta0jj_ww/Events/run0_750/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M800':['pp_eta0jj_ww/Events/run0_800/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M850':['pp_eta0jj_ww/Events/run0_850/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M900':['pp_eta0jj_ww/Events/run0_900/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M950':['pp_eta0jj_ww/Events/run0_950/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M1000':['pp_eta0jj_ww/Events/run0_1000/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M1100':['pp_eta0jj_ww2/Events/run0_1100/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M1200':['pp_eta0jj_ww2/Events/run0_1200/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M1500':['pp_eta0jj_ww2/Events/run0_1500/unweighted_events.lhe'],
'pp-eta0jj-WW-evmuv-M2000':['pp_eta0jj_ww2/Events/run0_2000/unweighted_events.lhe'],
'pp-wpwm-evmuv':['pp_wpwm_evmuv/Events/run_02/summary.txt'],
'pp-tt-wbwb-evbmuvb':['pp_tt_wbwb_evbmuvb/Events/run_02/summary.txt', 'pp_tt_wbwb_evbmuvb/Events/run_03/summary.txt'],
'pp-zz-eemumu':['pp_zz_eemumu/Events/run_02/summary.txt'],
'pp-wz-lvll':['pp_wz_lvll/Events/run_01/summary.txt'],
'pp-singletop':['pp_singletop/Events/run_02/unweighted_events.lhe'],
'pp-wjets':['pp_wjets/Events/run_01/summary.txt', 'pp_wjets/Events/run_02/summary.txt', 'pp_wjets/Events/run_03/summary.txt']
}

Luminosity['HWW100'] = 30000. # luminosity in fb

NVariables['HWW100'] = 10 # the number of variables that go into the BDT

BRElement['HWW100'] = 9 # the element of the Higgs Branching Ratio array corresponding to this decay mode

NBDTAnalysis['HWW100'] = 3 # the number of BDT runs to search for the optimal solution

SignalFactorAnalysis['HWW100'] = 1.E-4 # the initial signal factor guess for the BDT

HwSimLibrary['HWW100'] = 'HwSim' # the name of the HwSim-type library to be loaded by Herwig 

FatAnalysis['HWW100'] = '#' # add a "#" to remove the HwSimFat stuff in the Herwig file
